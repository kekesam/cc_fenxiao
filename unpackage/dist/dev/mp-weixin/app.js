"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
const Grace6_js_request = require("./Grace6/js/request.js");
require("./custom/graceRequestConfig.js");
require("./Grace6/js/md5.js");
if (!Math) {
  "./pages/tabbar/index.js";
  "./pages/tabbar/me.js";
  "./pages/tabbar/income.js";
  "./pages/login/login.js";
  "./pages/login/pwd.js";
  "./pages/category/course.js";
  "./pages/category/downloads.js";
  "./pages/category/note.js";
  "./pages/category/rule.js";
  "./pages/course/detail.js";
  "./pages/course/category.js";
  "./pages/course/special.js";
  "./pages/note/detail.js";
  "./pages/downloads/detail.js";
  "./pages/user/message.js";
  "./pages/user/xieyi.js";
  "./pages/shopcart/shopcart.js";
  "./pages/search/search.js";
  "./pages/search/detail.js";
  "./pages/user/bind.js";
  "./pages/user/settings.js";
  "./pages/order/confirm.js";
  "./pages/order/paysuccess.js";
  "./pages/share/share.js";
  "./pages/state/state.js";
  "./pages/error/permission.js";
  "./pages/coupon/coupon.js";
}
const _sfc_main = {
  onLaunch: function() {
    common_vendor.wx$1.onNeedPrivacyAuthorization(function(e) {
      console.log(e);
    });
  },
  onShow: function() {
  },
  onHide: function() {
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/App/分销系统/cc_fenxiao/App.vue"]]);
common_vendor.index.gRequest = Grace6_js_request.GraceRequest;
function createApp() {
  const app = common_vendor.createSSRApp(App);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
