"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      // 切换导航
      navItems: [{
        id: 1,
        name: "可使用"
      }, {
        id: 2,
        name: "已使用"
      }, {
        id: 3,
        name: "已过期"
      }]
    };
  },
  created() {
  },
  methods: {}
};
if (!Array) {
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_switch_navigation2 + _easycom_gui_page2)();
}
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_switch_navigation + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(_ctx.navChange),
    b: common_vendor.p({
      items: $data.navItems,
      isCenter: true,
      size: 180,
      lineHeight: "60rpx",
      width: "750",
      textAlign: "center",
      activeLineWidth: "180rpx",
      activeLineHeight: "6rpx",
      activeLineClass: ["gui-gtbg-red"],
      margin: 10
    }),
    c: common_vendor.f(20, (num, k0, i0) => {
      return {};
    }),
    d: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-96ba783d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/coupon/coupon.vue"]]);
wx.createPage(MiniProgramPage);
