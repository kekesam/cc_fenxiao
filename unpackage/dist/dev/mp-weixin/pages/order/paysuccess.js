"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      scroll: false,
      items: [
        [80, "", "我的订单"],
        [100, "", "优惠券"],
        [50, "", "关注店铺"],
        [99, "", "收藏"]
      ]
    };
  },
  methods: {
    toDetail() {
      common_vendor.index.navigateTo({
        url: "/pages/shop/detail"
      });
    },
    toOrder() {
      common_vendor.index.navigateTo({
        url: "/pages/user/order"
      });
    }
  }
};
if (!Array) {
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  _easycom_gui_page2();
}
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  _easycom_gui_page();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.toOrder && $options.toOrder(...args)),
    b: common_vendor.f(10, (num, k0, i0) => {
      return {};
    }),
    c: common_vendor.o((...args) => $options.toDetail && $options.toDetail(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-ae70eb78"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/order/paysuccess.vue"]]);
wx.createPage(MiniProgramPage);
