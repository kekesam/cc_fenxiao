"use strict";
const common_vendor = require("../../common/vendor.js");
var demoData = [
  { img: "/static/pay/alipay.png", title: "支付宝", checked: true },
  { img: "/static/pay/weixin.png", title: "微信", checked: false },
  { img: "/static/pay/huabei.png", title: "花呗分期", checked: false },
  { img: "/static/pay/code.png", title: "兑换码", checked: false }
];
const _sfc_main = {
  data() {
    return {
      lists: [],
      dialogXieYiRef: null,
      currentIndex: 0
    };
  },
  onLoad() {
    this.lists = demoData;
  },
  methods: {
    change1: function(e) {
      this.currentIndex = e;
      if (e == 3) {
        this.open1();
      }
    },
    handleBack() {
      common_vendor.index.switchTab({ url: "/pages/tabbar/me" });
    },
    handlePay(e) {
      common_vendor.index.navigateTo({
        url: "/pages/order/paysuccess"
      });
    },
    open1: function() {
      this.$refs.guimodal1.open();
    },
    close1: function() {
      this.$refs.guimodal1.close();
    },
    confirm1: function() {
      this.$refs.guimodal1.close();
    },
    openXieyi() {
      this.$refs.dialogXieYiRef.handleOpen();
    }
  }
};
if (!Array) {
  const _easycom_gui_select_list2 = common_vendor.resolveComponent("gui-select-list");
  const _easycom_cc_dialog_xieyi2 = common_vendor.resolveComponent("cc-dialog-xieyi");
  const _easycom_gui_modal2 = common_vendor.resolveComponent("gui-modal");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_select_list2 + _easycom_cc_dialog_xieyi2 + _easycom_gui_modal2 + _easycom_gui_page2)();
}
const _easycom_gui_select_list = () => "../../Grace6/components/gui-select-list.js";
const _easycom_cc_dialog_xieyi = () => "../../components/dialog/xieyi.js";
const _easycom_gui_modal = () => "../../Grace6/components/gui-modal.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_select_list + _easycom_cc_dialog_xieyi + _easycom_gui_modal + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o($options.change1),
    b: common_vendor.p({
      items: $data.lists
    }),
    c: common_vendor.o((...args) => $options.openXieyi && $options.openXieyi(...args)),
    d: common_vendor.sr("dialogXieYiRef", "324e7894-2,324e7894-0"),
    e: common_vendor.t($data.currentIndex == 3 ? "立即兑换" : "立即支付"),
    f: common_vendor.o((...args) => $options.handlePay && $options.handlePay(...args)),
    g: common_vendor.o((...args) => $options.close1 && $options.close1(...args)),
    h: common_vendor.o((...args) => $options.confirm1 && $options.confirm1(...args)),
    i: common_vendor.sr("guimodal1", "324e7894-3,324e7894-0"),
    j: common_vendor.p({
      customClass: ["gui-bg-white", "gui-dark-bg-level-3", "gui-border-radius"],
      title: "优惠码兑换"
    }),
    k: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-324e7894"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/order/confirm.vue"]]);
wx.createPage(MiniProgramPage);
