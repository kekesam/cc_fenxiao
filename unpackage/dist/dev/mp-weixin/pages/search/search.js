"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      tags: ["AI创造合集", "商业插画入门", "哈哈", "家庭AI", "绘画"]
    };
  },
  methods: {
    handleSearch(e) {
      common_vendor.index.navigateTo({
        url: `/pages/search/detail?keyword=${e}`
      });
    }
  }
};
if (!Array) {
  const _easycom_gui_search2 = common_vendor.resolveComponent("gui-search");
  const _easycom_cc_title_nav2 = common_vendor.resolveComponent("cc-title-nav");
  const _easycom_gui_tags2 = common_vendor.resolveComponent("gui-tags");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_search2 + _easycom_cc_title_nav2 + _easycom_gui_tags2 + _easycom_gui_page2)();
}
const _easycom_gui_search = () => "../../Grace6/components/gui-search.js";
const _easycom_cc_title_nav = () => "../../components/title/nav.js";
const _easycom_gui_tags = () => "../../Grace6/components/gui-tags.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_search + _easycom_cc_title_nav + _easycom_gui_tags + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(_ctx.search),
    b: common_vendor.o(_ctx.search),
    c: common_vendor.p({
      placeholder: "搜索课程"
    }),
    d: common_vendor.p({
      title: "历史记录",
      fsize: "fz14",
      ctitle: "清空记录"
    }),
    e: common_vendor.f($data.tags, (tag, index, i0) => {
      return {
        a: index,
        b: common_vendor.o(($event) => $options.handleSearch(tag), index),
        c: "173551fc-3-" + i0 + ",173551fc-0",
        d: common_vendor.p({
          text: tag,
          customClass: ["gui-transparent-bg", "gui-color-gray"],
          lineHeight: 2.2,
          size: 26,
          borderColor: "#eee"
        })
      };
    }),
    f: common_vendor.p({
      title: "热门搜索",
      fsize: "fz14",
      ctitle: ""
    }),
    g: common_vendor.f($data.tags, (tag, index, i0) => {
      return {
        a: index,
        b: common_vendor.o(($event) => $options.handleSearch(tag), index),
        c: "173551fc-5-" + i0 + ",173551fc-0",
        d: common_vendor.p({
          text: tag,
          customClass: ["gui-transparent-bg", "gui-color-gray"],
          lineHeight: 2.2,
          size: 26,
          borderColor: "#eee"
        })
      };
    }),
    h: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/search/search.vue"]]);
wx.createPage(MiniProgramPage);
