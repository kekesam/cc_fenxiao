"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      isLoad: false,
      navItems: [{
        id: 1,
        name: "编程开发"
      }, {
        id: 1,
        name: "设计创作"
      }, {
        id: 2,
        name: "数据分析"
      }, {
        id: 2,
        name: "人工智能"
      }, {
        id: 2,
        name: "新媒体"
      }],
      tags: ["AI创造合集", "商业插画入门", "哈哈", "家庭AI", "绘画"]
    };
  },
  methods: {
    handleSearch(e) {
      common_vendor.index.navigateTo({
        url: `/pages/search/detail?keyword=${e}`
      });
    },
    navchange() {
    }
  },
  onLoad() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  }
};
if (!Array) {
  const _easycom_gui_search2 = common_vendor.resolveComponent("gui-search");
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_cc_search_course2 = common_vendor.resolveComponent("cc-search-course");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_search2 + _easycom_gui_switch_navigation2 + _easycom_cc_search_course2 + _easycom_gui_page2)();
}
const _easycom_gui_search = () => "../../Grace6/components/gui-search.js";
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_cc_search_course = () => "../../components/search/course.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_search + _easycom_gui_switch_navigation + _easycom_cc_search_course + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: !$data.isLoad
  }, !$data.isLoad ? {} : {}, {
    b: $data.isLoad
  }, $data.isLoad ? {
    c: common_vendor.o(_ctx.search),
    d: common_vendor.o(_ctx.search),
    e: common_vendor.p({
      placeholder: "搜索课程"
    })
  } : {}, {
    f: !$data.isLoad
  }, !$data.isLoad ? {} : {}, {
    g: $data.isLoad
  }, $data.isLoad ? {
    h: common_vendor.o($options.navchange),
    i: common_vendor.p({
      items: $data.navItems,
      textAlign: "center",
      isCenter: true,
      activeDirection: "center",
      size: 0,
      margin: 20,
      padding: "10rpx"
    })
  } : {}, {
    j: common_vendor.p({
      waterfall: false
    }),
    k: common_vendor.p({
      customHeader: true
    })
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/search/detail.vue"]]);
wx.createPage(MiniProgramPage);
