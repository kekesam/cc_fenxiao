"use strict";
const common_vendor = require("../../common/vendor.js");
const Grace6_js_checker = require("../../Grace6/js/checker.js");
require("../../Grace6/js/checkIdCard.js");
const _sfc_main = {
  data() {
    return {
      phoneno: "",
      vcodeBtnName: "发送验证码",
      countNum: 120,
      countDownTimer: null
    };
  },
  methods: {
    loginbypwd() {
      common_vendor.index.redirectTo({
        url: "/pages/login/pwd"
      });
    },
    getVCode: function() {
      var myreg = /^[1][0-9]{10}$/;
      if (!myreg.test(this.phoneno)) {
        common_vendor.index.showToast({ title: "请正确填写手机号码", icon: "none" });
        return false;
      }
      if (this.vcodeBtnName != "发送验证码" && this.vcodeBtnName != "重新发送") {
        return;
      }
      this.vcodeBtnName = "发送中...";
      common_vendor.index.showToast({ title: "短信已发送，请注意查收", icon: "none" });
      this.countNum = 60;
      this.countDownTimer = setInterval(() => {
        this.countDown();
      }, 1e3);
    },
    countDown: function() {
      if (this.countNum < 1) {
        clearInterval(this.countDownTimer);
        this.vcodeBtnName = "重新发送";
        return;
      }
      this.countNum--;
      this.vcodeBtnName = this.countNum + "秒重发";
    },
    submit: function(e) {
      console.log(e.detail.value);
      var formData = e.detail.value;
      console.log(formData);
      var rule = [
        { name: "phoneno", checkType: "phoneno", checkRule: "", errorMsg: "手机号码有误" },
        { name: "pwd", checkType: "string", checkRule: "4,6", errorMsg: "短信验证码错误" }
      ];
      var checkRes = Grace6_js_checker.graceChecker.check(formData, rule);
      if (checkRes) {
        common_vendor.index.showToast({ title: "验证通过!", icon: "none" });
        common_vendor.index.switchTab({
          url: "/pages/tabbar/index"
        });
      } else {
        common_vendor.index.showToast({ title: Grace6_js_checker.graceChecker.error, icon: "none" });
      }
    }
  }
};
if (!Array) {
  const _easycom_gui_image2 = common_vendor.resolveComponent("gui-image");
  _easycom_gui_image2();
}
const _easycom_gui_image = () => "../../Grace6/components/gui-image.js";
if (!Math) {
  _easycom_gui_image();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      src: "/static/logo.png",
      width: 128
    }),
    b: $data.phoneno,
    c: common_vendor.o(($event) => $data.phoneno = $event.detail.value),
    d: common_vendor.t($data.vcodeBtnName),
    e: common_vendor.o((...args) => $options.getVCode && $options.getVCode(...args)),
    f: common_vendor.o((...args) => $options.loginbypwd && $options.loginbypwd(...args)),
    g: common_vendor.o((...args) => $options.submit && $options.submit(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-e4e4508d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/login/login.vue"]]);
wx.createPage(MiniProgramPage);
