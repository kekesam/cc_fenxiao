"use strict";
const common_vendor = require("../../common/vendor.js");
const Grace6_js_checker = require("../../Grace6/js/checker.js");
require("../../Grace6/js/checkIdCard.js");
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    forgetPwd: function() {
      console.log("请自行完善代码");
    },
    loginbymsg: function() {
      common_vendor.index.redirectTo({
        url: "/pages/login/login"
      });
    },
    submit: function(e) {
      var formData = e.detail.value;
      var rule = [
        { name: "username", checkType: "string", checkRule: "5,50", errorMsg: "登录账户输入有误" },
        { name: "password", checkType: "string", checkRule: "6,100", errorMsg: "登录密码至少6个字符" }
      ];
      var checkRes = Grace6_js_checker.graceChecker.check(formData, rule);
      if (checkRes) {
        common_vendor.index.showToast({ title: "验证通过!", icon: "none" });
        common_vendor.index.switchTab({
          url: "/pages/tabbar/index"
        });
      } else {
        common_vendor.index.showToast({ title: Grace6_js_checker.graceChecker.error, icon: "none" });
      }
    }
  }
};
if (!Array) {
  const _easycom_gui_image2 = common_vendor.resolveComponent("gui-image");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_image2 + _easycom_gui_page2)();
}
const _easycom_gui_image = () => "../../Grace6/components/gui-image.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_image + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      src: "/static/logo.png",
      width: 128
    }),
    b: common_vendor.o((...args) => $options.forgetPwd && $options.forgetPwd(...args)),
    c: common_vendor.o((...args) => $options.loginbymsg && $options.loginbymsg(...args)),
    d: common_vendor.o((...args) => $options.submit && $options.submit(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-8032d478"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/login/pwd.vue"]]);
wx.createPage(MiniProgramPage);
