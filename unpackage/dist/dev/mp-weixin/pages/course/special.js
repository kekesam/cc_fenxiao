"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      // 切换导航
      navItems: [{
        id: 1,
        name: "编程开发"
      }, {
        id: 2,
        name: "数据分析"
      }, {
        id: 3,
        name: "设计创造"
      }, {
        id: 2,
        name: "新媒体电商"
      }, {
        id: 3,
        name: "人工智能"
      }]
    };
  },
  created() {
  },
  methods: {}
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_cc_title_bar2 = common_vendor.resolveComponent("cc-title-bar");
  const _easycom_cc_advert_index2 = common_vendor.resolveComponent("cc-advert-index");
  const _easycom_cc_title_sub2 = common_vendor.resolveComponent("cc-title-sub");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_gui_switch_navigation2 + _easycom_cc_title_bar2 + _easycom_cc_advert_index2 + _easycom_cc_title_sub2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_cc_title_bar = () => "../../components/title/bar.js";
const _easycom_cc_advert_index = () => "../../components/advert/index.js";
const _easycom_cc_title_sub = () => "../../components/title/sub.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_gui_switch_navigation + _easycom_cc_title_bar + _easycom_cc_advert_index + _easycom_cc_title_sub + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(_ctx.handleBack),
    b: common_vendor.p({
      showback: true,
      showcart: true,
      title: "AI数字技能",
      btntext: ""
    }),
    c: common_vendor.o(_ctx.navChange),
    d: common_vendor.p({
      items: $data.navItems,
      isCenter: true,
      size: 180,
      lineHeight: "60rpx",
      width: "750",
      textAlign: "center",
      activeLineWidth: "180rpx",
      activeLineHeight: "6rpx",
      activeLineClass: ["gui-gtbg-red"],
      margin: 10
    }),
    e: common_vendor.p({
      showspace: false
    }),
    f: common_vendor.p({
      title: "数据分析",
      ctitle: "查看更多"
    }),
    g: common_vendor.f(20, (num, k0, i0) => {
      return {};
    }),
    h: common_vendor.f(20, (num, k0, i0) => {
      return {};
    }),
    i: common_vendor.f(20, (num, k0, i0) => {
      return {};
    }),
    j: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-7f133b12"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/course/special.vue"]]);
wx.createPage(MiniProgramPage);
