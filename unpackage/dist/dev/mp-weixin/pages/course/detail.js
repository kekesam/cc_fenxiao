"use strict";
const common_vendor = require("../../common/vendor.js");
const Grace6_js_parserHTML = require("../../Grace6/js/parserHTML.js");
var face = "https://images.unsplash.com/photo-1663717249250-804cb861ed74?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw5M3x8fGVufDB8fHx8&auto=format&fit=crop&w=100&q=80";
const _sfc_main = {
  data() {
    return {
      closemask: true,
      nodeArr: [],
      nodes: `<img src='/static/imgs/bg2.jpg'/><img src='/static/imgs/bg3.webp'/><img src='/static/imgs/bg4.webp'/><h1>23423</h1><p class="m-introImage u-coursebriefintro-content">
			<img src="https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg"/>
                        <img src="https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg" data-src="//study-image.nosdn.127.net/4c49087e146c43b6b8de5b1c989f1597.jpg?imageView&amp;quality=100&amp;type=webp" class="intro-img" id="auto-id-1707016924515" style="width: 100%;">
                    </p><p class="txt f-course-grey u-coursebriefintro-content ">
					<img src="https://img30.360buyimg.com/sku/jfs/t22021/327/2281785192/48707/57806074/5b4f1579Nae7adb49.jpg"/>
					<img src="https://img30.360buyimg.com/sku/jfs/t21682/256/2344553276/204456/cf7a2ddb/5b4ffbbfN48c54307.jpg"/>
          <span>2022年8月份开始，课程第5次更新已经开始，这次会颠覆之前所有课程，全面升级玩法，即将涨价！<br>具体可看详情介绍<br>现在的自媒体短视频，就像淘宝刚开始时期，特别好做，但很多人不做或不知道怎么做，确实，现在入局自媒体短视频还不晚，再等等就肯定会晚。<br>国人都感觉到了，现在是新媒体时代，个人自媒体和短视频兴起，头条，抖音，快手&nbsp;直播&nbsp;网红&nbsp;带货时代早已到来，一条带货视频一晚上带来百万收益，一场直播盈利百万&nbsp;千万&nbsp;甚至过亿，不管你现在是不是认可，是实实在在发生在我们身边的事情！有人通过新媒体短视频正闷声发大财，月入百万&nbsp;千万甚至更多，有人却还根本没有找到进入的大门。<br>当你想做自媒体和短视频创业时，有没有这种感觉？①想做新媒体和短视频，却不知道从哪下手?②学习资料太多，不知道该学什么?③新媒体短视频做起来迷茫，不懂规则，内容不推荐，阅读量，播放量低，不会写爆款文章，写标题，拍视频，剪辑视频...<br>不用发愁了，来学习本套课程，他会带你入局新媒体和短视频，掌握自媒体和短视频必备实用技能，开启你不一样人生，<br>客服微信：1534728904</span>
        </p>`,
      // 轮播图 
      swiperItems: [
        {
          img: "/static/imgs/bg.jpg",
          url: "",
          opentype: "navigate"
        },
        {
          img: "/static/imgs/bg.jpg",
          url: "",
          opentype: "navigate"
        }
      ],
      // 商品信息
      product: {
        name: "小米 MIX3 一面科技 一面艺术 ( 磁动力滑盖全面屏 | 故宫特别版 )",
        logo: "../../static/logo.png",
        price: 3188,
        priceMarket: 3200,
        imgs: []
      },
      // 切换导航
      navItems: [{
        id: 1,
        name: "简介"
      }, {
        id: 2,
        name: "目录"
      }, {
        id: 3,
        name: "评价"
      }],
      // 切换索引
      active: 0,
      // 属性选择
      attrShow: false,
      colors: [
        {
          id: 1,
          text: "红色",
          checked: false
        },
        {
          id: 2,
          text: "黑色",
          checked: false
        },
        {
          id: 3,
          text: "蓝色",
          checked: false
        }
      ],
      sizes: [
        {
          id: 1,
          text: "10 cm",
          checked: false
        },
        {
          id: 2,
          text: "20 cm",
          checked: false
        },
        {
          id: 3,
          text: "40 cm",
          checked: false
        }
      ],
      // 属性记录
      attrRes: {
        color: null,
        size: null,
        number: 1
      },
      commentContents: [
        {
          "content": "故国三千里，深宫二十年。一声何满子，双泪落君前。",
          "name": "回复昵称",
          "face": face,
          "date": "08/10 08:00",
          "praise": 188,
          "isPraise": false,
          "Reply": [
            {
              "name": "张晓曦",
              "content": "不错不错"
            },
            {
              "name": "王大陆",
              "content": "赞了~"
            }
          ]
        },
        {
          "content": "而今渐行渐远，渐觉虽悔难追。漫寄消寄息，终久奚为。也拟重论缱绻，争奈翻覆思维。纵再会，只恐恩情，难似当时。",
          "name": "路过繁华",
          "face": face,
          "date": "02/10 18:00",
          "praise": 288
        },
        {
          "content": "图片回复，点击图片可以预览......",
          "name": "林夕阳",
          "imgs": [
            "https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg",
            "https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg"
          ],
          "face": face,
          "date": "08/12 09:00",
          "praise": 955,
          "isPraise": true
        }
      ]
    };
  },
  created() {
    this.nodeArr = Grace6_js_parserHTML.parserHtml.parserHTML(this.nodes);
  },
  methods: {
    // 分享
    share: function() {
      common_vendor.index.navigateTo({
        url: "/pages/share/share"
      });
    },
    // 导航切换
    navChange: function(e) {
      this.active = e;
    },
    // 返回首页
    goHome: function() {
      common_vendor.index.switchTab({
        url: "/pages/tabbar/index"
      });
    },
    // 返回首页
    kf: function() {
      common_vendor.index.showToast({
        title: "加入购物车成功",
        icon: "none"
      });
      common_vendor.index.navigateTo({
        url: "/pages/shopcart/shopcart"
      });
    },
    // 加入购物车
    addtocard: function() {
      this.selectAttr();
    },
    // 购买 
    buynow: function() {
      common_vendor.index.navigateTo({
        url: "/pages/order/confirm"
      });
    },
    // 属性选择
    selectAttr: function() {
      if (!this.attrShow) {
        this.attrShow = true;
        return;
      }
      if (this.attrRes.color == null || this.attrRes.size == null) {
        common_vendor.index.showToast({
          icon: "none",
          title: "请选择属性"
        });
      }
      console.log("请完善提交代码");
    },
    colorChange: function(e) {
      this.attrRes.color = e.text;
    },
    sizeChange: function(e) {
      this.attrRes.size = e.text;
    },
    numberChange: function(e) {
      this.attrRes.number = e[0];
    },
    closeAttr: function() {
      this.attrShow = false;
    },
    showImgs: function(commentsIndex, imgIndex) {
      console.log(commentsIndex, imgIndex);
      common_vendor.index.previewImage({
        urls: this.commentContents[commentsIndex].imgs,
        current: this.commentContents[commentsIndex].imgs[imgIndex]
      });
    }
  }
};
if (!Array) {
  const _easycom_gui_swiper2 = common_vendor.resolveComponent("gui-swiper");
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_cc_course_chapter2 = common_vendor.resolveComponent("cc-course-chapter");
  const _easycom_gui_stags2 = common_vendor.resolveComponent("gui-stags");
  const _easycom_gui_step_box2 = common_vendor.resolveComponent("gui-step-box");
  const _easycom_gui_iphone_bottom2 = common_vendor.resolveComponent("gui-iphone-bottom");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_swiper2 + _easycom_gui_switch_navigation2 + _easycom_cc_course_chapter2 + _easycom_gui_stags2 + _easycom_gui_step_box2 + _easycom_gui_iphone_bottom2 + _easycom_gui_page2)();
}
const _easycom_gui_swiper = () => "../../Grace6/components/gui-swiper.js";
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_cc_course_chapter = () => "../../components/course/chapter.js";
const _easycom_gui_stags = () => "../../Grace6/components/gui-stags.js";
const _easycom_gui_step_box = () => "../../Grace6/components/gui-step-box.js";
const _easycom_gui_iphone_bottom = () => "../../Grace6/components/gui-iphone-bottom.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_swiper + _easycom_gui_switch_navigation + _easycom_cc_course_chapter + _easycom_gui_stags + _easycom_gui_step_box + _easycom_gui_iphone_bottom + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.p({
      swiperItems: $data.swiperItems,
      spacing: 0,
      padding: 0,
      borderRadius: "0rpx",
      width: 750,
      height: 420
    }),
    b: common_vendor.o($options.navChange),
    c: common_vendor.p({
      items: $data.navItems,
      isCenter: true,
      size: 180,
      lineHeight: "60rpx",
      width: "750",
      textAlign: "center",
      activeLineWidth: "180rpx",
      activeLineHeight: "4rpx",
      activeLineClass: ["gui-gtbg-red"],
      margin: 10
    }),
    d: $data.active == 0
  }, $data.active == 0 ? {
    e: common_vendor.t($data.product.name),
    f: common_vendor.o((...args) => $options.share && $options.share(...args))
  } : {}, {
    g: $data.active == 0
  }, $data.active == 0 ? {
    h: common_vendor.t($data.product.price),
    i: common_vendor.t($data.product.priceMarket)
  } : {}, {
    j: $data.active == 0
  }, $data.active == 0 ? {
    k: $data.nodeArr
  } : {}, {
    l: $data.active == 1
  }, $data.active == 1 ? {} : {}, {
    m: $data.active == 2
  }, $data.active == 2 ? {
    n: common_vendor.f($data.commentContents, (item, index, i0) => {
      return common_vendor.e({
        a: item.face,
        b: common_vendor.t(item.name),
        c: common_vendor.t(item.praise),
        d: common_vendor.n(item.isPraise ? "gui-primary-color" : ""),
        e: common_vendor.t(item.content),
        f: item.imgs && item.imgs.length > 0
      }, item.imgs && item.imgs.length > 0 ? {
        g: common_vendor.f(item.imgs, (img, indexImg, i1) => {
          return {
            a: img,
            b: indexImg,
            c: common_vendor.o(($event) => $options.showImgs(index, indexImg), indexImg)
          };
        })
      } : {}, {
        h: item.Reply
      }, item.Reply ? {
        i: common_vendor.f(item.Reply, (itemRe, indexRe, i1) => {
          return {
            a: common_vendor.t(itemRe.name),
            b: common_vendor.t(itemRe.content),
            c: indexRe
          };
        })
      } : {}, {
        j: common_vendor.t(item.date),
        k: index
      });
    })
  } : {}, {
    o: $data.attrShow
  }, $data.attrShow ? {
    p: common_vendor.o((...args) => $options.closeAttr && $options.closeAttr(...args)),
    q: common_vendor.o($options.colorChange),
    r: common_vendor.p({
      tags: $data.colors
    }),
    s: common_vendor.o($options.sizeChange),
    t: common_vendor.p({
      tags: $data.sizes
    }),
    v: common_vendor.o($options.numberChange),
    w: common_vendor.p({
      value: 1
    })
  } : {}, {
    x: common_vendor.o((...args) => $options.goHome && $options.goHome(...args)),
    y: common_vendor.o((...args) => $options.kf && $options.kf(...args)),
    z: common_vendor.o((...args) => $options.buynow && $options.buynow(...args)),
    A: common_vendor.o((...args) => $options.share && $options.share(...args)),
    B: $data.closemask
  }, $data.closemask ? {} : {}, {
    C: $data.closemask
  }, $data.closemask ? {
    D: common_vendor.o(($event) => $data.closemask = false)
  } : {}, {
    E: common_vendor.p({
      customHeader: true
    })
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-3d21314d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/course/detail.vue"]]);
wx.createPage(MiniProgramPage);
