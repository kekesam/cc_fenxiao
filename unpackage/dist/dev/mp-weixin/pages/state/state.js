"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      currentIndex: 0,
      isLoad: false,
      navItems: [{
        id: 1,
        name: "推广数据统计"
      }, {
        id: 2,
        name: "佣金余额明细"
      }, {
        id: 3,
        name: "提现记录"
      }]
    };
  },
  onLoad() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  },
  methods: {
    handleBack() {
      common_vendor.index.switchTab({
        url: "/pages/tabbar/me"
      });
    },
    navchange(e) {
      this.currentIndex = e;
    }
  }
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_cc_income_state2 = common_vendor.resolveComponent("cc-income-state");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_cc_income_state2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_cc_income_state = () => "../../components/income/state.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_cc_income_state + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.o($options.handleBack),
    b: common_vendor.p({
      title: "推广统计分析",
      showback: true,
      issearch: false,
      btntext: ""
    }),
    c: !$data.isLoad
  }, !$data.isLoad ? {} : {}, {
    d: common_vendor.p({
      customHeader: true
    })
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-5b779960"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/state/state.vue"]]);
wx.createPage(MiniProgramPage);
