"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      title: "",
      opid: 1,
      page: 1
    };
  },
  onLoad(options) {
    this.title = options.title;
    this.opid = options.id;
    this.page = options.page;
  },
  methods: {
    handleBack() {
      common_vendor.index.navigateBack({
        delta: 1
      });
    },
    toLink(url, flag) {
      if (flag) {
        common_vendor.index.switchTab({ url });
      } else {
        common_vendor.index.navigateTo({ url });
      }
    },
    toUser(msg) {
      console.log("联系客服。。。。");
    }
  }
};
if (!Array) {
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  _easycom_gui_page2();
}
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  _easycom_gui_page();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(($event) => $options.toLink("/pages/user/bind")),
    b: common_vendor.o(($event) => $options.toLink("/pages/tabbar/income", true)),
    c: common_vendor.o(($event) => $options.toUser("/pages/tabbar/income")),
    d: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/rule.vue"]]);
wx.createPage(MiniProgramPage);
