"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      // 切换导航
      navItems: [{
        id: 1,
        name: "编程开发"
      }, {
        id: 2,
        name: "数据分析"
      }, {
        id: 3,
        name: "设计创造"
      }, {
        id: 2,
        name: "新媒体电商"
      }, {
        id: 3,
        name: "人工智能"
      }]
    };
  },
  created() {
  },
  methods: {}
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_cc_advert_index2 = common_vendor.resolveComponent("cc-advert-index");
  const _easycom_cc_title_subitem2 = common_vendor.resolveComponent("cc-title-subitem");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_gui_switch_navigation2 + _easycom_cc_advert_index2 + _easycom_cc_title_subitem2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_cc_advert_index = () => "../../components/advert/index.js";
const _easycom_cc_title_subitem = () => "../../components/title/subitem.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_gui_switch_navigation + _easycom_cc_advert_index + _easycom_cc_title_subitem + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o(_ctx.handleBack),
    b: common_vendor.p({
      showback: true,
      showcart: true,
      title: "精品课程",
      btntext: ""
    }),
    c: common_vendor.o(_ctx.navChange),
    d: common_vendor.p({
      items: $data.navItems,
      isCenter: true,
      size: 180,
      lineHeight: "60rpx",
      width: "750",
      textAlign: "center",
      activeLineWidth: "180rpx",
      activeLineHeight: "6rpx",
      activeLineClass: ["gui-gtbg-red"],
      margin: 10
    }),
    e: common_vendor.p({
      showspace: false
    }),
    f: common_vendor.f(10, (num, k0, i0) => {
      return {};
    }),
    g: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-d2a1a735"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/course.vue"]]);
wx.createPage(MiniProgramPage);
