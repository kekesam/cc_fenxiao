"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      // 总价
      totalprice: "",
      // 选择文本
      selectText: "全选",
      // 购物车数据 可以来自 api 请求或本地数据
      shoppingCard: [
        {
          "checked": true,
          "shopName": "hcoder 官方店",
          "shopId": "1",
          "items": [
            {
              "goodsId": 1,
              "goodsName": "hcoder 演示商品",
              "price": 10,
              "count": 1,
              "img": "https://m.360buyimg.com/babel/jfs/t1/3730/7/3438/394579/5b996f2eE1727c59e/373cf10d42a53b72.jpg",
              "checked": true
            },
            {
              "goodsId": 2,
              "goodsName": "dcloud 演示商品",
              "price": 20,
              "count": 1,
              "img": "https://img14.360buyimg.com/n7/jfs/t1/1156/8/14017/123589/5bd9a4e8E7dbd4a15/70fbbccdf8811111.jpg",
              "checked": true
            }
          ]
        },
        {
          "checked": true,
          "shopName": "grace 官方旗舰店",
          "shopId": "2",
          "items": [{
            "goodsId": 3,
            "goodsName": "uni-app 演示商品",
            "price": 30,
            "count": 2,
            "img": "https://img10.360buyimg.com/n7/jfs/t19690/263/1947634738/190301/ad172397/5adfe5eaN42591f8c.jpg",
            "checked": true
          }]
        }
      ]
    };
  },
  onLoad: function() {
    this.countTotoal();
  },
  methods: {
    //计算总计函数
    countTotoal: function() {
      var total = 0;
      for (var i = 0; i < this.shoppingCard.length; i++) {
        for (var ii = 0; ii < this.shoppingCard[i].items.length; ii++) {
          if (this.shoppingCard[i].items[ii].checked) {
            total += Number(this.shoppingCard[i].items[ii].price) * Number(this.shoppingCard[i].items[ii].count);
          }
        }
      }
      this.totalprice = total;
    },
    numberChange: function(data) {
      this.shoppingCard[data[2]].items[data[1]].count = data[0];
      this.countTotoal();
    },
    removeGoods: function(e) {
      var index = e.currentTarget.id.replace("removeIndex_", "");
      index = index.split("_");
      var index1 = Number(index[0]);
      var index2 = Number(index[1]);
      common_vendor.index.showModal({
        title: "确认提醒",
        content: "您确定要移除此商品吗？",
        success: (e2) => {
          if (e2.confirm) {
            this.shoppingCard[index1].items.splice(index2, 1);
            if (this.shoppingCard[index1].items.length < 1) {
              this.shoppingCard.splice(index1, 1);
            }
            this.countTotoal();
          }
        }
      });
    },
    checkout: function() {
      common_vendor.index.showToast({
        title: "计算的数据保存在 shoppingCard 变量内 ^_^",
        icon: "none"
      });
    },
    // 店铺选中按钮状态切换
    shopChange: function(e) {
      var index = Number(e[1]);
      this.shoppingCard[index].checked = e[0];
      for (let i = 0; i < this.shoppingCard[index].items.length; i++) {
        this.shoppingCard[index].items[i].checked = e[0];
      }
      this.shoppingCard.splice(index, this.shoppingCard[index]);
      this.countTotoal();
    },
    // 商品选中
    itemChange: function(e) {
      var indexs = e[1].toString();
      var index = indexs.split("-");
      index[0] = Number(index[0]);
      index[1] = Number(index[1]);
      this.shoppingCard[index[0]].items[index[1]].checked = e[0];
      this.shoppingCard.splice(index[0], this.shoppingCard[index[0]]);
      var checkedNum = 0;
      for (let i = 0; i < this.shoppingCard[index[0]].items.length; i++) {
        if (!this.shoppingCard[index[0]].items[i].checked) {
          checkedNum++;
        }
      }
      if (checkedNum < 1) {
        this.shoppingCard[index[0]].checked = true;
      } else {
        this.shoppingCard[index[0]].checked = false;
      }
      this.shoppingCard = this.shoppingCard;
      this.countTotoal();
    },
    itemChangeAll: function(e) {
      this.selectText = e[0] ? "全选" : "全不选";
      for (var i = 0; i < this.shoppingCard.length; i++) {
        this.shoppingCard[i].checked = e[0];
        for (var ii = 0; ii < this.shoppingCard[i].items.length; ii++) {
          this.shoppingCard[i].items[ii].checked = e[0];
        }
      }
      this.countTotoal();
    },
    handleBack() {
      common_vendor.index.navigateBack({ delta: 1 });
    }
  }
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_gui_empty2 = common_vendor.resolveComponent("gui-empty");
  const _easycom_gui_radio2 = common_vendor.resolveComponent("gui-radio");
  const _easycom_gui_step_box2 = common_vendor.resolveComponent("gui-step-box");
  const _easycom_gui_iphone_bottom2 = common_vendor.resolveComponent("gui-iphone-bottom");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_gui_empty2 + _easycom_gui_radio2 + _easycom_gui_step_box2 + _easycom_gui_iphone_bottom2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_gui_empty = () => "../../Grace6/components/gui-empty.js";
const _easycom_gui_radio = () => "../../Grace6/components/gui-radio.js";
const _easycom_gui_step_box = () => "../../Grace6/components/gui-step-box.js";
const _easycom_gui_iphone_bottom = () => "../../Grace6/components/gui-iphone-bottom.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_gui_empty + _easycom_gui_radio + _easycom_gui_step_box + _easycom_gui_iphone_bottom + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.o($options.handleBack),
    b: common_vendor.p({
      title: "购物车",
      version: "",
      issearch: false,
      showback: true
    }),
    c: $data.shoppingCard.length < 1
  }, $data.shoppingCard.length < 1 ? common_vendor.e({
    d: _ctx.items == null
  }, _ctx.items == null ? {} : {}) : {}, {
    e: common_vendor.f($data.shoppingCard, (item, index, i0) => {
      return {
        a: common_vendor.t(item.shopName),
        b: common_vendor.o($options.shopChange, index),
        c: "83947b95-3-" + i0 + ",83947b95-0",
        d: common_vendor.p({
          parameter: [index],
          checked: item.checked
        }),
        e: common_vendor.f(item.items, (goods, indexItem, i1) => {
          return {
            a: common_vendor.o($options.itemChange, indexItem),
            b: "83947b95-4-" + i0 + "-" + i1 + ",83947b95-0",
            c: common_vendor.p({
              checked: goods.checked,
              parameter: [index + "-" + indexItem]
            }),
            d: goods.img,
            e: common_vendor.t(goods.goodsName),
            f: common_vendor.t(goods.price),
            g: common_vendor.o($options.numberChange, indexItem),
            h: "83947b95-5-" + i0 + "-" + i1 + ",83947b95-0",
            i: common_vendor.p({
              disabled: true,
              index: indexItem,
              datas: [index + ""],
              value: goods.count,
              maxNum: 100,
              minNum: 1
            }),
            j: common_vendor.o((...args) => $options.removeGoods && $options.removeGoods(...args), indexItem),
            k: "removeIndex_" + index + "_" + indexItem,
            l: indexItem
          };
        }),
        f: index
      };
    }),
    f: common_vendor.t($data.selectText),
    g: common_vendor.o($options.itemChangeAll),
    h: common_vendor.p({
      checked: true
    }),
    i: common_vendor.t($data.totalprice),
    j: common_vendor.o((...args) => $options.checkout && $options.checkout(...args)),
    k: common_vendor.p({
      customHeader: true
    })
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-83947b95"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/shopcart/shopcart.vue"]]);
wx.createPage(MiniProgramPage);
