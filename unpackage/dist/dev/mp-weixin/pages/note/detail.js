"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      title: "",
      article: []
    };
  },
  onLoad: function() {
    common_vendor.index.showLoading({ title: "loading", mask: true });
    common_vendor.index.setNavigationBarTitle({ title: "加载中" });
    common_vendor.index.request({
      url: "https://www.graceui.com/api/html2array",
      success: (res) => {
        console.log(res);
        this.article = res.data.data;
        this.title = "文章标题";
        common_vendor.index.setNavigationBarTitle({ title: this.title });
        common_vendor.index.hideLoading();
      }
    });
  }
};
if (!Array) {
  const _easycom_gui_article_info2 = common_vendor.resolveComponent("gui-article-info");
  const _easycom_gui_spread2 = common_vendor.resolveComponent("gui-spread");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_article_info2 + _easycom_gui_spread2 + _easycom_gui_page2)();
}
const _easycom_gui_article_info = () => "../../Grace6/components/gui-article-info.js";
const _easycom_gui_spread = () => "../../Grace6/components/gui-spread.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_article_info + _easycom_gui_spread + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      article: $data.article
    }),
    b: common_vendor.p({
      width: "750rpx",
      height: "600rpx",
      isShrink: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/note/detail.vue"]]);
wx.createPage(MiniProgramPage);
