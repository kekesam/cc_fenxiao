"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    handleBack() {
      common_vendor.index.switchTab({
        url: "/pages/tabbar/me"
      });
    }
  }
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_cc_advert_index2 = common_vendor.resolveComponent("cc-advert-index");
  const _easycom_cc_category_list2 = common_vendor.resolveComponent("cc-category-list");
  const _easycom_cc_course_index2 = common_vendor.resolveComponent("cc-course-index");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_cc_advert_index2 + _easycom_cc_category_list2 + _easycom_cc_course_index2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_cc_advert_index = () => "../../components/advert/index.js";
const _easycom_cc_category_list = () => "../../components/category/list.js";
const _easycom_cc_course_index = () => "../../components/course/index.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_cc_advert_index + _easycom_cc_category_list + _easycom_cc_course_index + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o($options.handleBack),
    b: common_vendor.p({
      showback: true,
      btntext: "",
      title: "资源推广"
    }),
    c: common_vendor.p({
      showspace: false
    }),
    d: common_vendor.p({
      customHeader: true
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/tabbar/index.vue"]]);
wx.createPage(MiniProgramPage);
