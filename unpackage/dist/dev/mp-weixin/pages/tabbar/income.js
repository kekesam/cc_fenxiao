"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      currentIndex: 0,
      isLoad: false,
      navItems: [{
        id: 2,
        name: "佣金明细"
      }, {
        id: 3,
        name: "提现记录"
      }]
    };
  },
  onLoad() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  },
  methods: {
    handleBack() {
      common_vendor.index.switchTab({
        url: "/pages/tabbar/me"
      });
    },
    navchange(e) {
      this.currentIndex = e;
    }
  }
};
if (!Array) {
  const _easycom_logo2 = common_vendor.resolveComponent("logo");
  const _easycom_gui_switch_navigation2 = common_vendor.resolveComponent("gui-switch-navigation");
  const _easycom_cc_income_yongjin2 = common_vendor.resolveComponent("cc-income-yongjin");
  const _easycom_cc_income_tixian2 = common_vendor.resolveComponent("cc-income-tixian");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_logo2 + _easycom_gui_switch_navigation2 + _easycom_cc_income_yongjin2 + _easycom_cc_income_tixian2 + _easycom_gui_page2)();
}
const _easycom_logo = () => "../../components/logo/logo.js";
const _easycom_gui_switch_navigation = () => "../../Grace6/components/gui-switch-navigation.js";
const _easycom_cc_income_yongjin = () => "../../components/income/yongjin.js";
const _easycom_cc_income_tixian = () => "../../components/income/tixian.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_logo + _easycom_gui_switch_navigation + _easycom_cc_income_yongjin + _easycom_cc_income_tixian + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.o($options.handleBack),
    b: common_vendor.p({
      title: "收益管理",
      showback: true,
      btntext: ""
    }),
    c: common_vendor.o($options.navchange),
    d: common_vendor.p({
      items: $data.navItems,
      textAlign: "center",
      isCenter: true,
      activeDirection: "center",
      size: 0,
      margin: 40,
      activeLineClass: ["gui-gtbg-green"]
    }),
    e: $data.currentIndex == 0
  }, $data.currentIndex == 0 ? {} : {}, {
    f: $data.currentIndex == 1
  }, $data.currentIndex == 1 ? {} : {}, {
    g: common_vendor.p({
      customHeader: true
    })
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-0cc8765e"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/tabbar/income.vue"]]);
wx.createPage(MiniProgramPage);
