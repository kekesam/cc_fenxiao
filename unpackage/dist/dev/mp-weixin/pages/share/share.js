"use strict";
const common_vendor = require("../../common/vendor.js");
const common_assets = require("../../common/assets.js");
const _sfc_main = {
  data() {
    return {
      dialogtgref: null,
      dialogShareref: null,
      currentIndex: 0
    };
  },
  methods: {
    handleIncome() {
      common_vendor.index.switchTab({
        url: "/pages/tabbar/income"
      });
    },
    handleOpen() {
      this.$refs.dialogtgref.handleOpen();
    },
    handleChangeImg(index) {
      this.currentIndex = index;
    },
    handleShare() {
      this.$refs.dialogShareref.handleOpen();
    }
  }
};
if (!Array) {
  const _easycom_cc_dialog_user2 = common_vendor.resolveComponent("cc-dialog-user");
  const _easycom_cc_dialog_tuiguang2 = common_vendor.resolveComponent("cc-dialog-tuiguang");
  (_easycom_cc_dialog_user2 + _easycom_cc_dialog_tuiguang2)();
}
const _easycom_cc_dialog_user = () => "../../components/dialog/user.js";
const _easycom_cc_dialog_tuiguang = () => "../../components/dialog/tuiguang.js";
if (!Math) {
  (_easycom_cc_dialog_user + _easycom_cc_dialog_tuiguang)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.handleShare && $options.handleShare(...args)),
    b: common_vendor.sr("dialogShareref", "ceb22cc9-0"),
    c: common_assets._imports_0,
    d: common_vendor.n($data.currentIndex == 0 ? "active" : ""),
    e: common_vendor.o(($event) => $options.handleChangeImg(0)),
    f: common_assets._imports_1,
    g: common_vendor.n($data.currentIndex == 1 ? "active" : ""),
    h: common_vendor.o(($event) => $options.handleChangeImg(1)),
    i: common_assets._imports_0,
    j: common_vendor.n($data.currentIndex == 2 ? "active" : ""),
    k: common_vendor.o(($event) => $options.handleChangeImg(2)),
    l: common_assets._imports_0,
    m: common_vendor.n($data.currentIndex == 3 ? "active" : ""),
    n: common_vendor.o(($event) => $options.handleChangeImg(3)),
    o: common_vendor.o((...args) => $options.handleIncome && $options.handleIncome(...args)),
    p: common_vendor.o((...args) => $options.handleOpen && $options.handleOpen(...args)),
    q: common_vendor.sr("dialogtgref", "ceb22cc9-1")
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-ceb22cc9"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/share/share.vue"]]);
wx.createPage(MiniProgramPage);
