"use strict";
const common_vendor = require("../../common/vendor.js");
const Grace6_js_checker = require("../../Grace6/js/checker.js");
require("../../Grace6/js/checkIdCard.js");
const _sfc_main = {
  data() {
    return {
      formData: {
        name1: "GraceUI",
        name2: "888888",
        name3: "888888",
        name4: "888888@qq.com",
        name5: "请选择性别",
        name7: "123456",
        name8: ""
      },
      gender: ["请选择性别", "男", "女"],
      genderIndex: 0
    };
  },
  methods: {
    submit: function(e) {
      var rule = [
        { name: "name1", checkType: "string", checkRule: "1,10", errorMsg: "**应为1-10个字符" },
        { name: "name2", checkType: "string", checkRule: "6,", errorMsg: "密码至少6个字符" },
        { name: "name3", checkType: "samewith", checkRule: "name2", errorMsg: "两次密码输入不一致" },
        { name: "name4", checkType: "email", checkRule: "", errorMsg: "邮箱格式错误" },
        { name: "name5", checkType: "notsame", checkRule: "请选择性别", errorMsg: "请选择性别" },
        // 正则表达式验证演示 : 例如检查必须是有效数字
        { name: "name7", checkType: "reg", checkRule: "^(-?\\d+)$", errorMsg: "必须是有效数字" },
        { name: "name8", checkType: "idCard", checkRule: "", errorMsg: "身份证号码错误" }
      ];
      var formData = e.detail.value;
      formData.name5 = this.formData.name5;
      console.log(formData);
      var checkRes = Grace6_js_checker.graceChecker.check(formData, rule);
      if (checkRes) {
        if (formData.name6.length < 1) {
          common_vendor.index.showToast({ title: "请选择爱好!", icon: "none" });
          return false;
        }
        common_vendor.index.showToast({ title: "验证通过!", icon: "none" });
      } else {
        common_vendor.index.showToast({ title: Grace6_js_checker.graceChecker.error, icon: "none" });
      }
    },
    pickerChange: function(e) {
      this.genderIndex = e.detail.value;
      this.formData.name5 = this.gender[this.genderIndex];
    }
  }
};
if (!Array) {
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  _easycom_gui_page2();
}
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  _easycom_gui_page();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.submit && $options.submit(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-e5823c2c"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/xieyi.vue"]]);
wx.createPage(MiniProgramPage);
