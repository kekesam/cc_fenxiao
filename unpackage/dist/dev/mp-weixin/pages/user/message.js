"use strict";
const common_vendor = require("../../common/vendor.js");
var face = "https://images.unsplash.com/photo-1662695089339-a2c24231a3ac?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxM3x8fGVufDB8fHx8&auto=format&fit=crop&w=100&q=60";
var msgsFromApi = [
  {
    img: face,
    msgnumber: 8,
    title: "某某某",
    time: "30分钟前",
    content: "消息内容 ..."
  },
  {
    img: face,
    msgnumber: 0,
    title: "某某某",
    time: "08/10/2020",
    content: "语音消息"
  },
  {
    img: face,
    msgnumber: 12,
    title: "某某某",
    time: "昨天",
    content: "图片消息"
  },
  {
    img: face,
    msgnumber: 1,
    title: "某某某",
    time: "08/08/2020",
    content: "系统消息"
  },
  {
    img: face,
    msgnumber: 8,
    title: "某某某",
    time: "30分钟前",
    content: "消息内容 ..."
  },
  {
    img: face,
    msgnumber: 0,
    title: "某某某",
    time: "08/10/2020",
    content: "语音消息"
  },
  {
    img: face,
    msgnumber: 12,
    title: "某某某",
    time: "昨天",
    content: "图片消息"
  },
  {
    img: face,
    msgnumber: 1,
    title: "某某某",
    time: "08/08/2020",
    content: "系统消息"
  },
  {
    img: face,
    msgnumber: 8,
    title: "某某某",
    time: "30分钟前",
    content: "消息内容 ..."
  },
  {
    img: face,
    msgnumber: 0,
    title: "某某某",
    time: "08/10/2020",
    content: "语音消息"
  },
  {
    img: face,
    msgnumber: 12,
    title: "某某某",
    time: "昨天",
    content: "图片消息"
  },
  {
    img: face,
    msgnumber: 1,
    title: "某某某",
    time: "08/08/2020",
    content: "系统消息"
  }
];
const _sfc_main = {
  data() {
    return {
      msgs: [],
      pageLoading: true
    };
  },
  onLoad: function() {
    setTimeout(() => {
      for (let i = 0; i < msgsFromApi.length; i++) {
        msgsFromApi[i].btns = [
          { "name": "标为已读", bgColor: "#323232" },
          { "name": "删除消息", bgColor: "#FF0036" }
        ];
      }
      this.msgs = msgsFromApi;
      this.pageLoading = false;
    }, 500);
  },
  methods: {
    btnTap: function(index, btnIndex) {
      console.log(index, btnIndex);
      if (btnIndex == 0) {
        if (this.msgs[index].btns[0].name == "标为已读") {
          this.msgs[index].btns = [
            { "name": "标为未读", bgColor: "#888888" },
            { "name": "删除消息", bgColor: "#FF0036" }
          ];
          this.msgs[index].msgnumber = 0;
        } else {
          this.msgs[index].btns = [
            { "name": "标为已读", bgColor: "#323232" },
            { "name": "删除消息", bgColor: "#FF0036" }
          ];
          this.msgs[index].msgnumber = 1;
        }
        setTimeout(() => {
          this.msgs.splice(index, 1, this.msgs[index]);
        }, 300);
      } else if (btnIndex == 1) {
        common_vendor.index.showModal({
          title: "确定要删除吗?",
          success: (e) => {
            if (e.confirm) {
              this.msgs.splice(index, 1);
              this.$refs.guiSlideList.moveIndex = -1;
            }
          }
        });
      }
    },
    // 列表本身被点击
    itemTap: function(e) {
      console.log(e);
      common_vendor.index.showToast({ title: "索引" + e });
    },
    loadMore: function() {
      this.$refs.guiSlideList.startLoadig();
      setTimeout(() => {
        for (let i = 0; i < msgsFromApi.length; i++) {
          msgsFromApi[i].btns = [
            { "name": "标为已读", bgColor: "#323232" },
            { "name": "删除消息", bgColor: "#FF0036" }
          ];
        }
        this.msgs = this.msgs.concat(msgsFromApi);
        this.$refs.guiSlideList.endLoading();
      }, 500);
    }
  }
};
if (!Array) {
  const _easycom_gui_slide_list2 = common_vendor.resolveComponent("gui-slide-list");
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  (_easycom_gui_slide_list2 + _easycom_gui_page2)();
}
const _easycom_gui_slide_list = () => "../../Grace6/components/gui-slide-list.js";
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  (_easycom_gui_slide_list + _easycom_gui_page)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.sr("guiSlideList", "2ca71946-1,2ca71946-0"),
    b: common_vendor.o($options.itemTap),
    c: common_vendor.o($options.btnTap),
    d: common_vendor.o($options.loadMore),
    e: common_vendor.p({
      msgs: $data.msgs
    }),
    f: common_vendor.sr("guiPage", "2ca71946-0"),
    g: common_vendor.p({
      fullPage: true,
      isLoading: $data.pageLoading
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/message.vue"]]);
wx.createPage(MiniProgramPage);
