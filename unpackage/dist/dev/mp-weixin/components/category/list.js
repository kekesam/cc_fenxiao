"use strict";
const Grace6_js_grace = require("../../Grace6/js/grace.js");
const common_vendor = require("../../common/vendor.js");
require("../../Grace6/js/md5.js");
const _sfc_main = {
  data() {
    return {
      isLoad: false,
      listItems: [
        {
          id: 1,
          page: 2,
          icon: "/static/list/book.png",
          url: "/pages/category/course",
          title: "精品课程"
        },
        {
          id: 2,
          page: 1,
          icon: "/static/list/deng.png",
          url: "/pages/category/note",
          title: "实战小册"
        },
        {
          id: 3,
          page: 2,
          icon: "/static/list/cp.png",
          url: "/pages/category/downloads",
          title: "资源下载"
        },
        {
          id: 4,
          page: 1,
          icon: "/static/list/ted.png",
          url: "/pages/category/downloads",
          title: "下载规则"
        },
        {
          id: 5,
          page: 2,
          icon: "/static/list/xueba.png",
          url: "/pages/category/rule",
          title: "推广规则"
        }
      ]
    };
  },
  created() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  },
  methods: {
    handleClick(item) {
      Grace6_js_grace.graceJS.navigate(`${item.url}?id=${item.id}&title=${item.title}&page=${item.page}`, "navigateTo");
    }
  }
};
if (!Array) {
  const _easycom_gui_image2 = common_vendor.resolveComponent("gui-image");
  _easycom_gui_image2();
}
const _easycom_gui_image = () => "../../Grace6/components/gui-image.js";
if (!Math) {
  _easycom_gui_image();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: !$data.isLoad
  }, !$data.isLoad ? {
    b: common_vendor.f($data.listItems, (item, index, i0) => {
      return {
        a: index
      };
    })
  } : {}, {
    c: $data.isLoad
  }, $data.isLoad ? {
    d: common_vendor.f($data.listItems, (item, index, i0) => {
      return {
        a: "d9cf5a24-0-" + i0,
        b: common_vendor.p({
          src: item.icon,
          borderRadius: "10rpx",
          width: 96,
          height: 96
        }),
        c: common_vendor.t(item.title),
        d: index,
        e: common_vendor.o(($event) => $options.handleClick(item), index)
      };
    })
  } : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/components/category/list.vue"]]);
wx.createComponent(Component);
