"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "logo",
  data() {
    return {};
  },
  props: {
    showcart: {
      type: Boolean,
      default: false
    },
    showback: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: "我是标题"
    },
    btntext: {
      type: String,
      default: ""
    },
    version: {
      type: String,
      default: "V1.0"
    },
    showsearch: {
      type: Boolean,
      default: true
    },
    desc: {
      type: String,
      default: "一款优秀的前端框架 · 更丰富 · 更高效 · 更稳定"
    },
    center: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    handleClick() {
      this.$emit("back");
    },
    search() {
      common_vendor.index.navigateTo({
        url: "/pages/search/search"
      });
    },
    toShopcart() {
      common_vendor.index.navigateTo({
        url: "/pages/shopcart/shopcart"
      });
    }
  }
};
if (!Array) {
  const _easycom_gui_search2 = common_vendor.resolveComponent("gui-search");
  _easycom_gui_search2();
}
const _easycom_gui_search = () => "../../Grace6/components/gui-search.js";
if (!Math) {
  _easycom_gui_search();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.t($props.title),
    b: $props.showback
  }, $props.showback ? common_vendor.e({
    c: $props.showsearch
  }, $props.showsearch ? {
    d: common_vendor.o($options.search),
    e: common_vendor.o($options.search),
    f: common_vendor.p({
      clearBtn: true,
      placeholder: "搜索课程"
    })
  } : {}, {
    g: !$props.showcart
  }, !$props.showcart ? {
    h: common_vendor.t($props.btntext || ""),
    i: common_vendor.o((...args) => $options.handleClick && $options.handleClick(...args))
  } : {}, {
    j: $props.showcart
  }, $props.showcart ? {
    k: common_vendor.o((...args) => $options.toShopcart && $options.toShopcart(...args))
  } : {}, {
    l: $props.showsearch ? 1 : "0"
  }) : {}, {
    m: common_vendor.n($props.center ? "gui-justify-content-center" : ""),
    n: common_vendor.t($props.desc),
    o: common_vendor.n($props.center ? "gui-text-center" : "")
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-918e1ca1"], ["__file", "D:/App/分销系统/cc_fenxiao/components/logo/logo.vue"]]);
wx.createComponent(Component);
