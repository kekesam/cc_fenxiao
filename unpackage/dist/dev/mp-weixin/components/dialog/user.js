"use strict";
const Grace6_js_parserHTML = require("../../Grace6/js/parserHTML.js");
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "dialog",
  data() {
    return {
      open: false,
      nodes: [],
      xyhtml: `<div class="markdown-body"><div class="privacy-dialog">
					  <div class="">
						<h3 style="text-align:center;">感谢您使用学习512！</h3>
						<p>为了更好地保障您的个人权益，请认真阅读<a target="_blank" href="//reg.163.com/agreement_wap.shtml?v=20171127">《使用协议》</a>、<a target="_blank" href="//study.163.com/topics/privacy_policy">《隐私政策》</a>和<a target="_blank" href="//study.163.com/topics/protocol_20005/">《服务条款》</a>的全部内容，同意并接受全部条款后开始使用我们的产品和服务。</p>
						<p>若不同意，将无法使用我们的产品和服务。</p>
					  </div>
					</div>
				</div>`
    };
  },
  created() {
    this.nodes = Grace6_js_parserHTML.parserHtml.parserHTML(this.xyhtml);
  },
  methods: {
    handleClose() {
      this.open = false;
    },
    handleOpen() {
      this.open = true;
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.open
  }, $data.open ? {
    b: $data.nodes,
    c: common_vendor.o((...args) => $options.handleClose && $options.handleClose(...args))
  } : {}, {
    d: $data.open
  }, $data.open ? {
    e: common_vendor.o((...args) => $options.handleClose && $options.handleClose(...args))
  } : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-99aeb015"], ["__file", "D:/App/分销系统/cc_fenxiao/components/dialog/user.vue"]]);
wx.createComponent(Component);
