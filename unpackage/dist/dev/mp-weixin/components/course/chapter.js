"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      chapterList: [],
      lessonIndex: 0,
      chapterIndex: 0
    };
  },
  created: function() {
    this.chapterList = [{ title: "发刊词", expand: true, id: 1 }, { title: "发刊词", expand: true, id: 4 }, { title: "发刊词", expand: true, id: 3 }];
  },
  methods: {
    handleChapter(item, index) {
      this.chapterIndex = index;
      item.expand = !item.expand;
    },
    handleLesson(citem, index, cindex) {
      this.chapterIndex = index;
      this.lessonIndex = cindex;
    }
  }
};
if (!Array) {
  const _easycom_gui_page2 = common_vendor.resolveComponent("gui-page");
  _easycom_gui_page2();
}
const _easycom_gui_page = () => "../../Grace6/components/gui-page.js";
if (!Math) {
  _easycom_gui_page();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.chapterList, (item, index, i0) => {
      return common_vendor.e({
        a: item.expand
      }, item.expand ? {} : {}, {
        b: item.expand
      }, item.expand ? {
        c: common_vendor.f(5, (num, cindex, i1) => {
          return common_vendor.e({
            a: $data.lessonIndex == cindex && $data.chapterIndex == index
          }, $data.lessonIndex == cindex && $data.chapterIndex == index ? {} : {}, {
            b: common_vendor.o(($event) => $options.handleLesson(item, index, cindex)),
            c: common_vendor.n($data.lessonIndex == cindex && $data.chapterIndex == index ? "green" : "")
          });
        })
      } : {}, {
        d: common_vendor.o(($event) => $options.handleChapter(item, index))
      });
    }),
    b: common_vendor.sr("guiPage", "7974a0ca-0"),
    c: common_vendor.p({
      fullPage: true
    })
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-7974a0ca"], ["__file", "D:/App/分销系统/cc_fenxiao/components/course/chapter.vue"]]);
wx.createComponent(Component);
