"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  props: {
    fsize: {
      type: String,
      default: "fz14"
    },
    title: {
      type: String,
      default: "历史记录"
    },
    ctitle: {
      type: String,
      default: "清空记录"
    }
  },
  name: "nav",
  data() {
    return {};
  },
  methods: {
    handleClick() {
      this.$emits("clear");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($props.title),
    b: common_vendor.n($props.fsize),
    c: common_vendor.t($props.ctitle),
    d: common_vendor.o((...args) => $options.handleClick && $options.handleClick(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/nav.vue"]]);
wx.createComponent(Component);
