"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  props: {
    fsize: {
      type: String,
      default: "fz14"
    },
    title: {
      type: String,
      default: "历史记录"
    },
    ctitle: {
      type: String,
      default: "查看更多"
    },
    showmore: {
      type: Boolean,
      default: true
    }
  },
  name: "nav",
  data() {
    return {};
  },
  methods: {
    handleClick() {
      this.$emits("clear");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.t($props.title),
    b: common_vendor.n($props.fsize),
    c: $props.showmore
  }, $props.showmore ? {
    d: common_vendor.t($props.ctitle),
    e: common_vendor.o((...args) => $options.handleClick && $options.handleClick(...args))
  } : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/subitem.vue"]]);
wx.createComponent(Component);
