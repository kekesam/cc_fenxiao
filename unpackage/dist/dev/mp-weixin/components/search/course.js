"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  components: {},
  props: {
    waterfall: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      isLoad: false,
      loading: false,
      // 数据表名
      collection: "opendb-mall-goods",
      // 查询字段，多个字段用 , 分割
      field: "goods_thumb,name,goods_tip,tag,goods_price,comment_count,month_sell_count,shop_name",
      productList: [{
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/ted.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/xue.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/deng.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/cp.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/ted.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/deng.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/deng.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }, {
        name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
        goods_thumb: "/static/list/xueba.png",
        goods_tip: "自营",
        tag: ["手机", "iphone", "笔记本"],
        goods_price: "3499",
        comment_count: "1244",
        month_sell_count: "640",
        shop_name: "飞哥的课程推荐"
      }],
      formData: {
        waterfall: true,
        // 布局方向切换
        status: "noMore"
        // 加载状态
      },
      tipShow: true
      // 是否显示顶部提示框
    };
  },
  created() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  },
  methods: {
    load(data, ended) {
      if (ended) {
        this.formData.status = "noMore";
      }
    },
    navchange() {
    }
  },
  /**
   * 下拉刷新回调函数
   */
  onPullDownRefresh() {
    this.formData.status = "more";
    this.$refs.udb.loadData({
      clear: true
    }, () => {
      this.tipShow = true;
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.tipShow = false;
      }, 1e3);
      common_vendor.index.stopPullDownRefresh();
    });
  },
  /**
   * 上拉加载回调函数
   */
  onReachBottom() {
    this.$refs.udb.loadMore();
  }
};
if (!Array) {
  const _easycom_uni_list_item2 = common_vendor.resolveComponent("uni-list-item");
  const _easycom_uni_list2 = common_vendor.resolveComponent("uni-list");
  const _easycom_uni_load_more2 = common_vendor.resolveComponent("uni-load-more");
  (_easycom_uni_list_item2 + _easycom_uni_list2 + _easycom_uni_load_more2)();
}
const _easycom_uni_list_item = () => "../../uni_modules/uni-list/components/uni-list-item/uni-list-item.js";
const _easycom_uni_list = () => "../../uni_modules/uni-list/components/uni-list/uni-list.js";
const _easycom_uni_load_more = () => "../../uni_modules/uni-load-more/components/uni-load-more/uni-load-more.js";
if (!Math) {
  (_easycom_uni_list_item + _easycom_uni_list + _easycom_uni_load_more)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.f($data.productList, (item, k0, i0) => {
      return common_vendor.e(!$data.isLoad ? {
        a: $props.waterfall ? 1 : ""
      } : {}, $data.isLoad ? {
        b: item.goods_thumb,
        c: $props.waterfall ? 1 : ""
      } : {}, !$data.isLoad ? {} : {}, $data.isLoad ? {
        d: common_vendor.t(item.name)
      } : {}, !$data.isLoad ? {} : {}, !$data.isLoad ? {} : {}, !$data.isLoad ? {} : {}, $data.isLoad ? {
        e: common_vendor.t(item.goods_tip)
      } : {}, $data.isLoad ? {
        f: common_vendor.f(item.tag, (tag, k1, i1) => {
          return {
            a: common_vendor.t(tag),
            b: tag
          };
        })
      } : {}, !$data.isLoad ? {} : {}, $data.isLoad ? {
        g: common_vendor.t(item.goods_price)
      } : {}, !$data.isLoad ? {} : {}, $data.isLoad ? {
        h: common_vendor.t(item.comment_count),
        i: common_vendor.t(item.month_sell_count)
      } : {}, !$data.isLoad ? {} : {}, $data.isLoad ? {
        j: common_vendor.t(item.shop_name)
      } : {}, {
        k: item._id,
        l: "b686f227-1-" + i0 + ",b686f227-0",
        m: common_vendor.p({
          border: !$props.waterfall,
          title: "自定义商品列表",
          to: "/pages/detail/detail?id=" + item._id + "&title=" + item.name
        })
      });
    }),
    b: !$data.isLoad,
    c: $data.isLoad,
    d: !$data.isLoad,
    e: $data.isLoad,
    f: !$data.isLoad,
    g: !$data.isLoad,
    h: !$data.isLoad,
    i: $data.isLoad,
    j: $data.isLoad,
    k: !$data.isLoad,
    l: $data.isLoad,
    m: !$data.isLoad,
    n: $data.isLoad,
    o: !$data.isLoad,
    p: $data.isLoad,
    q: $props.waterfall ? 1 : "",
    r: $data.isLoad && ($data.loading || $data.formData.status === "noMore")
  }, $data.isLoad && ($data.loading || $data.formData.status === "noMore") ? {
    s: common_vendor.p({
      status: $data.formData.status
    })
  } : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-b686f227"], ["__file", "D:/App/分销系统/cc_fenxiao/components/search/course.vue"]]);
wx.createComponent(Component);
