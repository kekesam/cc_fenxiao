"use strict";
const common_vendor = require("../../common/vendor.js");
var img = "https://images.unsplash.com/photo-1661956602868-6ae368943878?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHw2MHx8fGVufDB8fHx8&auto=format&fit=crop&w=600&q=90";
const _sfc_main = {
  name: "page-demo",
  props: {
    height: {
      type: Number,
      default: 230
    },
    showspace: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      isLoad: false,
      swiperItems: [
        {
          img,
          url: "",
          title: "测试标题 001",
          opentype: "navigate"
        },
        {
          img,
          url: "",
          title: "测试标题 002",
          opentype: "navigate"
        },
        {
          img,
          url: "",
          title: "测试标题很长很长很长很长很长很长很长很长很长很长很长很长很长很长",
          opentype: "navigate"
        }
      ]
    };
  },
  methods: {
    swiperchange: function(e) {
      console.log(e);
    },
    taped: function(e) {
      common_vendor.index.showToast({
        title: "您点击了第 " + e + " 个项目",
        icon: "none"
      });
    }
  },
  created() {
    setTimeout(() => {
      this.isLoad = true;
    }, 1e3);
  }
};
if (!Array) {
  const _easycom_gui_swiper2 = common_vendor.resolveComponent("gui-swiper");
  _easycom_gui_swiper2();
}
const _easycom_gui_swiper = () => "../../Grace6/components/gui-swiper.js";
if (!Math) {
  _easycom_gui_swiper();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: !$data.isLoad
  }, !$data.isLoad ? {
    b: $props.height + "rpx"
  } : {}, {
    c: $data.isLoad
  }, $data.isLoad ? {
    d: common_vendor.p({
      swiperItems: $data.swiperItems,
      imgMode: "scaleToFill",
      width: 750,
      spacing: 0,
      borderRadius: "0",
      height: $props.height
    })
  } : {}, {
    e: $props.showspace
  }, $props.showspace ? {} : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/components/advert/index.vue"]]);
wx.createComponent(Component);
