"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-stags",
  props: {
    width: { type: Number, default: 0 },
    size: { type: Number, default: 26 },
    lineHeight: { type: Number, default: 2 },
    padding: { type: Number, default: 15 },
    margin: { type: Number, default: 15 },
    defaultClass: {
      type: Array,
      default: function() {
        return ["gui-bg-gray", "gui-dark-bg-level-1", "gui-primary-text", "gui-dark-text-level-5"];
      }
    },
    checkedClass: {
      type: Array,
      default: function() {
        return ["gui-bg-primary", "gui-dark-bg", "gui-color-white"];
      }
    },
    borderRadius: { type: Number, default: 6 },
    data: { type: Array, default: function() {
      return [];
    } },
    tags: { type: Array, default: function() {
      return [];
    } },
    type: { type: String, default: "radio" }
  },
  data() {
    return {
      tagsIn: []
    };
  },
  created: function() {
    this.tagsIn = this.tags;
  },
  watch: {
    tags: function(val) {
      this.tagsIn = val;
    }
  },
  methods: {
    tapme: function(idx) {
      if (this.type == "radio") {
        if (this.tagsIn[idx].checked) {
          this.tagsIn[idx].checked = false;
          this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
          this.$emit("change", -1, this.tagsIn);
        } else {
          for (let i = 0; i < this.tagsIn.length; i++) {
            this.tagsIn[i].checked = false;
            this.tagsIn.splice(i, 1, this.tagsIn[i]);
          }
          this.tagsIn[idx].checked = true;
          this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
          this.$emit("change", this.tagsIn[idx], this.tagsIn);
        }
      } else if (this.type == "checkbox") {
        this.tagsIn[idx].checked = !this.tagsIn[idx].checked;
        this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
        var sedArr = [];
        for (let i = 0; i < this.tagsIn.length; i++) {
          if (this.tagsIn[i].checked) {
            sedArr.push(i);
          }
        }
        this.$emit("change", sedArr, this.tagsIn);
      } else {
        this.tagsIn.splice(idx, 1);
        this.$emit("change", this.tagsIn);
      }
    }
  },
  emits: ["change"]
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.type != "remove"
  }, $props.type != "remove" ? {
    b: common_vendor.f($data.tagsIn, (tag, idx, i0) => {
      return {
        a: common_vendor.t(tag.text),
        b: idx,
        c: common_vendor.n(tag.checked ? $props.checkedClass : $props.defaultClass),
        d: common_vendor.o(($event) => $options.tapme(idx), idx)
      };
    }),
    c: $props.width == 0 ? "" : $props.width + "rpx",
    d: $props.padding + "rpx",
    e: $props.padding + "rpx",
    f: $props.size * $props.lineHeight + "rpx",
    g: $props.size * $props.lineHeight + "rpx",
    h: $props.size + "rpx",
    i: $props.borderRadius + "rpx",
    j: $props.margin + "rpx",
    k: $props.margin + "rpx"
  } : {}, {
    l: $props.type == "remove"
  }, $props.type == "remove" ? {
    m: common_vendor.f($data.tagsIn, (tag, idx, i0) => {
      return {
        a: common_vendor.t(tag.text),
        b: idx,
        c: common_vendor.o(($event) => $options.tapme(idx), idx)
      };
    }),
    n: $props.size * $props.lineHeight + "rpx",
    o: $props.size * $props.lineHeight + "rpx",
    p: $props.size + "rpx",
    q: $props.size * $props.lineHeight + "rpx",
    r: $props.size * $props.lineHeight + "rpx",
    s: $props.size + "rpx",
    t: common_vendor.n($props.defaultClass),
    v: $props.width == 0 ? "" : $props.width + "rpx",
    w: $props.padding + "rpx",
    x: $props.padding + "rpx",
    y: $props.borderRadius + "rpx",
    z: $props.margin + "rpx",
    A: $props.margin + "rpx"
  } : {});
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-0c9885d7"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-stags.vue"]]);
wx.createComponent(Component);
