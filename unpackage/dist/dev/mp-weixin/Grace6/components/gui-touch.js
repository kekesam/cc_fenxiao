"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-touch",
  props: {
    datas: { type: Array, default: function() {
      return [];
    } }
  },
  data() {
    return {
      toucheTimer: 0,
      fingerRes: [],
      distance: 0,
      taptimer: 100
    };
  },
  methods: {
    toInt: function(arr) {
      var res = [];
      arr.forEach((item) => {
        item.pageX = parseInt(item.pageX);
        item.pageY = parseInt(item.pageY);
        res.push(item);
      });
      return res;
    },
    touchstart: function(e) {
      this.toucheTimer = (/* @__PURE__ */ new Date()).getTime();
      this.fingerRes = this.toInt(e.changedTouches);
      if (this.fingerRes.length > 2) {
        return;
      }
      var moves = [];
      this.fingerRes.forEach((finger) => {
        var xTouch = finger.pageX;
        var yTouch = finger.pageY;
        moves.push([xTouch, yTouch]);
      });
      this.$emit("thStart", moves, this.datas);
    },
    touchmove: function(e) {
      if (this.toucheTimer < 50) {
        return;
      }
      var timer = (/* @__PURE__ */ new Date()).getTime() - this.toucheTimer;
      if (timer < this.taptimer) {
        return;
      }
      var touches = this.toInt(e.changedTouches);
      if (touches.length > 2) {
        return;
      }
      if (touches.length == 1) {
        var i = 0, moves = [];
        touches.forEach((finger) => {
          var xTouch = finger.pageX - this.fingerRes[i].pageX;
          var yTouch = finger.pageY - this.fingerRes[i].pageY;
          moves.push([xTouch, yTouch]);
          i++;
        });
        this.$emit("thMove", moves, this.datas);
      } else if (touches.length == 2) {
        if (this.distance == 0) {
          this.distance = parseInt(this.getDistance(touches[0].pageX, touches[0].pageY, touches[1].pageX, touches[1].pageY));
        } else {
          var distance1 = parseInt(this.getDistance(touches[0].pageX, touches[0].pageY, touches[1].pageX, touches[1].pageY));
          var scale = distance1 / this.distance;
          scale = Math.floor(scale * 100) / 100;
          this.$emit("scale", scale, this.datas);
        }
      }
    },
    touchend: function(e) {
      var timer = (/* @__PURE__ */ new Date()).getTime() - this.toucheTimer;
      if (timer < this.taptimer) {
        this.$emit("tapme");
        return;
      }
      var touches = this.toInt(e.changedTouches);
      this.distance = 0;
      if (touches.length == 1) {
        var i = 0, moves = [];
        touches.forEach((finger) => {
          var xTouch = finger.pageX - this.fingerRes[i].pageX;
          var yTouch = finger.pageY - this.fingerRes[i].pageY;
          moves.push([xTouch, yTouch]);
          i++;
        });
        moves.push(timer);
        this.$emit("thEnd", moves, this.datas);
        if (timer < 300) {
          var mx = Math.abs(moves[0][0]);
          var my = Math.abs(moves[0][1]);
          if (mx > my) {
            if (mx >= 50) {
              if (moves[0][0] > 0) {
                this.$emit("swipe", "right", this.datas);
              } else {
                this.$emit("swipe", "left", this.datas);
              }
            }
          } else {
            if (my >= 50) {
              if (moves[0][1] > 0) {
                this.$emit("swipe", "down", this.datas);
              } else {
                this.$emit("swipe", "up", this.datas);
              }
            }
          }
        }
      }
    },
    getDistance: function(lat1, lng1, lat2, lng2) {
      var radLat1 = lat1 * Math.PI / 180;
      var radLat2 = lat2 * Math.PI / 180;
      var a = radLat1 - radLat2;
      var b = lng1 * Math.PI / 180 - lng2 * Math.PI / 180;
      var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
      s = s * 6378.137;
      return Math.round(s * 1e4) / 1e4;
    },
    tapme: function() {
      this.isTap = true;
    }
  },
  emits: ["thStart", "thMove", "scale", "tapme", "thEnd", "swipe"]
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.touchstart && $options.touchstart(...args)),
    b: common_vendor.o((...args) => $options.touchmove && $options.touchmove(...args)),
    c: common_vendor.o((...args) => $options.touchend && $options.touchend(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-touch.vue"]]);
wx.createComponent(Component);
