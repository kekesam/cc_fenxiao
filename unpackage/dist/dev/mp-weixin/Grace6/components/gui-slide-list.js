"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-slide-list",
  props: {
    width: { type: Number, default: 750 },
    msgs: { type: Array, default: function() {
      return [];
    } },
    btnWidth: { type: Number, default: 320 },
    titleEllipsis: { type: Boolean, default: true },
    descEllipsis: { type: Boolean, default: true }
  },
  data() {
    return {
      msgsIn: [],
      damping: 0.29,
      moveIndex: -1,
      x: 0,
      oX: 0,
      scY: true,
      btnWidthpx: 160,
      touchStart: false
    };
  },
  created: function() {
    this.init(this.msgs);
    this.btnWidthpx = common_vendor.index.upx2px(this.btnWidth) * -1 + 2;
  },
  watch: {
    msgs: function(nv) {
      this.init(nv);
    }
  },
  methods: {
    init: function(msgs) {
      this.moveIndex = -1;
      this.msgsIn = msgs;
    },
    thStart: function(e, index) {
      this.x = 0;
      this.moveIndex = index[0];
      this.damping = 0.25;
    },
    thMove: function(e, index) {
      var x = e[0][0];
      var y = e[0][1];
      if (Math.abs(x) < Math.abs(y)) {
        this.scY = true;
        return;
      } else {
        this.scY = false;
      }
      if (x < 0) {
        this.x += x * this.damping;
        if (this.x < this.btnWidthpx) {
          this.x = this.btnWidthpx;
        }
        this.damping *= 1.02;
      } else {
        this.scY = true;
      }
    },
    thEnd: function(e, index) {
      if (this.x > this.btnWidthpx / 8) {
        this.x = 0;
      } else {
        this.x = this.btnWidthpx;
      }
      this.scY = true;
      this.oX = this.x;
    },
    btnTap: function(index, indexBtn) {
      this.$emit("btnTap", index, indexBtn);
    },
    itemTap: function(index) {
      if (this.oX < 0) {
        this.oX = 0;
        this.moveIndex = -1;
        return;
      }
      this.$emit("itemTap", index);
      this.moveIndex = -1;
      this.oX = 0;
    },
    scrolltolower: function() {
      var laodStatus = this.$refs.loadmoreinslidelist.loadMoreStatus;
      if (laodStatus == 0) {
        this.$emit("scrolltolower");
      }
    },
    startLoadig: function() {
      this.$refs.loadmoreinslidelist.loading();
    },
    nomore: function() {
      this.$refs.loadmoreinslidelist.nomore();
    },
    endLoading: function() {
      this.$refs.loadmoreinslidelist.stoploadmore();
    }
  },
  emits: ["btnTap", "scrolltolower"]
};
if (!Array) {
  const _easycom_gui_touch2 = common_vendor.resolveComponent("gui-touch");
  const _easycom_gui_loadmore2 = common_vendor.resolveComponent("gui-loadmore");
  (_easycom_gui_touch2 + _easycom_gui_loadmore2)();
}
const _easycom_gui_touch = () => "./gui-touch.js";
const _easycom_gui_loadmore = () => "./gui-loadmore.js";
if (!Math) {
  (_easycom_gui_touch + _easycom_gui_loadmore)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.msgsIn, (item, index, i0) => {
      return common_vendor.e({
        a: item.img,
        b: item.msgnumber > 0
      }, item.msgnumber > 0 ? {
        c: common_vendor.t(item.msgnumber)
      } : {}, {
        d: common_vendor.o(($event) => $options.itemTap(index), index),
        e: common_vendor.t(item.title),
        f: common_vendor.t(item.time),
        g: common_vendor.t(item.content),
        h: common_vendor.o($options.thStart, index),
        i: common_vendor.o($options.thMove, index),
        j: common_vendor.o($options.thEnd, index),
        k: common_vendor.o(($event) => $options.itemTap(index), index),
        l: "3e1c9efb-0-" + i0,
        m: common_vendor.p({
          datas: [index]
        }),
        n: common_vendor.f(item.btns, (btn, btnIndex, i1) => {
          return {
            a: common_vendor.t(btn.name),
            b: btnIndex,
            c: btn.bgColor,
            d: common_vendor.o(($event) => $options.btnTap(index, btnIndex), btnIndex)
          };
        }),
        o: "translateX(" + ($data.moveIndex != index ? 0 : $data.x) + "px)",
        p: index
      });
    }),
    b: common_vendor.n($props.titleEllipsis ? "gui-ellipsis" : ""),
    c: common_vendor.n($props.descEllipsis ? "gui-ellipsis" : ""),
    d: $props.btnWidth - 2 + "rpx",
    e: $props.width + $props.btnWidth + "rpx",
    f: $props.width + "rpx",
    g: common_vendor.sr("loadmoreinslidelist", "3e1c9efb-1"),
    h: $data.scY,
    i: $props.width + "rpx",
    j: common_vendor.o((...args) => $options.scrolltolower && $options.scrolltolower(...args))
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-3e1c9efb"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-slide-list.vue"]]);
wx.createComponent(Component);
