"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-modal",
  props: {
    width: { type: String, default: "580rpx" },
    isCloseBtn: { type: Boolean, default: true },
    closeBtnStyle: { type: String, default: "font-size:28rpx;" },
    title: { type: String, default: "" },
    titleStyle: { type: String, default: "line-height:100rpx; font-size:28rpx; font-weight:700;" },
    canCloseByShade: { type: Boolean, default: true },
    zIndex: { type: Number, default: 99 },
    customClass: { type: Array, default: function() {
      return ["gui-bg-white", "gui-dark-bg-level-3"];
    } }
  },
  methods: {
    open: function() {
      this.$refs.guipopupformodal.open();
      this.$emit("open");
    },
    close: function() {
      this.$refs.guipopupformodal.close();
      this.$emit("close");
    },
    stopfun: function(e) {
      e.stopPropagation();
      return null;
    },
    eClose: function() {
      this.$emit("close");
    }
  },
  emits: ["open", "close"]
};
if (!Array) {
  const _easycom_gui_popup2 = common_vendor.resolveComponent("gui-popup");
  _easycom_gui_popup2();
}
const _easycom_gui_popup = () => "./gui-popup.js";
if (!Math) {
  _easycom_gui_popup();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $props.title != ""
  }, $props.title != "" ? {
    b: common_vendor.t($props.title),
    c: common_vendor.s($props.titleStyle)
  } : {}, {
    d: $props.isCloseBtn
  }, $props.isCloseBtn ? {
    e: common_vendor.s("width:76rpx; height:76rpx; line-height:76rpx; text-align:center;" + $props.closeBtnStyle),
    f: common_vendor.o((...args) => $options.close && $options.close(...args))
  } : {}, {
    g: common_vendor.n($props.customClass),
    h: common_vendor.o((...args) => $options.stopfun && $options.stopfun(...args)),
    i: common_vendor.sr("guipopupformodal", "c4b7096a-0"),
    j: common_vendor.o($options.eClose),
    k: common_vendor.p({
      width: $props.width,
      canCloseByShade: $props.canCloseByShade,
      zIndex: $props.zIndex
    })
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-modal.vue"]]);
wx.createComponent(Component);
