"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-radio",
  props: {
    size: {
      type: Number,
      default: 38
    },
    defaultClass: {
      type: Array,
      default: function() {
        return ["gui-color-gray"];
      }
    },
    checked: {
      type: Boolean,
      default: false
    },
    checkedClass: {
      type: Array,
      default: function() {
        return ["gui-bg-primary", "gui-color-white"];
      }
    },
    parameter: {
      type: Array,
      default: function() {
        return [];
      }
    }
  },
  data() {
    return {
      status: false
    };
  },
  watch: {
    checked: function(val, old) {
      this.status = val;
    }
  },
  created: function() {
    this.status = this.checked;
  },
  methods: {
    changeStatus: function() {
      this.status = !this.status;
      this.$emit("change", [this.status, this.parameter]);
    }
  },
  emits: ["change"]
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: $data.status
  }, $data.status ? {
    b: common_vendor.n($props.checkedClass),
    c: $props.size + "rpx",
    d: $props.size + "rpx",
    e: $props.size + "rpx",
    f: $props.size - 15 + "rpx",
    g: $props.size + "rpx"
  } : {
    h: common_vendor.n($props.defaultClass),
    i: $props.size + "rpx",
    j: $props.size + "rpx",
    k: $props.size + 2 + "rpx",
    l: $props.size - 8 + "rpx"
  }, {
    m: common_vendor.o((...args) => $options.changeStatus && $options.changeStatus(...args))
  });
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-0a0a5198"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-radio.vue"]]);
wx.createComponent(Component);
