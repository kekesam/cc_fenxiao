"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "gui-select-list",
  props: {
    items: { type: Array, default: function() {
      return [];
    } },
    type: { type: String, default: "radio" },
    checkedType: { type: String, default: "right" },
    isBorder: { type: Boolean, default: true },
    maxSize: { type: Number, default: 0 },
    checkedClass: { type: Array, default: function() {
      return ["gui-primary-color"];
    } }
  },
  data() {
    return {
      dataIn: []
    };
  },
  created: function() {
    this.dataIn = this.items;
  },
  watch: {
    items: function(val) {
      this.dataIn = val;
    }
  },
  methods: {
    // 获取选中数据的索引
    getSelectedIndex: function() {
      var tmpArr = [];
      this.dataIn.forEach((item, idx) => {
        if (item.checked) {
          tmpArr.push(idx);
        }
      });
      return tmpArr;
    },
    // 选择数据
    choose: function(e) {
      var index = e.currentTarget.dataset.index;
      if (this.type == "radio") {
        if (this.dataIn[index].checked) {
          this.dataIn[index].checked = false;
          this.$emit("change", -1);
        } else {
          for (let i = 0; i < this.dataIn.length; i++) {
            this.dataIn[i].checked = false;
          }
          this.dataIn[index].checked = true;
          this.$emit("change", index);
        }
      } else {
        if (this.dataIn[index].checked) {
          this.dataIn[index].checked = false;
        } else {
          if (this.maxSize > 0) {
            var size = 0;
            this.dataIn.forEach((item) => {
              if (item.checked) {
                size++;
              }
            });
            size++;
            if (size > this.maxSize) {
              this.$emit("maxSed");
              return;
            }
          }
          this.dataIn[index].checked = true;
        }
        var sedArr = [];
        for (let i = 0; i < this.dataIn.length; i++) {
          if (this.dataIn[i].checked) {
            sedArr.push(i);
          }
        }
        this.$emit("change", sedArr);
      }
    }
  },
  emits: ["change", "maxSed"]
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.dataIn, (item, index, i0) => {
      return common_vendor.e({
        a: $props.checkedType == "ring" && !item.checked
      }, $props.checkedType == "ring" && !item.checked ? {} : {}, {
        b: $props.checkedType == "ring" && item.checked
      }, $props.checkedType == "ring" && item.checked ? {
        c: common_vendor.n($props.checkedClass)
      } : {}, {
        d: item.img
      }, item.img ? {
        e: item.img
      } : {}, {
        f: common_vendor.t(item.title),
        g: item.desc
      }, item.desc ? {
        h: common_vendor.t(item.desc)
      } : {}, {
        i: item.checked && $props.checkedType == "right"
      }, item.checked && $props.checkedType == "right" ? {
        j: common_vendor.n($props.checkedClass)
      } : {}, {
        k: common_vendor.n($props.isBorder && index < $data.dataIn.length - 1 ? "gui-border-b" : ""),
        l: index,
        m: index,
        n: common_vendor.o((...args) => $options.choose && $options.choose(...args), index)
      });
    })
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-1485e3e2"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-select-list.vue"]]);
wx.createComponent(Component);
