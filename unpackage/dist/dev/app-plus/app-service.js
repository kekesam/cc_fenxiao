if (typeof Promise !== "undefined" && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor;
    return this.then(
      (value) => promise.resolve(callback()).then(() => value),
      (reason) => promise.resolve(callback()).then(() => {
        throw reason;
      })
    );
  };
}
;
if (typeof uni !== "undefined" && uni && uni.requireGlobal) {
  const global = uni.requireGlobal();
  ArrayBuffer = global.ArrayBuffer;
  Int8Array = global.Int8Array;
  Uint8Array = global.Uint8Array;
  Uint8ClampedArray = global.Uint8ClampedArray;
  Int16Array = global.Int16Array;
  Uint16Array = global.Uint16Array;
  Int32Array = global.Int32Array;
  Uint32Array = global.Uint32Array;
  Float32Array = global.Float32Array;
  Float64Array = global.Float64Array;
  BigInt64Array = global.BigInt64Array;
  BigUint64Array = global.BigUint64Array;
}
;
if (uni.restoreGlobal) {
  uni.restoreGlobal(Vue, weex, plus, setTimeout, clearTimeout, setInterval, clearInterval);
}
(function(vue) {
  "use strict";
  const _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };
  const _sfc_main$17 = {
    name: "gui-search",
    props: {
      height: { type: String, default: "66rpx" },
      customClass: { type: Array, default: function() {
        return ["gui-bg-gray", "gui-dark-bg-level-3"];
      } },
      fontSize: { type: String, default: "28rpx" },
      iconWidth: { type: String, default: "70rpx" },
      iconFontSize: { type: String, default: "30rpx" },
      inputHeight: { type: String, default: "30rpx" },
      inputFontSize: { type: String, default: "26rpx" },
      placeholder: { type: String, default: "关键字" },
      placeholderClass: { type: String, default: "" },
      kwd: { type: String, default: "" },
      borderRadius: { type: String, default: "66rpx" },
      disabled: { type: Boolean, default: false },
      focus: { type: Boolean, default: false },
      clearBtn: { type: Boolean, default: true }
    },
    data() {
      return {
        inputVal: ""
      };
    },
    created: function() {
      this.inputVal = this.kwd;
    },
    watch: {
      kwd: function(val, vo) {
        this.inputVal = val;
      }
    },
    methods: {
      clearKwd: function() {
        this.inputVal = "";
        this.$emit("clear", "");
      },
      inputting: function(e) {
        this.$emit("inputting", e.detail.value);
      },
      confirm: function(e) {
        this.$emit("confirm", e.detail.value);
        uni.hideKeyboard();
      },
      tapme: function() {
        this.$emit("tapme");
      }
    },
    emits: ["clear", "confirm", "tapme", "inputting"]
  };
  function _sfc_render$16(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["gui-flex gui-row gui-nowrap gui-align-items-center gui-flex1", $props.customClass]),
        style: vue.normalizeStyle({
          height: $props.height,
          borderRadius: $props.borderRadius
        })
      },
      [
        vue.createElementVNode(
          "text",
          {
            class: "gui-icons gui-block gui-text-center gui-color-gray",
            onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.tapme && $options.tapme(...args), ["stop"])),
            style: vue.normalizeStyle({
              fontSize: $props.iconFontSize,
              lineHeight: $props.height,
              width: $props.iconWidth,
              marginLeft: "12rpx"
            })
          },
          "",
          4
          /* STYLE */
        ),
        !$props.disabled ? vue.withDirectives((vue.openBlock(), vue.createElementBlock("input", {
          key: 0,
          type: "text",
          "placeholder-class": $props.placeholderClass,
          class: "gui-search-input gui-flex1 gui-primary-text",
          placeholder: $props.placeholder,
          "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.inputVal = $event),
          focus: $props.focus,
          style: vue.normalizeStyle({
            height: $props.inputHeight,
            lineHeight: $props.inputHeight,
            fontSize: $props.inputFontSize
          }),
          onInput: _cache[2] || (_cache[2] = (...args) => $options.inputting && $options.inputting(...args)),
          onConfirm: _cache[3] || (_cache[3] = (...args) => $options.confirm && $options.confirm(...args))
        }, null, 44, ["placeholder-class", "placeholder", "focus"])), [
          [vue.vModelText, $data.inputVal]
        ]) : vue.createCommentVNode("v-if", true),
        $props.disabled ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 1,
            class: "gui-search-input gui-flex1 gui-block gui-color-gray",
            onClick: _cache[4] || (_cache[4] = vue.withModifiers((...args) => $options.tapme && $options.tapme(...args), ["stop"])),
            style: vue.normalizeStyle({
              height: $props.inputHeight,
              lineHeight: $props.inputHeight,
              fontSize: $props.inputFontSize
            })
          },
          vue.toDisplayString($props.placeholder),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $data.inputVal.length > 0 && $props.clearBtn ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 2,
            class: "gui-search-icon gui-icons gui-block gui-text-center gui-color-gray",
            onClick: _cache[5] || (_cache[5] = vue.withModifiers((...args) => $options.clearKwd && $options.clearKwd(...args), ["stop"])),
            style: vue.normalizeStyle({
              fontSize: $props.iconFontSize,
              lineHeight: $props.height,
              width: $props.iconWidth,
              marginRight: "5rpx"
            })
          },
          "",
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_0$d = /* @__PURE__ */ _export_sfc(_sfc_main$17, [["render", _sfc_render$16], ["__scopeId", "data-v-c34f888c"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-search.vue"]]);
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return typeof component === "string" ? easycom : component;
  }
  const _sfc_main$16 = {
    name: "logo",
    data() {
      return {};
    },
    props: {
      showcart: {
        type: Boolean,
        default: false
      },
      showback: {
        type: Boolean,
        default: false
      },
      title: {
        type: String,
        default: "我是标题"
      },
      btntext: {
        type: String,
        default: ""
      },
      version: {
        type: String,
        default: "V1.0"
      },
      showsearch: {
        type: Boolean,
        default: true
      },
      desc: {
        type: String,
        default: "一款优秀的前端框架 · 更丰富 · 更高效 · 更稳定"
      },
      center: {
        type: Boolean,
        default: false
      }
    },
    methods: {
      handleClick() {
        this.$emit("back");
      },
      search() {
        uni.navigateTo({
          url: "/pages/search/search"
        });
      },
      toShopcart() {
        uni.navigateTo({
          url: "/pages/shopcart/shopcart"
        });
      }
    }
  };
  function _sfc_render$15(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_search = resolveEasycom(vue.resolveDynamicComponent("gui-search"), __easycom_0$d);
    return vue.openBlock(), vue.createElementBlock("view", { class: "padding-logo" }, [
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass([[$props.center ? "gui-justify-content-center" : ""], "gui-flex gui-nowrap gui-align-contentItems-center gui-space-between"])
        },
        [
          vue.createElementVNode(
            "text",
            { class: "gui-block gui-primary-color logo-text gui-dark-text-level-2 fb" },
            vue.toDisplayString($props.title),
            1
            /* TEXT */
          ),
          $props.showback ? (vue.openBlock(), vue.createElementBlock(
            "view",
            {
              key: 0,
              style: vue.normalizeStyle([{ "display": "flex", "justify-content": "space-between" }, { flex: $props.showsearch ? 1 : "0" }])
            },
            [
              vue.createCommentVNode(" 搜索框 "),
              $props.showsearch ? (vue.openBlock(), vue.createElementBlock("view", {
                key: 0,
                class: "header-search gui-bg-white gui-dark-bg-level-3 gui-border-box",
                style: { "flex": "1", "padding": "0 20px" }
              }, [
                vue.createVNode(_component_gui_search, {
                  onClear: $options.search,
                  clearBtn: "",
                  placeholder: "搜索课程",
                  onConfirm: $options.search
                }, null, 8, ["onClear", "onConfirm"])
              ])) : vue.createCommentVNode("v-if", true),
              !$props.showcart ? (vue.openBlock(), vue.createElementBlock("button", {
                key: 1,
                type: "default",
                onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args)),
                style: { "width": "64rpx", "height": "60rpx!important", "line-height": "60rpx!important" }
              }, [
                vue.createElementVNode(
                  "text",
                  {
                    class: "gui-color-black2 gui-button-text-mini gui-icons fb",
                    style: { "position": "relative", "left": "-4px" }
                  },
                  vue.toDisplayString($props.btntext || ""),
                  1
                  /* TEXT */
                )
              ])) : vue.createCommentVNode("v-if", true),
              $props.showcart ? (vue.openBlock(), vue.createElementBlock("view", {
                key: 2,
                class: "shopcartbox pr tp5",
                style: { "left": "-10px" },
                onClick: _cache[1] || (_cache[1] = (...args) => $options.toShopcart && $options.toShopcart(...args))
              }, [
                vue.createElementVNode("text", { class: "gui-icons fb fz22" }, ""),
                vue.createElementVNode("text", { class: "num" }, "114")
              ])) : vue.createCommentVNode("v-if", true)
            ],
            4
            /* STYLE */
          )) : vue.createCommentVNode("v-if", true)
        ],
        2
        /* CLASS */
      ),
      vue.createElementVNode("view", { style: { "margin-top": "18rpx" } }, [
        vue.createElementVNode(
          "text",
          {
            style: { "font-size": "24rpx" },
            class: vue.normalizeClass(["gui-color-gray", "gui-block", $props.center ? "gui-text-center" : ""])
          },
          [
            vue.renderSlot(_ctx.$slots, "default", {}, () => [
              vue.createTextVNode(
                vue.toDisplayString($props.desc),
                1
                /* TEXT */
              )
            ], true)
          ],
          2
          /* CLASS */
        )
      ])
    ]);
  }
  const __easycom_0$c = /* @__PURE__ */ _export_sfc(_sfc_main$16, [["render", _sfc_render$15], ["__scopeId", "data-v-918e1ca1"], ["__file", "D:/App/分销系统/cc_fenxiao/components/logo/logo.vue"]]);
  const _sfc_main$15 = {
    name: "gui-swiper",
    props: {
      width: { type: Number, default: 750 },
      height: { type: Number, default: 300 },
      swiperItems: {
        type: Array,
        default: function() {
          return new Array();
        }
      },
      borderRadius: { type: String, default: "10rpx" },
      indicatorBarHeight: { type: Number, default: 68 },
      indicatorBarBgClass: {
        type: Array,
        default: function() {
          return ["gui-bg-black-opacity5"];
        }
      },
      indicatorWidth: { type: Number, default: 18 },
      indicatorActiveWidth: { type: Number, default: 18 },
      indicatorHeight: { type: Number, default: 18 },
      indicatorRadius: { type: Number, default: 18 },
      indicatorClass: {
        type: Array,
        default: function() {
          return ["gui-bg-gray", "gui-dark-bg-level-5"];
        }
      },
      indicatorActiveClass: {
        type: Array,
        default: function() {
          return ["gui-bg-primary"];
        }
      },
      indicatorType: { type: String, default: "dot" },
      indicatorPosition: { type: String, default: "absolute" },
      indicatorDirection: { type: String, default: "center" },
      spacing: { type: Number, default: 50 },
      padding: { type: Number, default: 26 },
      interval: { type: Number, default: 5e3 },
      autoplay: { type: Boolean, default: true },
      currentIndex: { type: Number, default: 0 },
      opacity: { type: Number, default: 0.66 },
      titleColor: { type: String, default: "#FFFFFF" },
      titleSize: { type: String, default: "28rpx" },
      imgMode: { type: String, default: "aspectFill" }
    },
    data() {
      return {
        current: 0,
        isReady: false,
        widthIn: 750,
        heightIn: 300,
        widthInSamll: 700,
        heightInSmall: 280,
        paddingY: 0
      };
    },
    watch: {
      currentIndex: function(val) {
        this.current = val;
      }
    },
    created: function() {
      this.current = this.currentIndex;
      this.init();
    },
    methods: {
      init: function() {
        this.widthIn = this.width - this.spacing * 2;
        this.heightIn = this.height / this.width * this.widthIn;
        this.paddingY = this.padding * this.height / this.width;
        this.widthInSamll = this.widthIn - this.padding * 2;
        this.heightInSmall = this.heightIn - this.paddingY * 2;
      },
      swiperchange: function(e) {
        var current = e.detail.current;
        this.current = current;
        this.$emit("swiperchange", current);
      },
      taped: function(e) {
        this.$emit("taped", e.currentTarget.dataset.index);
      }
    },
    emits: ["swiperchange", "taped"]
  };
  function _sfc_render$14(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-swiper-card-wrap" }, [
      vue.createElementVNode("swiper", {
        style: vue.normalizeStyle({
          width: $props.width + "rpx",
          height: $data.heightIn + "rpx"
        }),
        class: "gui-swiper-card",
        "indicator-dots": false,
        interval: $props.interval,
        circular: true,
        autoplay: $props.autoplay,
        current: $props.currentIndex,
        "previous-margin": $props.spacing + "rpx",
        "next-margin": $props.spacing + "rpx",
        onChange: _cache[1] || (_cache[1] = (...args) => $options.swiperchange && $options.swiperchange(...args))
      }, [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($props.swiperItems, (item, index) => {
            return vue.openBlock(), vue.createElementBlock("swiper-item", {
              key: index,
              class: "gui-swiper-card-item gui-border-box"
            }, [
              item.opentype != "click" ? (vue.openBlock(), vue.createElementBlock("navigator", {
                key: 0,
                class: "gui-swiper-card-nav gui-transition-all",
                url: item.url,
                "open-type": item.opentype,
                "hover-class": "none",
                style: vue.normalizeStyle({
                  paddingLeft: $data.current != index ? $props.padding + "rpx" : "0rpx",
                  paddingRight: $data.current != index ? $props.padding + "rpx" : "0rpx",
                  paddingTop: $data.current != index ? $data.paddingY + "rpx" : "0rpx",
                  paddingBottom: $data.current != index ? $data.paddingY + "rpx" : "0rpx"
                })
              }, [
                vue.createElementVNode("image", {
                  style: vue.normalizeStyle({
                    borderRadius: $props.borderRadius,
                    width: $data.current != index ? $data.widthInSamll + "rpx" : $data.widthIn + "rpx",
                    height: $data.current != index ? $data.heightInSmall + "rpx" : $data.heightIn + "rpx",
                    opacity: $data.current != index ? $props.opacity : 1
                  }),
                  src: item.img,
                  mode: $props.imgMode,
                  class: "gui-swiper-card-image gui-transition-all"
                }, null, 12, ["src", "mode"])
              ], 12, ["url", "open-type"])) : vue.createCommentVNode("v-if", true),
              item.opentype == "click" ? (vue.openBlock(), vue.createElementBlock("view", {
                key: 1,
                class: "gui-swiper-card-nav gui-transition-all",
                "hover-class": "none",
                onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.taped && $options.taped(...args), ["stop"])),
                "data-index": index,
                style: vue.normalizeStyle({
                  paddingLeft: $data.current != index ? $props.padding + "rpx" : "0rpx",
                  paddingRight: $data.current != index ? $props.padding + "rpx" : "0rpx",
                  paddingTop: $data.current != index ? $data.paddingY + "rpx" : "0rpx",
                  paddingBottom: $data.current != index ? $data.paddingY + "rpx" : "0rpx"
                })
              }, [
                vue.createElementVNode("image", {
                  style: vue.normalizeStyle({
                    borderRadius: $props.borderRadius,
                    width: $data.current != index ? $data.widthInSamll + "rpx" : $data.widthIn + "rpx",
                    height: $data.current != index ? $data.heightInSmall + "rpx" : $data.heightIn + "rpx",
                    opacity: $data.current != index ? $props.opacity : 1
                  }),
                  src: item.img,
                  mode: $props.imgMode,
                  class: "gui-swiper-card-image gui-transition-all"
                }, null, 12, ["src", "mode"])
              ], 12, ["data-index"])) : vue.createCommentVNode("v-if", true),
              $props.indicatorType == "number" ? (vue.openBlock(), vue.createElementBlock(
                "view",
                {
                  key: 2,
                  class: vue.normalizeClass(["gui-indicator-dot-numbers gui-flex gui-row gui-nowrap", $props.indicatorBarBgClass]),
                  style: vue.normalizeStyle({
                    height: $props.indicatorBarHeight + "rpx",
                    "border-bottom-left-radius": $props.borderRadius,
                    "border-bottom-right-radius": $props.borderRadius,
                    width: $data.current != index ? $data.widthInSamll + "rpx" : $data.widthIn + "rpx",
                    left: $data.current != index ? $props.padding + "rpx" : "0rpx",
                    bottom: $data.current != index ? $data.paddingY + "rpx" : "0rpx"
                  })
                },
                [
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-indicator-dot-text",
                      style: vue.normalizeStyle({
                        paddingLeft: "20rpx",
                        "fontStyle": "italic",
                        color: $props.titleColor
                      })
                    },
                    vue.toDisplayString(index + 1),
                    5
                    /* TEXT, STYLE */
                  ),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-indicator-dot-text",
                      style: vue.normalizeStyle({
                        "fontSize": "36rpx",
                        color: $props.titleColor
                      })
                    },
                    "/",
                    4
                    /* STYLE */
                  ),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-indicator-dot-text",
                      style: vue.normalizeStyle({
                        fontSize: "28rpx",
                        paddingRight: "20rpx",
                        fontStyle: "italic",
                        color: $props.titleColor
                      })
                    },
                    vue.toDisplayString($props.swiperItems.length),
                    5
                    /* TEXT, STYLE */
                  ),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-swiper-text gui-block-text gui-flex1 gui-ellipsis",
                      style: vue.normalizeStyle({
                        color: $props.titleColor,
                        fontSize: $props.titleSize,
                        height: $props.indicatorBarHeight + "rpx",
                        lineHeight: $props.indicatorBarHeight + "rpx"
                      })
                    },
                    vue.toDisplayString(item.title),
                    5
                    /* TEXT, STYLE */
                  )
                ],
                6
                /* CLASS, STYLE */
              )) : vue.createCommentVNode("v-if", true)
            ]);
          }),
          128
          /* KEYED_FRAGMENT */
        ))
      ], 44, ["interval", "autoplay", "current", "previous-margin", "next-margin"]),
      vue.createCommentVNode("  进度圆点  "),
      $props.indicatorType == "dot" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          class: "gui-indicator-dots gui-flex gui-row gui-nowrap gui-align-items-center gui-border-box",
          style: vue.normalizeStyle({
            width: $props.width + "rpx",
            height: $props.indicatorBarHeight + "rpx",
            position: $props.indicatorPosition,
            paddingLeft: $props.spacing + "rpx",
            paddingRight: $props.spacing + "rpx",
            "justify-content": $props.indicatorDirection
          })
        },
        [
          vue.createElementVNode("view", { class: "gui-indicator-dots-wrap gui-flex gui-row gui-nowrap gui-justify-content-center" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList($props.swiperItems, (item, index) => {
                return vue.openBlock(), vue.createElementBlock(
                  "view",
                  {
                    key: index,
                    class: vue.normalizeClass([
                      "gui-indicator-dot",
                      $data.current == index ? "dot-show" : "",
                      $data.current == index ? $props.indicatorActiveClass : $props.indicatorClass
                    ]),
                    style: vue.normalizeStyle({
                      width: $data.current != index ? $props.indicatorWidth + "rpx" : $props.indicatorActiveWidth + "rpx",
                      height: $props.indicatorHeight + "rpx",
                      borderRadius: $props.indicatorRadius + "rpx"
                    })
                  },
                  null,
                  6
                  /* CLASS, STYLE */
                );
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ])
        ],
        4
        /* STYLE */
      )) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_0$b = /* @__PURE__ */ _export_sfc(_sfc_main$15, [["render", _sfc_render$14], ["__scopeId", "data-v-97684a3b"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-swiper.vue"]]);
  var img = "https://images.unsplash.com/photo-1661956602868-6ae368943878?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHw2MHx8fGVufDB8fHx8&auto=format&fit=crop&w=600&q=90";
  const _sfc_main$14 = {
    name: "page-demo",
    props: {
      height: {
        type: Number,
        default: 230
      },
      showspace: {
        type: Boolean,
        default: true
      }
    },
    data() {
      return {
        isLoad: false,
        swiperItems: [
          {
            img,
            url: "",
            title: "测试标题 001",
            opentype: "navigate"
          },
          {
            img,
            url: "",
            title: "测试标题 002",
            opentype: "navigate"
          },
          {
            img,
            url: "",
            title: "测试标题很长很长很长很长很长很长很长很长很长很长很长很长很长很长",
            opentype: "navigate"
          }
        ]
      };
    },
    methods: {
      swiperchange: function(e) {
        formatAppLog("log", "at components/advert/index.vue:60", e);
      },
      taped: function(e) {
        uni.showToast({
          title: "您点击了第 " + e + " 个项目",
          icon: "none"
        });
      }
    },
    created() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    }
  };
  function _sfc_render$13(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_swiper = resolveEasycom(vue.resolveDynamicComponent("gui-swiper"), __easycom_0$b);
    return vue.openBlock(), vue.createElementBlock("view", null, [
      !$data.isLoad ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          class: "demo-banner demo-bg gui-dark-bg-level-3 gui-border-radius",
          style: vue.normalizeStyle({ "height": $props.height + "rpx" })
        },
        null,
        4
        /* STYLE */
      )) : vue.createCommentVNode("v-if", true),
      $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", { key: 1 }, [
        vue.createVNode(_component_gui_swiper, {
          swiperItems: $data.swiperItems,
          imgMode: "scaleToFill",
          width: 750,
          spacing: 0,
          borderRadius: "0",
          height: $props.height
        }, null, 8, ["swiperItems", "height"])
      ])) : vue.createCommentVNode("v-if", true),
      $props.showspace ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 2,
        class: "demo-space"
      })) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_3$5 = /* @__PURE__ */ _export_sfc(_sfc_main$14, [["render", _sfc_render$13], ["__file", "D:/App/分销系统/cc_fenxiao/components/advert/index.vue"]]);
  const _sfc_main$13 = {
    name: "gui-image",
    props: {
      src: { type: String, default: "" },
      width: { type: Number, default: 300 },
      height: { type: Number, default: 0 },
      timer: { type: Number, default: 200 },
      borderRadius: { type: String, default: "0rpx" },
      mode: { type: String, default: "widthFix" }
    },
    data() {
      return {
        isLoading: true,
        imgHeight: 180,
        opacity: 0,
        animate: false,
        failed: false
      };
    },
    methods: {
      imgLoad: function(e) {
        var scale = e.detail.width / e.detail.height;
        if (this.mode == "widthFix") {
          this.imgHeight = this.width / scale;
        } else {
          this.imgHeight = this.height;
        }
        this.animate = true;
        setTimeout(() => {
          this.isLoading = false;
          this.opacity = 1;
        }, this.timer);
      },
      error: function() {
        this.isLoading = false;
        this.failed = true;
      }
    }
  };
  function _sfc_render$12(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "gui-img gui-bg-white gui-dark-bg-level-3",
        style: vue.normalizeStyle({
          width: $props.width + "rpx",
          height: $props.height == 0 ? $data.imgHeight + "rpx" : $props.height + "rpx",
          borderRadius: $props.borderRadius
        })
      },
      [
        vue.createElementVNode("image", {
          src: $props.src,
          onLoad: _cache[0] || (_cache[0] = (...args) => $options.imgLoad && $options.imgLoad(...args)),
          onError: _cache[1] || (_cache[1] = (...args) => $options.error && $options.error(...args)),
          mode: $props.mode,
          style: vue.normalizeStyle({
            width: $props.width + "rpx",
            height: $data.imgHeight + "rpx",
            borderRadius: $props.borderRadius,
            opacity: $data.opacity
          })
        }, null, 44, ["src", "mode"]),
        $data.isLoading ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 0,
            class: vue.normalizeClass(["gui-img-loading gui-icons gui-color-gray", [$data.animate ? "gui-fade-out" : ""]]),
            style: vue.normalizeStyle({
              width: $props.width + "rpx",
              height: $props.height == 0 ? $data.imgHeight + "rpx" : $props.height + "rpx",
              lineHeight: $props.height == 0 ? $data.imgHeight + "rpx" : $props.height + "rpx",
              borderRadius: $props.borderRadius
            })
          },
          "",
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $data.failed ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 1,
            class: vue.normalizeClass(["gui-img-loading gui-icons gui-color-gray", [$data.animate ? "gui-fade-out" : ""]]),
            style: vue.normalizeStyle({
              width: $props.width + "rpx",
              height: $props.height == 0 ? $data.imgHeight + "rpx" : $props.height + "rpx",
              lineHeight: $props.height == 0 ? $data.imgHeight + "rpx" : $props.height + "rpx",
              borderRadius: $props.borderRadius
            })
          },
          "",
          6
          /* CLASS, STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_0$a = /* @__PURE__ */ _export_sfc(_sfc_main$13, [["render", _sfc_render$12], ["__scopeId", "data-v-2a3c977b"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-image.vue"]]);
  var hexcase = 0;
  function hex_md5(s) {
    return rstr2hex(rstr_md5(str2rstr_utf8(s)));
  }
  function rstr_md5(s) {
    return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
  }
  function rstr2hex(input) {
    try {
      hexcase;
    } catch (e) {
      hexcase = 0;
    }
    var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
    var output = "";
    var x;
    for (var i = 0; i < input.length; i++) {
      x = input.charCodeAt(i);
      output += hex_tab.charAt(x >>> 4 & 15) + hex_tab.charAt(x & 15);
    }
    return output;
  }
  function str2rstr_utf8(input) {
    var output = "";
    var i = -1;
    var x, y;
    while (++i < input.length) {
      x = input.charCodeAt(i);
      y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
      if (55296 <= x && x <= 56319 && 56320 <= y && y <= 57343) {
        x = 65536 + ((x & 1023) << 10) + (y & 1023);
        i++;
      }
      if (x <= 127)
        output += String.fromCharCode(x);
      else if (x <= 2047)
        output += String.fromCharCode(
          192 | x >>> 6 & 31,
          128 | x & 63
        );
      else if (x <= 65535)
        output += String.fromCharCode(
          224 | x >>> 12 & 15,
          128 | x >>> 6 & 63,
          128 | x & 63
        );
      else if (x <= 2097151)
        output += String.fromCharCode(
          240 | x >>> 18 & 7,
          128 | x >>> 12 & 63,
          128 | x >>> 6 & 63,
          128 | x & 63
        );
    }
    return output;
  }
  function rstr2binl(input) {
    var output = Array(input.length >> 2);
    for (var i = 0; i < output.length; i++)
      output[i] = 0;
    for (var i = 0; i < input.length * 8; i += 8)
      output[i >> 5] |= (input.charCodeAt(i / 8) & 255) << i % 32;
    return output;
  }
  function binl2rstr(input) {
    var output = "";
    for (var i = 0; i < input.length * 32; i += 8)
      output += String.fromCharCode(input[i >> 5] >>> i % 32 & 255);
    return output;
  }
  function binl_md5(x, len) {
    x[len >> 5] |= 128 << len % 32;
    x[(len + 64 >>> 9 << 4) + 14] = len;
    var a = 1732584193;
    var b = -271733879;
    var c = -1732584194;
    var d = 271733878;
    for (var i = 0; i < x.length; i += 16) {
      var olda = a;
      var oldb = b;
      var oldc = c;
      var oldd = d;
      a = md5_ff(a, b, c, d, x[i + 0], 7, -680876936);
      d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
      c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
      b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
      a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
      d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
      c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
      b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
      a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
      d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
      c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
      b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
      a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
      d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
      c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
      b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);
      a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
      d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
      c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
      b = md5_gg(b, c, d, a, x[i + 0], 20, -373897302);
      a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
      d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
      c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
      b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
      a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
      d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
      c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
      b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
      a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
      d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
      c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
      b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);
      a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
      d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
      c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
      b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
      a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
      d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
      c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
      b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
      a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
      d = md5_hh(d, a, b, c, x[i + 0], 11, -358537222);
      c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
      b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
      a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
      d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
      c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
      b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);
      a = md5_ii(a, b, c, d, x[i + 0], 6, -198630844);
      d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
      c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
      b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
      a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
      d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
      c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
      b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
      a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
      d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
      c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
      b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
      a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
      d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
      c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
      b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);
      a = safe_add(a, olda);
      b = safe_add(b, oldb);
      c = safe_add(c, oldc);
      d = safe_add(d, oldd);
    }
    return Array(a, b, c, d);
  }
  function md5_cmn(q, a, b, x, s, t) {
    return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
  }
  function md5_ff(a, b, c, d, x, s, t) {
    return md5_cmn(b & c | ~b & d, a, b, x, s, t);
  }
  function md5_gg(a, b, c, d, x, s, t) {
    return md5_cmn(b & d | c & ~d, a, b, x, s, t);
  }
  function md5_hh(a, b, c, d, x, s, t) {
    return md5_cmn(b ^ c ^ d, a, b, x, s, t);
  }
  function md5_ii(a, b, c, d, x, s, t) {
    return md5_cmn(c ^ (b | ~d), a, b, x, s, t);
  }
  function safe_add(x, y) {
    var lsw = (x & 65535) + (y & 65535);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return msw << 16 | lsw & 65535;
  }
  function bit_rol(num, cnt) {
    return num << cnt | num >>> 32 - cnt;
  }
  const md5 = {
    md5: function(str) {
      return hex_md5(str);
    }
  };
  const graceJS = {
    // 版本检查
    verson: function() {
      var currentVersion = "6.0";
      formatAppLog("log", "at Grace6/js/grace.js:6", currentVersion);
      return currentVersion;
    },
    // --- 页面跳转相关 ---
    // 页面跳转
    navigate: function(url, type, success, fail, complete) {
      if (!type) {
        type = "navigateTo";
      }
      if (!success) {
        success = function() {
        };
      }
      if (!fail) {
        fail = function() {
        };
      }
      if (!complete) {
        complete = function() {
        };
      }
      switch (type) {
        case "navigateTo":
          uni.navigateTo({
            url,
            success,
            fail,
            complete
          });
          break;
        case "redirectTo":
          uni.redirectTo({
            url,
            success,
            fail,
            complete
          });
          break;
        case "switchTab":
          uni.switchTab({
            url,
            success,
            fail,
            complete
          });
          break;
        case "reLaunch":
          uni.reLaunch({
            url,
            success,
            fail,
            complete
          });
          break;
      }
    },
    // 返回
    back: function(delta) {
      if (!delta) {
        delta = 1;
      }
      uni.navigateBack({
        delta
      });
    },
    // --- 网络请求 ---
    // get
    get: function(url, data, headers, success, fail) {
      if (!fail) {
        fail = () => {
          this.msg("网络请求失败");
        };
      }
      if (!headers) {
        headers = {};
      }
      if (this.__before != null) {
        this.__before();
        this.__before = null;
      }
      uni.request({
        url,
        data,
        method: "GET",
        dataType: "json",
        header: headers,
        success: (res) => {
          success(res.data);
        },
        fail,
        complete: () => {
          if (this.__after != null) {
            this.__after();
            this.__after = null;
          }
        }
      });
    },
    // post
    post: function(url, data, contentType, headers, success, fail) {
      if (!fail) {
        fail = () => {
          this.msg("网络请求失败");
        };
      }
      if (!headers) {
        headers = {};
      }
      if (!contentType) {
        contentType = "form";
      }
      if (this.__before != null) {
        this.__before();
        this.__before = null;
      }
      switch (contentType) {
        case "form":
          headers["content-type"] = "application/x-www-form-urlencoded";
          break;
        case "json":
          headers["content-type"] = "application/json";
          break;
        default:
          headers["content-type"] = "application/x-www-form-urlencoded";
      }
      uni.request({
        url,
        data,
        method: "POST",
        dataType: "json",
        header: headers,
        success: (res) => {
          success(res.data);
        },
        fail,
        complete: () => {
          if (this.__after != null) {
            this.__after();
            this.__after = null;
          }
        }
      });
    },
    // 请求前置函数
    __before: null,
    setBefore: function(func) {
      this.__before = func;
    },
    // 请求后置函数
    __after: null,
    setAfter: function(func) {
      this.__after = func;
    },
    // --- 数据缓存 ---
    setStorage: function(data) {
      try {
        for (let k in data) {
          uni.setStorageSync(k, data[k] + "");
        }
        return true;
      } catch (e) {
        return false;
      }
    },
    getStorage: function(keyName) {
      try {
        var tmpVal = uni.getStorageSync(keyName);
        if (tmpVal == "") {
          return false;
        }
        return tmpVal;
      } catch (e) {
        return false;
      }
    },
    removeStorage: function(keyName) {
      try {
        uni.removeStorageSync(keyName);
        return true;
      } catch (e) {
        return false;
      }
    },
    clearStorage: function() {
      try {
        uni.clearStorageSync();
      } catch (e) {
      }
    },
    // --- 图片相关 ---
    chooseImgs: function(sets, success, fail, complete) {
      if (!sets.count) {
        sets.count = 1;
      }
      if (!sets.sizeType) {
        sets.sizeType = ["original", "compressed"];
      }
      if (!sets.sourceType) {
        sets.sourceType = ["album", "camera"];
      }
      uni.chooseImage({
        count: sets.count,
        //默认9
        sizeType: sets.sizeType,
        //可以指定是原图还是压缩图，默认二者都有
        sourceType: sets.sourceType,
        //从相册选择
        success: (res) => {
          success(res.tempFilePaths);
        },
        fail: (e) => {
          if (fail) {
            fail(e);
          }
        },
        complete: (e) => {
          if (complete) {
            complete(e);
          }
        }
      });
    },
    getImageInfo: function(imgUrl, success, fail, complete) {
      uni.getImageInfo({
        src: imgUrl,
        success: function(info) {
          success(info);
        },
        fail: (e) => {
          if (fail) {
            fail(e);
          }
        },
        complete: (e) => {
          if (complete) {
            complete(e);
          }
        }
      });
    },
    previewImage: function(items, currentImg) {
      uni.previewImage({
        urls: items,
        current: currentImg
      });
    },
    // --- 系统信息 ---
    system: function() {
      try {
        var res = uni.getSystemInfoSync();
        var iPhoneXBottom = 0;
        if (!res.model) {
          res.model = "no";
        }
        res.model = res.model.replace(" ", "");
        res.model = res.model.toLowerCase();
        var res1 = res.model.indexOf("iphonex");
        if (res1 > 5) {
          res1 = -1;
        }
        var res2 = res.model.indexOf("iphone1");
        if (res2 > 5) {
          res2 = -1;
        }
        if (res1 != -1 || res2 != -1) {
          res.iPhoneXBottomHeightRpx = 60;
          res.iPhoneXBottomHeightPx = uni.upx2px(60);
        } else {
          res.iPhoneXBottomHeightRpx = 0;
          res.iPhoneXBottomHeightPx = 0;
        }
        return res;
      } catch (e) {
        return null;
      }
    },
    // --- 消息弹框 ---
    msg: function(msg) {
      uni.showToast({
        title: msg,
        icon: "none"
      });
    },
    showLoading: function(title) {
      uni.showLoading({
        title,
        mask: true
      });
    },
    // --- 导航条设置 ---
    setNavBar: function(sets) {
      if (sets.title) {
        uni.setNavigationBarTitle({
          title: sets.title
        });
      }
      if (sets.color) {
        uni.setNavigationBarColor({
          frontColor: sets.color.frontColor,
          backgroundColor: sets.color.backgroundColor,
          animation: {
            duration: 400,
            timingFunc: "easeIn"
          }
        });
      }
      if (sets.loading) {
        uni.showNavigationBarLoading();
      } else {
        uni.hideNavigationBarLoading();
      }
    },
    // --- 元素选择 ---
    // 单个元素选择
    select: function(selector, callBack) {
      uni.createSelectorQuery().select(selector).boundingClientRect().exec((res) => {
        callBack(res[0]);
      });
    },
    // 多个元素获取
    selectAll: function(selector, callBack) {
      uni.createSelectorQuery().selectAll(selector).boundingClientRect().exec((res) => {
        callBack(res[0]);
      });
    },
    // --- 数组操作 ---
    // 数组合并
    arrayConcat: function() {
      var tmpArr = [];
      for (let i = 0; i < arguments.length; i++) {
        tmpArr = tmpArr.concat(arguments[i]);
      }
      return tmpArr;
    },
    arrayDrop: function(array, index, howmany) {
      if (!index) {
        index = 0;
      }
      if (!howmany) {
        howmany = 1;
      }
      array.splice(index, howmany);
      return array;
    },
    arrayIndexOf: function(arr, needFind) {
      var index = -1;
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] == needFind) {
          index = i;
          return i;
        }
      }
      return index;
    },
    arrayDifference: function(a, b) {
      const set = new Set(b);
      return a.filter((x) => !set.has(x));
    },
    arrayShuffle: function(arr) {
      let l = arr.length;
      while (l) {
        const i = Math.floor(Math.random() * l--);
        [arr[l], arr[i]] = [arr[i], arr[l]];
      }
      return arr;
    },
    arraySum: function(arr) {
      return arr.reduce((acc, val) => acc + val, 0);
    },
    arrayAvg: function(arr) {
      return arr.reduce((acc, val) => acc + val, 0) / arr.length;
    },
    arrayEach: function(arr, fun) {
      for (let i = 0; i < arr.length; i++) {
        fun(arr[i], i);
      }
    },
    // 2数之间的随机数
    random: function(min, max) {
      switch (arguments.length) {
        case 1:
          return parseInt(Math.random() * min + 1, 10);
        case 2:
          return parseInt(Math.random() * (max - min + 1) + min, 10);
        default:
          return 0;
      }
    },
    // UUID
    uuid: function(len) {
      var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
      var uuid = [], i;
      if (len) {
        for (i = 0; i < len; i++) {
          uuid[i] = chars[0 | Math.random() * chars.length];
        }
      } else {
        var r;
        uuid[8] = uuid[13] = uuid[18] = uuid[23] = "-";
        uuid[14] = "4";
        for (i = 0; i < 36; i++) {
          if (!uuid[i]) {
            r = 0 | Math.random() * 16;
            uuid[i] = chars[i == 19 ? r & 3 | 8 : r];
          }
        }
      }
      return uuid.join("");
    },
    // --- 日期时间 ---
    now: function(type, addTime) {
      var dateObj = /* @__PURE__ */ new Date();
      var cTime = dateObj.getTime();
      if (addTime) {
        cTime += addTime;
      }
      if (!type) {
        type = "number";
      }
      if (type == "number") {
        return cTime;
      } else if (type == "str") {
        return this.toDate(cTime / 1e3, "str");
      } else if (type == "array") {
        return this.toDate(cTime / 1e3, "array");
      }
    },
    // 时间戳转 YY-mm-dd HH:ii:ss
    toDate: function(timeStamp, returnType) {
      timeStamp = parseInt(timeStamp);
      var date = /* @__PURE__ */ new Date();
      if (timeStamp < 9e10) {
        date.setTime(timeStamp * 1e3);
      } else {
        date.setTime(timeStamp);
      }
      var y = date.getFullYear();
      var m = date.getMonth() + 1;
      m = m < 10 ? "0" + m : m;
      var d = date.getDate();
      d = d < 10 ? "0" + d : d;
      var h = date.getHours();
      h = h < 10 ? "0" + h : h;
      var minute = date.getMinutes();
      var second = date.getSeconds();
      minute = minute < 10 ? "0" + minute : minute;
      second = second < 10 ? "0" + second : second;
      if (returnType == "str") {
        return y + "-" + m + "-" + d + " " + h + ":" + minute + ":" + second;
      }
      return [y, m, d, h, minute, second];
    },
    // 字符串转时间戳
    toTimeStamp: function(timeStamp) {
      var reg = /^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
      var res = timeStamp.match(reg);
      if (res == null) {
        var reg2 = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
        var res2 = timeStamp.match(reg2);
        if (res2 == null) {
          formatAppLog("log", "at Grace6/js/grace.js:473", "时间格式错误 E001");
          return false;
        }
        var year = parseInt(res2[3]);
        var month = parseInt(res2[1]);
        var day = parseInt(res2[2]);
        var h = parseInt(res2[4]);
        var i = parseInt(res2[5]);
        var s = parseInt(res2[6]);
      } else {
        var year = parseInt(res[1]);
        var month = parseInt(res[2]);
        var day = parseInt(res[3]);
        var h = parseInt(res[4]);
        var i = parseInt(res[5]);
        var s = parseInt(res[6]);
      }
      if (year < 1e3) {
        formatAppLog("log", "at Grace6/js/grace.js:491", "时间格式错误");
        return false;
      }
      if (h < 0 || h > 24) {
        formatAppLog("log", "at Grace6/js/grace.js:495", "时间格式错误");
        return false;
      }
      if (i < 0 || i > 60) {
        formatAppLog("log", "at Grace6/js/grace.js:499", "时间格式错误");
        return false;
      }
      if (s < 0 || s > 60) {
        formatAppLog("log", "at Grace6/js/grace.js:503", "时间格式错误");
        return false;
      }
      return Date.parse(new Date(year, month - 1, day, h, i, s));
    },
    // 根据时间戳计算多少分钟/小时/天之前
    fromTime: function(time) {
      if (time < 9e10) {
        time *= 1e3;
      }
      var timer = (/* @__PURE__ */ new Date()).getTime() - time;
      timer = parseInt(timer / 1e3);
      if (timer < 180) {
        return "刚刚";
      } else if (timer >= 180 && timer < 3600) {
        return parseInt(timer / 60) + "分钟前";
      } else if (timer >= 3600 && timer < 86400) {
        return parseInt(timer / 3600) + "小时前";
      } else if (timer >= 86400 && timer < 2592e3) {
        return parseInt(timer / 86400) + "天前";
      } else {
        return this.toDate(time, "str");
      }
    },
    // 延迟操作
    delay: function(timer, func) {
      return setTimeout(func, timer);
    },
    // 间隔指定时间循环某个函数
    interval: function(timer, func) {
      return setInterval(func, timer);
    },
    // 对象操作
    assign: function(obj, key, val) {
      obj[key] = val;
    },
    removeByKey: function(obj, key) {
      delete obj[key];
    },
    each: function(obj, func) {
      for (let k in obj) {
        func(k, obj[k]);
      }
    },
    isEmptyObj: function(obj) {
      return JSON.stringify(obj) === "{}";
    },
    // 获取ref ( 循环获取，直到 组件创建完成并获取成功 )
    getRefs: function(ref, _this, count, fun) {
      if (count >= 50) {
        fun(_this.$refs[ref]);
        return false;
      }
      var refReturn = _this.$refs[ref];
      if (refReturn) {
        fun(refReturn);
      } else {
        count++;
        setTimeout(() => {
          this.getRefs(ref, _this, count, fun);
        }, 100);
      }
    },
    // md5
    md5: function(str) {
      return md5.md5(str);
    }
  };
  const _sfc_main$12 = {
    data() {
      return {
        isLoad: false,
        listItems: [
          {
            id: 1,
            page: 2,
            icon: "/static/list/book.png",
            url: "/pages/category/course",
            title: "精品课程"
          },
          {
            id: 2,
            page: 1,
            icon: "/static/list/deng.png",
            url: "/pages/category/note",
            title: "实战小册"
          },
          {
            id: 3,
            page: 2,
            icon: "/static/list/cp.png",
            url: "/pages/category/downloads",
            title: "资源下载"
          },
          {
            id: 4,
            page: 1,
            icon: "/static/list/ted.png",
            url: "/pages/category/downloads",
            title: "下载规则"
          },
          {
            id: 5,
            page: 2,
            icon: "/static/list/xueba.png",
            url: "/pages/category/rule",
            title: "推广规则"
          }
        ]
      };
    },
    created() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    },
    methods: {
      handleClick(item) {
        graceJS.navigate(`${item.url}?id=${item.id}&title=${item.title}&page=${item.page}`, "navigateTo");
      }
    }
  };
  function _sfc_render$11(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_image = resolveEasycom(vue.resolveDynamicComponent("gui-image"), __easycom_0$a);
    return vue.openBlock(), vue.createElementBlock("view", null, [
      !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "gui-flex gui-row gui-nowrap gui-space-between gui-padding",
        style: { "padding-bottom": "5px" }
      }, [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($data.listItems, (item, index) => {
            return vue.openBlock(), vue.createElementBlock("view", {
              key: index,
              class: "demo-grid demo-bg gui-dark-bg-level-3 gui-border-radius"
            });
          }),
          128
          /* KEYED_FRAGMENT */
        ))
      ])) : vue.createCommentVNode("v-if", true),
      $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 1,
        class: "gui-flex gui-row gui-nowrap gui-space-between gui-padding",
        style: { "padding-bottom": "5px" }
      }, [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($data.listItems, (item, index) => {
            return vue.openBlock(), vue.createElementBlock("view", {
              key: index,
              onClick: ($event) => $options.handleClick(item)
            }, [
              vue.createVNode(_component_gui_image, {
                src: item.icon,
                borderRadius: "10rpx",
                class: "gui-bg-white",
                width: 96,
                height: 96
              }, null, 8, ["src"]),
              vue.createElementVNode(
                "view",
                { class: "fz12 mt5" },
                vue.toDisplayString(item.title),
                1
                /* TEXT */
              )
            ], 8, ["onClick"]);
          }),
          128
          /* KEYED_FRAGMENT */
        ))
      ])) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode("view", { class: "demo-space" })
    ]);
  }
  const __easycom_2$8 = /* @__PURE__ */ _export_sfc(_sfc_main$12, [["render", _sfc_render$11], ["__file", "D:/App/分销系统/cc_fenxiao/components/category/list.vue"]]);
  const _sfc_main$11 = {
    name: "UniSection",
    props: {
      type: {
        type: String,
        default: ""
      },
      title: {
        type: String,
        default: ""
      },
      subTitle: {
        type: String,
        default: ""
      }
    },
    data() {
      return {};
    },
    watch: {
      title(newVal) {
        if (uni.report && newVal !== "") {
          uni.report("title", newVal);
        }
      }
    },
    methods: {
      onClick() {
        this.$emit("click");
      }
    }
  };
  function _sfc_render$10(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", {
      class: "uni-section",
      nvue: ""
    }, [
      $props.type ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "uni-section__head"
      }, [
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass([$props.type, "uni-section__head-tag"])
          },
          null,
          2
          /* CLASS */
        )
      ])) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode("view", { class: "uni-section__content" }, [
        vue.createElementVNode(
          "text",
          {
            class: vue.normalizeClass([{ "distraction": !$props.subTitle }, "uni-section__content-title"])
          },
          vue.toDisplayString($props.title),
          3
          /* TEXT, CLASS */
        ),
        $props.subTitle ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 0,
            class: "uni-section__content-sub"
          },
          vue.toDisplayString($props.subTitle),
          1
          /* TEXT */
        )) : vue.createCommentVNode("v-if", true)
      ]),
      vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
    ]);
  }
  const __easycom_1$c = /* @__PURE__ */ _export_sfc(_sfc_main$11, [["render", _sfc_render$10], ["__scopeId", "data-v-2f279ad5"], ["__file", "D:/App/分销系统/cc_fenxiao/components/uni-section/uni-section.vue"]]);
  const icons = {
    "pulldown": "",
    "refreshempty": "",
    "back": "",
    "forward": "",
    "more": "",
    "more-filled": "",
    "scan": "",
    "qq": "",
    "weibo": "",
    "weixin": "",
    "pengyouquan": "",
    "loop": "",
    "refresh": "",
    "refresh-filled": "",
    "arrowthindown": "",
    "arrowthinleft": "",
    "arrowthinright": "",
    "arrowthinup": "",
    "undo-filled": "",
    "undo": "",
    "redo": "",
    "redo-filled": "",
    "bars": "",
    "chatboxes": "",
    "camera": "",
    "chatboxes-filled": "",
    "camera-filled": "",
    "cart-filled": "",
    "cart": "",
    "checkbox-filled": "",
    "checkbox": "",
    "arrowleft": "",
    "arrowdown": "",
    "arrowright": "",
    "smallcircle-filled": "",
    "arrowup": "",
    "circle": "",
    "eye-filled": "",
    "eye-slash-filled": "",
    "eye-slash": "",
    "eye": "",
    "flag-filled": "",
    "flag": "",
    "gear-filled": "",
    "reload": "",
    "gear": "",
    "hand-thumbsdown-filled": "",
    "hand-thumbsdown": "",
    "hand-thumbsup-filled": "",
    "heart-filled": "",
    "hand-thumbsup": "",
    "heart": "",
    "home": "",
    "info": "",
    "home-filled": "",
    "info-filled": "",
    "circle-filled": "",
    "chat-filled": "",
    "chat": "",
    "mail-open-filled": "",
    "email-filled": "",
    "mail-open": "",
    "email": "",
    "checkmarkempty": "",
    "list": "",
    "locked-filled": "",
    "locked": "",
    "map-filled": "",
    "map-pin": "",
    "map-pin-ellipse": "",
    "map": "",
    "minus-filled": "",
    "mic-filled": "",
    "minus": "",
    "micoff": "",
    "mic": "",
    "clear": "",
    "smallcircle": "",
    "close": "",
    "closeempty": "",
    "paperclip": "",
    "paperplane": "",
    "paperplane-filled": "",
    "person-filled": "",
    "contact-filled": "",
    "person": "",
    "contact": "",
    "images-filled": "",
    "phone": "",
    "images": "",
    "image": "",
    "image-filled": "",
    "location-filled": "",
    "location": "",
    "plus-filled": "",
    "plus": "",
    "plusempty": "",
    "help-filled": "",
    "help": "",
    "navigate-filled": "",
    "navigate": "",
    "mic-slash-filled": "",
    "search": "",
    "settings": "",
    "sound": "",
    "sound-filled": "",
    "spinner-cycle": "",
    "download-filled": "",
    "personadd-filled": "",
    "videocam-filled": "",
    "personadd": "",
    "upload": "",
    "upload-filled": "",
    "starhalf": "",
    "star-filled": "",
    "star": "",
    "trash": "",
    "phone-filled": "",
    "compose": "",
    "videocam": "",
    "trash-filled": "",
    "download": "",
    "chatbubble-filled": "",
    "chatbubble": "",
    "cloud-download": "",
    "cloud-upload-filled": "",
    "cloud-upload": "",
    "cloud-download-filled": "",
    "headphones": "",
    "shop": ""
  };
  const _sfc_main$10 = {
    name: "UniIcons",
    props: {
      type: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#333333"
      },
      size: {
        type: [Number, String],
        default: 16
      },
      customIcons: {
        type: String,
        default: ""
      }
    },
    data() {
      return {
        icons
      };
    },
    methods: {
      _onClick() {
        this.$emit("click");
      }
    }
  };
  function _sfc_render$$(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "text",
      {
        style: vue.normalizeStyle({ color: $props.color, "font-size": $props.size + "px" }),
        class: vue.normalizeClass(["uni-icons", [$props.customIcons, $props.customIcons ? $props.type : ""]]),
        onClick: _cache[0] || (_cache[0] = (...args) => $options._onClick && $options._onClick(...args))
      },
      vue.toDisplayString($data.icons[$props.type]),
      7
      /* TEXT, CLASS, STYLE */
    );
  }
  const __easycom_0$9 = /* @__PURE__ */ _export_sfc(_sfc_main$10, [["render", _sfc_render$$], ["__scopeId", "data-v-d31e1c47"], ["__file", "D:/App/分销系统/cc_fenxiao/uni_modules/uni-icons/components/uni-icons/uni-icons.vue"]]);
  const _sfc_main$$ = {
    name: "UniBadge",
    props: {
      type: {
        type: String,
        default: "default"
      },
      inverted: {
        type: Boolean,
        default: false
      },
      isDot: {
        type: Boolean,
        default: false
      },
      maxNum: {
        type: Number,
        default: 99
      },
      absolute: {
        type: String,
        default: ""
      },
      offset: {
        type: Array,
        default() {
          return [0, 0];
        }
      },
      text: {
        type: [String, Number],
        default: ""
      },
      size: {
        type: String,
        default: "normal"
      },
      customStyle: {
        type: Object,
        default() {
          return {};
        }
      }
    },
    data() {
      return {};
    },
    computed: {
      width() {
        return String(this.text).length * 8 + 12;
      },
      classNames() {
        const {
          inverted,
          type,
          size,
          absolute
        } = this;
        return [
          inverted ? "uni-badge--" + type + "-inverted" : "",
          "uni-badge--" + type,
          "uni-badge--" + size,
          absolute ? "uni-badge--absolute" : ""
        ];
      },
      positionStyle() {
        if (!this.absolute)
          return {};
        let w = this.width / 2, h = 10;
        if (this.isDot) {
          w = 5;
          h = 5;
        }
        const x = `${-w + this.offset[0]}px`;
        const y = `${-h + this.offset[1]}px`;
        const whiteList = {
          rightTop: {
            right: x,
            top: y
          },
          rightBottom: {
            right: x,
            bottom: y
          },
          leftBottom: {
            left: x,
            bottom: y
          },
          leftTop: {
            left: x,
            top: y
          }
        };
        const match = whiteList[this.absolute];
        return match ? match : whiteList["rightTop"];
      },
      badgeWidth() {
        return {
          width: `${this.width}px`
        };
      },
      dotStyle() {
        if (!this.isDot)
          return {};
        return {
          width: "10px",
          height: "10px",
          borderRadius: "10px"
        };
      },
      displayValue() {
        const { isDot, text, maxNum } = this;
        return isDot ? "" : Number(text) > maxNum ? `${maxNum}+` : text;
      }
    },
    methods: {
      onClick() {
        this.$emit("click");
      }
    }
  };
  function _sfc_render$_(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "uni-badge--x" }, [
      vue.renderSlot(_ctx.$slots, "default", {}, void 0, true),
      $props.text ? (vue.openBlock(), vue.createElementBlock(
        "text",
        {
          key: 0,
          class: vue.normalizeClass([$options.classNames, "uni-badge"]),
          style: vue.normalizeStyle([$options.badgeWidth, $options.positionStyle, $props.customStyle, $options.dotStyle]),
          onClick: _cache[0] || (_cache[0] = ($event) => $options.onClick())
        },
        vue.toDisplayString($options.displayValue),
        7
        /* TEXT, CLASS, STYLE */
      )) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_1$b = /* @__PURE__ */ _export_sfc(_sfc_main$$, [["render", _sfc_render$_], ["__scopeId", "data-v-c97cb896"], ["__file", "D:/App/分销系统/cc_fenxiao/uni_modules/uni-badge/components/uni-badge/uni-badge.vue"]]);
  const _sfc_main$_ = {
    name: "UniListItem",
    props: {
      direction: {
        type: String,
        default: "row"
      },
      title: {
        type: String,
        default: ""
      },
      note: {
        type: String,
        default: ""
      },
      ellipsis: {
        type: [Number],
        default: 0
      },
      disabled: {
        type: [Boolean, String],
        default: false
      },
      clickable: {
        type: Boolean,
        default: false
      },
      showArrow: {
        type: [Boolean, String],
        default: false
      },
      link: {
        type: [Boolean, String],
        default: false
      },
      to: {
        type: String,
        default: ""
      },
      showBadge: {
        type: [Boolean, String],
        default: false
      },
      showSwitch: {
        type: [Boolean, String],
        default: false
      },
      switchChecked: {
        type: [Boolean, String],
        default: false
      },
      badgeText: {
        type: String,
        default: ""
      },
      badgeType: {
        type: String,
        default: "success"
      },
      rightText: {
        type: String,
        default: ""
      },
      thumb: {
        type: String,
        default: ""
      },
      thumbSize: {
        type: String,
        default: "base"
      },
      showExtraIcon: {
        type: [Boolean, String],
        default: false
      },
      extraIcon: {
        type: Object,
        default() {
          return {
            type: "contact",
            color: "#000000",
            size: 20
          };
        }
      },
      border: {
        type: Boolean,
        default: true
      }
    },
    // inject: ['list'],
    data() {
      return {
        isFirstChild: false
      };
    },
    mounted() {
      this.list = this.getForm();
      if (this.list) {
        if (!this.list.firstChildAppend) {
          this.list.firstChildAppend = true;
          this.isFirstChild = true;
        }
      }
    },
    methods: {
      /**
       * 获取父元素实例
       */
      getForm(name = "uniList") {
        let parent = this.$parent;
        let parentName = parent.$options.name;
        while (parentName !== name) {
          parent = parent.$parent;
          if (!parent)
            return false;
          parentName = parent.$options.name;
        }
        return parent;
      },
      onClick() {
        if (this.to !== "") {
          this.openPage();
          return;
        }
        if (this.clickable || this.link) {
          this.$emit("click", {
            data: {}
          });
        }
      },
      onSwitchChange(e) {
        this.$emit("switchChange", e.detail);
      },
      openPage() {
        if (["navigateTo", "redirectTo", "reLaunch", "switchTab"].indexOf(this.link) !== -1) {
          this.pageApi(this.link);
        } else {
          this.pageApi("navigateTo");
        }
      },
      pageApi(api) {
        uni[api]({
          url: this.to,
          success: (res) => {
            this.$emit("click", {
              data: res
            });
          },
          fail: (err) => {
            this.$emit("click", {
              data: err
            });
            formatAppLog("error", "at uni_modules/uni-list/components/uni-list-item/uni-list-item.vue:228", err.errMsg);
          }
        });
      }
    }
  };
  function _sfc_render$Z(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_uni_icons = resolveEasycom(vue.resolveDynamicComponent("uni-icons"), __easycom_0$9);
    const _component_uni_badge = resolveEasycom(vue.resolveDynamicComponent("uni-badge"), __easycom_1$b);
    return vue.openBlock(), vue.createElementBlock("view", {
      class: vue.normalizeClass([{ "uni-list-item--disabled": $props.disabled }, "uni-list-item"]),
      "hover-class": !$props.clickable && !$props.link || $props.disabled || $props.showSwitch ? "" : "uni-list-item--hover",
      onClick: _cache[1] || (_cache[1] = vue.withModifiers((...args) => $options.onClick && $options.onClick(...args), ["stop"]))
    }, [
      !$data.isFirstChild ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          class: vue.normalizeClass(["border--left", { "uni-list--border": $props.border }])
        },
        null,
        2
        /* CLASS */
      )) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["uni-list-item__container", { "container--right": $props.showArrow || $props.link, "flex--direction": $props.direction === "column" }])
        },
        [
          vue.renderSlot(_ctx.$slots, "header", {}, () => [
            vue.createElementVNode("view", { class: "uni-list-item__header" }, [
              $props.thumb ? (vue.openBlock(), vue.createElementBlock("view", {
                key: 0,
                class: "uni-list-item__icon"
              }, [
                vue.createElementVNode("image", {
                  src: $props.thumb,
                  class: vue.normalizeClass(["uni-list-item__icon-img", ["uni-list--" + $props.thumbSize]])
                }, null, 10, ["src"])
              ])) : $props.showExtraIcon ? (vue.openBlock(), vue.createElementBlock("view", {
                key: 1,
                class: "uni-list-item__icon"
              }, [
                vue.createVNode(_component_uni_icons, {
                  color: $props.extraIcon.color,
                  size: $props.extraIcon.size,
                  type: $props.extraIcon.type
                }, null, 8, ["color", "size", "type"])
              ])) : vue.createCommentVNode("v-if", true)
            ])
          ], true),
          vue.renderSlot(_ctx.$slots, "body", {}, () => [
            vue.createElementVNode(
              "view",
              {
                class: vue.normalizeClass(["uni-list-item__content", { "uni-list-item__content--center": $props.thumb || $props.showExtraIcon || $props.showBadge || $props.showSwitch }])
              },
              [
                $props.title ? (vue.openBlock(), vue.createElementBlock(
                  "text",
                  {
                    key: 0,
                    class: vue.normalizeClass(["uni-list-item__content-title", [$props.ellipsis !== 0 && $props.ellipsis <= 2 ? "uni-ellipsis-" + $props.ellipsis : ""]])
                  },
                  vue.toDisplayString($props.title),
                  3
                  /* TEXT, CLASS */
                )) : vue.createCommentVNode("v-if", true),
                $props.note ? (vue.openBlock(), vue.createElementBlock(
                  "text",
                  {
                    key: 1,
                    class: "uni-list-item__content-note"
                  },
                  vue.toDisplayString($props.note),
                  1
                  /* TEXT */
                )) : vue.createCommentVNode("v-if", true)
              ],
              2
              /* CLASS */
            )
          ], true),
          vue.renderSlot(_ctx.$slots, "footer", {}, () => [
            $props.rightText || $props.showBadge || $props.showSwitch ? (vue.openBlock(), vue.createElementBlock(
              "view",
              {
                key: 0,
                class: vue.normalizeClass(["uni-list-item__extra", { "flex--justify": $props.direction === "column" }])
              },
              [
                $props.rightText ? (vue.openBlock(), vue.createElementBlock(
                  "text",
                  {
                    key: 0,
                    class: "uni-list-item__extra-text"
                  },
                  vue.toDisplayString($props.rightText),
                  1
                  /* TEXT */
                )) : vue.createCommentVNode("v-if", true),
                $props.showBadge ? (vue.openBlock(), vue.createBlock(_component_uni_badge, {
                  key: 1,
                  type: $props.badgeType,
                  text: $props.badgeText
                }, null, 8, ["type", "text"])) : vue.createCommentVNode("v-if", true),
                $props.showSwitch ? (vue.openBlock(), vue.createElementBlock("switch", {
                  key: 2,
                  disabled: $props.disabled,
                  checked: $props.switchChecked,
                  onChange: _cache[0] || (_cache[0] = (...args) => $options.onSwitchChange && $options.onSwitchChange(...args))
                }, null, 40, ["disabled", "checked"])) : vue.createCommentVNode("v-if", true)
              ],
              2
              /* CLASS */
            )) : vue.createCommentVNode("v-if", true)
          ], true)
        ],
        2
        /* CLASS */
      ),
      $props.showArrow || $props.link ? (vue.openBlock(), vue.createBlock(_component_uni_icons, {
        key: 1,
        size: 16,
        class: "uni-icon-wrapper",
        color: "#bbb",
        type: "arrowright"
      })) : vue.createCommentVNode("v-if", true)
    ], 10, ["hover-class"]);
  }
  const __easycom_0$8 = /* @__PURE__ */ _export_sfc(_sfc_main$_, [["render", _sfc_render$Z], ["__scopeId", "data-v-c7524739"], ["__file", "D:/App/分销系统/cc_fenxiao/uni_modules/uni-list/components/uni-list-item/uni-list-item.vue"]]);
  const _sfc_main$Z = {
    name: "uniList",
    "mp-weixin": {
      options: {
        multipleSlots: false
      }
    },
    props: {
      enableBackToTop: {
        type: [Boolean, String],
        default: false
      },
      scrollY: {
        type: [Boolean, String],
        default: false
      },
      border: {
        type: Boolean,
        default: true
      }
    },
    // provide() {
    // 	return {
    // 		list: this
    // 	};
    // },
    created() {
      this.firstChildAppend = false;
    },
    methods: {
      loadMore(e) {
        this.$emit("scrolltolower");
      }
    }
  };
  function _sfc_render$Y(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "uni-list uni-border-top-bottom" }, [
      $props.border ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "uni-list--border-top"
      })) : vue.createCommentVNode("v-if", true),
      vue.renderSlot(_ctx.$slots, "default", {}, void 0, true),
      $props.border ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 1,
        class: "uni-list--border-bottom"
      })) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_1$a = /* @__PURE__ */ _export_sfc(_sfc_main$Z, [["render", _sfc_render$Y], ["__scopeId", "data-v-c2f1266a"], ["__file", "D:/App/分销系统/cc_fenxiao/uni_modules/uni-list/components/uni-list/uni-list.vue"]]);
  let platform;
  setTimeout(() => {
    platform = uni.getSystemInfoSync().platform;
  }, 16);
  const _sfc_main$Y = {
    name: "UniLoadMore",
    props: {
      status: {
        // 上拉的状态：more-loading前；loading-loading中；noMore-没有更多了
        type: String,
        default: "more"
      },
      showIcon: {
        type: Boolean,
        default: true
      },
      iconType: {
        type: String,
        default: "auto"
      },
      iconSize: {
        type: Number,
        default: 24
      },
      color: {
        type: String,
        default: "#777777"
      },
      contentText: {
        type: Object,
        default() {
          return {
            contentdown: "上拉显示更多",
            contentrefresh: "正在加载...",
            contentnomore: "没有更多数据了"
          };
        }
      }
    },
    data() {
      return {
        webviewHide: false,
        platform
      };
    },
    computed: {
      iconSnowWidth() {
        return (Math.floor(this.iconSize / 24) || 1) * 2;
      }
    },
    mounted() {
      var pages = getCurrentPages();
      var page = pages[pages.length - 1];
      var currentWebview = page.$getAppWebview();
      currentWebview.addEventListener("hide", () => {
        this.webviewHide = true;
      });
      currentWebview.addEventListener("show", () => {
        this.webviewHide = false;
      });
    },
    methods: {
      onClick() {
        this.$emit("clickLoadMore", {
          detail: {
            status: this.status
          }
        });
      }
    }
  };
  function _sfc_render$X(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", {
      class: "uni-load-more",
      onClick: _cache[0] || (_cache[0] = (...args) => $options.onClick && $options.onClick(...args))
    }, [
      !$data.webviewHide && ($props.iconType === "circle" || $props.iconType === "auto" && $data.platform === "android") && $props.status === "loading" && $props.showIcon ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          style: vue.normalizeStyle({ width: $props.iconSize + "px", height: $props.iconSize + "px" }),
          class: "uni-load-more__img uni-load-more__img--android-MP"
        },
        [
          vue.createElementVNode(
            "view",
            {
              style: vue.normalizeStyle({ borderTopColor: $props.color, borderTopWidth: $props.iconSize / 12 })
            },
            null,
            4
            /* STYLE */
          ),
          vue.createElementVNode(
            "view",
            {
              style: vue.normalizeStyle({ borderTopColor: $props.color, borderTopWidth: $props.iconSize / 12 })
            },
            null,
            4
            /* STYLE */
          ),
          vue.createElementVNode(
            "view",
            {
              style: vue.normalizeStyle({ borderTopColor: $props.color, borderTopWidth: $props.iconSize / 12 })
            },
            null,
            4
            /* STYLE */
          )
        ],
        4
        /* STYLE */
      )) : !$data.webviewHide && $props.status === "loading" && $props.showIcon ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 1,
          style: vue.normalizeStyle({ width: $props.iconSize + "px", height: $props.iconSize + "px" }),
          class: "uni-load-more__img uni-load-more__img--ios-H5"
        },
        [
          vue.createElementVNode("image", {
            src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QzlBMzU3OTlEOUM0MTFFOUI0NTZDNERBQURBQzI4RkUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QzlBMzU3OUFEOUM0MTFFOUI0NTZDNERBQURBQzI4RkUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDOUEzNTc5N0Q5QzQxMUU5QjQ1NkM0REFBREFDMjhGRSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDOUEzNTc5OEQ5QzQxMUU5QjQ1NkM0REFBREFDMjhGRSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pt+ALSwAAA6CSURBVHja1FsLkFZVHb98LM+F5bHL8khA1iSeiyQBCRM+YGqKUnnJTDLGI0BGZlKDIU2MMglUiDApEZvSsZnQtBRJtKwQNKQMFYeRDR10WOLd8ljYXdh+v8v5fR3Od+797t1dnOnO/Ofce77z+J//+b/P+ZqtXbs2sJ9MJhNUV1cHJ06cCJo3bx7EPc2aNcvpy7pWrVoF+/fvDyoqKoI2bdoE9fX1F7TjN8a+EXBn/fkfvw942Tf+wYMHg9mzZwfjxo0LDhw4EPa1x2MbFw/fOGfPng1qa2tzcCkILsLDydq2bRsunpOTMM7TD/W/tZDZhPdeKD+yGxHhdu3aBV27dg3OnDlzMVANMheLAO3btw8KCwuDmpoaX5OxbgUIMEq7K8IcPnw4KCsrC/r37x8cP378/4cAXAB3vqSkJMuiDhTkw+XcuXNhOWbMmKBly5YhUT8xArhyFvP0BfwRsAuwxJZJsm/nzp2DTp06he/OU+cZ64K6o0ePBkOHDg2GDx8e6gEbJ5Q/NHNuAJQ1hgBeHUDlR7nVTkY8rQAvAi4z34vR/mPs1FoRsaCgIJThI0eOBC1atEiFGGV+5MiRoS45efJkqFjJFXV1dQuA012m2WcwTw98fy6CqBdsaiIO4CScrGPHjvk4odhavPquRtFWXEC25VgkREKOCh/qDSq+vn37htzD/mZTOmOc5U7zKzBPEedygWshcDyWvs30igAbU+6oyMgJBCFhwQE0fccxN60Ay9iebbjoDh06hMowjQxT4fXq1SskArmHZpkArvixp/kWzHdMeArExSJEaiXIjjRjRJ4DaAGWpibLzXN3Fm1vA5teBgh3j1Rv3bp1YgKwPdmf2p9zcyNYYgPKMfY0T5f5nNYdw158nJ8QawW4CLKwiOBSEgO/hok2eBydR+3dYH+PLxA5J8Vv0KBBwenTp0P2JWAx6+yFEBfs8lMY+y0SWMBNI9E4ThKi58VKTg3FQZS1RQF1cz27eC0QHMu+3E0SkUowjhVt5VdaWhp07949ZHv2Qd1EjDXM2cla1M0nl3GxAs3J9yREzyTdFVKVFOaE9qRA8GM0WebRuo9JGZKA7Mv2SeS/Z8+eoQ9BArMfFrLGo6jvxbhHbJZnKX2Rzz1O7QhJJ9Cs2ZMaWIyq/zhdeqPNfIoHd58clIQD+JSXl4dKlyIAuBdVXZwFVWKspSSoxE++h8x4k3uCnEhE4I5KwRiFWGOU0QWKiCYLbdoRMRKAu2kQ9vkfLU6dOhX06NEjlH+yMRZSinnuyWnYosVcji8CEA/6Cg2JF+IIUBqnGKUTCNwtwBN4f89RiK1R96DEgO2o0NDmtEdvVFdVVYV+P3UAPUEs6GFwV3PHmXkD4vh74iDFJysVI/MlaQhwKeBNTLYX5VuA8T4/gZxA4MRGFxDB6R7OmYPfyykGRJbyie+XnGYnQIC/coH9+vULiYrxrkL9ZA9+0ykaHIfEpM7ge8TiJ2CsHYwyMfafAF1yCGBHYIbCVDjDjKt7BeB51D+LgQa6OkG7IDYEEtvQ7lnXLKLtLdLuJBpE4gPUXcW2+PkZwOex+4cGDhwYDBkyRL7/HFcEwUGPo/8uWRUpYnfxGHco8HkewLHLyYmAawAPuIFZxhOpDfJQ8gbUv41yORAptMWBNr6oqMhWird5+u+iHmBb2nhjDV7HWBNQTgK8y11l5NetWzc5ULscAtSj7nbNI0skhWeUZCc0W4nyH/jO4Vz0u1IeYhbk4AiwM6tjxIWByHsoZ9qcIBPJd/y+DwPfBESOmCa/QF3WiZHucLlEDpNxcNhmheEOPgdQNx6/VZFQzFZ5TN08AHXQt2Ii3EdyFuUsPtTcGPhW5iMiCNELvz+Gdn9huG4HUJaW/w3g0wxV0XaG7arG2WeKiUWYM4Y7GO5ezshTARbbWGw/DvXkpp/ivVvE0JVoMxN4rpGzJMhE5Pl+xlATsDIqikP9F9D2z3h9nOksEUFhK+qO4rcPkoalMQ/HqJLIyb3F3JdjrCcw1yZ8joyJLR5gCo54etlag7qIoeNh1N1BRYj3DTFJ0elotxPlVzkGuYAmL0VSJVGAJA41c4Z6A3BzTLfn0HYwYKEI6CUAMzZEWvLsIcQOo1AmmyyM72nHJCfYsogflGV6jEk9vyQZXSuq6w4c16NsGcGZbwOPr+H1RkOk2LEzjNepxQkihHSCQ4ynAYNRx2zMKV92CQMWqj8J0BRE8EShxRFN6YrfCRhC0x3r/Zm4IbQCcmJoV0kMamllccR6FjHqUC5F2R/wS2dcymOlfAKOS4KmzQb5cpNC2MC7JhVn5wjXoJ44rYhLh8n0eXOCorJxa7POjbSlCGVczr34/RsAmrcvo9s+wGp3tzVhntxiXiJ4nvEYb4FJkf0O8HocAePmLvCxnL0AORraVekJk6TYjDabRVXfRE2lCN1h6ZQRN1+InUbsCpKwoBZHh0dODN9JBCUffItXxEavTQkUtnfTVAplCWL3JISz29h4NjotnuSsQKJCk8dF+kJR6RARjrqFVmfPnj3ZbK8cIJ0msd6jgHPGtfVTQ8VLmlvh4mct9sobRmPic0DyDQQnx/NlfYUgyz59+oScsH379pAwXABD32nTpoUHIToESeI5mnbE/UqDdyLcafEBf2MCqgC7NwxIbMREJQ0g4D4sfJwnD+AmRrII05cfMWJE+L1169bQr+fip06dGp4oJ83lmYd5wj/EmMa4TaHivo4EeCguYZBnkB5g2aWA69OIEnUHOaGysjIYMGBAMGnSpODYsWPZwCpFmm4lNq+4gSLQA7jcX8DwtjEyRC8wjabnXEx9kfWnTJkSJkAo90xpJVV+FmcVNeYAF5zWngS4C4O91MBxmAv8blLEpbjI5sz9MTdAhcgkCT1RO8mZkAjfiYpTEvStAS53Uw1vAiUGgZ3GpuQEYvoiBqlIan7kSDHnTwJQFNiPu0+5VxCVYhcZIjNrdXUDdp+Eq5AZ3Gkg8QAyVZRZIk4Tl4QAbF9cXJxNYZMAtAokgs4BrNxEpCtteXg7DDTMDKYNSuQdKsnJBek7HxewvxaosWxLYXtw+cJp18217wql4aKCfBNoEu0O5VU+PhctJ0YeXD4C6JQpyrlpSLTojpGGGN5YwNziChdIZLk4lvLcFJ9jMX3QdiImY9bmGQU+TRUL5CHITTRlgF8D9ouD1MfmLoEPl5xokIumZ2cfgMpHt47IW9N64Hsh7wQYYjyIugWuF5fCqYncXRd5vPMWyizzvhi/32+nvG0dZc9vR6fZOu0md5e+uC408FvKSIOZwXlGvxPv95izA2Vtvg1xKFWARI+vMX66HUhpQQb643uW1bSjuTWyw2SBvDrBvjFic1eGGlz5esq3ko9uSIlBRqPuFcCv8F4WIcN12nVaBd0SaYwI6PDDImR11JkqgHcPmQssjxIn6bUshygDFJUTxPMpHk+jfjPgupgdnYV2R/g7xSjtpah8RJBewhwf0gGK6XI92u4wXFEU40afJ4DN4h5LcAd+40HI3JgJecuT0c062W0i2hQJUTcxan3/CMW1PF2K6bbA+Daz4xRs1D3Br1Cm0OihKCqizW78/nXAF/G5TXrEcVzaNMH6CyMswqsAHqDyDLEyou8lwOXnKF8DjI6KjV3KzMBiXkDH8ij/H214J5A596ekrZ3F0zXlWeL7+P5eUrNo3/QwC15uxthuzidy7DzKRwEDaAViiDgKbTbz7CJnzo0bN7pIfIiid8SuPwn25o3QCmpnyjlZkyxPP8EomCJzrGb7GJMx7tNsq4MT2xMUYaiErZOluTzKsnz3gwCeCZyVRZJfYplNEokEjwrPtxlxjeYAk+F1F74VAzPxQRNYYdtpOUvWs8J1sGhBJMNsb7igN8plJs1eSmLIhLKE4rvaCX27gOhLpLOsIzJ7qn/i+wZzcvSOZ23/du8TZjwV8zHIXoP4R3ifBxiFz1dcVpa3aPntPE+c6TmIWE9EtcMmAcPdWAhYhAXxcLOQi9L1WhD1Sc8p1d2oL7XGiRKp8F4A2i8K/nfI+y/gsTDJ/YC/8+AD5Uh04KHiGl+cIFPnBDDrPMjwRGkLXyxO4VGbfQWnDH2v0bVWE3C9QOXlepbgjEfIJQI6XDG3z5ahD9cw2pS78ipB85wyScNTvsVzlzzhL8/jRrnmVjfFJK/m3m4nj9vbgQTguT8XZTjsm672R5uJKEaQmBI/c58gyus8ZDagLpEVSJBIyHp4jn++xqPV71OgQgJYEWOtZ/haxRtKmWOBu8xdBLftWltsY84zE6WIEy/eIOWL+BaayMx+KHtL7EAkqdNDLiEXmEMUHniedtJqg9HmZtfvt26vNi0BdG3Ft3g8ZOf7PAu59TxtzivLNIekyi+wD1i8CuUiD9FXAa8C+/xS3JPmZnomyc7H+fb4/Se0bk41Fel621r4cgVxbq91V4jVqwB7HTe2M7jgB+QWHavZkDRPmZcASoZEmBx6i75bGjPcMdL4/VKGFAGWZkGzPG0XAbdL9A81G5LOmUnC9hHKJeO7dcUMjblSl12867ElFTtaGl20xvvLGPdVz/8TVuU7y0x1PG7vtNg24oz9Uo/Z412++VFWI7Fcog9tu9Lm6gvRmIPv9x1xmQAu6RDkXtbOtlGEmpgD5Nvnyc0dcv0EE6cfdi1HmhMf9wDF3k3gtRvEedhxjpgfqPb9PU9iEJHnyOUA7bQUXh6kq/D7l2iTjWv7XOD530BDr8jIrus+srXjt4MzumJMHuTsBa63YKE1+RR5lBjEikCCnWKWiHdzOgKO+nRIBAF88za/IFmJ3eMZov4CYxGBabcpGL8EYx+SeMXJeRwHNsV/h+vdxeuhEpN3ZyNY78Gm2fknJxVGhyjixPiQvVkNzT1elD9Py/aTAL64Hb9vcYmC9zfdXdT/C1LeGbg4rnBaAihDFJH12W5ulfNCNe/xTsP3bp8ikzJs5BF+5PNfAQYAPaseTdsEcaYAAAAASUVORK5CYII=",
            mode: "widthFix"
          })
        ],
        4
        /* STYLE */
      )) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode(
        "text",
        {
          class: "uni-load-more__text",
          style: vue.normalizeStyle({ color: $props.color })
        },
        vue.toDisplayString($props.status === "more" ? $props.contentText.contentdown : $props.status === "loading" ? $props.contentText.contentrefresh : $props.contentText.contentnomore),
        5
        /* TEXT, STYLE */
      )
    ]);
  }
  const __easycom_2$7 = /* @__PURE__ */ _export_sfc(_sfc_main$Y, [["render", _sfc_render$X], ["__scopeId", "data-v-9245e42c"], ["__file", "D:/App/分销系统/cc_fenxiao/uni_modules/uni-load-more/components/uni-load-more/uni-load-more.vue"]]);
  const _sfc_main$X = {
    components: {},
    data() {
      return {
        isLoad: false,
        loading: false,
        // 数据表名
        collection: "opendb-mall-goods",
        // 查询字段，多个字段用 , 分割
        field: "goods_thumb,name,goods_tip,tag,goods_price,comment_count,month_sell_count,shop_name",
        productList: [{
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/imgs/bg.jpg",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }],
        formData: {
          waterfall: false,
          // 布局方向切换
          status: "noMore"
          // 加载状态
        },
        tipShow: false
        // 是否显示顶部提示框
      };
    },
    methods: {
      load(data, ended) {
        if (ended) {
          this.formData.status = "noMore";
        }
      }
    },
    created() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    },
    /**
     * 下拉刷新回调函数
     */
    onPullDownRefresh() {
      this.formData.status = "more";
      this.$refs.udb.loadData({
        clear: true
      }, () => {
        this.tipShow = true;
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          this.tipShow = false;
        }, 1e3);
        uni.stopPullDownRefresh();
      });
    },
    /**
     * 上拉加载回调函数
     */
    onReachBottom() {
      this.$refs.udb.loadMore();
    }
  };
  function _sfc_render$W(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_image = resolveEasycom(vue.resolveDynamicComponent("gui-image"), __easycom_0$a);
    const _component_uni_section = resolveEasycom(vue.resolveDynamicComponent("uni-section"), __easycom_1$c);
    const _component_uni_list_item = resolveEasycom(vue.resolveDynamicComponent("uni-list-item"), __easycom_0$8);
    const _component_uni_list = resolveEasycom(vue.resolveDynamicComponent("uni-list"), __easycom_1$a);
    const _component_uni_load_more = resolveEasycom(vue.resolveDynamicComponent("uni-load-more"), __easycom_2$7);
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.createCommentVNode("\r\n	本页面模板教程：https://ext.dcloud.net.cn/plugin?id=2651\r\n	uni-list 文档：https://ext.dcloud.net.cn/plugin?id=24\r\n	uniCloud 文档：https://uniapp.dcloud.io/uniCloud/README\r\n	unicloud-db 组件文档：https://uniapp.dcloud.io/uniCloud/unicloud-db\r\n	DB Schema 规范：https://uniapp.dcloud.net.cn/uniCloud/schema\r\n	 "),
        vue.createElementVNode("view", { class: "list" }, [
          vue.createCommentVNode(" 刷新页面后的顶部提示框 "),
          vue.createCommentVNode(" 当前弹出内容没有实际逻辑 ，可根据当前业务修改弹出提示 "),
          $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
            "view",
            {
              key: 0,
              class: vue.normalizeClass(["tips", { "tips-ani": $data.tipShow }])
            },
            "为您更新了10条最新新闻动态",
            2
            /* CLASS */
          )) : vue.createCommentVNode("v-if", true),
          !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 1,
            class: "tips-ani demo-bg gui-dark-bg-level-3 gui-border-radius"
          })) : vue.createCommentVNode("v-if", true),
          vue.createCommentVNode(" 页面分类标题 "),
          $data.isLoad ? (vue.openBlock(), vue.createBlock(_component_uni_section, {
            key: 2,
            title: "精选内容",
            type: "line"
          }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "button-box" }, [
                vue.createVNode(_component_gui_image, {
                  class: "gui-image-more",
                  src: "/static/list/more.png",
                  width: 36,
                  height: 36
                })
              ])
            ]),
            _: 1
            /* STABLE */
          })) : vue.createCommentVNode("v-if", true),
          vue.createCommentVNode(" 基于 uni-list 的页面布局 "),
          vue.createVNode(_component_uni_list, {
            class: vue.normalizeClass({ "uni-list--waterfall": $data.formData.waterfall })
          }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "uni-list" }, [
                vue.createCommentVNode(" 通过 uni-list--waterfall 类决定页面布局方向 "),
                vue.createCommentVNode(" to 属性携带参数跳转详情页面，当前只为参考 "),
                (vue.openBlock(true), vue.createElementBlock(
                  vue.Fragment,
                  null,
                  vue.renderList($data.productList, (item) => {
                    return vue.openBlock(), vue.createBlock(_component_uni_list_item, {
                      border: !$data.formData.waterfall,
                      class: "uni-list-item--waterfall",
                      title: "自定义商品列表",
                      key: item._id,
                      to: "/pages/course/detail?id=" + item._id + "&title=" + item.name
                    }, {
                      header: vue.withCtx(() => [
                        !$data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            key: 0,
                            class: vue.normalizeClass(["uni-thumb demo-bg shop-picture", { "shop-picture-column": $data.formData.waterfall }])
                          },
                          null,
                          2
                          /* CLASS */
                        )) : vue.createCommentVNode("v-if", true),
                        $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            key: 1,
                            class: vue.normalizeClass(["uni-thumb shop-picture", { "shop-picture-column": $data.formData.waterfall }])
                          },
                          [
                            vue.createElementVNode("image", {
                              src: item.goods_thumb,
                              mode: "scaleToFill"
                            }, null, 8, ["src"])
                          ],
                          2
                          /* CLASS */
                        )) : vue.createCommentVNode("v-if", true)
                      ]),
                      body: vue.withCtx(() => [
                        vue.createElementVNode("view", { class: "shop" }, [
                          vue.createElementVNode("view", null, [
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "uni-title demo-bg demo-title"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 1,
                              class: "uni-title"
                            }, [
                              vue.createElementVNode(
                                "text",
                                { class: "uni-ellipsis-2" },
                                vue.toDisplayString(item.name),
                                1
                                /* TEXT */
                              )
                            ])) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                              "text",
                              {
                                key: 2,
                                class: "uni-tag hot-tag"
                              },
                              vue.toDisplayString(item.goods_tip),
                              1
                              /* TEXT */
                            )) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(true), vue.createElementBlock(
                              vue.Fragment,
                              { key: 3 },
                              vue.renderList(item.tag, (tag) => {
                                return vue.openBlock(), vue.createElementBlock(
                                  "text",
                                  {
                                    key: tag,
                                    class: "uni-tag"
                                  },
                                  vue.toDisplayString(tag),
                                  1
                                  /* TEXT */
                                );
                              }),
                              128
                              /* KEYED_FRAGMENT */
                            )) : vue.createCommentVNode("v-if", true)
                          ]),
                          vue.createElementVNode("view", { class: "mt5" }, [
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "uni-note demo-bg demo-tag"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 1,
                              class: "shop-price gui-flex gui-align-items-center gui-space-between"
                            }, [
                              vue.createElementVNode("view", { class: "fb" }, [
                                vue.createElementVNode("text", null, "¥"),
                                vue.createElementVNode(
                                  "text",
                                  { class: "shop-price-text" },
                                  vue.toDisplayString(item.goods_price),
                                  1
                                  /* TEXT */
                                ),
                                vue.createElementVNode("text", null, ".00")
                              ]),
                              vue.createElementVNode("view", { class: "fb" }, [
                                vue.createElementVNode("view", { class: "sharebox" }, "赚￥2323.4")
                              ])
                            ])) : vue.createCommentVNode("v-if", true),
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 2,
                              class: "uni-note demo-bg demo-space"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                              "view",
                              {
                                key: 3,
                                class: "uni-note"
                              },
                              vue.toDisplayString(item.comment_count) + "人购买 / 月销量 " + vue.toDisplayString(item.month_sell_count) + " +",
                              1
                              /* TEXT */
                            )) : vue.createCommentVNode("v-if", true),
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 4,
                              class: "uni-note demo-bg demo-space"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 5,
                              class: "uni-note ellipsis"
                            }, [
                              vue.createElementVNode(
                                "text",
                                { class: "uni-ellipsis-1" },
                                vue.toDisplayString(item.shop_name),
                                1
                                /* TEXT */
                              )
                            ])) : vue.createCommentVNode("v-if", true)
                          ])
                        ])
                      ]),
                      _: 2
                      /* DYNAMIC */
                    }, 1032, ["border", "to"]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ])
            ]),
            _: 1
            /* STABLE */
          }, 8, ["class"]),
          vue.createCommentVNode(" 通过 loadMore 组件实现上拉加载效果，如需自定义显示内容，可参考：https://ext.dcloud.net.cn/plugin?id=29 "),
          $data.isLoad && ($data.loading || $data.formData.status === "noMore") ? (vue.openBlock(), vue.createBlock(_component_uni_load_more, {
            key: 3,
            status: $data.formData.status
          }, null, 8, ["status"])) : vue.createCommentVNode("v-if", true)
        ])
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  const __easycom_3$4 = /* @__PURE__ */ _export_sfc(_sfc_main$X, [["render", _sfc_render$W], ["__scopeId", "data-v-29d1d86b"], ["__file", "D:/App/分销系统/cc_fenxiao/components/course/index.vue"]]);
  const _sfc_main$W = {
    name: "gui-refresh",
    props: {
      refreshText: { type: Array, default: function() {
        return ["继续下拉刷新", "松开手指开始刷新", "数据刷新中", "数据已刷新"];
      } },
      customClass: { type: Array, default: function() {
        return [
          ["gui-color-gray"],
          ["gui-color-gray"],
          ["gui-primary-text"],
          ["gui-color-green"]
        ];
      } },
      refreshFontSize: { type: Number, default: 28 },
      triggerHeight: { type: Number, default: 50 }
    },
    data() {
      return {
        reScrollTop: 0,
        refreshHeight: 0,
        refreshY: 0,
        refreshStatus: 0,
        refreshTimer: 0
      };
    },
    methods: {
      touchstart: function(e) {
        if (this.reScrollTop > 10) {
          return;
        }
        this.refreshY = e.changedTouches[0].pageY;
      },
      touchmove: function(e) {
        if (this.refreshStatus >= 1) {
          return null;
        }
        if (this.reScrollTop > 10) {
          return;
        }
        var moveY = e.changedTouches[0].pageY - this.refreshY;
        moveY = moveY / 2;
        if (moveY >= this.triggerHeight) {
          moveY = this.triggerHeight;
          this.refreshStatus = 1;
        }
        if (moveY > 15) {
          this.refreshHeight = moveY;
        }
      },
      touchend: function(e) {
        if (this.reScrollTop > 10) {
          return;
        }
        if (this.refreshStatus < 1) {
          return this.resetFresh();
        } else if (this.refreshStatus == 1) {
          this.refreshStatus = 2;
          this.$emit("reload");
        }
      },
      scroll: function(e) {
        this.reScrollTop = e.detail.scrollTop;
      },
      endReload: function() {
        this.refreshStatus = 3;
        setTimeout(() => {
          this.resetFresh();
        }, 1e3);
      },
      resetFresh: function() {
        this.refreshHeight = 0;
        this.refreshStatus = 0;
        return null;
      },
      rotate360: function() {
        var el = this.$refs.loadingIcon;
        animation.transition(el, {
          styles: {
            transform: "rotate(7200deg)",
            transformOrigin: "center"
          },
          duration: 2e4,
          timingFunction: "linear",
          needLayout: false,
          delay: 0
        });
      }
    },
    emits: ["reload"]
  };
  function _sfc_render$V(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "gui-page-refresh gui-flex gui-column gui-justify-content-center",
        style: vue.normalizeStyle({ height: $data.refreshHeight + "px" })
      },
      [
        vue.createElementVNode(
          "view",
          {
            style: vue.normalizeStyle({ height: $props.refreshFontSize + "rpx" }),
            class: "gui-flex gui-row gui-justify-content-center gui-align-items-center"
          },
          [
            $data.refreshStatus == 0 || $data.refreshStatus == 1 ? (vue.openBlock(), vue.createElementBlock(
              "text",
              {
                key: 0,
                class: vue.normalizeClass(["gui-icons gui-block", $props.customClass[$data.refreshStatus]]),
                style: vue.normalizeStyle({
                  fontSize: $props.refreshFontSize + "rpx",
                  width: $props.refreshFontSize + "rpx",
                  height: $props.refreshFontSize + "rpx",
                  lineHeight: $props.refreshFontSize + "rpx"
                })
              },
              "",
              6
              /* CLASS, STYLE */
            )) : vue.createCommentVNode("v-if", true),
            $data.refreshStatus == 2 ? (vue.openBlock(), vue.createElementBlock(
              "text",
              {
                key: 1,
                ref: "loadingIcon",
                class: vue.normalizeClass(["gui-icons gui-block gui-text-center gui-rotate360", $props.customClass[$data.refreshStatus]]),
                style: vue.normalizeStyle({
                  fontSize: $props.refreshFontSize + "rpx",
                  width: $props.refreshFontSize + "rpx",
                  height: $props.refreshFontSize + "rpx",
                  lineHeight: $props.refreshFontSize + "rpx"
                })
              },
              "",
              6
              /* CLASS, STYLE */
            )) : vue.createCommentVNode("v-if", true),
            $data.refreshStatus == 3 ? (vue.openBlock(), vue.createElementBlock(
              "text",
              {
                key: 2,
                class: vue.normalizeClass(["gui-icons", $props.customClass[$data.refreshStatus]]),
                style: vue.normalizeStyle({
                  fontSize: $props.refreshFontSize + "rpx",
                  width: $props.refreshFontSize + "rpx",
                  height: $props.refreshFontSize + "rpx",
                  lineHeight: $props.refreshFontSize + "rpx"
                })
              },
              "",
              6
              /* CLASS, STYLE */
            )) : vue.createCommentVNode("v-if", true),
            vue.createElementVNode(
              "text",
              {
                style: vue.normalizeStyle([{ "margin-left": "12rpx" }, { fontSize: $props.refreshFontSize + "rpx" }]),
                class: vue.normalizeClass(["gui-page-refresh-text gui-block", $props.customClass[$data.refreshStatus]])
              },
              vue.toDisplayString($props.refreshText[$data.refreshStatus]),
              7
              /* TEXT, CLASS, STYLE */
            )
          ],
          4
          /* STYLE */
        )
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_0$7 = /* @__PURE__ */ _export_sfc(_sfc_main$W, [["render", _sfc_render$V], ["__scopeId", "data-v-adbb42b5"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-refresh.vue"]]);
  const _sfc_main$V = {
    name: "gui-loadmore",
    props: {
      loadMoreText: { type: Array, default: function() {
        return ["", "数据加载中", "已加载全部数据", "暂无数据"];
      } },
      customClass: { type: Array, default: function() {
        return ["gui-color-gray"];
      } },
      loadMoreFontSize: { type: String, default: "26rpx" },
      status: { type: Number, default: 0 }
    },
    data() {
      return {
        loadMoreStatus: 0,
        hidden: false
      };
    },
    created: function() {
      this.loadMoreStatus = this.status;
      if (this.status == 1)
        ;
    },
    methods: {
      loading: function() {
        this.loadMoreStatus = 1;
      },
      stoploadmore: function() {
        this.loadMoreStatus = 0;
      },
      stopLoadmore: function() {
        this.loadMoreStatus = 0;
      },
      nomore: function() {
        this.loadMoreStatus = 2;
      },
      empty: function() {
        this.loadMoreStatus = 3;
      },
      hide: function() {
        this.hidden = true;
      },
      rotate360: function() {
        var el = this.$refs.loadingiconforloadmore;
        animation.transition(el, {
          styles: { transform: "rotate(7200deg)" },
          duration: 2e4,
          timingFunction: "linear",
          needLayout: false,
          delay: 0
        });
      },
      tapme: function() {
        if (this.loadMoreStatus == 0) {
          this.$emit("tapme");
        }
      }
    },
    emits: ["tapme"]
  };
  function _sfc_render$U(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", {
      class: "gui-load-more gui-flex gui-row gui-align-items-center gui-justify-content-center",
      onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.tapme && $options.tapme(...args), ["stop", "prevent"]))
    }, [
      $data.loadMoreStatus == 0 ? (vue.openBlock(), vue.createElementBlock("view", { key: 0 }, [
        vue.createElementVNode(
          "text",
          {
            class: "gui-block",
            style: vue.normalizeStyle({
              height: $props.loadMoreFontSize
            })
          },
          null,
          4
          /* STYLE */
        )
      ])) : vue.createCommentVNode("v-if", true),
      $data.loadMoreStatus == 1 ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 1,
          class: "gui-load-more-icon",
          ref: "loadingiconforloadmore"
        },
        [
          vue.createElementVNode(
            "text",
            {
              class: vue.normalizeClass([$props.customClass, "gui-icons gui-rotate360 gui-block"]),
              style: vue.normalizeStyle({
                fontSize: $props.loadMoreFontSize
              })
            },
            "",
            6
            /* CLASS, STYLE */
          )
        ],
        512
        /* NEED_PATCH */
      )) : vue.createCommentVNode("v-if", true),
      vue.createElementVNode(
        "text",
        {
          class: vue.normalizeClass(["gui-block", $props.customClass]),
          style: vue.normalizeStyle({
            fontSize: $props.loadMoreFontSize
          })
        },
        vue.toDisplayString($props.loadMoreText[$data.loadMoreStatus]),
        7
        /* TEXT, CLASS, STYLE */
      )
    ]);
  }
  const __easycom_1$9 = /* @__PURE__ */ _export_sfc(_sfc_main$V, [["render", _sfc_render$U], ["__scopeId", "data-v-7ad38fb8"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-loadmore.vue"]]);
  const _sfc_main$U = {
    name: "gui-iphone-bottom",
    props: {
      height: { type: String, default: "60rpx" },
      isSwitchPage: { type: Boolean, default: false },
      customClass: { type: Array, default: function() {
        return ["gui-bg-transparent"];
      } }
    },
    data() {
      return {
        need: false
      };
    },
    created: function() {
      if (this.isSwitchPage) {
        return;
      }
      var system = uni.getSystemInfoSync();
      if (system.model) {
        system.model = system.model.replace(" ", "");
        system.model = system.model.toLowerCase();
        var res1 = system.model.indexOf("iphonex");
        if (res1 > 5) {
          res1 = -1;
        }
        var res2 = system.model.indexOf("iphone1");
        if (res2 > 5) {
          res2 = -1;
        }
        if (res1 != -1 || res2 != -1) {
          this.need = true;
        }
      }
    }
  };
  function _sfc_render$T(_ctx, _cache, $props, $setup, $data, $options) {
    return $data.need ? (vue.openBlock(), vue.createElementBlock(
      "view",
      {
        key: 0,
        style: vue.normalizeStyle({ height: $props.height }),
        class: vue.normalizeClass($props.customClass)
      },
      null,
      6
      /* CLASS, STYLE */
    )) : vue.createCommentVNode("v-if", true);
  }
  const __easycom_4$1 = /* @__PURE__ */ _export_sfc(_sfc_main$U, [["render", _sfc_render$T], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-iphone-bottom.vue"]]);
  const _sfc_main$T = {
    name: "gui-page-loading",
    props: {},
    data() {
      return {
        isLoading: false,
        BindingXObjs: [null, null, null],
        AnimateObjs: [null, null, null],
        animateTimer: 800,
        intervalID: null
      };
    },
    watch: {},
    methods: {
      stopfun: function(e) {
        e.stopPropagation();
        return null;
      },
      open: function() {
        this.isLoading = true;
      },
      close: function() {
        setTimeout(() => {
          this.isLoading = false;
        }, 100);
      },
      getRefs: function(ref, count, fun) {
        if (count >= 50) {
          fun(this.$refs[ref]);
          return false;
        }
        var refReturn = this.$refs[ref];
        if (refReturn) {
          fun(refReturn);
        } else {
          count++;
          setTimeout(() => {
            this.getRefs(ref, count, fun);
          }, 100);
        }
      }
    }
  };
  function _sfc_render$S(_ctx, _cache, $props, $setup, $data, $options) {
    return $data.isLoading ? (vue.openBlock(), vue.createElementBlock(
      "view",
      {
        key: 0,
        class: "gui-page-loading gui-flex gui-nowrap gui-align-items-center gui-justify-content-center gui-page-loading-bg",
        onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
        onTouchmove: _cache[1] || (_cache[1] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"]))
      },
      [
        vue.createElementVNode("view", { class: "gui-column" }, [
          vue.createElementVNode("view", { class: "gui-page-loading-point gui-flex gui-rows gui-justify-content-center" }, [
            vue.createElementVNode("view", { class: "gui-page-loading-points animate1 gui-page-loading-color" }),
            vue.createElementVNode("view", { class: "gui-page-loading-points animate2 gui-page-loading-color" }),
            vue.createElementVNode("view", { class: "gui-page-loading-points animate3 gui-page-loading-color" })
          ]),
          vue.createElementVNode("view", { class: "gui-row gui-justify-content-center" }, [
            vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
          ])
        ])
      ],
      32
      /* NEED_HYDRATION */
    )) : vue.createCommentVNode("v-if", true);
  }
  const __easycom_3$3 = /* @__PURE__ */ _export_sfc(_sfc_main$T, [["render", _sfc_render$S], ["__scopeId", "data-v-cdb33dc3"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-page-loading.vue"]]);
  const _sfc_main$S = {
    name: "gui-page",
    props: {
      fullPage: { type: Boolean, default: false },
      // 自定义头部
      customHeader: { type: Boolean, default: false },
      headerClass: { type: Array, default: function() {
        return [];
      } },
      isHeaderSized: { type: Boolean, default: true },
      statusBarClass: { type: Array, default: function() {
        return [];
      } },
      // 自定义底部
      customFooter: { type: Boolean, default: false },
      footerClass: { type: Array, default: function() {
        return [];
      } },
      footerSpaceClass: { type: Array, default: function() {
        return ["gui-bg-gray", "gui-dark-bg-level-2"];
      } },
      customFooterHeight: { type: Number, default: 100 },
      // 挂件
      pendantClass: { type: Array, default: function() {
        return [];
      } },
      // 全屏加载状态
      isLoading: { type: Boolean, default: false },
      isSwitchPage: { type: Boolean, default: false },
      // 吸顶插槽样式
      fixedTopClass: { type: Array, default: function() {
        return [];
      } },
      /* 刷新 */
      refresh: { type: Boolean, default: false },
      refreshText: { type: Array, default: function() {
        return ["继续下拉刷新", "松开手指开始刷新", "数据刷新中", "数据已刷新"];
      } },
      refreshClasses: { type: Array, default: function() {
        return [
          ["gui-color-gray"],
          ["gui-color-gray"],
          ["gui-primary-text"],
          ["gui-color-green"]
        ];
      } },
      refreshFontSize: { type: Number, default: 26 },
      /* 加载更多 */
      loadmore: { type: Boolean, default: false },
      loadMoreText: { type: Array, default: function() {
        return ["", "数据加载中", "已加载全部数据", "暂无数据"];
      } },
      loadMoreClass: { type: Array, default: function() {
        return ["gui-color-gray"];
      } },
      loadMoreFontSize: { type: String, default: "26rpx" },
      loadMoreStatus: { type: Number, default: 1 },
      apiLoadingStatus: { type: Boolean, default: false },
      reFreshTriggerHeight: { type: Number, default: 50 }
    },
    data() {
      return {
        pageStatus: false,
        footerHeight: 50,
        statusBarHeight: 44,
        headerHeight: 72,
        headerTapNumber: 0,
        fixedTop: 0,
        loadMoreTimer: null,
        fixedTopMargin: 0,
        scrollTop: 0,
        srcollTimer: null,
        refreshing: false,
        pullingdownVal: 0,
        topTagID: "no"
      };
    },
    watch: {
      isLoading: function(val) {
        if (val) {
          this.pageLoadingOpen();
        } else {
          this.pageLoadingClose();
        }
      }
    },
    mounted: function() {
      if (this.customFooter) {
        this.footerHeight = uni.upx2px(this.customFooterHeight);
      }
      if (this.isLoading) {
        this.pageLoadingOpen();
      }
      try {
        var system = uni.getSystemInfoSync();
        if (system.model) {
          this.statusBarHeight = system.statusBarHeight;
        }
        if (plus.navigator.isFullscreen()) {
          this.statusBarHeight = 0;
        }
      } catch (e) {
        return null;
      }
      if (this.customFooter) {
        setTimeout(() => {
          this.getDomSize("guiPageFooter", (res) => {
            this.footerHeight = res.height;
          }, 0);
        }, 200);
      }
      if (this.customHeader) {
        setTimeout(() => {
          this.getDomSize("guiPageHeader", (res) => {
            this.headerHeight = res.height;
            this.$nextTick(() => {
              this.pageStatus = true;
            });
          }, 0);
        }, 200);
      } else {
        this.pageStatus = true;
      }
      if (this.customHeader) {
        setTimeout(() => {
          this.getDomSize("guiPageHeader", (res) => {
            this.fixedTop = res.height;
          }, 0);
        }, 200);
      }
      setTimeout(() => {
        this.getDomSize("guiPageFixedTop", (res) => {
          this.fixedTopMargin = res.height;
        }, 0);
      }, 200);
    },
    methods: {
      onpullingdown: function(e) {
        if (this.apiLoadingStatus) {
          return false;
        }
        e.changedTouches = [{ pageY: e.pullingDistance }];
        this.$refs.guiPageRefresh.touchmove(e);
      },
      onrefresh: function() {
        if (this.apiLoadingStatus) {
          return false;
        }
        this.refreshing = true;
        this.$refs.guiPageRefresh.refreshStatus = 2;
        setTimeout(() => {
          this.$refs.guiPageRefresh.rotate360();
        }, 200);
        this.$emit("reload");
      },
      pageLoadingOpen: function() {
        this.getRefs("guipageloading", 0, (ref) => {
          ref.open();
        });
      },
      pageLoadingClose: function() {
        this.getRefs("guipageloading", 0, (ref) => {
          ref.close();
        });
      },
      // 下拉刷新相关
      touchstart: function(e) {
        if (!this.refresh) {
          return false;
        }
        if (this.apiLoadingStatus) {
          return false;
        }
        this.$refs.guiPageRefresh.touchstart(e);
      },
      touchmove: function(e) {
        if (!this.refresh) {
          return false;
        }
        if (this.apiLoadingStatus) {
          return false;
        }
        this.$refs.guiPageRefresh.touchmove(e);
      },
      touchend: function(e) {
        if (!this.refresh) {
          return false;
        }
        if (this.apiLoadingStatus) {
          return false;
        }
        this.$refs.guiPageRefresh.touchend(e);
      },
      scroll: function(e) {
        if (this.srcollTimer != null) {
          clearTimeout(this.srcollTimer);
        }
        this.srcollTimer = setTimeout(() => {
          this.$refs.guiPageRefresh.scroll(e);
          this.$emit("scroll", e);
          this.scrollTop = e.detail.scrollTop;
        }, 500);
      },
      toTop: function() {
        this.topTagID = "guiPageBodyTopTag";
        setTimeout(() => {
          this.topTagID = "no";
        }, 500);
      },
      endReload: function() {
        this.$refs.guiPageRefresh.endReload();
        this.refreshing = false;
      },
      reload: function() {
        if (this.apiLoadingStatus) {
          return false;
        }
        this.$emit("reload");
        if (this.loadmore) {
          this.$refs.guipageloadmore.stoploadmore();
        }
      },
      // 获取元素尺寸
      getDomSize: function(domIDOrRef, fun, count) {
        if (!count) {
          count = 1;
        }
        if (count >= 50) {
          fun({ width: 0, height: 0 });
          return false;
        }
        uni.createSelectorQuery().in(this).select("#" + domIDOrRef).boundingClientRect().exec((res) => {
          if (res[0] == null) {
            count += 1;
            setTimeout(() => {
              this.getDomSize(domIDOrRef, fun, count);
            }, 50);
            return;
          } else {
            if (res[0].height == void 0) {
              count += 1;
              setTimeout(() => {
                this.getDomSize(domIDOrRef, fun, count);
              }, 50);
              return;
            }
            fun(res[0]);
            return;
          }
        });
      },
      stopfun: function(e) {
        e.stopPropagation();
        return null;
      },
      headerTap: function() {
        this.headerTapNumber++;
        if (this.headerTapNumber >= 2) {
          this.$emit("gotoTop");
          this.headerTapNumber = 0;
        } else {
          setTimeout(() => {
            this.headerTapNumber = 0;
          }, 1e3);
        }
      },
      getRefs: function(ref, count, fun) {
        if (count >= 50) {
          fun(this.$refs[ref]);
          return false;
        }
        var refReturn = this.$refs[ref];
        if (refReturn) {
          fun(refReturn);
        } else {
          count++;
          setTimeout(() => {
            this.getRefs(ref, count, fun);
          }, 100);
        }
      },
      loadmorefun: function() {
        if (!this.loadmore) {
          return false;
        }
        if (this.apiLoadingStatus) {
          return false;
        }
        if (this.loadMoreTimer != null) {
          clearTimeout(this.loadMoreTimer);
        }
        this.loadMoreTimer = setTimeout(() => {
          var status = this.$refs.guipageloadmore.loadMoreStatus;
          if (status != 0) {
            return null;
          }
          this.$refs.guipageloadmore.loading();
          this.$emit("loadmorefun");
        }, 80);
      },
      stopLoadmore: function() {
        this.$refs.guipageloadmore.stoploadmore();
      },
      stoploadmore: function() {
        this.$refs.guipageloadmore.stoploadmore();
      },
      nomore: function() {
        this.$refs.guipageloadmore.nomore();
      },
      empty: function() {
        this.$refs.guipageloadmore.empty();
      },
      toast: function(msg) {
        uni.showToast({
          title: msg,
          icon: "none"
        });
      },
      resetFooterHeight: function() {
        if (this.customFooter) {
          setTimeout(() => {
            this.getDomSize("guiPageFooter", (res) => {
              this.footerHeight = res.height;
            }, 0);
          }, 500);
        }
      },
      isIphoneBottom: function() {
        var system = uni.getSystemInfoSync();
        if (system.model) {
          system.model = system.model.replace(" ", "");
          system.model = system.model.toLowerCase();
          var res1 = system.model.indexOf("iphonex");
          if (res1 > 5) {
            res1 = -1;
          }
          var res2 = system.model.indexOf("iphone1");
          if (res2 > 5) {
            res2 = -1;
          }
          if (res1 != -1 || res2 != -1) {
            return true;
          }
        }
        return false;
      }
    },
    emits: ["scroll", "reload", "loadmorefun", "gotoTop"]
  };
  function _sfc_render$R(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_refresh = resolveEasycom(vue.resolveDynamicComponent("gui-refresh"), __easycom_0$7);
    const _component_gui_loadmore = resolveEasycom(vue.resolveDynamicComponent("gui-loadmore"), __easycom_1$9);
    const _component_gui_iphone_bottom = resolveEasycom(vue.resolveDynamicComponent("gui-iphone-bottom"), __easycom_4$1);
    const _component_gui_page_loading = resolveEasycom(vue.resolveDynamicComponent("gui-page-loading"), __easycom_3$3);
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        style: vue.normalizeStyle({ opacity: $data.pageStatus ? 1 : 0 }),
        class: vue.normalizeClass(["gui-sbody gui-flex gui-column", [$props.fullPage || $props.refresh || $props.loadmore ? "gui-flex1" : ""]])
      },
      [
        vue.createCommentVNode(" 自定义头部 "),
        $props.customHeader ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 0,
            class: vue.normalizeClass(["gui-header gui-transition-all", $props.headerClass]),
            id: "guiPageHeader",
            ref: "guiPageHeader"
          },
          [
            vue.createCommentVNode(" 状态栏 "),
            vue.createElementVNode(
              "view",
              {
                class: vue.normalizeClass(["gui-page-status-bar", $props.statusBarClass]),
                style: vue.normalizeStyle({ height: $data.statusBarHeight + "px" })
              },
              [
                vue.renderSlot(_ctx.$slots, "gStatusBar")
              ],
              6
              /* CLASS, STYLE */
            ),
            vue.createCommentVNode(" 头部插槽 "),
            vue.createElementVNode("view", {
              class: "gui-flex gui-column gui-justify-content-center",
              onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.headerTap && $options.headerTap(...args), ["stop"]))
            }, [
              vue.renderSlot(_ctx.$slots, "gHeader")
            ])
          ],
          2
          /* CLASS */
        )) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 自定义头部占位 "),
        $props.customHeader && $props.isHeaderSized ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 1,
            style: vue.normalizeStyle({ height: $data.headerHeight + "px" })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 页面主体 "),
        !$props.refresh && !$props.loadmore ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 2,
            class: vue.normalizeClass(["gui-flex gui-column gui-relative", [$props.fullPage ? "gui-flex1" : ""]]),
            id: "guiPageBody",
            ref: "guiPageBody"
          },
          [
            vue.renderSlot(_ctx.$slots, "gBody")
          ],
          2
          /* CLASS */
        )) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 刷新加载主体  非 nvue "),
        $props.refresh || $props.loadmore ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 3,
            class: "gui-flex1 gui-relative",
            id: "guiPageBody",
            ref: "guiPageBody",
            style: vue.normalizeStyle({ marginTop: $data.fixedTopMargin + "px" })
          },
          [
            vue.createElementVNode("scroll-view", {
              class: "gui-absolute-full",
              "scroll-y": true,
              "show-scrollbar": false,
              onTouchstart: _cache[1] || (_cache[1] = (...args) => $options.touchstart && $options.touchstart(...args)),
              onTouchmove: _cache[2] || (_cache[2] = (...args) => $options.touchmove && $options.touchmove(...args)),
              onTouchend: _cache[3] || (_cache[3] = (...args) => $options.touchend && $options.touchend(...args)),
              onScroll: _cache[4] || (_cache[4] = (...args) => $options.scroll && $options.scroll(...args)),
              "scroll-into-view": $data.topTagID,
              "scroll-with-animation": false,
              onScrolltolower: _cache[5] || (_cache[5] = (...args) => $options.loadmorefun && $options.loadmorefun(...args))
            }, [
              vue.createElementVNode("view", { id: "guiPageBodyTopTag" }, [
                vue.createVNode(_component_gui_refresh, {
                  ref: "guiPageRefresh",
                  onReload: $options.reload,
                  refreshText: $props.refreshText,
                  customClass: $props.refreshClasses,
                  triggerHeight: $props.reFreshTriggerHeight,
                  refreshFontSize: $props.refreshFontSize
                }, null, 8, ["onReload", "refreshText", "customClass", "triggerHeight", "refreshFontSize"])
              ]),
              vue.renderSlot(_ctx.$slots, "gBody"),
              $props.loadmore ? (vue.openBlock(), vue.createElementBlock("view", { key: 0 }, [
                vue.createVNode(_component_gui_loadmore, {
                  ref: "guipageloadmore",
                  status: $props.loadMoreStatus,
                  loadMoreText: $props.loadMoreText,
                  customClass: $props.loadMoreClass,
                  loadMoreFontSize: $props.loadMoreFontSize
                }, null, 8, ["status", "loadMoreText", "customClass", "loadMoreFontSize"])
              ])) : vue.createCommentVNode("v-if", true)
            ], 40, ["scroll-into-view"])
          ],
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 刷新加载主体 nvue "),
        vue.createCommentVNode(" 页面底部 "),
        vue.createCommentVNode(" 底部占位 "),
        $props.customFooter ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 4,
            style: vue.normalizeStyle({ height: $data.footerHeight + "px" })
          },
          null,
          4
          /* STYLE */
        )) : vue.createCommentVNode("v-if", true),
        $props.customFooter ? (vue.openBlock(), vue.createElementBlock(
          "view",
          {
            key: 5,
            class: vue.normalizeClass(["gui-page-footer gui-border-box", $props.footerClass]),
            id: "guiPageFooter",
            ref: "guiPageFooter"
          },
          [
            vue.renderSlot(_ctx.$slots, "gFooter"),
            vue.createVNode(_component_gui_iphone_bottom, {
              need: !$props.isSwitchPage,
              customClass: $props.footerSpaceClass
            }, null, 8, ["need", "customClass"])
          ],
          2
          /* CLASS */
        )) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 右下角悬浮挂件 "),
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass(["gui-page-pendant", $props.pendantClass])
          },
          [
            vue.renderSlot(_ctx.$slots, "gPendant")
          ],
          2
          /* CLASS */
        ),
        vue.createCommentVNode(" 吸顶元素 "),
        vue.createElementVNode(
          "view",
          {
            class: "gui-page-fixed-top",
            ref: "guiPageFixedTop",
            id: "guiPageFixedTop",
            style: vue.normalizeStyle({ top: $data.fixedTop + "px" })
          },
          [
            vue.renderSlot(_ctx.$slots, "gFixedTop")
          ],
          4
          /* STYLE */
        ),
        vue.createCommentVNode(" 全屏 loading "),
        vue.createVNode(
          _component_gui_page_loading,
          { ref: "guipageloading" },
          null,
          512
          /* NEED_PATCH */
        )
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_1$8 = /* @__PURE__ */ _export_sfc(_sfc_main$S, [["render", _sfc_render$R], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-page.vue"]]);
  const _sfc_main$R = {
    data() {
      return {};
    },
    methods: {
      handleBack() {
        uni.switchTab({
          url: "/pages/tabbar/me"
        });
      }
    }
  };
  function _sfc_render$Q(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_cc_advert_index = resolveEasycom(vue.resolveDynamicComponent("cc-advert-index"), __easycom_3$5);
    const _component_cc_category_list = resolveEasycom(vue.resolveDynamicComponent("cc-category-list"), __easycom_2$8);
    const _component_cc_course_index = resolveEasycom(vue.resolveDynamicComponent("cc-course-index"), __easycom_3$4);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding gui-bg-white ps tophead" }, [
          vue.createVNode(_component_logo, {
            showback: "",
            onBack: $options.handleBack,
            btntext: "",
            title: "资源推广"
          }, null, 8, ["onBack"])
        ]),
        vue.createElementVNode("view", { style: { "margin-top": "90px" } }),
        vue.createElementVNode("view", null, [
          vue.createVNode(_component_cc_advert_index, { showspace: false }),
          vue.createElementVNode("view", { class: "gui-padding-m" }, [
            vue.createElementVNode("view", { class: "gui-bg-white gui-border-radius" }, [
              vue.createVNode(_component_cc_category_list)
            ]),
            vue.createElementVNode("view", { class: "gui-padding-m gui-bg-white gui-border-radius mt10" }, [
              vue.createVNode(_component_cc_course_index)
            ])
          ])
        ]),
        vue.createElementVNode("view", { style: { "height": "30rpx" } })
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesTabbarIndex = /* @__PURE__ */ _export_sfc(_sfc_main$R, [["render", _sfc_render$Q], ["__file", "D:/App/分销系统/cc_fenxiao/pages/tabbar/index.vue"]]);
  const _sfc_main$Q = {
    data() {
      return {
        items: [
          [80, "", "我的订单"],
          [100, "", "优惠券"],
          [50, "", "关注店铺"],
          [99, "", "收藏"]
        ]
      };
    },
    methods: {}
  };
  function _sfc_render$P(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-list gui-padding" }, [
          vue.createElementVNode("navigator", { url: "/pages/user/settings" }, [
            vue.createElementVNode("view", { class: "gui-list-items" }, [
              vue.createElementVNode("view", { class: "gui-list-image ucenter-face gui-relative" }, [
                vue.createElementVNode("image", {
                  class: "gui-list-image ucenter-face-image",
                  src: "https://images.unsplash.com/photo-1662695094714-966fc339a2e8?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyNnx8fGVufDB8fHx8&auto=format&fit=crop&w=100&q=80",
                  mode: "aspectFill"
                })
              ]),
              vue.createElementVNode("view", { class: "gui-list-body" }, [
                vue.createElementVNode("view", { class: "gui-list-title" }, [
                  vue.createElementVNode("text", { class: "gui-list-title-text gui-primary-color" }, "用户昵称")
                ]),
                vue.createElementVNode("text", { class: "gui-list-body-desc gui-color-gray gui-block" }, "描述信息或签名信息")
              ])
            ])
          ])
        ]),
        vue.createElementVNode("view", {
          class: "gui-padding-x gui-bg-gray",
          style: { "height": "10rpx" }
        }),
        vue.createElementVNode("view", { class: "gui-list gui-margin-top gui-padding-x" }, [
          vue.createElementVNode("navigator", { url: "/pages/state/state" }, [
            vue.createElementVNode("view", { class: "gui-list-items" }, [
              vue.createElementVNode("text", { class: "gui-list-icon gui-icons gui-color-blue" }, ""),
              vue.createElementVNode("view", { class: "gui-list-body gui-border-b" }, [
                vue.createElementVNode("view", { class: "gui-list-title" }, [
                  vue.createElementVNode("text", { class: "gui-list-title-text" }, "推广数据统计"),
                  vue.createElementVNode("text", { class: "gui-badge gui-bg-red gui-color-white" }, "12")
                ])
              ]),
              vue.createElementVNode("text", { class: "gui-list-arrow-right gui-icons gui-color-gray" }, "")
            ])
          ]),
          vue.createElementVNode("navigator", { url: "/pages/user/xieyi" }, [
            vue.createElementVNode("view", { class: "gui-list-items" }, [
              vue.createElementVNode("text", { class: "gui-list-icon gui-icons gui-color-orange" }, ""),
              vue.createElementVNode("view", { class: "gui-list-body gui-border-b" }, [
                vue.createElementVNode("view", { class: "gui-list-title" }, [
                  vue.createElementVNode("text", { class: "gui-list-title-text" }, "推广规则")
                ])
              ]),
              vue.createElementVNode("text", { class: "gui-list-arrow-right gui-icons gui-color-gray" }, "")
            ])
          ]),
          vue.createElementVNode("navigator", { url: "/pages/user/bind" }, [
            vue.createElementVNode("view", { class: "gui-list-items" }, [
              vue.createElementVNode("text", { class: "gui-list-icon gui-icons gui-color-orange" }, ""),
              vue.createElementVNode("view", { class: "gui-list-body gui-border-b" }, [
                vue.createElementVNode("view", { class: "gui-list-title" }, [
                  vue.createElementVNode("text", { class: "gui-list-title-text" }, "绑定账户"),
                  vue.createElementVNode("text", { class: "gui-list-body-desc gui-color-gray" }, "可提现金将自动转账到你的账户")
                ])
              ]),
              vue.createElementVNode("text", { class: "gui-list-arrow-right gui-icons gui-color-gray" }, "")
            ])
          ]),
          vue.createElementVNode("navigator", { url: "/pages/user/validator" }, [
            vue.createElementVNode("view", { class: "gui-list-items" }, [
              vue.createElementVNode("text", { class: "gui-list-icon gui-icons gui-color-red" }, ""),
              vue.createElementVNode("view", { class: "gui-list-body" }, [
                vue.createElementVNode("view", { class: "gui-list-title" }, [
                  vue.createElementVNode("text", { class: "gui-list-title-text" }, "实名认证"),
                  vue.createElementVNode("text", { class: "gui-list-body-desc gui-color-gray" }, "您的账号还没实名验证，会影响你体现")
                ])
              ]),
              vue.createElementVNode("text", { class: "gui-list-arrow-right gui-icons gui-color-gray" }, "")
            ])
          ])
        ]),
        vue.createElementVNode("view", { class: "ucenter-line gui-bg-white gui-dark-bg-level-3" }),
        vue.createElementVNode("view", { style: { "padding": "0 20px" } }, [
          vue.createElementVNode("view", {
            "hover-class": "gui-tap",
            class: "cc-fixed-button gui-text-center gui-border-radius gui-bg-gray",
            style: { "bottom": "80px", "right": "20px", "left": "20px" }
          }, [
            vue.createElementVNode("navigator", {
              url: "/pages/login/login",
              class: "gui-padding-x"
            }, [
              vue.createElementVNode("text", { class: "gui-text-center gui-block-text gui-icons logoff gui-color-red" }, " 退出登录")
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesTabbarMe = /* @__PURE__ */ _export_sfc(_sfc_main$Q, [["render", _sfc_render$P], ["__scopeId", "data-v-edaa9d3c"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/tabbar/me.vue"]]);
  const _sfc_main$P = {
    name: "gui-switch-navigation",
    props: {
      width: { type: Number, default: 690 },
      isCenter: { type: Boolean, default: false },
      currentIndex: { type: Number, default: 0 },
      size: { type: Number, default: 120 },
      fontSize: { type: String, default: "28rpx" },
      activeFontSize: { type: String, default: "28rpx" },
      items: { type: Array, default: function() {
        return [];
      } },
      activeLineClass: { type: Array, default: function() {
        return ["gui-nav-scale", "gui-gtbg-blue", "gui-gtbg-black"];
      } },
      titleClass: { type: Array, default: function() {
        return ["gui-primary-text"];
      } },
      titleActiveClass: { type: Array, default: function() {
        return ["gui-primary-text"];
      } },
      activeLineHeight: { type: String, default: "5rpx" },
      activeLineWidth: { type: String, default: "36rpx" },
      activeLineRadius: { type: String, default: "0rpx" },
      activeDirection: { type: String, default: "" },
      activeFontWeight: { type: Number, default: 700 },
      margin: { type: Number, default: 0 },
      textAlign: { type: String, default: "" },
      lineHeight: { type: String, default: "50rpx" },
      padding: { type: String, default: "0rpx" },
      animatie: { type: Boolean, default: true },
      scorllAnimation: { type: Boolean, default: true },
      tipsStyle: {
        type: String,
        default: "background-color:#FF0000; padding:0 10rpx; color:#FFFFFF; font-size:22rpx"
      }
    },
    data() {
      return {
        currentIndexIn: 0,
        itemsIn: [],
        random: 1,
        scrollLeft: 0,
        scrllTimer: null,
        autoLeft: ""
      };
    },
    created: function() {
      this.currentIndexIn = this.currentIndex;
      this.itemsIn = this.items;
      this.random = this.randomNum();
    },
    watch: {
      currentIndex: function(value) {
        this.currentIndexIn = value;
      },
      currentIndexIn: function(val) {
        if (this.scrllTimer != null) {
          clearTimeout(this.scrllTimer);
        }
        this.scrllTimer = setTimeout(() => {
          this.setLeft();
        }, 200);
      },
      items: function(value) {
        this.itemsIn = value;
      }
    },
    methods: {
      change: function(e) {
        this.currentIndexIn = e.currentTarget.dataset.index;
        this.$emit("change", Number(e.currentTarget.dataset.index));
      },
      randomNum: function() {
        return parseInt(Math.random() * 1e3);
      },
      setLeft: function() {
        if (this.size < 1) {
          this.autoLeft = "tab-" + this.currentIndexIn + this.random;
          return;
        }
        var itemWidth = Number(this.margin) + Number(this.size);
        var left = (Number(this.currentIndexIn) + 1) * itemWidth - Number(this.width) / 2 - itemWidth / 2;
        var maxLeft = Number(this.itemsIn.length) * itemWidth - this.width;
        maxLeft = uni.upx2px(maxLeft - 30);
        left = uni.upx2px(left);
        if (left > maxLeft) {
          left = maxLeft;
        }
        if (left < 0) {
          left = 0;
        }
        this.scrollLeft = left;
      }
    },
    emits: ["change"]
  };
  function _sfc_render$O(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("scroll-view", {
      "scroll-with-animation": $props.scorllAnimation,
      "scroll-x": true,
      "show-scrollbar": false,
      class: vue.normalizeClass(["gui-scroll-x", $props.isCenter ? "gui-nav-center" : ""]),
      style: vue.normalizeStyle({ width: $props.width + "rpx" }),
      "scroll-into-view": $data.autoLeft,
      "scroll-left": $data.scrollLeft
    }, [
      (vue.openBlock(true), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList($data.itemsIn, (item, index) => {
          return vue.openBlock(), vue.createElementBlock("view", {
            class: "gui-scroll-x-items gui-columns",
            id: "tab-" + index + ($data.random + ""),
            style: vue.normalizeStyle({
              width: $props.size == 0 ? "auto" : $props.size + "rpx",
              marginRight: $props.margin + "rpx",
              paddingLeft: $props.padding,
              paddingRight: $props.padding
            }),
            key: index,
            onClick: _cache[0] || (_cache[0] = (...args) => $options.change && $options.change(...args)),
            "data-index": index
          }, [
            vue.createElementVNode(
              "view",
              {
                class: vue.normalizeClass(["gui-flex gui-nowrap gui-align-items-start", [$props.textAlign == "center" ? "gui-justify-content-center" : ""]])
              },
              [
                vue.createElementVNode(
                  "text",
                  {
                    class: vue.normalizeClass(["gui-block gui-border-box", [$props.titleClass, index == $data.currentIndexIn ? $props.titleActiveClass : []]]),
                    style: vue.normalizeStyle({
                      textAlign: $props.textAlign,
                      lineHeight: $props.lineHeight,
                      fontSize: $data.currentIndexIn == index ? $props.activeFontSize : $props.fontSize,
                      fontWeight: $data.currentIndexIn == index ? $props.activeFontWeight : ""
                    })
                  },
                  vue.toDisplayString(item.name),
                  7
                  /* TEXT, CLASS, STYLE */
                ),
                item.tips ? (vue.openBlock(), vue.createElementBlock("view", { key: 0 }, [
                  item.tips != "point" ? (vue.openBlock(), vue.createElementBlock(
                    "text",
                    {
                      key: 0,
                      class: "gui-nav-tips gui-block",
                      style: vue.normalizeStyle($props.tipsStyle)
                    },
                    vue.toDisplayString(item.tips),
                    5
                    /* TEXT, STYLE */
                  )) : (vue.openBlock(), vue.createElementBlock(
                    "text",
                    {
                      key: 1,
                      class: "gui-nav-tips gui-block",
                      style: vue.normalizeStyle($props.tipsStyle + "; width:12rpx; height:12rpx; over-flow:hidden; padding:0rpx; margin-top:10rpx;")
                    },
                    null,
                    4
                    /* STYLE */
                  ))
                ])) : vue.createCommentVNode("v-if", true)
              ],
              2
              /* CLASS */
            ),
            vue.createElementVNode(
              "view",
              {
                class: "gui-flex gui-rows",
                style: vue.normalizeStyle({ justifyContent: $props.activeDirection })
              },
              [
                $data.currentIndexIn == index ? (vue.openBlock(), vue.createElementBlock(
                  "view",
                  {
                    key: 0,
                    class: vue.normalizeClass(["nav-active-line", $data.currentIndexIn == index ? $props.activeLineClass : []]),
                    style: vue.normalizeStyle({
                      width: $props.activeLineWidth,
                      height: $props.activeLineHeight,
                      borderRadius: $props.activeLineRadius
                    })
                  },
                  null,
                  6
                  /* CLASS, STYLE */
                )) : vue.createCommentVNode("v-if", true)
              ],
              4
              /* STYLE */
            )
          ], 12, ["id", "data-index"]);
        }),
        128
        /* KEYED_FRAGMENT */
      ))
    ], 14, ["scroll-with-animation", "scroll-into-view", "scroll-left"]);
  }
  const __easycom_0$6 = /* @__PURE__ */ _export_sfc(_sfc_main$P, [["render", _sfc_render$O], ["__scopeId", "data-v-7f73495d"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-switch-navigation.vue"]]);
  const _sfc_main$O = {
    name: "income",
    data() {
      return {};
    }
  };
  function _sfc_render$N(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "cc-box-container" }, [
      (vue.openBlock(), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList(20, (num) => {
          return vue.createElementVNode("view", { class: "cc-boxc" }, [
            vue.createElementVNode("view", { class: "tagb" }, "推广收入"),
            vue.createElementVNode("view", { class: "gui-flex gui-column gui-column-items" }, [
              vue.createElementVNode("view", { class: "tit" }, "Redis与SpringBoot互联网实战"),
              vue.createElementVNode("view", { class: "dt" }, "2024-02-04 12:05:41"),
              vue.createElementVNode("view", { class: "gui-flex gui-space-between" }, [
                vue.createElementVNode("view", { class: "desc" }, "还剩下21天解冻"),
                vue.createElementVNode("view", { class: "price gui-color-red fb" }, "+4.55")
              ])
            ])
          ]);
        }),
        64
        /* STABLE_FRAGMENT */
      ))
    ]);
  }
  const __easycom_2$6 = /* @__PURE__ */ _export_sfc(_sfc_main$O, [["render", _sfc_render$N], ["__scopeId", "data-v-135c72a2"], ["__file", "D:/App/分销系统/cc_fenxiao/components/income/yongjin.vue"]]);
  const _sfc_main$N = {
    name: "income",
    data() {
      return {};
    }
  };
  function _sfc_render$M(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "cc-box-container" }, [
      (vue.openBlock(), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList(20, (num) => {
          return vue.createElementVNode("view", { class: "cc-boxc" }, [
            vue.createElementVNode("view", { class: "tagb" }, "提现记录"),
            vue.createElementVNode("view", { class: "gui-flex gui-column gui-column-items" }, [
              vue.createElementVNode("view", { class: "tit" }, "Redis与SpringBoot互联网实战"),
              vue.createElementVNode("view", { class: "dt" }, "2024-02-04 12:05:41"),
              vue.createElementVNode("view", { class: "gui-flex gui-space-between" }, [
                vue.createElementVNode("view", { class: "desc" }, "还剩下21天解冻"),
                vue.createElementVNode("view", { class: "price gui-color-red fb" }, "+4.55")
              ])
            ])
          ]);
        }),
        64
        /* STABLE_FRAGMENT */
      ))
    ]);
  }
  const __easycom_3$2 = /* @__PURE__ */ _export_sfc(_sfc_main$N, [["render", _sfc_render$M], ["__scopeId", "data-v-13ad1917"], ["__file", "D:/App/分销系统/cc_fenxiao/components/income/tixian.vue"]]);
  const _sfc_main$M = {
    data() {
      return {
        currentIndex: 0,
        isLoad: false,
        navItems: [{
          id: 2,
          name: "佣金明细"
        }, {
          id: 3,
          name: "提现记录"
        }]
      };
    },
    onLoad() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    },
    methods: {
      handleBack() {
        uni.switchTab({
          url: "/pages/tabbar/me"
        });
      },
      navchange(e) {
        this.currentIndex = e;
      }
    }
  };
  function _sfc_render$L(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_income_yongjin = resolveEasycom(vue.resolveDynamicComponent("cc-income-yongjin"), __easycom_2$6);
    const _component_cc_income_tixian = resolveEasycom(vue.resolveDynamicComponent("cc-income-tixian"), __easycom_3$2);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding gui-bg-white ps tophead" }, [
          vue.createVNode(_component_logo, {
            title: "收益管理",
            showback: "",
            onBack: $options.handleBack,
            btntext: ""
          }, null, 8, ["onBack"])
        ]),
        vue.createElementVNode("view", { style: { "margin-top": "90px" } }),
        vue.createElementVNode("view", { class: "tipbox" }, [
          vue.createElementVNode("text", { class: "gui-icons mr5" }, ""),
          vue.createTextVNode("推广订单过了冻结周期后，对应的收益转为可提现")
        ]),
        vue.createElementVNode("view", { class: "cc-box-main" }, [
          vue.createElementVNode("view", { class: "cc-l cc-c" }, [
            vue.createElementVNode("view", { class: "tit" }, "可提现"),
            vue.createElementVNode("view", { class: "price" }, [
              vue.createElementVNode("text", null, "￥"),
              vue.createTextVNode("0.00")
            ])
          ]),
          vue.createElementVNode("view", { class: "c-sp" }),
          vue.createElementVNode("view", { class: "cc-r cc-c" }, [
            vue.createElementVNode("view", { class: "tit" }, "佣金金额"),
            vue.createElementVNode("view", { class: "price" }, [
              vue.createElementVNode("text", null, "￥"),
              vue.createTextVNode("0.00")
            ])
          ])
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-bg-white detail-items" }, [
            vue.createElementVNode("view", { class: "fb fz16" }, "提现提醒"),
            vue.createElementVNode("view", { class: "flex justify-content-between align-items mt10" }, [
              vue.createElementVNode("view", null, [
                vue.createElementVNode("text", { class: "fz18 fb pr ftp4" }, "."),
                vue.createElementVNode("text", { class: "ml5 fz14" }, [
                  vue.createTextVNode("当前可提现金额是："),
                  vue.createElementVNode("text", { class: "red fb" }, "￥3124545元")
                ])
              ]),
              vue.createElementVNode("view", { class: "ml20" }, [
                vue.createElementVNode("button", {
                  type: "default",
                  class: "gui-button-mini mainbg"
                }, [
                  vue.createElementVNode("text", { class: "gui-color-white gui-button-text-mini gui-icons" }, " 点击提现")
                ])
              ])
            ]),
            vue.createElementVNode("view", { class: "flex justify-content-between align-items mt5" }, [
              vue.createElementVNode("view", { class: "flex1" }, [
                vue.createElementVNode("text", { class: "fz18 fb pr ftp4" }, "."),
                vue.createElementVNode("text", { class: "ml5 fz14" }, "如果遇到问题联系客服：545454545")
              ])
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-bg-white detail-items mt10" }, [
            vue.createElementVNode("view", { class: "gui-bg-gray gui-padding-small" }, [
              vue.createVNode(_component_gui_switch_navigation, {
                items: $data.navItems,
                textAlign: "center",
                isCenter: true,
                activeDirection: "center",
                size: 0,
                margin: 40,
                activeLineClass: ["gui-gtbg-green"],
                onChange: $options.navchange
              }, null, 8, ["items", "onChange"])
            ]),
            $data.currentIndex == 0 ? (vue.openBlock(), vue.createBlock(_component_cc_income_yongjin, { key: 0 })) : vue.createCommentVNode("v-if", true),
            $data.currentIndex == 1 ? (vue.openBlock(), vue.createBlock(_component_cc_income_tixian, { key: 1 })) : vue.createCommentVNode("v-if", true)
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesTabbarIncome = /* @__PURE__ */ _export_sfc(_sfc_main$M, [["render", _sfc_render$L], ["__scopeId", "data-v-0cc8765e"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/tabbar/income.vue"]]);
  function checkIdcard(idcard) {
    var Errors = new Array(
      "ok",
      "身份证号码位数错误",
      "身份证号码出生日期错误",
      "身份证号码校验错误",
      "身份证地区错误"
    );
    var area = {
      11: "北京",
      12: "天津",
      13: "河北",
      14: "山西",
      15: "内蒙古",
      21: "辽宁",
      22: "吉林",
      23: "黑龙江",
      31: "上海",
      32: "江苏",
      33: "浙江",
      34: "安徽",
      35: "福建",
      36: "江西",
      37: "山东",
      41: "河南",
      42: "湖北",
      43: "湖南",
      44: "广东",
      45: "广西",
      46: "海南",
      50: "重庆",
      51: "四川",
      52: "贵州",
      53: "云南",
      54: "西藏",
      61: "陕西",
      62: "甘肃",
      63: "青海",
      64: "宁夏",
      65: "新疆",
      71: "台湾",
      81: "香港",
      82: "澳门",
      91: "国外"
    };
    var idcard, Y, JYM, ereg;
    var S, M;
    var idcard_array = new Array();
    idcard_array = idcard.split("");
    if (area[parseInt(idcard.substr(0, 2))] == null)
      return Errors[4];
    switch (idcard.length) {
      case 15:
        if ((parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0 || (parseInt(idcard.substr(6, 2)) + 1900) % 100 == 0 && (parseInt(idcard.substr(6, 2)) + 1900) % 4 == 0) {
          ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/;
        } else {
          ereg = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/;
        }
        if (ereg.test(idcard))
          return Errors[0];
        else
          return Errors[2];
      case 18:
        if (parseInt(idcard.substr(6, 4)) % 4 == 0 || parseInt(idcard.substr(6, 4)) % 100 == 0 && parseInt(idcard.substr(6, 4)) % 4 == 0) {
          ereg = /^[1-9][0-9]{5}[1-2]+[0-9]{3}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/;
        } else {
          ereg = /^[1-9][0-9]{5}[1-2]+[0-9]{3}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/;
        }
        if (ereg.test(idcard)) {
          S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7 + (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9 + (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10 + (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5 + (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8 + (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4 + (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2 + parseInt(idcard_array[7]) * 1 + parseInt(idcard_array[8]) * 6 + parseInt(idcard_array[9]) * 3;
          Y = S % 11;
          M = "F";
          JYM = "10X98765432";
          M = JYM.substr(Y, 1);
          if (M == idcard_array[17])
            return Errors[0];
          else
            return Errors[3];
        } else {
          return Errors[2];
        }
      default:
        return Errors[1];
    }
  }
  const idCardChecker = {
    checkIdcard
  };
  const graceChecker = {
    error: "",
    check: function(dataBeCheck, rule) {
      dataBeCheck = JSON.stringify(dataBeCheck);
      var data = JSON.parse(dataBeCheck);
      for (var i = 0; i < rule.length; i++) {
        if (!rule[i].checkType) {
          return true;
        }
        if (!rule[i].name) {
          return true;
        }
        if (!rule[i].errorMsg) {
          return true;
        }
        if (typeof data[rule[i].name] == "undefined" || data[rule[i].name] === "") {
          this.error = rule[i].errorMsg;
          return false;
        }
        if (typeof data[rule[i].name] == "string") {
          data[rule[i].name] = data[rule[i].name].replace(/\s/g, "");
        }
        switch (rule[i].checkType) {
          case "string":
            var reg = new RegExp("^.{" + rule[i].checkRule + "}$");
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "contain":
            var cData = data[rule[i].name] + "";
            if (cData.indexOf(rule[i].checkRule) == -1) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "notContain":
            var cData = data[rule[i].name] + "";
            if (cData.indexOf(rule[i].checkRule) != -1) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "inArray":
            if (typeof rule[i].checkRule != "object") {
              this.error = rule[i].errorMsg;
              return false;
            }
            var resInArray = rule[i].checkRule.find(
              (val) => {
                if (val == data[rule[i].name]) {
                  return true;
                }
              }
            );
            if (!resInArray) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "notInArray":
            if (typeof rule[i].checkRule != "object") {
              this.error = rule[i].errorMsg;
              return false;
            }
            var resInArray = rule[i].checkRule.find(
              (val) => {
                if (val == data[rule[i].name]) {
                  return true;
                }
              }
            );
            if (resInArray) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "int":
            var ruleArr = rule[i].checkRule.split(",");
            if (rule.length < 2) {
              ruleArr[0] = Number(ruleArr[0]) - 1;
              ruleArr[1] = "";
            } else {
              ruleArr[0] = Number(ruleArr[0]) - 1;
              ruleArr[1] = Number(ruleArr[1]) - 1;
            }
            var reg = new RegExp("^-?\\d{" + ruleArr[0] + "," + ruleArr[1] + "}$");
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "between":
            if (!this.isNumber(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            var minMax = rule[i].checkRule.split(",");
            minMax[0] = Number(minMax[0]);
            minMax[1] = Number(minMax[1]);
            if (data[rule[i].name] > minMax[1] || data[rule[i].name] < minMax[0]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "intBetween":
            var reg = /^-?\d+$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            var minMax = rule[i].checkRule.split(",");
            minMax[0] = Number(minMax[0]);
            minMax[1] = Number(minMax[1]);
            if (data[rule[i].name] > minMax[1] || data[rule[i].name] < minMax[0]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "betweenD":
            var reg = /^-?\d+$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            var minMax = rule[i].checkRule.split(",");
            minMax[0] = Number(minMax[0]);
            minMax[1] = Number(minMax[1]);
            if (data[rule[i].name] > minMax[1] || data[rule[i].name] < minMax[0]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "doubleBetween":
            var reg = /^-?\d?.+\d+$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            var minMax = rule[i].checkRule.split(",");
            minMax[0] = Number(minMax[0]);
            minMax[1] = Number(minMax[1]);
            if (data[rule[i].name] > minMax[1] || data[rule[i].name] < minMax[0]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "betweenF":
            var reg = /^-?\d?.+\d+$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            var minMax = rule[i].checkRule.split(",");
            minMax[0] = Number(minMax[0]);
            minMax[1] = Number(minMax[1]);
            if (data[rule[i].name] > minMax[1] || data[rule[i].name] < minMax[0]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "doubleLength":
            var reg = new RegExp("^-?\\d+.\\d{" + rule[i].checkRule + "}$");
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "gt":
            if (data[rule[i].name] <= rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "gtAndSame":
            if (data[rule[i].name] < rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "lt":
            if (data[rule[i].name] >= rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "ltAndSame":
            if (data[rule[i].name] > rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "same":
            if (data[rule[i].name] != rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "notSame":
            if (data[rule[i].name] == rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "notsame":
            if (data[rule[i].name] == rule[i].checkRule) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "email":
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "phoneno":
            var reg = /^1[0-9]{10,10}$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "phone":
            var reg = /^1[0-9]{10,10}$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "zipcode":
            var reg = /^[0-9]{6}$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "reg":
            var reg = new RegExp(rule[i].checkRule);
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "in":
            if (rule[i].checkRule.indexOf(data[rule[i].name]) == -1) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "notnull":
            if (data[rule[i].name] == null || data[rule[i].name].length < 1) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "samewith":
            if (data[rule[i].name] != data[rule[i].checkRule]) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "numbers":
            var reg = new RegExp("^[0-9]{" + rule[i].checkRule + "}$");
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "url":
            var reg = /^(\w+:\/\/)?\w+(\.\w+)+.*$/;
            if (!reg.test(data[rule[i].name])) {
              this.error = rule[i].errorMsg;
              return false;
            }
            break;
          case "idCard":
            var idCardRes = idCardChecker.checkIdcard(data[rule[i].name]);
            if (idCardRes != "ok") {
              this.error = idCardRes;
              return false;
            }
            break;
        }
      }
      return true;
    },
    isNumber: function(checkVal) {
      checkVal = Number(checkVal);
      if (isNaN(checkVal)) {
        return false;
      }
      return true;
    }
  };
  const _sfc_main$L = {
    data() {
      return {
        phoneno: "",
        vcodeBtnName: "发送验证码",
        countNum: 120,
        countDownTimer: null
      };
    },
    methods: {
      loginbypwd() {
        uni.redirectTo({
          url: "/pages/login/pwd"
        });
      },
      getVCode: function() {
        var myreg = /^[1][0-9]{10}$/;
        if (!myreg.test(this.phoneno)) {
          uni.showToast({ title: "请正确填写手机号码", icon: "none" });
          return false;
        }
        if (this.vcodeBtnName != "发送验证码" && this.vcodeBtnName != "重新发送") {
          return;
        }
        this.vcodeBtnName = "发送中...";
        uni.showToast({ title: "短信已发送，请注意查收", icon: "none" });
        this.countNum = 60;
        this.countDownTimer = setInterval(() => {
          this.countDown();
        }, 1e3);
      },
      countDown: function() {
        if (this.countNum < 1) {
          clearInterval(this.countDownTimer);
          this.vcodeBtnName = "重新发送";
          return;
        }
        this.countNum--;
        this.vcodeBtnName = this.countNum + "秒重发";
      },
      submit: function(e) {
        formatAppLog("log", "at pages/login/login.vue:117", e.detail.value);
        var formData = e.detail.value;
        formatAppLog("log", "at pages/login/login.vue:121", formData);
        var rule = [
          { name: "phoneno", checkType: "phoneno", checkRule: "", errorMsg: "手机号码有误" },
          { name: "pwd", checkType: "string", checkRule: "4,6", errorMsg: "短信验证码错误" }
        ];
        var checkRes = graceChecker.check(formData, rule);
        if (checkRes) {
          uni.showToast({ title: "验证通过!", icon: "none" });
          uni.switchTab({
            url: "/pages/tabbar/index"
          });
        } else {
          uni.showToast({ title: graceChecker.error, icon: "none" });
        }
      }
    }
  };
  function _sfc_render$K(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_image = resolveEasycom(vue.resolveDynamicComponent("gui-image"), __easycom_0$a);
    return vue.openBlock(), vue.createElementBlock("view", { style: { "padding": "50rpx" } }, [
      vue.createElementVNode("view", { style: { "height": "150rpx" } }),
      vue.createElementVNode("view", { class: "gui-flex gui-row gui-justify-content-center" }, [
        vue.createVNode(_component_gui_image, {
          src: "/static/logo.png",
          width: 128
        })
      ]),
      vue.createElementVNode("view", { style: { "margin-top": "180rpx" } }, [
        vue.createElementVNode(
          "form",
          {
            onSubmit: _cache[3] || (_cache[3] = (...args) => $options.submit && $options.submit(...args))
          },
          [
            vue.createElementVNode("view", null, [
              vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" })
            ]),
            vue.createElementVNode("view", { class: "gui-border-b" }, [
              vue.withDirectives(vue.createElementVNode(
                "input",
                {
                  type: "number",
                  class: "gui-form-input",
                  "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => $data.phoneno = $event),
                  name: "phoneno",
                  placeholder: "您的手机号"
                },
                null,
                512
                /* NEED_PATCH */
              ), [
                [vue.vModelText, $data.phoneno]
              ])
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" })
            ]),
            vue.createElementVNode("view", { class: "gui-border-b gui-flex gui-row gui-nowrap gui-space-between gui-align-items-center" }, [
              vue.createElementVNode("input", {
                type: "number",
                class: "gui-form-input gui-flex1",
                name: "pwd",
                placeholder: "短信验证码"
              }),
              vue.createElementVNode(
                "text",
                {
                  class: "sendmsg gui-block gui-border-l gui-text-right gui-primary-color",
                  onClick: _cache[1] || (_cache[1] = (...args) => $options.getVCode && $options.getVCode(...args))
                },
                vue.toDisplayString($data.vcodeBtnName),
                1
                /* TEXT */
              )
            ]),
            vue.createElementVNode("view", {
              class: "gui-margin-top gui-flex gui-rows gui-space-between",
              "hover-class": "gui-tap"
            }, [
              vue.createElementVNode("text"),
              vue.createElementVNode("text", {
                class: "gui-text gui-color-gray gui-block gui-text-right",
                onClick: _cache[2] || (_cache[2] = (...args) => $options.loginbypwd && $options.loginbypwd(...args))
              }, "密码登录")
            ]),
            vue.createElementVNode("view", { style: { "margin-top": "38rpx" } }, [
              vue.createElementVNode("button", {
                type: "default",
                class: "gui-button gui-noborder",
                formType: "submit",
                style: { "border-radius": "50rpx", "background": "#170b1a" }
              }, [
                vue.createElementVNode("text", { class: "gui-color-white gui-button-text" }, "登 录")
              ])
            ])
          ],
          32
          /* NEED_HYDRATION */
        )
      ]),
      vue.createElementVNode("view", {
        class: "gui-flex gui-row gui-nowrap gui-align-items-center",
        style: { "margin-top": "80rpx" }
      }, [
        vue.createElementVNode("view", { class: "gui-title-line" }),
        vue.createElementVNode("text", {
          class: "gui-h6",
          style: { "padding-left": "50rpx", "padding-right": "50rpx" }
        }, "其他方式登录"),
        vue.createElementVNode("view", { class: "gui-title-line" })
      ]),
      vue.createElementVNode("view", { class: "gui-flex gui-row gui-nowrap gui-justify-content-center gui-margin-top" }, [
        vue.createElementVNode("view", {
          class: "other-login-icons",
          "hover-class": "gui-tap"
        }, [
          vue.createElementVNode("text", { class: "other-login-icons gui-icons gui-color-gray" }, "")
        ]),
        vue.createElementVNode("view", {
          class: "other-login-icons",
          "hover-class": "gui-tap"
        }, [
          vue.createElementVNode("text", { class: "other-login-icons gui-icons gui-color-gray" }, "")
        ])
      ])
    ]);
  }
  const PagesLoginLogin = /* @__PURE__ */ _export_sfc(_sfc_main$L, [["render", _sfc_render$K], ["__scopeId", "data-v-e4e4508d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/login/login.vue"]]);
  const _sfc_main$K = {
    data() {
      return {};
    },
    methods: {
      forgetPwd: function() {
        formatAppLog("log", "at pages/login/pwd.vue:90", "请自行完善代码");
      },
      loginbymsg: function() {
        uni.redirectTo({
          url: "/pages/login/login"
        });
      },
      submit: function(e) {
        var formData = e.detail.value;
        var rule = [
          { name: "username", checkType: "string", checkRule: "5,50", errorMsg: "登录账户输入有误" },
          { name: "password", checkType: "string", checkRule: "6,100", errorMsg: "登录密码至少6个字符" }
        ];
        var checkRes = graceChecker.check(formData, rule);
        if (checkRes) {
          uni.showToast({ title: "验证通过!", icon: "none" });
          uni.switchTab({
            url: "/pages/tabbar/index"
          });
        } else {
          uni.showToast({ title: graceChecker.error, icon: "none" });
        }
      }
    }
  };
  function _sfc_render$J(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_image = resolveEasycom(vue.resolveDynamicComponent("gui-image"), __easycom_0$a);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, null, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { style: { "padding": "50rpx" } }, [
          vue.createElementVNode("view", { style: { "height": "150rpx" } }),
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-justify-content-center" }, [
            vue.createVNode(_component_gui_image, {
              src: "/static/logo.png",
              width: 128
            })
          ]),
          vue.createElementVNode("view", { style: { "margin-top": "180rpx" } }, [
            vue.createElementVNode(
              "form",
              {
                onSubmit: _cache[2] || (_cache[2] = (...args) => $options.submit && $options.submit(...args))
              },
              [
                vue.createElementVNode("view", null, [
                  vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" })
                ]),
                vue.createElementVNode("view", { class: "gui-border-b" }, [
                  vue.createElementVNode("input", {
                    type: "text",
                    class: "gui-form-input",
                    name: "username",
                    placeholder: "登录账户"
                  })
                ]),
                vue.createElementVNode("view", { class: "gui-margin-top" }, [
                  vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" })
                ]),
                vue.createElementVNode("view", { class: "gui-border-b" }, [
                  vue.createElementVNode("input", {
                    type: "password",
                    class: "gui-form-input",
                    name: "password",
                    placeholder: "密码"
                  })
                ]),
                vue.createElementVNode("view", {
                  class: "gui-margin-top gui-flex gui-rows gui-space-between",
                  "hover-class": "gui-tap"
                }, [
                  vue.createElementVNode("text", {
                    class: "gui-text gui-color-gray gui-block",
                    onClick: _cache[0] || (_cache[0] = (...args) => $options.forgetPwd && $options.forgetPwd(...args))
                  }, "忘记密码"),
                  vue.createElementVNode("text", {
                    class: "gui-text gui-color-gray gui-block gui-text-right",
                    onClick: _cache[1] || (_cache[1] = (...args) => $options.loginbymsg && $options.loginbymsg(...args))
                  }, "短信登录")
                ]),
                vue.createElementVNode("view", { style: { "margin-top": "38rpx" } }, [
                  vue.createElementVNode("button", {
                    type: "default",
                    class: "gui-button gui-noborder",
                    formType: "submit",
                    style: { "border-radius": "50rpx", "background": "#170b1a" }
                  }, [
                    vue.createElementVNode("text", { class: "gui-color-white gui-button-text" }, "登 录")
                  ])
                ])
              ],
              32
              /* NEED_HYDRATION */
            )
          ]),
          vue.createElementVNode("view", {
            class: "gui-flex gui-rows gui-nowrap gui-align-items-center",
            style: { "margin-top": "80rpx" }
          }, [
            vue.createElementVNode("view", { class: "gui-title-line" }),
            vue.createElementVNode("text", {
              class: "gui-h6",
              style: { "padding-left": "50rpx", "padding-right": "50rpx" }
            }, "其他方式登录"),
            vue.createElementVNode("view", { class: "gui-title-line" })
          ]),
          vue.createElementVNode("view", { class: "gui-flex gui-rows gui-nowrap gui-justify-content-center gui-margin-top" }, [
            vue.createElementVNode("view", {
              class: "other-login-icons",
              "hover-class": "gui-tap"
            }, [
              vue.createElementVNode("text", { class: "other-login-icons gui-icons gui-color-gray" }, "")
            ]),
            vue.createElementVNode("view", {
              class: "other-login-icons",
              "hover-class": "gui-tap"
            }, [
              vue.createElementVNode("text", { class: "other-login-icons gui-icons gui-color-gray" }, "")
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesLoginPwd = /* @__PURE__ */ _export_sfc(_sfc_main$K, [["render", _sfc_render$J], ["__scopeId", "data-v-8032d478"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/login/pwd.vue"]]);
  const _sfc_main$J = {
    props: {
      fsize: {
        type: String,
        default: "fz14"
      },
      title: {
        type: String,
        default: "历史记录"
      },
      ctitle: {
        type: String,
        default: "查看更多"
      },
      showmore: {
        type: Boolean,
        default: true
      }
    },
    name: "nav",
    data() {
      return {};
    },
    methods: {
      handleClick() {
        this.$emits("clear");
      }
    }
  };
  function _sfc_render$I(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-flex gui-space-between ql-align-center mb15" }, [
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["gui-color-black1 fz16 fb", [$props.fsize]])
        },
        vue.toDisplayString($props.title),
        3
        /* TEXT, CLASS */
      ),
      $props.showmore ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "fz11 gui-color-gray",
        onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
      }, [
        vue.createTextVNode(
          vue.toDisplayString($props.ctitle),
          1
          /* TEXT */
        ),
        vue.createElementVNode("text", { class: "gui-icons ml2 fb" }, "")
      ])) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_1$7 = /* @__PURE__ */ _export_sfc(_sfc_main$J, [["render", _sfc_render$I], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/subitem.vue"]]);
  const _sfc_main$I = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 3,
          name: "设计创造"
        }, {
          id: 2,
          name: "新媒体电商"
        }, {
          id: 3,
          name: "人工智能"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render$H(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_advert_index = resolveEasycom(vue.resolveDynamicComponent("cc-advert-index"), __easycom_3$5);
    const _component_cc_title_subitem = resolveEasycom(vue.resolveDynamicComponent("cc-title-subitem"), __easycom_1$7);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white" }, [
            vue.createVNode(_component_logo, {
              showback: "",
              onBack: _ctx.handleBack,
              showcart: "",
              title: "精品课程",
              btntext: ""
            }, null, 8, ["onBack"])
          ]),
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createElementVNode("view", null, [
          vue.createVNode(_component_cc_advert_index, { showspace: false })
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-normal" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item flex justify-content-between mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "ml15" }, [
                      vue.createElementVNode("view", { class: "titbox" }, [
                        vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                        vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                        vue.createElementVNode("text", { class: "hottag ml5" }, "最热"),
                        vue.createElementVNode("text", { class: "ml5 fb" }, "邢不行-Python股票量化投资课程")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                        vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                      ])
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCategoryCourse = /* @__PURE__ */ _export_sfc(_sfc_main$I, [["render", _sfc_render$H], ["__scopeId", "data-v-d2a1a735"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/course.vue"]]);
  const _sfc_main$H = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 3,
          name: "设计创造"
        }, {
          id: 2,
          name: "新媒体电商"
        }, {
          id: 3,
          name: "人工智能"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render$G(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_advert_index = resolveEasycom(vue.resolveDynamicComponent("cc-advert-index"), __easycom_3$5);
    const _component_cc_title_subitem = resolveEasycom(vue.resolveDynamicComponent("cc-title-subitem"), __easycom_1$7);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white" }, [
            vue.createVNode(_component_logo, {
              showback: "",
              onBack: _ctx.handleBack,
              showcart: "",
              title: "资源下载",
              btntext: ""
            }, null, 8, ["onBack"])
          ]),
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createElementVNode("view", null, [
          vue.createVNode(_component_cc_advert_index, { showspace: false })
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-normal" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item flex justify-content-between mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "ml15" }, [
                      vue.createElementVNode("view", { class: "titbox" }, [
                        vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                        vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                        vue.createElementVNode("text", { class: "hottag ml5" }, "最热"),
                        vue.createElementVNode("text", { class: "ml5 fb" }, "邢不行-Python股票量化投资课程")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                        vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                      ])
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCategoryDownloads = /* @__PURE__ */ _export_sfc(_sfc_main$H, [["render", _sfc_render$G], ["__scopeId", "data-v-e77978a2"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/downloads.vue"]]);
  const _sfc_main$G = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 3,
          name: "设计创造"
        }, {
          id: 2,
          name: "新媒体电商"
        }, {
          id: 3,
          name: "人工智能"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render$F(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_advert_index = resolveEasycom(vue.resolveDynamicComponent("cc-advert-index"), __easycom_3$5);
    const _component_cc_title_subitem = resolveEasycom(vue.resolveDynamicComponent("cc-title-subitem"), __easycom_1$7);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white" }, [
            vue.createVNode(_component_logo, {
              showback: "",
              onBack: _ctx.handleBack,
              showcart: "",
              title: "实战小册",
              btntext: ""
            }, null, 8, ["onBack"])
          ]),
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createElementVNode("view", null, [
          vue.createVNode(_component_cc_advert_index, { showspace: false })
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-normal" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item flex justify-content-between mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "ml15" }, [
                      vue.createElementVNode("view", { class: "titbox" }, [
                        vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                        vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                        vue.createElementVNode("text", { class: "hottag ml5" }, "最热"),
                        vue.createElementVNode("text", { class: "ml5 fb" }, "邢不行-Python股票量化投资课程")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                        vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                      ])
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCategoryNote = /* @__PURE__ */ _export_sfc(_sfc_main$G, [["render", _sfc_render$F], ["__scopeId", "data-v-ccbe5444"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/note.vue"]]);
  const _sfc_main$F = {
    data() {
      return {
        title: "",
        opid: 1,
        page: 1
      };
    },
    onLoad(options) {
      this.title = options.title;
      this.opid = options.id;
      this.page = options.page;
    },
    methods: {
      handleBack() {
        uni.navigateBack({
          delta: 1
        });
      },
      toLink(url, flag) {
        if (flag) {
          uni.switchTab({ url });
        } else {
          uni.navigateTo({ url });
        }
      },
      toUser(msg) {
        formatAppLog("log", "at pages/category/rule.vue:82", "联系客服。。。。");
      }
    }
  };
  function _sfc_render$E(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createElementVNode("view", null, [
            vue.createElementVNode("view", { class: "fb fz18 gui-padding-m gui-bg-gray" }, "推广目的"),
            vue.createElementVNode("view", { class: "mt15" }, "成城学习平台推广分销软件指具备产品销售咨询和服务能力，与平台共同拓展市场，推动更多客户学习和提升自我。")
          ]),
          vue.createElementVNode("view", { class: "mt20" }, [
            vue.createElementVNode("view", { class: "fb fz18 gui-padding-m gui-bg-gray" }, "推广条件"),
            vue.createElementVNode("view", { class: "mt15" }, [
              vue.createElementVNode("view", { class: "mt5" }, "1：成城学习平台推广分销软件是一款没有门槛的推广软件。现有的推广的平台提供非常精品的课程和资源。"),
              vue.createElementVNode("view", { class: "mt5" }, "2：如果你想在成城推广平台上获取一定的收益。可以让你足不出户就可以获取到不错的额外收入。"),
              vue.createElementVNode("view", { class: "mt5" }, "3：还等待什么呢？赶紧申请吧！"),
              vue.createElementVNode("button", {
                type: "default",
                class: "gui-button gui-bg-primary gui-noborder mt20",
                onClick: _cache[0] || (_cache[0] = ($event) => $options.toLink("/pages/user/bind"))
              }, [
                vue.createElementVNode("text", { class: "gui-icons gui-color-white gui-button-text" }, " 开始提交申请")
              ])
            ])
          ]),
          vue.createElementVNode("view", { class: "mt20" }, [
            vue.createElementVNode("view", { class: "fb fz18 gui-padding-m gui-bg-gray" }, "推广佣金获取"),
            vue.createElementVNode("view", { class: "mt15" }, [
              vue.createElementVNode("view", null, "1：成城学习平台推广获取的收益需要推广者自己在：“我的收益”中进行提现经过平台的财务审批才可以获得。"),
              vue.createElementVNode("view", { class: "mt5" }, "2：平台在5个工作日内会进行核查和确认，然后与你取得联系。然后在打款。所以填写的个人信息和收款账号必须准确无误。"),
              vue.createElementVNode("view", { class: "mt5" }, "3：一旦放款，无论收款人正确与否信息正确就代表放款成功。平台不予以承担后续的任何责任。"),
              vue.createElementVNode("view", { class: "mt5" }, "4：平台提现的规则会根据平台所在公司注册地的税收进行扣减。如果提现金额超过2万元以上，建议联系客服进行协商处理。"),
              vue.createElementVNode("button", {
                type: "default",
                class: "gui-button gui-bg-primary gui-noborder mt20",
                onClick: _cache[1] || (_cache[1] = ($event) => $options.toLink("/pages/tabbar/income", true))
              }, [
                vue.createElementVNode("text", { class: "gui-icons gui-color-white gui-button-text" }, " 点击前往提现")
              ])
            ])
          ]),
          vue.createElementVNode("view", { class: "mt20" }, [
            vue.createElementVNode("view", { class: "fb fz18 gui-padding-m gui-bg-gray" }, "推广注意"),
            vue.createElementVNode("view", { class: "mt15" }, [
              vue.createElementVNode("view", { class: "mt5" }, "1：成城学习平台推广分销软件是一款没有门槛的推广软件。现有的推广的平台提供非常精品的课程和资源。"),
              vue.createElementVNode("view", { class: "mt5" }, "2：如果发现推广者有恶意或者造谣对平台产生任何的影响都会直接冻结体现金额，平台的权限直接取缔，并追究法律的责任。"),
              vue.createElementVNode("view", { class: "mt5" }, "3：也欢迎大家举报一些恶意和攻击平台不法分子，举报成功平台会给予一定的奖励。咱们一起共勉，共同创造一个美好的平台和家园。"),
              vue.createElementVNode("button", {
                type: "default",
                class: "gui-button gui-bg-primary gui-noborder mt20",
                onClick: _cache[2] || (_cache[2] = ($event) => $options.toUser("/pages/tabbar/income"))
              }, [
                vue.createElementVNode("text", { class: "gui-icons gui-color-white gui-button-text" }, " 联系客服")
              ])
            ])
          ]),
          vue.createElementVNode("view", { style: { "height": "30rpx" } })
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCategoryRule = /* @__PURE__ */ _export_sfc(_sfc_main$F, [["render", _sfc_render$E], ["__file", "D:/App/分销系统/cc_fenxiao/pages/category/rule.vue"]]);
  const _sfc_main$E = {
    data() {
      return {
        chapterList: [],
        lessonIndex: 0,
        chapterIndex: 0
      };
    },
    created: function() {
      this.chapterList = [{ title: "发刊词", expand: true, id: 1 }, { title: "发刊词", expand: true, id: 4 }, { title: "发刊词", expand: true, id: 3 }];
    },
    methods: {
      handleChapter(item, index) {
        this.chapterIndex = index;
        item.expand = !item.expand;
      },
      handleLesson(citem, index, cindex) {
        this.chapterIndex = index;
        this.lessonIndex = cindex;
      }
    }
  };
  function _sfc_render$D(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(
      _component_gui_page,
      {
        fullPage: true,
        ref: "guiPage"
      },
      {
        gBody: vue.withCtx(() => [
          vue.createElementVNode("view", { class: "gui-flex1 gui-flex gui-column" }, [
            vue.createCommentVNode(' view style="height:200rpx;" class="gui-bg-blue"></view '),
            vue.createElementVNode("view", { class: "gui-flex1 gui-relative cc-chapterbox" }, [
              (vue.openBlock(true), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList($data.chapterList, (item, index) => {
                  return vue.openBlock(), vue.createElementBlock("view", {
                    class: "chapter-items",
                    onClick: ($event) => $options.handleChapter(item, index)
                  }, [
                    vue.createElementVNode("view", { class: "titbox flex justify-content-between align-items gui-padding gui-bg-gray" }, [
                      vue.createElementVNode("view", { class: "tit" }, "发刊词"),
                      item.expand ? (vue.openBlock(), vue.createElementBlock("view", { key: 0 }, [
                        vue.createElementVNode("text", { class: "gui-icons" }, "")
                      ])) : (vue.openBlock(), vue.createElementBlock("view", { key: 1 }, [
                        vue.createElementVNode("text", { class: "gui-icons" }, "")
                      ]))
                    ]),
                    item.expand ? (vue.openBlock(), vue.createElementBlock("view", {
                      key: 0,
                      class: "lesson-box gui-padding-m"
                    }, [
                      (vue.openBlock(), vue.createElementBlock(
                        vue.Fragment,
                        null,
                        vue.renderList(5, (num, cindex) => {
                          return vue.createElementVNode("view", {
                            class: vue.normalizeClass(["lesson-items flex justify-content-between align-items gui-padding-m", [$data.lessonIndex == cindex && $data.chapterIndex == index ? "green" : ""]]),
                            onClick: vue.withModifiers(($event) => $options.handleLesson(item, index, cindex), ["stop"])
                          }, [
                            vue.createElementVNode("view", { class: "flex align-items" }, [
                              vue.createElementVNode("view", { class: "tag" }, "视频"),
                              vue.createElementVNode("view", { class: "tit" }, "1.搜索，10倍提升各种效率的技能")
                            ]),
                            $data.lessonIndex == cindex && $data.chapterIndex == index ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "fz12 gui-color-green"
                            }, "播放中")) : (vue.openBlock(), vue.createElementBlock("view", { key: 1 }, [
                              (vue.openBlock(), vue.createElementBlock("view", { key: 1 }, [
                                vue.createElementVNode("text", { class: "gui-icons" }, "")
                              ]))
                            ]))
                          ], 10, ["onClick"]);
                        }),
                        64
                        /* STABLE_FRAGMENT */
                      ))
                    ])) : vue.createCommentVNode("v-if", true)
                  ], 8, ["onClick"]);
                }),
                256
                /* UNKEYED_FRAGMENT */
              ))
            ])
          ])
        ]),
        _: 1
        /* STABLE */
      },
      512
      /* NEED_PATCH */
    );
  }
  const __easycom_2$5 = /* @__PURE__ */ _export_sfc(_sfc_main$E, [["render", _sfc_render$D], ["__scopeId", "data-v-7974a0ca"], ["__file", "D:/App/分销系统/cc_fenxiao/components/course/chapter.vue"]]);
  const _sfc_main$D = {
    name: "gui-stags",
    props: {
      width: { type: Number, default: 0 },
      size: { type: Number, default: 26 },
      lineHeight: { type: Number, default: 2 },
      padding: { type: Number, default: 15 },
      margin: { type: Number, default: 15 },
      defaultClass: {
        type: Array,
        default: function() {
          return ["gui-bg-gray", "gui-dark-bg-level-1", "gui-primary-text", "gui-dark-text-level-5"];
        }
      },
      checkedClass: {
        type: Array,
        default: function() {
          return ["gui-bg-primary", "gui-dark-bg", "gui-color-white"];
        }
      },
      borderRadius: { type: Number, default: 6 },
      data: { type: Array, default: function() {
        return [];
      } },
      tags: { type: Array, default: function() {
        return [];
      } },
      type: { type: String, default: "radio" }
    },
    data() {
      return {
        tagsIn: []
      };
    },
    created: function() {
      this.tagsIn = this.tags;
    },
    watch: {
      tags: function(val) {
        this.tagsIn = val;
      }
    },
    methods: {
      tapme: function(idx) {
        if (this.type == "radio") {
          if (this.tagsIn[idx].checked) {
            this.tagsIn[idx].checked = false;
            this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
            this.$emit("change", -1, this.tagsIn);
          } else {
            for (let i = 0; i < this.tagsIn.length; i++) {
              this.tagsIn[i].checked = false;
              this.tagsIn.splice(i, 1, this.tagsIn[i]);
            }
            this.tagsIn[idx].checked = true;
            this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
            this.$emit("change", this.tagsIn[idx], this.tagsIn);
          }
        } else if (this.type == "checkbox") {
          this.tagsIn[idx].checked = !this.tagsIn[idx].checked;
          this.tagsIn.splice(idx, 1, this.tagsIn[idx]);
          var sedArr = [];
          for (let i = 0; i < this.tagsIn.length; i++) {
            if (this.tagsIn[i].checked) {
              sedArr.push(i);
            }
          }
          this.$emit("change", sedArr, this.tagsIn);
        } else {
          this.tagsIn.splice(idx, 1);
          this.$emit("change", this.tagsIn);
        }
      }
    },
    emits: ["change"]
  };
  function _sfc_render$C(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-flex gui-wrap gui-row" }, [
      $props.type != "remove" ? (vue.openBlock(true), vue.createElementBlock(
        vue.Fragment,
        { key: 0 },
        vue.renderList($data.tagsIn, (tag, idx) => {
          return vue.openBlock(), vue.createElementBlock("text", {
            key: idx,
            class: vue.normalizeClass(["gui-block gui-ellipsis", tag.checked ? $props.checkedClass : $props.defaultClass]),
            style: vue.normalizeStyle({
              width: $props.width == 0 ? "" : $props.width + "rpx",
              paddingLeft: $props.padding + "rpx",
              paddingRight: $props.padding + "rpx",
              lineHeight: $props.size * $props.lineHeight + "rpx",
              height: $props.size * $props.lineHeight + "rpx",
              fontSize: $props.size + "rpx",
              borderRadius: $props.borderRadius + "rpx",
              marginRight: $props.margin + "rpx",
              marginBottom: $props.margin + "rpx"
            }),
            onClick: ($event) => $options.tapme(idx)
          }, vue.toDisplayString(tag.text), 15, ["onClick"]);
        }),
        128
        /* KEYED_FRAGMENT */
      )) : vue.createCommentVNode("v-if", true),
      $props.type == "remove" ? (vue.openBlock(true), vue.createElementBlock(
        vue.Fragment,
        { key: 1 },
        vue.renderList($data.tagsIn, (tag, idx) => {
          return vue.openBlock(), vue.createElementBlock("view", {
            class: vue.normalizeClass(["gui-flex gui-row gui-space-between gui-align-items-center", $props.defaultClass]),
            key: idx,
            style: vue.normalizeStyle({
              width: $props.width == 0 ? "" : $props.width + "rpx",
              paddingLeft: $props.padding + "rpx",
              paddingRight: $props.padding + "rpx",
              borderRadius: $props.borderRadius + "rpx",
              marginRight: $props.margin + "rpx",
              marginBottom: $props.margin + "rpx"
            }),
            onClick: ($event) => $options.tapme(idx)
          }, [
            vue.createElementVNode(
              "text",
              {
                class: "gui-block gui-ellipsis",
                style: vue.normalizeStyle({
                  lineHeight: $props.size * $props.lineHeight + "rpx",
                  height: $props.size * $props.lineHeight + "rpx",
                  fontSize: $props.size + "rpx"
                })
              },
              vue.toDisplayString(tag.text),
              5
              /* TEXT, STYLE */
            ),
            vue.createElementVNode(
              "text",
              {
                class: "gui-block gui-icons gui-tags-remove-btn",
                style: vue.normalizeStyle({
                  lineHeight: $props.size * $props.lineHeight + "rpx",
                  height: $props.size * $props.lineHeight + "rpx",
                  fontSize: $props.size + "rpx"
                })
              },
              "",
              4
              /* STYLE */
            )
          ], 14, ["onClick"]);
        }),
        128
        /* KEYED_FRAGMENT */
      )) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_3$1 = /* @__PURE__ */ _export_sfc(_sfc_main$D, [["render", _sfc_render$C], ["__scopeId", "data-v-0c9885d7"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-stags.vue"]]);
  const _sfc_main$C = {
    name: "gui-step-box",
    props: {
      width: { type: String, default: "200rpx" },
      value: { type: Number, default: 0 },
      step: { type: Number, default: 1 },
      maxNum: { type: Number, default: 9999 },
      minNum: { type: Number, default: 0 },
      buttonClass: { type: Array, default: function() {
        return ["gui-step-box-button", "gui-color-gray"];
      } },
      inputClass: { type: Array, default: function() {
        return ["gui-step-box-input", "gui-border-radius"];
      } },
      disabled: { type: Boolean, default: false },
      index: { type: Number, default: 0 },
      datas: { type: Array, default: function() {
        return [];
      } },
      decimal: { type: Number, default: 2 }
    },
    data() {
      return {
        inputNumber: 0,
        callbackNumber: 0
      };
    },
    created: function() {
      this.inputNumber = Number(this.value);
    },
    watch: {
      value: function(val, vo) {
        this.inputNumber = Number(val);
      },
      inputNumber: function(val, vo) {
        if (val == "") {
          return;
        }
        val = Number(val);
        if (isNaN(val)) {
          setTimeout(() => {
            this.inputNumber = Number(vo);
          }, 200);
          return;
        }
        var newVal = this.decimalVal(val);
        if (newVal != val) {
          setTimeout(() => {
            this.inputNumber = Number(newVal);
          }, 200);
          return;
        }
        if (val > this.maxNum) {
          setTimeout(() => {
            this.inputNumber = this.maxNum;
          }, 200);
          return;
        }
        if (val < this.minNum) {
          setTimeout(() => {
            this.inputNumber = this.minNum;
          }, 200);
          return;
        }
      }
    },
    methods: {
      add: function() {
        var newVal = Number(this.inputNumber) + Number(this.step);
        newVal = this.decimalVal(newVal);
        if (newVal > this.maxNum) {
          return;
        }
        this.inputNumber = Number(newVal);
        setTimeout(() => {
          this.$emit("change", [this.inputNumber, this.index, this.datas]);
        }, 300);
      },
      reduce: function() {
        var newVal = Number(this.inputNumber) - Number(this.step);
        newVal = this.decimalVal(newVal);
        if (newVal < this.minNum) {
          return;
        }
        this.inputNumber = newVal;
        setTimeout(() => {
          this.$emit("change", [this.inputNumber, this.index, this.datas]);
        }, 300);
      },
      inputval: function(e) {
        this.inputNumber = e.detail.value;
        setTimeout(() => {
          this.$emit("change", [this.inputNumber, this.index, this.datas]);
        }, 300);
      },
      decimalVal: function(val) {
        var isDecimal = String(val).indexOf(".");
        if (isDecimal != -1) {
          val = val.toFixed(this.decimal);
          var valArr = String(val).split(".");
          if (valArr[1].length > this.decimal) {
            valArr[1] = valArr[1].substr(0, this.decimal);
            val = Number(valArr.join("."));
          }
        }
        return val;
      }
    },
    emits: ["change"]
  };
  function _sfc_render$B(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "gui-flex gui-rows gui-nowrap gui-align-items-center",
        style: vue.normalizeStyle({ width: $props.width })
      },
      [
        vue.createElementVNode("view", { "hover-class": "gui-tap" }, [
          vue.createElementVNode(
            "text",
            {
              class: vue.normalizeClass(["gui-block gui-text-center", $props.buttonClass]),
              onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.reduce && $options.reduce(...args), ["stop"]))
            },
            "-",
            2
            /* CLASS */
          )
        ]),
        vue.withDirectives(vue.createElementVNode("input", {
          class: vue.normalizeClass(["gui-form-input gui-text-center gui-flex1", $props.inputClass]),
          disabled: $props.disabled,
          "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.inputNumber = $event),
          type: "digit",
          onBlur: _cache[2] || (_cache[2] = (...args) => $options.inputval && $options.inputval(...args))
        }, null, 42, ["disabled"]), [
          [vue.vModelText, $data.inputNumber]
        ]),
        vue.createElementVNode("view", { "hover-class": "gui-tap" }, [
          vue.createElementVNode(
            "text",
            {
              class: vue.normalizeClass(["gui-block gui-text-center", $props.buttonClass]),
              onClick: _cache[3] || (_cache[3] = vue.withModifiers((...args) => $options.add && $options.add(...args), ["stop"]))
            },
            "+",
            2
            /* CLASS */
          )
        ])
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main$C, [["render", _sfc_render$B], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-step-box.vue"]]);
  var startTag = /^<([-A-Za-z0-9_]+)((?:\s+[a-zA-Z_:][-a-zA-Z0-9_:.]*(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/;
  var endTag = /^<\/([-A-Za-z0-9_]+)[^>]*>/;
  var attr = /([a-zA-Z_:][-a-zA-Z0-9_:.]*)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g;
  var empty = makeMap("area,base,basefont,br,col,frame,hr,img,input,link,meta,param,embed,command,keygen,source,track,wbr,\n,	,	");
  var block = makeMap("a,address,article,applet,aside,audio,blockquote,button,canvas,center,dd,del,dir,div,dl,dt,fieldset,figcaption,figure,footer,form,frameset,h1,h2,h3,h4,h5,h6,header,hgroup,hr,iframe,isindex,li,map,menu,noframes,noscript,object,ol,output,p,pre,section,script,table,tbody,td,tfoot,th,thead,tr,ul,video");
  var inline = makeMap("abbr,acronym,applet,b,basefont,bdo,big,br,button,cite,code,del,dfn,em,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,span,strike,strong,sub,sup,textarea,tt,u,var");
  var closeSelf = makeMap("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr");
  var fillAttrs = makeMap("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected");
  var special = makeMap("script,style");
  function HTMLParser(html, handler) {
    var index;
    var chars;
    var match;
    var stack = [];
    var last = html;
    stack.last = function() {
      return this[this.length - 1];
    };
    while (html) {
      chars = true;
      if (!stack.last() || !special[stack.last()]) {
        if (html.indexOf("<!--") == 0) {
          index = html.indexOf("-->");
          if (index >= 0) {
            if (handler.comment) {
              handler.comment(html.substring(4, index));
            }
            html = html.substring(index + 3);
            chars = false;
          }
        } else if (html.indexOf("</") == 0) {
          match = html.match(endTag);
          if (match) {
            html = html.substring(match[0].length);
            match[0].replace(endTag, parseEndTag);
            chars = false;
          }
        } else if (html.indexOf("<") == 0) {
          match = html.match(startTag);
          if (match) {
            html = html.substring(match[0].length);
            match[0].replace(startTag, parseStartTag);
            chars = false;
          }
        }
        if (chars) {
          index = html.indexOf("<");
          var text = index < 0 ? html : html.substring(0, index);
          html = index < 0 ? "" : html.substring(index);
          if (handler.chars) {
            handler.chars(text);
          }
        }
      } else {
        html = html.replace(new RegExp("([\\s\\S]*?)</" + stack.last() + "[^>]*>"), function(all, text2) {
          text2 = text2.replace(/<!--([\s\S]*?)-->|<!\[CDATA\[([\s\S]*?)]]>/g, "$1$2");
          if (handler.chars) {
            handler.chars(text2);
          }
          return "";
        });
        parseEndTag("", stack.last());
      }
      if (html == last) {
        throw "Parse Error: " + html;
      }
      last = html;
    }
    parseEndTag();
    function parseStartTag(tag, tagName, rest, unary) {
      tagName = tagName.toLowerCase();
      if (block[tagName]) {
        while (stack.last() && inline[stack.last()]) {
          parseEndTag("", stack.last());
        }
      }
      if (closeSelf[tagName] && stack.last() == tagName) {
        parseEndTag("", tagName);
      }
      unary = empty[tagName] || !!unary;
      if (!unary) {
        stack.push(tagName);
      }
      if (handler.start) {
        var attrs = [];
        rest.replace(attr, function(match2, name) {
          var value = arguments[2] ? arguments[2] : arguments[3] ? arguments[3] : arguments[4] ? arguments[4] : fillAttrs[name] ? name : "";
          attrs.push({
            name,
            value,
            escaped: value.replace(/(^|[^\\])"/g, '$1\\"')
            // "
          });
        });
        if (handler.start) {
          handler.start(tagName, attrs, unary);
        }
      }
    }
    function parseEndTag(tag, tagName) {
      if (!tagName) {
        var pos = 0;
      } else {
        for (var pos = stack.length - 1; pos >= 0; pos--) {
          if (stack[pos] == tagName) {
            break;
          }
        }
      }
      if (pos >= 0) {
        for (var i = stack.length - 1; i >= pos; i--) {
          if (handler.end) {
            handler.end(stack[i]);
          }
        }
        stack.length = pos;
      }
    }
  }
  function makeMap(str) {
    var obj = {};
    var items = str.split(",");
    for (var i = 0; i < items.length; i++) {
      obj[items[i]] = true;
    }
    return obj;
  }
  function removeDOCTYPE(html) {
    return html.replace(/<\?xml.*\?>\n/, "").replace(/<!doctype.*>\n/, "").replace(/<!DOCTYPE.*>\n/, "");
  }
  function parseAttrs(attrs) {
    return attrs.reduce(function(pre, attr2) {
      var value = attr2.value;
      var name = attr2.name;
      if (value.indexOf("width") != -1) {
        value += "; width:100%; height:auto;";
      }
      if (pre[name]) {
        pre[name] = pre[name] + " " + value;
      } else {
        pre[name] = value;
      }
      return pre;
    }, {});
  }
  function parseHtml(html) {
    html = removeDOCTYPE(html);
    html = html.replace(/\n/g, "");
    html = html.replace(/\t/g, "");
    var stacks = [];
    var results = {
      node: "root",
      children: []
    };
    HTMLParser(html, {
      start: function start(tag, attrs, unary) {
        var node = { name: tag };
        if (attrs.length !== 0) {
          node.attrs = parseAttrs(attrs);
        }
        if (unary) {
          var parent = stacks[0] || results;
          if (!parent.children) {
            parent.children = [];
          }
          parent.children.push(node);
        } else {
          stacks.unshift(node);
        }
      },
      end: function end(tag) {
        var node = stacks.shift();
        if (node.name !== tag) {
          formatAppLog("error", "at Grace6/js/parserHTML.js:210", "invalid state: mismatch end tag");
        }
        if (stacks.length === 0) {
          results.children.push(node);
        } else {
          var parent = stacks[0];
          if (!parent.children) {
            parent.children = [];
          }
          parent.children.push(node);
        }
      },
      chars: function chars(text) {
        var node = {
          type: "text",
          text
        };
        if (stacks.length === 0) {
          results.children.push(node);
        } else {
          var parent = stacks[0];
          if (!parent.children) {
            parent.children = [];
          }
          parent.children.push(node);
        }
      },
      comment: function comment(text) {
        var node = {
          node: "comment",
          text
        };
        var parent = stacks[0];
        if (!parent.children) {
          parent.children = [];
        }
        parent.children.push(node);
      }
    });
    return results.children;
  }
  const parserHtml = {
    parserHTML: parseHtml
  };
  var face$1 = "https://images.unsplash.com/photo-1663717249250-804cb861ed74?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw5M3x8fGVufDB8fHx8&auto=format&fit=crop&w=100&q=80";
  const _sfc_main$B = {
    data() {
      return {
        closemask: true,
        nodeArr: [],
        nodes: `<img src='/static/imgs/bg2.jpg'/><img src='/static/imgs/bg3.webp'/><img src='/static/imgs/bg4.webp'/><h1>23423</h1><p class="m-introImage u-coursebriefintro-content">
			<img src="https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg"/>
                        <img src="https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg" data-src="//study-image.nosdn.127.net/4c49087e146c43b6b8de5b1c989f1597.jpg?imageView&amp;quality=100&amp;type=webp" class="intro-img" id="auto-id-1707016924515" style="width: 100%;">
                    </p><p class="txt f-course-grey u-coursebriefintro-content ">
					<img src="https://img30.360buyimg.com/sku/jfs/t22021/327/2281785192/48707/57806074/5b4f1579Nae7adb49.jpg"/>
					<img src="https://img30.360buyimg.com/sku/jfs/t21682/256/2344553276/204456/cf7a2ddb/5b4ffbbfN48c54307.jpg"/>
          <span>2022年8月份开始，课程第5次更新已经开始，这次会颠覆之前所有课程，全面升级玩法，即将涨价！<br>具体可看详情介绍<br>现在的自媒体短视频，就像淘宝刚开始时期，特别好做，但很多人不做或不知道怎么做，确实，现在入局自媒体短视频还不晚，再等等就肯定会晚。<br>国人都感觉到了，现在是新媒体时代，个人自媒体和短视频兴起，头条，抖音，快手&nbsp;直播&nbsp;网红&nbsp;带货时代早已到来，一条带货视频一晚上带来百万收益，一场直播盈利百万&nbsp;千万&nbsp;甚至过亿，不管你现在是不是认可，是实实在在发生在我们身边的事情！有人通过新媒体短视频正闷声发大财，月入百万&nbsp;千万甚至更多，有人却还根本没有找到进入的大门。<br>当你想做自媒体和短视频创业时，有没有这种感觉？①想做新媒体和短视频，却不知道从哪下手?②学习资料太多，不知道该学什么?③新媒体短视频做起来迷茫，不懂规则，内容不推荐，阅读量，播放量低，不会写爆款文章，写标题，拍视频，剪辑视频...<br>不用发愁了，来学习本套课程，他会带你入局新媒体和短视频，掌握自媒体和短视频必备实用技能，开启你不一样人生，<br>客服微信：1534728904</span>
        </p>`,
        // 轮播图 
        swiperItems: [
          {
            img: "/static/imgs/bg.jpg",
            url: "",
            opentype: "navigate"
          },
          {
            img: "/static/imgs/bg.jpg",
            url: "",
            opentype: "navigate"
          }
        ],
        // 商品信息
        product: {
          name: "小米 MIX3 一面科技 一面艺术 ( 磁动力滑盖全面屏 | 故宫特别版 )",
          logo: "../../static/logo.png",
          price: 3188,
          priceMarket: 3200,
          imgs: []
        },
        // 切换导航
        navItems: [{
          id: 1,
          name: "简介"
        }, {
          id: 2,
          name: "目录"
        }, {
          id: 3,
          name: "评价"
        }],
        // 切换索引
        active: 0,
        // 属性选择
        attrShow: false,
        colors: [
          {
            id: 1,
            text: "红色",
            checked: false
          },
          {
            id: 2,
            text: "黑色",
            checked: false
          },
          {
            id: 3,
            text: "蓝色",
            checked: false
          }
        ],
        sizes: [
          {
            id: 1,
            text: "10 cm",
            checked: false
          },
          {
            id: 2,
            text: "20 cm",
            checked: false
          },
          {
            id: 3,
            text: "40 cm",
            checked: false
          }
        ],
        // 属性记录
        attrRes: {
          color: null,
          size: null,
          number: 1
        },
        commentContents: [
          {
            "content": "故国三千里，深宫二十年。一声何满子，双泪落君前。",
            "name": "回复昵称",
            "face": face$1,
            "date": "08/10 08:00",
            "praise": 188,
            "isPraise": false,
            "Reply": [
              {
                "name": "张晓曦",
                "content": "不错不错"
              },
              {
                "name": "王大陆",
                "content": "赞了~"
              }
            ]
          },
          {
            "content": "而今渐行渐远，渐觉虽悔难追。漫寄消寄息，终久奚为。也拟重论缱绻，争奈翻覆思维。纵再会，只恐恩情，难似当时。",
            "name": "路过繁华",
            "face": face$1,
            "date": "02/10 18:00",
            "praise": 288
          },
          {
            "content": "图片回复，点击图片可以预览......",
            "name": "林夕阳",
            "imgs": [
              "https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg",
              "https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg"
            ],
            "face": face$1,
            "date": "08/12 09:00",
            "praise": 955,
            "isPraise": true
          }
        ]
      };
    },
    created() {
      this.nodeArr = parserHtml.parserHTML(this.nodes);
    },
    methods: {
      // 分享
      share: function() {
        uni.navigateTo({
          url: "/pages/share/share"
        });
      },
      // 导航切换
      navChange: function(e) {
        this.active = e;
      },
      // 返回首页
      goHome: function() {
        uni.switchTab({
          url: "/pages/tabbar/index"
        });
      },
      // 返回首页
      kf: function() {
        uni.showToast({
          title: "加入购物车成功",
          icon: "none"
        });
        uni.navigateTo({
          url: "/pages/shopcart/shopcart"
        });
      },
      // 加入购物车
      addtocard: function() {
        this.selectAttr();
      },
      // 购买 
      buynow: function() {
        uni.navigateTo({
          url: "/pages/order/confirm"
        });
      },
      // 属性选择
      selectAttr: function() {
        if (!this.attrShow) {
          this.attrShow = true;
          return;
        }
        if (this.attrRes.color == null || this.attrRes.size == null) {
          uni.showToast({
            icon: "none",
            title: "请选择属性"
          });
        }
        formatAppLog("log", "at pages/course/detail.vue:344", "请完善提交代码");
      },
      colorChange: function(e) {
        this.attrRes.color = e.text;
      },
      sizeChange: function(e) {
        this.attrRes.size = e.text;
      },
      numberChange: function(e) {
        this.attrRes.number = e[0];
      },
      closeAttr: function() {
        this.attrShow = false;
      },
      showImgs: function(commentsIndex, imgIndex) {
        formatAppLog("log", "at pages/course/detail.vue:359", commentsIndex, imgIndex);
        uni.previewImage({
          urls: this.commentContents[commentsIndex].imgs,
          current: this.commentContents[commentsIndex].imgs[imgIndex]
        });
      }
    }
  };
  function _sfc_render$A(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_swiper = resolveEasycom(vue.resolveDynamicComponent("gui-swiper"), __easycom_0$b);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_course_chapter = resolveEasycom(vue.resolveDynamicComponent("cc-course-chapter"), __easycom_2$5);
    const _component_gui_stags = resolveEasycom(vue.resolveDynamicComponent("gui-stags"), __easycom_3$1);
    const _component_gui_step_box = resolveEasycom(vue.resolveDynamicComponent("gui-step-box"), __easycom_3);
    const _component_gui_iphone_bottom = resolveEasycom(vue.resolveDynamicComponent("gui-iphone-bottom"), __easycom_4$1);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createCommentVNode(" 轮播图  "),
          vue.createElementVNode("view", { class: "gui-bg-white" }, [
            vue.createVNode(_component_gui_swiper, {
              swiperItems: $data.swiperItems,
              spacing: 0,
              padding: 0,
              borderRadius: "0rpx",
              width: 750,
              height: 420
            }, null, 8, ["swiperItems"])
          ]),
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "4rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: $options.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createCommentVNode(" 商品标题 分享按钮 "),
        $data.active == 0 ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 0,
          class: "gui-padding gui-flex gui-row gui-space-between gui-align-items-center gui-bg-white gui-dark-bg-level-3"
        }, [
          vue.createElementVNode(
            "text",
            { class: "gui-h5 gui-block product-name gui-block" },
            vue.toDisplayString($data.product.name),
            1
            /* TEXT */
          ),
          vue.createElementVNode("view", {
            class: "product-share",
            onClick: _cache[0] || (_cache[0] = (...args) => $options.share && $options.share(...args))
          }, [
            vue.createElementVNode("text", { class: "product-share gui-icons gui-color-orange" }, "")
          ])
        ])) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 价格 "),
        $data.active == 0 ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 1,
          class: "gui-padding gui-bg-white gui-dark-bg-level-3",
          style: { "margin-top": "10rpx", "padding-bottom": "20px" }
        }, [
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-nowrap gui-align-items-center gui-space-between" }, [
            vue.createElementVNode("view", null, [
              vue.createElementVNode(
                "text",
                { class: "product-price" },
                "￥" + vue.toDisplayString($data.product.price),
                1
                /* TEXT */
              ),
              vue.createElementVNode(
                "text",
                {
                  class: "gui-text gui-color-gray gui-line-through",
                  style: { "margin-left": "30rpx" }
                },
                "￥" + vue.toDisplayString($data.product.priceMarket),
                1
                /* TEXT */
              )
            ]),
            vue.createElementVNode("view", { class: "gui-box-tags" }, [
              vue.createElementVNode("text", { class: "tag icon-tag-dj" }, "独家"),
              vue.createElementVNode("text", { class: "tag icon-tag-lz" }, "连载")
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-nowrap gui-align-items-center gui-space-between mt5" }, [
            vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" }, "运费 ￥0.00"),
            vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" }, "已售 21008 件"),
            vue.createElementVNode("text", { class: "gui-text-small gui-color-gray" }, "浏览 36万次")
          ])
        ])) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 详情 请根据项目情况自行改进 可以使用 富文本"),
        $data.active == 0 ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 2,
          class: "gui-padding markdown-body",
          style: { "border-top": "5px solid #eee!important" }
        }, [
          vue.createElementVNode("rich-text", { nodes: $data.nodeArr }, null, 8, ["nodes"])
        ])) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 评论 请复制我们提供的评论布局来实现 "),
        $data.active == 1 ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 3,
          style: { "border-top": "5px solid #eee!important" }
        }, [
          vue.createVNode(_component_cc_course_chapter)
        ])) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 评论 请复制我们提供的评论布局来实现 "),
        $data.active == 2 ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 4,
          class: "gui-padding-small",
          style: { "border-top": "5px solid #eee!important" }
        }, [
          (vue.openBlock(true), vue.createElementBlock(
            vue.Fragment,
            null,
            vue.renderList($data.commentContents, (item, index) => {
              return vue.openBlock(), vue.createElementBlock("view", {
                style: { "margin-top": "30rpx" },
                class: "gui-comments-items gui-flex gui-row gui-nowrap gui-space-between gui-bg-white gui-dark-bg-level-3",
                key: index
              }, [
                vue.createElementVNode("image", {
                  src: item.face,
                  class: "gui-comments-face"
                }, null, 8, ["src"]),
                vue.createElementVNode("view", { class: "gui-comments-body" }, [
                  vue.createElementVNode("view", { class: "gui-flex gui-row gui-nowrap gui-space-between gui-align-items-center" }, [
                    vue.createElementVNode(
                      "text",
                      { class: "gui-comments-header-text gui-text gui-primary-color" },
                      vue.toDisplayString(item.name),
                      1
                      /* TEXT */
                    ),
                    vue.createElementVNode(
                      "text",
                      {
                        class: vue.normalizeClass(["gui-comments-header-text gui-icons gui-color-gray gui-text-small", [item.isPraise ? "gui-primary-color" : ""]])
                      },
                      " " + vue.toDisplayString(item.praise),
                      3
                      /* TEXT, CLASS */
                    )
                  ]),
                  vue.createElementVNode(
                    "text",
                    { class: "gui-comments-content gui-block" },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  ),
                  item.imgs && item.imgs.length > 0 ? (vue.openBlock(), vue.createElementBlock("view", {
                    key: 0,
                    class: "gui-comments-imgs gui-flex gui-rows gui-wrap"
                  }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList(item.imgs, (img2, indexImg) => {
                        return vue.openBlock(), vue.createElementBlock("view", {
                          class: "gui-comments-image",
                          key: indexImg,
                          onClick: vue.withModifiers(($event) => $options.showImgs(index, indexImg), ["stop"])
                        }, [
                          vue.createElementVNode("image", {
                            src: img2,
                            width: 180,
                            mode: "aspectFill"
                          }, null, 8, ["src"])
                        ], 8, ["onClick"]);
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  item.Reply ? (vue.openBlock(), vue.createElementBlock("view", { key: 1 }, [
                    (vue.openBlock(true), vue.createElementBlock(
                      vue.Fragment,
                      null,
                      vue.renderList(item.Reply, (itemRe, indexRe) => {
                        return vue.openBlock(), vue.createElementBlock(
                          "text",
                          {
                            key: indexRe,
                            class: "gui-comments-replay gui-block gui-bg-gray gui-dark-bg-level-2"
                          },
                          vue.toDisplayString(itemRe.name) + " : " + vue.toDisplayString(itemRe.content),
                          1
                          /* TEXT */
                        );
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])) : vue.createCommentVNode("v-if", true),
                  vue.createElementVNode("view", { class: "gui-comments-info gui-flex gui-rows gui-nowrap gui-space-between gui-align-items-center" }, [
                    vue.createElementVNode(
                      "text",
                      { class: "gui-comments-info-text gui-color-gray" },
                      vue.toDisplayString(item.date),
                      1
                      /* TEXT */
                    )
                  ])
                ])
              ]);
            }),
            128
            /* KEYED_FRAGMENT */
          ))
        ])) : vue.createCommentVNode("v-if", true),
        vue.createCommentVNode(" 底部 "),
        vue.createElementVNode("view", { class: "product-footer gui-bg-white gui-dark-bg-level-3" }, [
          vue.createCommentVNode(" 属性选择 "),
          $data.attrShow ? (vue.openBlock(), vue.createElementBlock("scroll-view", {
            key: 0,
            class: "product-attr",
            "scroll-y": true
          }, [
            vue.createElementVNode("view", { class: "gui-flex gui-row gui-space-between" }, [
              vue.createElementVNode("text", { class: "gui-text gui-color-gray" }, "选择颜色"),
              vue.createElementVNode("text", {
                class: "gui-color-gray gui-h5 gui-icons",
                onClick: _cache[1] || (_cache[1] = (...args) => $options.closeAttr && $options.closeAttr(...args))
              }, "")
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createVNode(_component_gui_stags, {
                tags: $data.colors,
                onChange: $options.colorChange
              }, null, 8, ["tags", "onChange"])
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createElementVNode("text", { class: "gui-text gui-color-gray" }, "选择尺寸")
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createVNode(_component_gui_stags, {
                tags: $data.sizes,
                onChange: $options.sizeChange
              }, null, 8, ["tags", "onChange"])
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createElementVNode("text", { class: "gui-text gui-color-gray" }, "购买数量")
            ]),
            vue.createElementVNode("view", { class: "gui-margin-top" }, [
              vue.createVNode(_component_gui_step_box, {
                value: 1,
                onChange: $options.numberChange
              }, null, 8, ["onChange"])
            ])
          ])) : vue.createCommentVNode("v-if", true),
          vue.createCommentVNode(" 底部按钮栏 "),
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-space-between gui-align-items-center gui-border-t" }, [
            vue.createCommentVNode(" 2个底部按钮 "),
            vue.createElementVNode("view", {
              class: "gui-footer-icon-buttons",
              style: { "margin-left": "30rpx" },
              "hover-class": "gui-tap",
              onClick: _cache[2] || (_cache[2] = (...args) => $options.goHome && $options.goHome(...args))
            }, [
              vue.createElementVNode("text", { class: "gui-footer-icon-buttons-icon gui-block gui-icons gui-color-black2" }, ""),
              vue.createElementVNode("text", { class: "gui-footer-icon-buttons-text gui-block gui-icons gui-color-black2" }, "首页")
            ]),
            vue.createElementVNode("view", {
              class: "gui-footer-icon-buttons",
              "hover-class": "gui-tap",
              onClick: _cache[3] || (_cache[3] = (...args) => $options.kf && $options.kf(...args))
            }, [
              vue.createElementVNode("text", { class: "gui-footer-icon-buttons-icon gui-block gui-icons gui-color-black2" }, ""),
              vue.createElementVNode("text", { class: "gui-footer-icon-buttons-text gui-block gui-icons gui-color-black2" }, "购物车")
            ]),
            vue.createCommentVNode(" 2个大按钮 "),
            vue.createElementVNode("view", { class: "gui-footer-large-buttons gui-flex1 gui-flex gui-row gui-nowrap gui-justify-content-end" }, [
              vue.createElementVNode("view", {
                class: "gui-footer-large-button gui-bg-red",
                style: { "border-radius": "50rpx", "width": "200px" },
                "hover-class": "gui-tap",
                onClick: _cache[4] || (_cache[4] = (...args) => $options.buynow && $options.buynow(...args))
              }, [
                vue.createElementVNode("text", { class: "gui-text gui-text-center gui-block gui-color-white gui-footer-large-button-text" }, [
                  vue.createElementVNode("text", { class: "gui-icons mr5" }, ""),
                  vue.createTextVNode("立即购买")
                ])
              ])
            ])
          ]),
          vue.createVNode(_component_gui_iphone_bottom)
        ]),
        vue.createCommentVNode(" 底部占位 "),
        vue.createElementVNode("view", { style: { "height": "120rpx" } }),
        vue.createElementVNode("view", { class: "um-cps-ux-promoter-course-detail-collect f-pa" }, [
          vue.createElementVNode("view", {
            onClick: _cache[5] || (_cache[5] = (...args) => $options.share && $options.share(...args)),
            class: "um-cps-ux-promoter-course-detail-collect_btn"
          }, "分享赚￥39.8"),
          $data.closemask ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 0,
            class: "um-cps-ux-promoter-course-detail-collect-bubble f-pa"
          }, [
            vue.createElementVNode("image", {
              src: "/static/imgs/tip.png",
              style: { "width": "190px", "height": "50px" }
            }),
            vue.createElementVNode("view", { class: "um-cps-ux-promoter-course-detail-collect-bubble_info f-pa" }, "点击可获得专属分享链接")
          ])) : vue.createCommentVNode("v-if", true)
        ]),
        $data.closemask ? (vue.openBlock(), vue.createElementBlock("view", {
          key: 5,
          onClick: _cache[6] || (_cache[6] = ($event) => $data.closemask = false),
          class: "ux-mask ux-promoter-course-detail-collect-mask ux-modal-fadeIn"
        })) : vue.createCommentVNode("v-if", true)
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCourseDetail = /* @__PURE__ */ _export_sfc(_sfc_main$B, [["render", _sfc_render$A], ["__scopeId", "data-v-3d21314d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/course/detail.vue"]]);
  const _sfc_main$A = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 3,
          name: "设计创造"
        }, {
          id: 2,
          name: "新媒体电商"
        }, {
          id: 3,
          name: "人工智能"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render$z(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_title_subitem = resolveEasycom(vue.resolveDynamicComponent("cc-title-subitem"), __easycom_1$7);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-course-gridbox-nine" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(9, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-course-gridbox-oot" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(5, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-course-gridbox" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(3, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-full" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item mb10" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-scroll" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ]),
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white gui-border-radius mb10" }, [
            vue.createVNode(_component_cc_title_subitem),
            vue.createElementVNode("view", { class: "cc-coursebox-list-normal" }, [
              (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                null,
                vue.renderList(10, (num) => {
                  return vue.createElementVNode("view", { class: "list-course-item flex justify-content-between" }, [
                    vue.createElementVNode("view", { class: "imgbox" }, [
                      vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                    ]),
                    vue.createElementVNode("view", { class: "ml15" }, [
                      vue.createElementVNode("view", { class: "titbox" }, [
                        vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                        vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                        vue.createElementVNode("text", { class: "hottag ml5" }, "最热"),
                        vue.createElementVNode("text", { class: "ml5 fb" }, "邢不行-Python股票量化投资课程")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                        vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                      ]),
                      vue.createElementVNode("view", { class: "mt5" }, [
                        vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                        vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                      ])
                    ])
                  ]);
                }),
                64
                /* STABLE_FRAGMENT */
              ))
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCourseCategory = /* @__PURE__ */ _export_sfc(_sfc_main$A, [["render", _sfc_render$z], ["__file", "D:/App/分销系统/cc_fenxiao/pages/course/category.vue"]]);
  const _sfc_main$z = {
    props: {},
    data() {
      return {};
    },
    methods: {}
  };
  function _sfc_render$y(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "flex justify-content-around" }, [
      vue.createElementVNode("view", { class: "flex flex-c justify-content-center" }, [
        vue.createElementVNode("view", null, [
          vue.createElementVNode("image", {
            src: "/static/imgs/ai.png",
            style: { "width": "50px", "height": "50px" }
          })
        ]),
        vue.createElementVNode("view", { class: "mt5 fz12" }, "人工智能")
      ]),
      vue.createElementVNode("view", { class: "flex flex-c justify-content-center" }, [
        vue.createElementVNode("view", null, [
          vue.createElementVNode("image", {
            src: "/static/imgs/yu.png",
            style: { "width": "50px", "height": "50px" }
          })
        ]),
        vue.createElementVNode("view", { class: "mt5 fz12" }, "语言学习")
      ]),
      vue.createElementVNode("view", { class: "flex flex-c justify-content-center" }, [
        vue.createElementVNode("view", null, [
          vue.createElementVNode("image", {
            src: "/static/imgs/yan.png",
            style: { "width": "50px", "height": "50px" }
          })
        ]),
        vue.createElementVNode("view", { class: "mt5 fz12" }, "自学考研")
      ]),
      vue.createElementVNode("view", { class: "flex flex-c justify-content-center" }, [
        vue.createElementVNode("view", null, [
          vue.createElementVNode("image", {
            src: "/static/imgs/wei.png",
            style: { "width": "50px", "height": "50px" }
          })
        ]),
        vue.createElementVNode("view", { class: "mt5 fz12" }, "微专业")
      ]),
      vue.createElementVNode("view", { class: "flex flex-c justify-content-center" }, [
        vue.createElementVNode("view", null, [
          vue.createElementVNode("image", {
            src: "/static/imgs/chan.png",
            style: { "width": "50px", "height": "50px" }
          })
        ]),
        vue.createElementVNode("view", { class: "mt5 fz12" }, "产品&运营")
      ])
    ]);
  }
  const __easycom_2$4 = /* @__PURE__ */ _export_sfc(_sfc_main$z, [["render", _sfc_render$y], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/bar.vue"]]);
  const _sfc_main$y = {
    props: {
      fsize: {
        type: String,
        default: "fz14"
      },
      title: {
        type: String,
        default: "历史记录"
      },
      ctitle: {
        type: String,
        default: "查看更多"
      },
      showmore: {
        type: Boolean,
        default: true
      }
    },
    name: "nav",
    data() {
      return {};
    },
    methods: {
      handleClick() {
        this.$emits("clear");
      }
    }
  };
  function _sfc_render$x(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-flex gui-space-between ql-align-center gui-padding-m gui-bg-white mb10 gui-border-radius" }, [
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["gui-color-black1 fz16 fb", [$props.fsize]])
        },
        vue.toDisplayString($props.title),
        3
        /* TEXT, CLASS */
      ),
      $props.showmore ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "fz11 gui-color-gray",
        onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
      }, [
        vue.createTextVNode(
          vue.toDisplayString($props.ctitle),
          1
          /* TEXT */
        ),
        vue.createElementVNode("text", { class: "gui-icons ml2 fb" }, "")
      ])) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main$y, [["render", _sfc_render$x], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/sub.vue"]]);
  const _sfc_main$x = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 3,
          name: "设计创造"
        }, {
          id: 2,
          name: "新媒体电商"
        }, {
          id: 3,
          name: "人工智能"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render$w(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_title_bar = resolveEasycom(vue.resolveDynamicComponent("cc-title-bar"), __easycom_2$4);
    const _component_cc_advert_index = resolveEasycom(vue.resolveDynamicComponent("cc-advert-index"), __easycom_3$5);
    const _component_cc_title_sub = resolveEasycom(vue.resolveDynamicComponent("cc-title-sub"), __easycom_4);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      style: { "background": "#f3f3f3" }
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createElementVNode("view", { class: "gui-padding gui-bg-white" }, [
            vue.createVNode(_component_logo, {
              showback: "",
              onBack: _ctx.handleBack,
              showcart: "",
              title: "AI数字技能",
              btntext: ""
            }, null, 8, ["onBack"])
          ]),
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ])
        ]),
        vue.createElementVNode("view", {
          style: { "padding": "20px 0" },
          class: "gui-bg-white mt5 mb5"
        }, [
          vue.createVNode(_component_cc_title_bar)
        ]),
        vue.createElementVNode("view", null, [
          vue.createVNode(_component_cc_advert_index, { showspace: false })
        ]),
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createVNode(_component_cc_title_sub, {
            title: "数据分析",
            ctitle: "查看更多"
          }),
          vue.createElementVNode("view", { class: "cc-coursebox-list" }, [
            (vue.openBlock(), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(20, (num) => {
                return vue.createElementVNode("view", { class: "list-course-item gui-padding gui-bg-white mb10" }, [
                  vue.createElementVNode("view", { class: "imgbox" }, [
                    vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                    vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                    vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                    vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                  ])
                ]);
              }),
              64
              /* STABLE_FRAGMENT */
            ))
          ]),
          vue.createElementVNode("view", { class: "cc-coursebox-list-scroll" }, [
            (vue.openBlock(), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(20, (num) => {
                return vue.createElementVNode("view", { class: "list-course-item gui-padding gui-bg-white mb10" }, [
                  vue.createElementVNode("view", { class: "imgbox" }, [
                    vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                    vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                    vue.createElementVNode("text", { class: "hottag ml5" }, "最热")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "fb" }, "邢不行-Python股票量化投资课程")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                    vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                  ]),
                  vue.createElementVNode("view", { class: "mt5" }, [
                    vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                    vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                  ])
                ]);
              }),
              64
              /* STABLE_FRAGMENT */
            ))
          ]),
          vue.createElementVNode("view", { class: "cc-coursebox-list-normal" }, [
            (vue.openBlock(), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(20, (num) => {
                return vue.createElementVNode("view", { class: "list-course-item gui-padding-m gui-bg-white mb10 flex justify-content-between" }, [
                  vue.createElementVNode("view", { class: "imgbox" }, [
                    vue.createElementVNode("image", { src: "/static/imgs/bg.jpg" })
                  ]),
                  vue.createElementVNode("view", { class: "ml15" }, [
                    vue.createElementVNode("view", { class: "titbox" }, [
                      vue.createElementVNode("text", { class: "pushtag" }, "推荐"),
                      vue.createElementVNode("text", { class: "newtag ml5" }, "最新"),
                      vue.createElementVNode("text", { class: "hottag ml5" }, "最热"),
                      vue.createElementVNode("text", { class: "ml5 fb" }, "邢不行-Python股票量化投资课程")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "gui-icons yellow fz12" }, ""),
                      vue.createElementVNode("text", { class: "fz12 ml5" }, "5.0")
                    ]),
                    vue.createElementVNode("view", { class: "mt5" }, [
                      vue.createElementVNode("text", { class: "red fb fz16" }, "￥2488"),
                      vue.createElementVNode("text", { class: "textdel fz12 gui-color-gray ml5" }, "￥15888")
                    ])
                  ])
                ]);
              }),
              64
              /* STABLE_FRAGMENT */
            ))
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCourseSpecial = /* @__PURE__ */ _export_sfc(_sfc_main$x, [["render", _sfc_render$w], ["__scopeId", "data-v-7f133b12"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/course/special.vue"]]);
  const _sfc_main$w = {
    name: "gui-link",
    props: {
      url: {
        type: String,
        default: ""
      },
      title: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#008AFF"
      },
      fontSize: {
        type: String,
        default: "28rpx"
      },
      lineHeight: {
        type: String,
        default: "50rpx"
      }
    },
    methods: {
      openUrlForApp: function(e) {
        var link = e.currentTarget.dataset.url;
        plus.runtime.openURL(link);
      }
    }
  };
  function _sfc_render$v(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("text", {
      class: "link",
      "data-url": $props.url,
      onClick: _cache[0] || (_cache[0] = (...args) => $options.openUrlForApp && $options.openUrlForApp(...args)),
      style: vue.normalizeStyle({
        color: $props.color,
        lineHeight: $props.lineHeight,
        fontSize: $props.fontSize
      })
    }, vue.toDisplayString($props.title), 13, ["data-url"]);
  }
  const __easycom_1$6 = /* @__PURE__ */ _export_sfc(_sfc_main$w, [["render", _sfc_render$v], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-link.vue"]]);
  const _sfc_main$v = {
    name: "gui-article-info",
    props: {
      article: {
        type: Array,
        default: function() {
          return new Array();
        }
      },
      itemMargin: {
        type: String,
        default: "20rpx"
      },
      padding: {
        type: Number,
        default: 30
      },
      textClass: {
        type: Array,
        default: function() {
          return ["gui-article-text", "gui-primary-text"];
        }
      },
      centerClass: {
        type: Array,
        default: function() {
          return ["gui-article-center", "gui-primary-text"];
        }
      },
      imgRadius: {
        type: String,
        default: "6rpx"
      },
      quoteClass: {
        type: Array,
        default: function() {
          return ["gui-article-quote", "gui-primary-text", "gui-bg-gray", "gui-dark-bg-level-2"];
        }
      },
      strongClass: {
        type: Array,
        default: function() {
          return ["gui-article-strong", "gui-primary-text"];
        }
      },
      splineClass: {
        type: Array,
        default: function() {
          return ["gui-article-spline", "gui-color-gray"];
        }
      }
    },
    methods: {
      showImgs: function(e) {
        var currentUrl = e.currentTarget.dataset.url;
        var imgs = [];
        var items = this.article;
        for (let i = 0; i < items.length; i++) {
          if (items[i].type == "img") {
            imgs.push(items[i].content);
          }
        }
        uni.previewImage({
          urls: imgs,
          current: currentUrl
        });
      }
    }
  };
  function _sfc_render$u(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_image = resolveEasycom(vue.resolveDynamicComponent("gui-image"), __easycom_0$a);
    const _component_gui_link = resolveEasycom(vue.resolveDynamicComponent("gui-link"), __easycom_1$6);
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: "gui-editor-show gui-border-box",
        style: vue.normalizeStyle({
          paddingLeft: $props.padding + "rpx",
          paddingRight: $props.padding + "rpx",
          width: "750rpx"
        })
      },
      [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($props.article, (item, index) => {
            return vue.openBlock(), vue.createElementBlock("view", { key: index }, [
              vue.createCommentVNode(" 文本 "),
              item.type == "txt" || item.type == "text" ? (vue.openBlock(), vue.createElementBlock(
                "text",
                {
                  key: 0,
                  class: vue.normalizeClass(["gui-block", $props.textClass]),
                  decode: true,
                  selectable: true,
                  "user-select": true
                },
                vue.toDisplayString(item.content),
                3
                /* TEXT, CLASS */
              )) : item.type == "center" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 1 },
                [
                  vue.createCommentVNode(" 居中 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: vue.normalizeClass(["gui-block gui-text-center", $props.centerClass]),
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    3
                    /* TEXT, CLASS */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "img" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 2 },
                [
                  vue.createCommentVNode(" 图片 "),
                  vue.createElementVNode("view", {
                    class: "gui-img-in",
                    "data-url": item.content,
                    onClick: _cache[0] || (_cache[0] = (...args) => $options.showImgs && $options.showImgs(...args))
                  }, [
                    vue.createVNode(_component_gui_image, {
                      src: item.content,
                      height: 0,
                      borderRadius: $props.imgRadius,
                      width: 750 - $props.padding * 2
                    }, null, 8, ["src", "borderRadius", "width"])
                  ], 8, ["data-url"])
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "quote" || item.type == "pre" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 3 },
                [
                  vue.createCommentVNode(" 引用 源码 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: vue.normalizeClass(["gui-block", $props.quoteClass]),
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    3
                    /* TEXT, CLASS */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "strong" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 4 },
                [
                  vue.createCommentVNode(" 加粗 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: vue.normalizeClass(["gui-block gui-bold", $props.strongClass]),
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    3
                    /* TEXT, CLASS */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "link" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 5 },
                [
                  vue.createCommentVNode(" 链接 "),
                  vue.createElementVNode("view", null, [
                    vue.createVNode(_component_gui_link, {
                      url: item.content,
                      title: item.content
                    }, null, 8, ["url", "title"])
                  ])
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "spline" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 6 },
                [
                  vue.createCommentVNode(" 分割符 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: vue.normalizeClass(["gui-block gui-text-center", $props.splineClass]),
                      selectable: true,
                      "user-select": true
                    },
                    "● ● ●",
                    2
                    /* CLASS */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h1" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 7 },
                [
                  vue.createCommentVNode(" h1 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h1 gui-primary-text",
                      decode: true,
                      selectable: true,
                      "user-select": true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h2" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 8 },
                [
                  vue.createCommentVNode(" h2 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h2 gui-primary-text",
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h3" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 9 },
                [
                  vue.createCommentVNode(" h3 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h3 gui-primary-text",
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h4" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 10 },
                [
                  vue.createCommentVNode(" h4 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h4 gui-primary-text",
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h5" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 11 },
                [
                  vue.createCommentVNode(" h5 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h5 gui-primary-text",
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : item.type == "h6" ? (vue.openBlock(), vue.createElementBlock(
                vue.Fragment,
                { key: 12 },
                [
                  vue.createCommentVNode(" h6 "),
                  vue.createElementVNode(
                    "text",
                    {
                      class: "gui-block gui-h6 gui-primary-text",
                      selectable: true,
                      "user-select": true,
                      decode: true
                    },
                    vue.toDisplayString(item.content),
                    1
                    /* TEXT */
                  )
                ],
                2112
                /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
              )) : vue.createCommentVNode("v-if", true),
              vue.createCommentVNode(" 视频 "),
              item.type == "video" ? (vue.openBlock(), vue.createElementBlock("view", { key: 13 }, [
                vue.createElementVNode("video", {
                  style: vue.normalizeStyle({ width: 750 - $props.padding * 2 + "rpx" }),
                  src: item.content,
                  controls: ""
                }, null, 12, ["src"])
              ])) : vue.createCommentVNode("v-if", true),
              vue.createCommentVNode(" 间距 "),
              vue.createElementVNode(
                "view",
                {
                  style: vue.normalizeStyle({ height: $props.itemMargin })
                },
                null,
                4
                /* STYLE */
              )
            ]);
          }),
          128
          /* KEYED_FRAGMENT */
        ))
      ],
      4
      /* STYLE */
    );
  }
  const __easycom_0$5 = /* @__PURE__ */ _export_sfc(_sfc_main$v, [["render", _sfc_render$u], ["__scopeId", "data-v-2ce5e431"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-article-info.vue"]]);
  const _sfc_main$u = {
    name: "gui-spread",
    props: {
      width: { type: String, default: "690rpx" },
      height: { type: String, default: "600rpx" },
      btnTxt: { type: String, default: "展开阅读全文" },
      btnTxtSize: { type: String, default: "28rpx" },
      zIndex: { type: Number, default: 1 },
      isShrink: { type: Boolean, default: false },
      shrinkBtnTxt: { type: String, default: "收缩文章" }
    },
    data() {
      return {
        reHeight: "600px",
        isBtn: true
      };
    },
    created: function() {
      this.reHeight = this.height;
    },
    methods: {
      spreadContent: function() {
        this.reHeight = "auto";
        this.isBtn = false;
      },
      shrinkContent: function() {
        this.reHeight = this.height;
        this.isBtn = true;
      }
    }
  };
  function _sfc_render$t(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        class: vue.normalizeClass(["gui-spread", [$props.isShrink ? "gui-transition-all" : ""]]),
        style: vue.normalizeStyle({ height: $data.reHeight })
      },
      [
        vue.createElementVNode(
          "view",
          {
            style: vue.normalizeStyle({
              paddingBottom: !$data.isBtn && $props.isShrink ? "80rpx" : "0rpx"
            })
          },
          [
            vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
          ],
          4
          /* STYLE */
        ),
        $data.isBtn ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 0,
            onClick: _cache[0] || (_cache[0] = (...args) => $options.spreadContent && $options.spreadContent(...args)),
            style: vue.normalizeStyle({ fontSize: $props.btnTxtSize, zIndex: $props.zIndex, width: $props.width }),
            class: "gui-primary-text gui-icons gui-block gui-spread-btn gui-bg-white gui-dark-bg-level-1"
          },
          " " + vue.toDisplayString($props.btnTxt),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true),
        !$data.isBtn && $props.isShrink ? (vue.openBlock(), vue.createElementBlock(
          "text",
          {
            key: 1,
            onClick: _cache[1] || (_cache[1] = (...args) => $options.shrinkContent && $options.shrinkContent(...args)),
            style: vue.normalizeStyle({ fontSize: $props.btnTxtSize, zIndex: $props.zIndex, width: $props.width }),
            class: "gui-primary-text gui-icons gui-block gui-spread-btn gui-bg-white gui-dark-bg-level-1"
          },
          " " + vue.toDisplayString($props.shrinkBtnTxt),
          5
          /* TEXT, STYLE */
        )) : vue.createCommentVNode("v-if", true)
      ],
      6
      /* CLASS, STYLE */
    );
  }
  const __easycom_1$5 = /* @__PURE__ */ _export_sfc(_sfc_main$u, [["render", _sfc_render$t], ["__scopeId", "data-v-556ee095"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-spread.vue"]]);
  const _sfc_main$t = {
    data() {
      return {
        title: "",
        article: []
      };
    },
    onLoad: function() {
      uni.showLoading({ title: "loading", mask: true });
      uni.setNavigationBarTitle({ title: "加载中" });
      uni.request({
        url: "https://www.graceui.com/api/html2array",
        success: (res) => {
          formatAppLog("log", "at pages/note/detail.vue:31", res);
          this.article = res.data.data;
          this.title = "文章标题";
          uni.setNavigationBarTitle({ title: this.title });
          uni.hideLoading();
        }
      });
    }
  };
  function _sfc_render$s(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_article_info = resolveEasycom(vue.resolveDynamicComponent("gui-article-info"), __easycom_0$5);
    const _component_gui_spread = resolveEasycom(vue.resolveDynamicComponent("gui-spread"), __easycom_1$5);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, null, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3" }, [
          vue.createVNode(_component_gui_spread, {
            width: "750rpx",
            height: "600rpx",
            isShrink: true
          }, {
            default: vue.withCtx(() => [
              vue.createVNode(_component_gui_article_info, { article: $data.article }, null, 8, ["article"])
            ]),
            _: 1
            /* STABLE */
          })
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesNoteDetail = /* @__PURE__ */ _export_sfc(_sfc_main$t, [["render", _sfc_render$s], ["__file", "D:/App/分销系统/cc_fenxiao/pages/note/detail.vue"]]);
  const _sfc_main$s = {
    data() {
      return {};
    },
    methods: {}
  };
  function _sfc_render$r(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view");
  }
  const PagesDownloadsDetail = /* @__PURE__ */ _export_sfc(_sfc_main$s, [["render", _sfc_render$r], ["__file", "D:/App/分销系统/cc_fenxiao/pages/downloads/detail.vue"]]);
  const _sfc_main$r = {
    name: "gui-touch",
    props: {
      datas: { type: Array, default: function() {
        return [];
      } }
    },
    data() {
      return {
        toucheTimer: 0,
        fingerRes: [],
        distance: 0,
        taptimer: 100
      };
    },
    methods: {
      toInt: function(arr) {
        var res = [];
        arr.forEach((item) => {
          item.pageX = parseInt(item.pageX);
          item.pageY = parseInt(item.pageY);
          res.push(item);
        });
        return res;
      },
      touchstart: function(e) {
        this.toucheTimer = (/* @__PURE__ */ new Date()).getTime();
        this.fingerRes = this.toInt(e.changedTouches);
        if (this.fingerRes.length > 2) {
          return;
        }
        var moves = [];
        this.fingerRes.forEach((finger) => {
          var xTouch = finger.pageX;
          var yTouch = finger.pageY;
          moves.push([xTouch, yTouch]);
        });
        this.$emit("thStart", moves, this.datas);
      },
      touchmove: function(e) {
        if (this.toucheTimer < 50) {
          return;
        }
        var timer = (/* @__PURE__ */ new Date()).getTime() - this.toucheTimer;
        if (timer < this.taptimer) {
          return;
        }
        var touches = this.toInt(e.changedTouches);
        if (touches.length > 2) {
          return;
        }
        if (touches.length == 1) {
          var i = 0, moves = [];
          touches.forEach((finger) => {
            var xTouch = finger.pageX - this.fingerRes[i].pageX;
            var yTouch = finger.pageY - this.fingerRes[i].pageY;
            moves.push([xTouch, yTouch]);
            i++;
          });
          this.$emit("thMove", moves, this.datas);
        } else if (touches.length == 2) {
          if (this.distance == 0) {
            this.distance = parseInt(this.getDistance(touches[0].pageX, touches[0].pageY, touches[1].pageX, touches[1].pageY));
          } else {
            var distance1 = parseInt(this.getDistance(touches[0].pageX, touches[0].pageY, touches[1].pageX, touches[1].pageY));
            var scale = distance1 / this.distance;
            scale = Math.floor(scale * 100) / 100;
            this.$emit("scale", scale, this.datas);
          }
        }
      },
      touchend: function(e) {
        var timer = (/* @__PURE__ */ new Date()).getTime() - this.toucheTimer;
        if (timer < this.taptimer) {
          this.$emit("tapme");
          return;
        }
        var touches = this.toInt(e.changedTouches);
        this.distance = 0;
        if (touches.length == 1) {
          var i = 0, moves = [];
          touches.forEach((finger) => {
            var xTouch = finger.pageX - this.fingerRes[i].pageX;
            var yTouch = finger.pageY - this.fingerRes[i].pageY;
            moves.push([xTouch, yTouch]);
            i++;
          });
          moves.push(timer);
          this.$emit("thEnd", moves, this.datas);
          if (timer < 300) {
            var mx = Math.abs(moves[0][0]);
            var my = Math.abs(moves[0][1]);
            if (mx > my) {
              if (mx >= 50) {
                if (moves[0][0] > 0) {
                  this.$emit("swipe", "right", this.datas);
                } else {
                  this.$emit("swipe", "left", this.datas);
                }
              }
            } else {
              if (my >= 50) {
                if (moves[0][1] > 0) {
                  this.$emit("swipe", "down", this.datas);
                } else {
                  this.$emit("swipe", "up", this.datas);
                }
              }
            }
          }
        }
      },
      getDistance: function(lat1, lng1, lat2, lng2) {
        var radLat1 = lat1 * Math.PI / 180;
        var radLat2 = lat2 * Math.PI / 180;
        var a = radLat1 - radLat2;
        var b = lng1 * Math.PI / 180 - lng2 * Math.PI / 180;
        var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * 6378.137;
        return Math.round(s * 1e4) / 1e4;
      },
      tapme: function() {
        this.isTap = true;
      }
    },
    emits: ["thStart", "thMove", "scale", "tapme", "thEnd", "swipe"]
  };
  function _sfc_render$q(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "view",
      {
        onTouchstart: _cache[0] || (_cache[0] = (...args) => $options.touchstart && $options.touchstart(...args)),
        onTouchmove: _cache[1] || (_cache[1] = (...args) => $options.touchmove && $options.touchmove(...args)),
        onTouchend: _cache[2] || (_cache[2] = (...args) => $options.touchend && $options.touchend(...args))
      },
      [
        vue.renderSlot(_ctx.$slots, "default")
      ],
      32
      /* NEED_HYDRATION */
    );
  }
  const __easycom_0$4 = /* @__PURE__ */ _export_sfc(_sfc_main$r, [["render", _sfc_render$q], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-touch.vue"]]);
  const _sfc_main$q = {
    name: "gui-slide-list",
    props: {
      width: { type: Number, default: 750 },
      msgs: { type: Array, default: function() {
        return [];
      } },
      btnWidth: { type: Number, default: 320 },
      titleEllipsis: { type: Boolean, default: true },
      descEllipsis: { type: Boolean, default: true }
    },
    data() {
      return {
        msgsIn: [],
        damping: 0.29,
        moveIndex: -1,
        x: 0,
        oX: 0,
        scY: true,
        btnWidthpx: 160,
        touchStart: false
      };
    },
    created: function() {
      this.init(this.msgs);
      this.btnWidthpx = uni.upx2px(this.btnWidth) * -1 + 2;
    },
    watch: {
      msgs: function(nv) {
        this.init(nv);
      }
    },
    methods: {
      init: function(msgs) {
        this.moveIndex = -1;
        this.msgsIn = msgs;
      },
      thStart: function(e, index) {
        this.x = 0;
        this.moveIndex = index[0];
        this.damping = 0.25;
      },
      thMove: function(e, index) {
        var x = e[0][0];
        var y = e[0][1];
        if (Math.abs(x) < Math.abs(y)) {
          this.scY = true;
          return;
        } else {
          this.scY = false;
        }
        if (x < 0) {
          this.x += x * this.damping;
          if (this.x < this.btnWidthpx) {
            this.x = this.btnWidthpx;
          }
          this.damping *= 1.02;
        } else {
          this.scY = true;
        }
      },
      thEnd: function(e, index) {
        if (this.x > this.btnWidthpx / 8) {
          this.x = 0;
        } else {
          this.x = this.btnWidthpx;
        }
        this.scY = true;
        this.oX = this.x;
      },
      btnTap: function(index, indexBtn) {
        this.$emit("btnTap", index, indexBtn);
      },
      itemTap: function(index) {
        if (this.oX < 0) {
          this.oX = 0;
          this.moveIndex = -1;
          return;
        }
        this.$emit("itemTap", index);
        this.moveIndex = -1;
        this.oX = 0;
      },
      scrolltolower: function() {
        var laodStatus = this.$refs.loadmoreinslidelist.loadMoreStatus;
        if (laodStatus == 0) {
          this.$emit("scrolltolower");
        }
      },
      startLoadig: function() {
        this.$refs.loadmoreinslidelist.loading();
      },
      nomore: function() {
        this.$refs.loadmoreinslidelist.nomore();
      },
      endLoading: function() {
        this.$refs.loadmoreinslidelist.stoploadmore();
      }
    },
    emits: ["btnTap", "scrolltolower"]
  };
  function _sfc_render$p(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_touch = resolveEasycom(vue.resolveDynamicComponent("gui-touch"), __easycom_0$4);
    const _component_gui_loadmore = resolveEasycom(vue.resolveDynamicComponent("gui-loadmore"), __easycom_1$9);
    return vue.openBlock(), vue.createElementBlock("scroll-view", {
      class: "gui-slide-list gui-absolute-full",
      "scroll-y": $data.scY,
      "show-scrollbar": false,
      style: vue.normalizeStyle({ width: $props.width + "rpx" }),
      onScrolltolower: _cache[0] || (_cache[0] = (...args) => $options.scrolltolower && $options.scrolltolower(...args))
    }, [
      (vue.openBlock(true), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList($data.msgsIn, (item, index) => {
          return vue.openBlock(), vue.createElementBlock(
            "view",
            {
              class: "gui-slide-list-item gui-border-b",
              key: index,
              style: vue.normalizeStyle({ width: $props.width + "rpx" })
            },
            [
              vue.createElementVNode(
                "view",
                {
                  class: "gui-flex gui-row gui-nowrap gui-align-items-center",
                  style: vue.normalizeStyle({
                    width: $props.width + $props.btnWidth + "rpx",
                    overflow: "hidden",
                    transform: "translateX(" + ($data.moveIndex != index ? 0 : $data.x) + "px)"
                  })
                },
                [
                  vue.createElementVNode("view", {
                    class: "gui-slide-list-img-wrap",
                    "hover-class": "gui-tap",
                    onClick: vue.withModifiers(($event) => $options.itemTap(index), ["stop"])
                  }, [
                    vue.createElementVNode("image", {
                      src: item.img,
                      class: "gui-slide-list-img",
                      mode: "aspectFill"
                    }, null, 8, ["src"]),
                    item.msgnumber > 0 ? (vue.openBlock(), vue.createElementBlock(
                      "text",
                      {
                        key: 0,
                        class: "gui-slide-list-point gui-block gui-bg-red gui-color-white"
                      },
                      vue.toDisplayString(item.msgnumber),
                      1
                      /* TEXT */
                    )) : vue.createCommentVNode("v-if", true)
                  ], 8, ["onClick"]),
                  vue.createElementVNode("view", {
                    class: "gui-slide-list-content",
                    "hover-class": "gui-tap"
                  }, [
                    vue.createVNode(_component_gui_touch, {
                      onThStart: $options.thStart,
                      onThMove: $options.thMove,
                      onThEnd: $options.thEnd,
                      onTapme: ($event) => $options.itemTap(index),
                      datas: [index]
                    }, {
                      default: vue.withCtx(() => [
                        vue.createElementVNode("view", { class: "gui-flex gui-row gui-nowrap gui-space-between" }, [
                          vue.createElementVNode(
                            "text",
                            {
                              class: vue.normalizeClass([[$props.titleEllipsis ? "gui-ellipsis" : ""], "gui-slide-list-title-text gui-primary-text gui-block"])
                            },
                            vue.toDisplayString(item.title),
                            3
                            /* TEXT, CLASS */
                          ),
                          vue.createElementVNode(
                            "text",
                            { class: "gui-slide-list-desc gui-block gui-color-gray ps-space gui-flex-shrink0" },
                            vue.toDisplayString(item.time),
                            1
                            /* TEXT */
                          )
                        ]),
                        vue.createElementVNode(
                          "text",
                          {
                            class: vue.normalizeClass([[$props.descEllipsis ? "gui-ellipsis" : ""], "gui-slide-list-desc gui-third-text gui-block"])
                          },
                          vue.toDisplayString(item.content),
                          3
                          /* TEXT, CLASS */
                        )
                      ]),
                      _: 2
                      /* DYNAMIC */
                    }, 1032, ["onThStart", "onThMove", "onThEnd", "onTapme", "datas"])
                  ]),
                  vue.createElementVNode(
                    "view",
                    {
                      class: "gui-slide-btns gui-flex gui-row gui-nowrap",
                      style: vue.normalizeStyle({ width: $props.btnWidth - 2 + "rpx" })
                    },
                    [
                      (vue.openBlock(true), vue.createElementBlock(
                        vue.Fragment,
                        null,
                        vue.renderList(item.btns, (btn, btnIndex) => {
                          return vue.openBlock(), vue.createElementBlock("text", {
                            class: "gui-slide-btn gui-block gui-text-center",
                            key: btnIndex,
                            style: vue.normalizeStyle({ backgroundColor: btn.bgColor }),
                            onClick: vue.withModifiers(($event) => $options.btnTap(index, btnIndex), ["stop"])
                          }, vue.toDisplayString(btn.name), 13, ["onClick"]);
                        }),
                        128
                        /* KEYED_FRAGMENT */
                      ))
                    ],
                    4
                    /* STYLE */
                  )
                ],
                4
                /* STYLE */
              )
            ],
            4
            /* STYLE */
          );
        }),
        128
        /* KEYED_FRAGMENT */
      )),
      vue.createCommentVNode(" 加载组件 "),
      vue.createElementVNode("view", { style: { "padding": "20rpx" } }, [
        vue.createVNode(
          _component_gui_loadmore,
          { ref: "loadmoreinslidelist" },
          null,
          512
          /* NEED_PATCH */
        )
      ])
    ], 44, ["scroll-y"]);
  }
  const __easycom_0$3 = /* @__PURE__ */ _export_sfc(_sfc_main$q, [["render", _sfc_render$p], ["__scopeId", "data-v-3e1c9efb"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-slide-list.vue"]]);
  var face = "https://images.unsplash.com/photo-1662695089339-a2c24231a3ac?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxM3x8fGVufDB8fHx8&auto=format&fit=crop&w=100&q=60";
  var msgsFromApi = [
    {
      img: face,
      msgnumber: 8,
      title: "某某某",
      time: "30分钟前",
      content: "消息内容 ..."
    },
    {
      img: face,
      msgnumber: 0,
      title: "某某某",
      time: "08/10/2020",
      content: "语音消息"
    },
    {
      img: face,
      msgnumber: 12,
      title: "某某某",
      time: "昨天",
      content: "图片消息"
    },
    {
      img: face,
      msgnumber: 1,
      title: "某某某",
      time: "08/08/2020",
      content: "系统消息"
    },
    {
      img: face,
      msgnumber: 8,
      title: "某某某",
      time: "30分钟前",
      content: "消息内容 ..."
    },
    {
      img: face,
      msgnumber: 0,
      title: "某某某",
      time: "08/10/2020",
      content: "语音消息"
    },
    {
      img: face,
      msgnumber: 12,
      title: "某某某",
      time: "昨天",
      content: "图片消息"
    },
    {
      img: face,
      msgnumber: 1,
      title: "某某某",
      time: "08/08/2020",
      content: "系统消息"
    },
    {
      img: face,
      msgnumber: 8,
      title: "某某某",
      time: "30分钟前",
      content: "消息内容 ..."
    },
    {
      img: face,
      msgnumber: 0,
      title: "某某某",
      time: "08/10/2020",
      content: "语音消息"
    },
    {
      img: face,
      msgnumber: 12,
      title: "某某某",
      time: "昨天",
      content: "图片消息"
    },
    {
      img: face,
      msgnumber: 1,
      title: "某某某",
      time: "08/08/2020",
      content: "系统消息"
    }
  ];
  const _sfc_main$p = {
    data() {
      return {
        msgs: [],
        pageLoading: true
      };
    },
    onLoad: function() {
      setTimeout(() => {
        for (let i = 0; i < msgsFromApi.length; i++) {
          msgsFromApi[i].btns = [
            { "name": "标为已读", bgColor: "#323232" },
            { "name": "删除消息", bgColor: "#FF0036" }
          ];
        }
        this.msgs = msgsFromApi;
        this.pageLoading = false;
      }, 500);
    },
    methods: {
      btnTap: function(index, btnIndex) {
        formatAppLog("log", "at pages/user/message.vue:138", index, btnIndex);
        if (btnIndex == 0) {
          if (this.msgs[index].btns[0].name == "标为已读") {
            this.msgs[index].btns = [
              { "name": "标为未读", bgColor: "#888888" },
              { "name": "删除消息", bgColor: "#FF0036" }
            ];
            this.msgs[index].msgnumber = 0;
          } else {
            this.msgs[index].btns = [
              { "name": "标为已读", bgColor: "#323232" },
              { "name": "删除消息", bgColor: "#FF0036" }
            ];
            this.msgs[index].msgnumber = 1;
          }
          setTimeout(() => {
            this.msgs.splice(index, 1, this.msgs[index]);
          }, 300);
        } else if (btnIndex == 1) {
          uni.showModal({
            title: "确定要删除吗?",
            success: (e) => {
              if (e.confirm) {
                this.msgs.splice(index, 1);
                this.$refs.guiSlideList.moveIndex = -1;
              }
            }
          });
        }
      },
      // 列表本身被点击
      itemTap: function(e) {
        formatAppLog("log", "at pages/user/message.vue:173", e);
        uni.showToast({ title: "索引" + e });
      },
      loadMore: function() {
        this.$refs.guiSlideList.startLoadig();
        setTimeout(() => {
          for (let i = 0; i < msgsFromApi.length; i++) {
            msgsFromApi[i].btns = [
              { "name": "标为已读", bgColor: "#323232" },
              { "name": "删除消息", bgColor: "#FF0036" }
            ];
          }
          this.msgs = this.msgs.concat(msgsFromApi);
          this.$refs.guiSlideList.endLoading();
        }, 500);
      }
    }
  };
  function _sfc_render$o(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_slide_list = resolveEasycom(vue.resolveDynamicComponent("gui-slide-list"), __easycom_0$3);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      fullPage: true,
      isLoading: $data.pageLoading,
      ref: "guiPage"
    }, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-flex1 gui-flex gui-column" }, [
          vue.createCommentVNode(" 如果页面有其他元素请使用 view 布局即可 "),
          vue.createCommentVNode(' view style="height:200rpx;" class="gui-bg-blue"></view '),
          vue.createElementVNode("view", { class: "gui-flex1 gui-relative" }, [
            vue.createVNode(_component_gui_slide_list, {
              class: "gui-flex1",
              msgs: $data.msgs,
              onItemTap: $options.itemTap,
              onBtnTap: $options.btnTap,
              onScrolltolower: $options.loadMore,
              ref: "guiSlideList"
            }, null, 8, ["msgs", "onItemTap", "onBtnTap", "onScrolltolower"])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    }, 8, ["isLoading"]);
  }
  const PagesUserMessage = /* @__PURE__ */ _export_sfc(_sfc_main$p, [["render", _sfc_render$o], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/message.vue"]]);
  const _sfc_main$o = {
    data() {
      return {
        formData: {
          name1: "GraceUI",
          name2: "888888",
          name3: "888888",
          name4: "888888@qq.com",
          name5: "请选择性别",
          name7: "123456",
          name8: ""
        },
        gender: ["请选择性别", "男", "女"],
        genderIndex: 0
      };
    },
    methods: {
      submit: function(e) {
        var rule = [
          { name: "name1", checkType: "string", checkRule: "1,10", errorMsg: "**应为1-10个字符" },
          { name: "name2", checkType: "string", checkRule: "6,", errorMsg: "密码至少6个字符" },
          { name: "name3", checkType: "samewith", checkRule: "name2", errorMsg: "两次密码输入不一致" },
          { name: "name4", checkType: "email", checkRule: "", errorMsg: "邮箱格式错误" },
          { name: "name5", checkType: "notsame", checkRule: "请选择性别", errorMsg: "请选择性别" },
          // 正则表达式验证演示 : 例如检查必须是有效数字
          { name: "name7", checkType: "reg", checkRule: "^(-?\\d+)$", errorMsg: "必须是有效数字" },
          { name: "name8", checkType: "idCard", checkRule: "", errorMsg: "身份证号码错误" }
        ];
        var formData = e.detail.value;
        formData.name5 = this.formData.name5;
        formatAppLog("log", "at pages/user/xieyi.vue:55", formData);
        var checkRes = graceChecker.check(formData, rule);
        if (checkRes) {
          if (formData.name6.length < 1) {
            uni.showToast({ title: "请选择爱好!", icon: "none" });
            return false;
          }
          uni.showToast({ title: "验证通过!", icon: "none" });
        } else {
          uni.showToast({ title: graceChecker.error, icon: "none" });
        }
      },
      pickerChange: function(e) {
        this.genderIndex = e.detail.value;
        this.formData.name5 = this.gender[this.genderIndex];
      }
    }
  };
  function _sfc_render$n(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, null, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", null, [
          vue.createElementVNode(
            "form",
            {
              onSubmit: _cache[0] || (_cache[0] = (...args) => $options.submit && $options.submit(...args))
            },
            [
              vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3" }, [
                vue.createElementVNode("view", { class: "tipbox" }, [
                  vue.createElementVNode("view", { class: "t" }, "1: 支付宝/微信账户姓名必须与实名认证名保持一致，否则无法提现"),
                  vue.createElementVNode("view", { class: "t" }, "2: 每日支付宝/微信个人账户转入上线是5万元，企业账户转入上线是10万元，如超出额度请联系客服。"),
                  vue.createElementVNode("view", { class: "t" }, "3: 目前使用支付宝/微信账户进行提现，但若后期平台策略发生变更将更具后续进行调整和另行通知，谢谢。")
                ])
              ]),
              vue.createElementVNode("view", { style: { "height": "60rpx" } })
            ],
            32
            /* NEED_HYDRATION */
          )
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesUserXieyi = /* @__PURE__ */ _export_sfc(_sfc_main$o, [["render", _sfc_render$n], ["__scopeId", "data-v-e5823c2c"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/xieyi.vue"]]);
  const _sfc_main$n = {};
  function _sfc_render$m(_ctx, _cache) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-flex gui-column gui-justify-content-center gui-align-items-center" }, [
      vue.renderSlot(_ctx.$slots, "img"),
      vue.renderSlot(_ctx.$slots, "text"),
      vue.renderSlot(_ctx.$slots, "other")
    ]);
  }
  const __easycom_1$4 = /* @__PURE__ */ _export_sfc(_sfc_main$n, [["render", _sfc_render$m], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-empty.vue"]]);
  const _sfc_main$m = {
    name: "gui-radio",
    props: {
      size: {
        type: Number,
        default: 38
      },
      defaultClass: {
        type: Array,
        default: function() {
          return ["gui-color-gray"];
        }
      },
      checked: {
        type: Boolean,
        default: false
      },
      checkedClass: {
        type: Array,
        default: function() {
          return ["gui-bg-primary", "gui-color-white"];
        }
      },
      parameter: {
        type: Array,
        default: function() {
          return [];
        }
      }
    },
    data() {
      return {
        status: false
      };
    },
    watch: {
      checked: function(val, old) {
        this.status = val;
      }
    },
    created: function() {
      this.status = this.checked;
    },
    methods: {
      changeStatus: function() {
        this.status = !this.status;
        this.$emit("change", [this.status, this.parameter]);
      }
    },
    emits: ["change"]
  };
  function _sfc_render$l(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", {
      class: "gui-flex gui-row gui-nowrap gui-align-items-center",
      onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.changeStatus && $options.changeStatus(...args), ["stop"]))
    }, [
      $data.status ? (vue.openBlock(), vue.createElementBlock(
        "text",
        {
          key: 0,
          class: vue.normalizeClass(["gui-icons gui-block gui-text-center", $props.checkedClass]),
          style: vue.normalizeStyle({
            width: $props.size + "rpx",
            height: $props.size + "rpx",
            lineHeight: $props.size + "rpx",
            fontSize: $props.size - 15 + "rpx",
            borderRadius: $props.size + "rpx"
          })
        },
        "",
        6
        /* CLASS, STYLE */
      )) : (vue.openBlock(), vue.createElementBlock(
        "text",
        {
          key: 1,
          class: vue.normalizeClass(["gui-icons gui-block gui-text-center", $props.defaultClass]),
          style: vue.normalizeStyle({
            width: $props.size + "rpx",
            height: $props.size + "rpx",
            lineHeight: $props.size + 2 + "rpx",
            fontSize: $props.size - 8 + "rpx"
          })
        },
        "",
        6
        /* CLASS, STYLE */
      )),
      vue.createElementVNode("view", { class: "gui-radio-lable gui-flex1" }, [
        vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
      ])
    ]);
  }
  const __easycom_2$3 = /* @__PURE__ */ _export_sfc(_sfc_main$m, [["render", _sfc_render$l], ["__scopeId", "data-v-0a0a5198"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-radio.vue"]]);
  const _sfc_main$l = {
    data() {
      return {
        // 总价
        totalprice: "",
        // 选择文本
        selectText: "全选",
        // 购物车数据 可以来自 api 请求或本地数据
        shoppingCard: [
          {
            "checked": true,
            "shopName": "hcoder 官方店",
            "shopId": "1",
            "items": [
              {
                "goodsId": 1,
                "goodsName": "hcoder 演示商品",
                "price": 10,
                "count": 1,
                "img": "https://m.360buyimg.com/babel/jfs/t1/3730/7/3438/394579/5b996f2eE1727c59e/373cf10d42a53b72.jpg",
                "checked": true
              },
              {
                "goodsId": 2,
                "goodsName": "dcloud 演示商品",
                "price": 20,
                "count": 1,
                "img": "https://img14.360buyimg.com/n7/jfs/t1/1156/8/14017/123589/5bd9a4e8E7dbd4a15/70fbbccdf8811111.jpg",
                "checked": true
              }
            ]
          },
          {
            "checked": true,
            "shopName": "grace 官方旗舰店",
            "shopId": "2",
            "items": [{
              "goodsId": 3,
              "goodsName": "uni-app 演示商品",
              "price": 30,
              "count": 2,
              "img": "https://img10.360buyimg.com/n7/jfs/t19690/263/1947634738/190301/ad172397/5adfe5eaN42591f8c.jpg",
              "checked": true
            }]
          }
        ]
      };
    },
    onLoad: function() {
      this.countTotoal();
    },
    methods: {
      //计算总计函数
      countTotoal: function() {
        var total = 0;
        for (var i = 0; i < this.shoppingCard.length; i++) {
          for (var ii = 0; ii < this.shoppingCard[i].items.length; ii++) {
            if (this.shoppingCard[i].items[ii].checked) {
              total += Number(this.shoppingCard[i].items[ii].price) * Number(this.shoppingCard[i].items[ii].count);
            }
          }
        }
        this.totalprice = total;
      },
      numberChange: function(data) {
        this.shoppingCard[data[2]].items[data[1]].count = data[0];
        this.countTotoal();
      },
      removeGoods: function(e) {
        var index = e.currentTarget.id.replace("removeIndex_", "");
        index = index.split("_");
        var index1 = Number(index[0]);
        var index2 = Number(index[1]);
        uni.showModal({
          title: "确认提醒",
          content: "您确定要移除此商品吗？",
          success: (e2) => {
            if (e2.confirm) {
              this.shoppingCard[index1].items.splice(index2, 1);
              if (this.shoppingCard[index1].items.length < 1) {
                this.shoppingCard.splice(index1, 1);
              }
              this.countTotoal();
            }
          }
        });
      },
      checkout: function() {
        uni.showToast({
          title: "计算的数据保存在 shoppingCard 变量内 ^_^",
          icon: "none"
        });
      },
      // 店铺选中按钮状态切换
      shopChange: function(e) {
        var index = Number(e[1]);
        this.shoppingCard[index].checked = e[0];
        for (let i = 0; i < this.shoppingCard[index].items.length; i++) {
          this.shoppingCard[index].items[i].checked = e[0];
        }
        this.shoppingCard.splice(index, this.shoppingCard[index]);
        this.countTotoal();
      },
      // 商品选中
      itemChange: function(e) {
        var indexs = e[1].toString();
        var index = indexs.split("-");
        index[0] = Number(index[0]);
        index[1] = Number(index[1]);
        this.shoppingCard[index[0]].items[index[1]].checked = e[0];
        this.shoppingCard.splice(index[0], this.shoppingCard[index[0]]);
        var checkedNum = 0;
        for (let i = 0; i < this.shoppingCard[index[0]].items.length; i++) {
          if (!this.shoppingCard[index[0]].items[i].checked) {
            checkedNum++;
          }
        }
        if (checkedNum < 1) {
          this.shoppingCard[index[0]].checked = true;
        } else {
          this.shoppingCard[index[0]].checked = false;
        }
        this.shoppingCard = this.shoppingCard;
        this.countTotoal();
      },
      itemChangeAll: function(e) {
        this.selectText = e[0] ? "全选" : "全不选";
        for (var i = 0; i < this.shoppingCard.length; i++) {
          this.shoppingCard[i].checked = e[0];
          for (var ii = 0; ii < this.shoppingCard[i].items.length; ii++) {
            this.shoppingCard[i].items[ii].checked = e[0];
          }
        }
        this.countTotoal();
      },
      handleBack() {
        uni.navigateBack({ delta: 1 });
      }
    }
  };
  function _sfc_render$k(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_gui_empty = resolveEasycom(vue.resolveDynamicComponent("gui-empty"), __easycom_1$4);
    const _component_gui_radio = resolveEasycom(vue.resolveDynamicComponent("gui-radio"), __easycom_2$3);
    const _component_gui_step_box = resolveEasycom(vue.resolveDynamicComponent("gui-step-box"), __easycom_3);
    const _component_gui_iphone_bottom = resolveEasycom(vue.resolveDynamicComponent("gui-iphone-bottom"), __easycom_4$1);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createElementVNode("view", null, [
            vue.createVNode(_component_logo, {
              title: "购物车",
              version: "",
              issearch: false,
              onBack: $options.handleBack,
              showback: ""
            }, null, 8, ["onBack"])
          ]),
          vue.createCommentVNode(" 购物车为空 "),
          $data.shoppingCard.length < 1 ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 0,
            style: { "margin-top": "100px" }
          }, [
            _ctx.items == null ? (vue.openBlock(), vue.createBlock(_component_gui_empty, { key: 0 }, {
              img: vue.withCtx(() => [
                vue.createElementVNode("view", { class: "gui-flex gui-row gui-justify-content-center" }, [
                  vue.createElementVNode("text", { class: "gui-icons gui-empty-icon gui-color-gray" }, "")
                ])
              ]),
              text: vue.withCtx(() => [
                vue.createElementVNode("text", { class: "gui-text-small gui-block gui-text-center gui-margin-top gui-color-gray" }, "购物车空空如也 ~")
              ]),
              _: 1
              /* STABLE */
            })) : vue.createCommentVNode("v-if", true)
          ])) : vue.createCommentVNode("v-if", true),
          vue.createCommentVNode(" 购物车主结构  "),
          vue.createElementVNode("view", { class: "gui-common-line" }),
          (vue.openBlock(true), vue.createElementBlock(
            vue.Fragment,
            null,
            vue.renderList($data.shoppingCard, (item, index) => {
              return vue.openBlock(), vue.createElementBlock("view", { key: index }, [
                vue.createElementVNode("view", { class: "gui-shoppingcard gui-bg-white gui-dark-bg-level-3" }, [
                  vue.createElementVNode("view", { class: "gui-flex gui-rows gui-nowrap gui-align-items-center" }, [
                    vue.createElementVNode("view", { class: "gui-shopp-name" }, [
                      vue.createVNode(_component_gui_radio, {
                        onChange: $options.shopChange,
                        parameter: [index],
                        checked: item.checked
                      }, {
                        default: vue.withCtx(() => [
                          vue.createElementVNode(
                            "text",
                            { class: "gui-text gui-primary-text gui-bold" },
                            vue.toDisplayString(item.shopName),
                            1
                            /* TEXT */
                          )
                        ]),
                        _: 2
                        /* DYNAMIC */
                      }, 1032, ["onChange", "parameter", "checked"])
                    ]),
                    vue.createElementVNode("text", { class: "gui-shopp-go gui-text-small gui-icons gui-block gui-color-gray" }, "进店 ")
                  ]),
                  vue.createElementVNode("view", { style: { "height": "25rpx" } }),
                  (vue.openBlock(true), vue.createElementBlock(
                    vue.Fragment,
                    null,
                    vue.renderList(item.items, (goods, indexItem) => {
                      return vue.openBlock(), vue.createElementBlock("view", {
                        class: "gui-shoppingcard-goods gui-flex gui-row gui-nowrap gui-space-between",
                        key: indexItem
                      }, [
                        vue.createElementVNode("view", { class: "gui-shoppingcard-goods-checkbtn" }, [
                          vue.createVNode(_component_gui_radio, {
                            checked: goods.checked,
                            onChange: $options.itemChange,
                            parameter: [index + "-" + indexItem]
                          }, null, 8, ["checked", "onChange", "parameter"])
                        ]),
                        vue.createElementVNode("image", {
                          src: goods.img,
                          mode: "widthFix",
                          class: "gui-shoppingcard-goods-image"
                        }, null, 8, ["src"]),
                        vue.createElementVNode("view", { class: "gui-shoppingcard-goods-body" }, [
                          vue.createElementVNode(
                            "text",
                            { class: "gui-shoppingcard-goods-title gui-text gui-primary-text gui-block" },
                            vue.toDisplayString(goods.goodsName),
                            1
                            /* TEXT */
                          ),
                          vue.createElementVNode("view", { class: "gui-flex gui-rows gui-space-between" }, [
                            vue.createElementVNode(
                              "text",
                              { class: "gui-shoppingcard-goods-price gui-block" },
                              "￥" + vue.toDisplayString(goods.price),
                              1
                              /* TEXT */
                            ),
                            vue.createElementVNode("view", { class: "gui-shoppingcard-goods-number" }, [
                              vue.createVNode(_component_gui_step_box, {
                                disabled: true,
                                index: indexItem,
                                datas: [index + ""],
                                value: goods.count,
                                maxNum: 100,
                                minNum: 1,
                                onChange: $options.numberChange
                              }, null, 8, ["index", "datas", "value", "onChange"])
                            ])
                          ]),
                          vue.createElementVNode("text", {
                            class: "gui-shoppingcard-remove gui-block gui-icons gui-color-gray",
                            onClick: _cache[0] || (_cache[0] = (...args) => $options.removeGoods && $options.removeGoods(...args)),
                            id: "removeIndex_" + index + "_" + indexItem
                          }, " 删除", 8, ["id"])
                        ])
                      ]);
                    }),
                    128
                    /* KEYED_FRAGMENT */
                  ))
                ]),
                vue.createElementVNode("view", { class: "gui-common-line" })
              ]);
            }),
            128
            /* KEYED_FRAGMENT */
          )),
          vue.createCommentVNode(" 购物车底部 "),
          vue.createElementVNode("view", { style: { "height": "120rpx" } }),
          vue.createElementVNode("view", { class: "gui-shoppingcard-footer gui-bg-gray gui-dark-bg-level-1 gui-border-box gui-border-t" }, [
            vue.createElementVNode("view", { class: "gui-flex gui-row gui-space-between gui-align-items-center" }, [
              vue.createElementVNode("view", { class: "gui-shoppingcard-checkbtn gui-flex grace-nowrap" }, [
                vue.createVNode(_component_gui_radio, {
                  checked: true,
                  onChange: $options.itemChangeAll
                }, {
                  default: vue.withCtx(() => [
                    vue.createElementVNode(
                      "text",
                      { class: "gui-text gui-primary-text" },
                      vue.toDisplayString($data.selectText),
                      1
                      /* TEXT */
                    )
                  ]),
                  _: 1
                  /* STABLE */
                }, 8, ["onChange"])
              ]),
              vue.createElementVNode("view", { class: "gui-shoppingcard-count gui-flex gui-row gui-nowrap" }, [
                vue.createElementVNode("text", { class: "gui-text gui-primary-text" }, "合计 :"),
                vue.createElementVNode(
                  "text",
                  {
                    style: { "font-size": "32rpx" },
                    class: "gui-color-red"
                  },
                  "￥" + vue.toDisplayString($data.totalprice),
                  1
                  /* TEXT */
                )
              ]),
              vue.createElementVNode("text", {
                class: "gui-shoppingcard-checkout gui-bg-red gui-block gui-color-white",
                onClick: _cache[1] || (_cache[1] = (...args) => $options.checkout && $options.checkout(...args))
              }, "立即结算")
            ]),
            vue.createCommentVNode(" 如果购物车处于switch页面，请删除下面的组件 "),
            vue.createElementVNode("view", null, [
              vue.createVNode(_component_gui_iphone_bottom)
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesShopcartShopcart = /* @__PURE__ */ _export_sfc(_sfc_main$l, [["render", _sfc_render$k], ["__scopeId", "data-v-83947b95"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/shopcart/shopcart.vue"]]);
  const _sfc_main$k = {
    props: {
      fsize: {
        type: String,
        default: "fz14"
      },
      title: {
        type: String,
        default: "历史记录"
      },
      ctitle: {
        type: String,
        default: "清空记录"
      }
    },
    name: "nav",
    data() {
      return {};
    },
    methods: {
      handleClick() {
        this.$emits("clear");
      }
    }
  };
  function _sfc_render$j(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-flex gui-space-between ql-align-center" }, [
      vue.createElementVNode(
        "view",
        {
          class: vue.normalizeClass(["gui-color-black1 fb", [$props.fsize]])
        },
        vue.toDisplayString($props.title),
        3
        /* TEXT, CLASS */
      ),
      vue.createElementVNode(
        "view",
        {
          class: "gui-color-red fz11",
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
        },
        vue.toDisplayString($props.ctitle),
        1
        /* TEXT */
      )
    ]);
  }
  const __easycom_1$3 = /* @__PURE__ */ _export_sfc(_sfc_main$k, [["render", _sfc_render$j], ["__file", "D:/App/分销系统/cc_fenxiao/components/title/nav.vue"]]);
  const _sfc_main$j = {
    name: "gui-tags",
    props: {
      width: { type: Number, default: 0 },
      text: { type: String, default: "" },
      size: { type: Number, default: 26 },
      lineHeight: { type: Number, default: 1.8 },
      padding: { type: Number, default: 15 },
      margin: { type: Number, default: 15 },
      customClass: { type: Array, default: function() {
        return ["gui-bg-primary", "gui-color-white"];
      } },
      borderRadius: { type: Number, default: 6 },
      data: { type: Array, default: function() {
        return [];
      } },
      borderColor: { type: String, default: "rgba(255,255,255,0)" }
    },
    data() {
      return {
        tapping: false
      };
    },
    methods: {
      tapme: function() {
        this.$emit("tapme", this.data);
      }
    },
    emits: ["tapme"]
  };
  function _sfc_render$i(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock(
      "text",
      {
        class: vue.normalizeClass(["gui-block gui-tags gui-ellipsis gui-border", $props.customClass]),
        style: vue.normalizeStyle({
          width: $props.width == 0 ? "" : $props.width + "rpx",
          paddingLeft: $props.padding + "rpx",
          paddingRight: $props.padding + "rpx",
          lineHeight: $props.size * $props.lineHeight + "rpx",
          height: $props.size * $props.lineHeight + "rpx",
          fontSize: $props.size + "rpx",
          borderRadius: $props.borderRadius + "rpx",
          marginRight: $props.margin + "rpx",
          marginBottom: $props.margin + "rpx",
          borderColor: $props.borderColor + " !important"
        }),
        onClick: _cache[0] || (_cache[0] = (...args) => $options.tapme && $options.tapme(...args))
      },
      vue.toDisplayString($props.text),
      7
      /* TEXT, CLASS, STYLE */
    );
  }
  const __easycom_2$2 = /* @__PURE__ */ _export_sfc(_sfc_main$j, [["render", _sfc_render$i], ["__scopeId", "data-v-d4c871d4"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-tags.vue"]]);
  const _sfc_main$i = {
    data() {
      return {
        tags: ["AI创造合集", "商业插画入门", "哈哈", "家庭AI", "绘画"]
      };
    },
    methods: {
      handleSearch(e) {
        uni.navigateTo({
          url: `/pages/search/detail?keyword=${e}`
        });
      }
    }
  };
  function _sfc_render$h(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_search = resolveEasycom(vue.resolveDynamicComponent("gui-search"), __easycom_0$d);
    const _component_cc_title_nav = resolveEasycom(vue.resolveDynamicComponent("cc-title-nav"), __easycom_1$3);
    const _component_gui_tags = resolveEasycom(vue.resolveDynamicComponent("gui-tags"), __easycom_2$2);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createCommentVNode(" 搜索框 "),
          vue.createElementVNode("view", { class: "header-search gui-bg-white gui-dark-bg-level-3 gui-border-box" }, [
            vue.createVNode(_component_gui_search, {
              onClear: _ctx.search,
              placeholder: "搜索课程",
              onConfirm: _ctx.search
            }, null, 8, ["onClear", "onConfirm"])
          ]),
          vue.createVNode(_component_cc_title_nav, {
            title: "历史记录",
            fsize: "fz14",
            ctitle: "清空记录"
          }),
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-wrap mt10" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList($data.tags, (tag, index) => {
                return vue.openBlock(), vue.createBlock(_component_gui_tags, {
                  key: index,
                  text: tag,
                  customClass: ["gui-transparent-bg", "gui-color-gray"],
                  lineHeight: 2.2,
                  size: 26,
                  onTapme: ($event) => $options.handleSearch(tag),
                  borderColor: "#eee"
                }, null, 8, ["text", "onTapme"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ]),
          vue.createVNode(_component_cc_title_nav, {
            title: "热门搜索",
            fsize: "fz14",
            ctitle: ""
          }),
          vue.createElementVNode("view", { class: "gui-flex gui-row gui-wrap mt10" }, [
            (vue.openBlock(true), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList($data.tags, (tag, index) => {
                return vue.openBlock(), vue.createBlock(_component_gui_tags, {
                  key: index,
                  text: tag,
                  customClass: ["gui-transparent-bg", "gui-color-gray"],
                  lineHeight: 2.2,
                  size: 26,
                  onTapme: ($event) => $options.handleSearch(tag),
                  borderColor: "#eee"
                }, null, 8, ["text", "onTapme"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesSearchSearch = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["render", _sfc_render$h], ["__file", "D:/App/分销系统/cc_fenxiao/pages/search/search.vue"]]);
  const _sfc_main$h = {
    components: {},
    props: {
      waterfall: {
        type: Boolean,
        default: true
      }
    },
    data() {
      return {
        isLoad: false,
        loading: false,
        // 数据表名
        collection: "opendb-mall-goods",
        // 查询字段，多个字段用 , 分割
        field: "goods_thumb,name,goods_tip,tag,goods_price,comment_count,month_sell_count,shop_name",
        productList: [{
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/ted.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/xue.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/deng.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/cp.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/ted.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/deng.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/deng.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }, {
          name: "早起5分钟弓箭步蹲，胜过跑步一小时，瘦腿瘦臀，更显年轻",
          goods_thumb: "/static/list/xueba.png",
          goods_tip: "自营",
          tag: ["手机", "iphone", "笔记本"],
          goods_price: "3499",
          comment_count: "1244",
          month_sell_count: "640",
          shop_name: "飞哥的课程推荐"
        }],
        formData: {
          waterfall: true,
          // 布局方向切换
          status: "noMore"
          // 加载状态
        },
        tipShow: true
        // 是否显示顶部提示框
      };
    },
    created() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    },
    methods: {
      load(data, ended) {
        if (ended) {
          this.formData.status = "noMore";
        }
      },
      navchange() {
      }
    },
    /**
     * 下拉刷新回调函数
     */
    onPullDownRefresh() {
      this.formData.status = "more";
      this.$refs.udb.loadData({
        clear: true
      }, () => {
        this.tipShow = true;
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          this.tipShow = false;
        }, 1e3);
        uni.stopPullDownRefresh();
      });
    },
    /**
     * 上拉加载回调函数
     */
    onReachBottom() {
      this.$refs.udb.loadMore();
    }
  };
  function _sfc_render$g(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_uni_list_item = resolveEasycom(vue.resolveDynamicComponent("uni-list-item"), __easycom_0$8);
    const _component_uni_list = resolveEasycom(vue.resolveDynamicComponent("uni-list"), __easycom_1$a);
    const _component_uni_load_more = resolveEasycom(vue.resolveDynamicComponent("uni-load-more"), __easycom_2$7);
    return vue.openBlock(), vue.createElementBlock(
      vue.Fragment,
      null,
      [
        vue.createCommentVNode("\r\n	本页面模板教程：https://ext.dcloud.net.cn/plugin?id=2651\r\n	uni-list 文档：https://ext.dcloud.net.cn/plugin?id=24\r\n	uniCloud 文档：https://uniapp.dcloud.io/uniCloud/README\r\n	unicloud-db 组件文档：https://uniapp.dcloud.io/uniCloud/unicloud-db\r\n	DB Schema 规范：https://uniapp.dcloud.net.cn/uniCloud/schema\r\n	 "),
        vue.createElementVNode("view", { class: "list" }, [
          vue.createCommentVNode(" 基于 uni-list 的页面布局 "),
          vue.createVNode(_component_uni_list, {
            class: vue.normalizeClass({ "uni-list--waterfall": $props.waterfall })
          }, {
            default: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "uni-list" }, [
                vue.createCommentVNode(" 通过 uni-list--waterfall 类决定页面布局方向 "),
                vue.createCommentVNode(" to 属性携带参数跳转详情页面，当前只为参考 "),
                (vue.openBlock(true), vue.createElementBlock(
                  vue.Fragment,
                  null,
                  vue.renderList($data.productList, (item) => {
                    return vue.openBlock(), vue.createBlock(_component_uni_list_item, {
                      border: !$props.waterfall,
                      class: "uni-list-item--waterfall",
                      title: "自定义商品列表",
                      key: item._id,
                      to: "/pages/detail/detail?id=" + item._id + "&title=" + item.name
                    }, {
                      header: vue.withCtx(() => [
                        !$data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            key: 0,
                            class: vue.normalizeClass(["uni-thumb demo-bg shop-picture", { "shop-picture-column": $props.waterfall }])
                          },
                          null,
                          2
                          /* CLASS */
                        )) : vue.createCommentVNode("v-if", true),
                        $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                          "view",
                          {
                            key: 1,
                            class: vue.normalizeClass(["uni-thumb shop-picture", { "shop-picture-column": $props.waterfall }])
                          },
                          [
                            vue.createElementVNode("image", {
                              src: item.goods_thumb,
                              mode: "aspectFill"
                            }, null, 8, ["src"])
                          ],
                          2
                          /* CLASS */
                        )) : vue.createCommentVNode("v-if", true)
                      ]),
                      body: vue.withCtx(() => [
                        vue.createElementVNode("view", { class: "shop" }, [
                          vue.createElementVNode("view", null, [
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "uni-title demo-bg demo-title"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 1,
                              class: "uni-title"
                            }, [
                              vue.createElementVNode(
                                "text",
                                { class: "uni-ellipsis-2" },
                                vue.toDisplayString(item.name),
                                1
                                /* TEXT */
                              )
                            ])) : vue.createCommentVNode("v-if", true),
                            vue.createElementVNode("view", { class: "gui-flex" }, [
                              !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                                key: 0,
                                class: "uni-note demo-bg demo-tag"
                              })) : vue.createCommentVNode("v-if", true),
                              !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                                key: 1,
                                class: "uni-note demo-bg demo-tag",
                                style: { "margin-left": "10px" }
                              })) : vue.createCommentVNode("v-if", true),
                              !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                                key: 2,
                                class: "uni-note demo-bg demo-tag",
                                style: { "margin-left": "10px" }
                              })) : vue.createCommentVNode("v-if", true),
                              $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                                "text",
                                {
                                  key: 3,
                                  class: "uni-tag hot-tag"
                                },
                                vue.toDisplayString(item.goods_tip),
                                1
                                /* TEXT */
                              )) : vue.createCommentVNode("v-if", true),
                              $data.isLoad ? (vue.openBlock(true), vue.createElementBlock(
                                vue.Fragment,
                                { key: 4 },
                                vue.renderList(item.tag, (tag) => {
                                  return vue.openBlock(), vue.createElementBlock(
                                    "text",
                                    {
                                      key: tag,
                                      class: "uni-tag"
                                    },
                                    vue.toDisplayString(tag),
                                    1
                                    /* TEXT */
                                  );
                                }),
                                128
                                /* KEYED_FRAGMENT */
                              )) : vue.createCommentVNode("v-if", true)
                            ])
                          ]),
                          vue.createElementVNode("view", null, [
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 0,
                              class: "uni-note demo-bg demo-tag"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 1,
                              class: "shop-price"
                            }, [
                              vue.createElementVNode("text", null, "¥"),
                              vue.createElementVNode(
                                "text",
                                { class: "shop-price-text" },
                                vue.toDisplayString(item.goods_price),
                                1
                                /* TEXT */
                              ),
                              vue.createElementVNode("text", null, ".00")
                            ])) : vue.createCommentVNode("v-if", true),
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 2,
                              class: "uni-note demo-bg demo-space"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock(
                              "view",
                              {
                                key: 3,
                                class: "uni-note"
                              },
                              vue.toDisplayString(item.comment_count) + "条评论 月销量 " + vue.toDisplayString(item.month_sell_count),
                              1
                              /* TEXT */
                            )) : vue.createCommentVNode("v-if", true),
                            !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 4,
                              class: "uni-note demo-bg demo-space"
                            })) : vue.createCommentVNode("v-if", true),
                            $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
                              key: 5,
                              class: "uni-note ellipsis"
                            }, [
                              vue.createElementVNode(
                                "text",
                                { class: "uni-ellipsis-1" },
                                vue.toDisplayString(item.shop_name),
                                1
                                /* TEXT */
                              ),
                              vue.createElementVNode("text", { class: "uni-link" }, "进店 >")
                            ])) : vue.createCommentVNode("v-if", true)
                          ])
                        ])
                      ]),
                      _: 2
                      /* DYNAMIC */
                    }, 1032, ["border", "to"]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ])
            ]),
            _: 1
            /* STABLE */
          }, 8, ["class"]),
          vue.createCommentVNode(" 通过 loadMore 组件实现上拉加载效果，如需自定义显示内容，可参考：https://ext.dcloud.net.cn/plugin?id=29 "),
          $data.isLoad && ($data.loading || $data.formData.status === "noMore") ? (vue.openBlock(), vue.createBlock(_component_uni_load_more, {
            key: 0,
            status: $data.formData.status
          }, null, 8, ["status"])) : vue.createCommentVNode("v-if", true)
        ])
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  const __easycom_2$1 = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["render", _sfc_render$g], ["__scopeId", "data-v-b686f227"], ["__file", "D:/App/分销系统/cc_fenxiao/components/search/course.vue"]]);
  const _sfc_main$g = {
    data() {
      return {
        isLoad: false,
        navItems: [{
          id: 1,
          name: "编程开发"
        }, {
          id: 1,
          name: "设计创作"
        }, {
          id: 2,
          name: "数据分析"
        }, {
          id: 2,
          name: "人工智能"
        }, {
          id: 2,
          name: "新媒体"
        }],
        tags: ["AI创造合集", "商业插画入门", "哈哈", "家庭AI", "绘画"]
      };
    },
    methods: {
      handleSearch(e) {
        uni.navigateTo({
          url: `/pages/search/detail?keyword=${e}`
        });
      },
      navchange() {
      }
    },
    onLoad() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    }
  };
  function _sfc_render$f(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_search = resolveEasycom(vue.resolveDynamicComponent("gui-search"), __easycom_0$d);
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_cc_search_course = resolveEasycom(vue.resolveDynamicComponent("cc-search-course"), __easycom_2$1);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createCommentVNode(" 搜索框 "),
          !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 0,
            class: "demo-bg demo-title header-search gui-border-radius"
          })) : vue.createCommentVNode("v-if", true),
          $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 1,
            class: "header-search gui-bg-white gui-dark-bg-level-3 gui-border-box"
          }, [
            vue.createVNode(_component_gui_search, {
              onClear: _ctx.search,
              placeholder: "搜索课程",
              onConfirm: _ctx.search
            }, null, 8, ["onClear", "onConfirm"])
          ])) : vue.createCommentVNode("v-if", true),
          !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 2,
            class: "demo-bg demo-title gui-border-radius mt10"
          })) : vue.createCommentVNode("v-if", true),
          $data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 3,
            class: "mt10"
          }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              textAlign: "center",
              isCenter: true,
              activeDirection: "center",
              size: 0,
              margin: 20,
              padding: "10rpx",
              onChange: $options.navchange
            }, null, 8, ["items", "onChange"])
          ])) : vue.createCommentVNode("v-if", true),
          vue.createVNode(_component_cc_search_course, { waterfall: false })
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesSearchDetail = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["render", _sfc_render$f], ["__file", "D:/App/分销系统/cc_fenxiao/pages/search/detail.vue"]]);
  const _sfc_main$f = {
    data() {
      return {
        formData: {
          name1: "GraceUI",
          name2: "888888",
          name3: "888888",
          name4: "888888@qq.com",
          name5: "请选择性别",
          name7: "123456",
          name8: ""
        },
        gender: ["请选择性别", "男", "女"],
        genderIndex: 0
      };
    },
    methods: {
      submit: function(e) {
        var rule = [
          { name: "name1", checkType: "string", checkRule: "1,10", errorMsg: "**应为1-10个字符" },
          { name: "name2", checkType: "string", checkRule: "6,", errorMsg: "密码至少6个字符" },
          { name: "name3", checkType: "samewith", checkRule: "name2", errorMsg: "两次密码输入不一致" },
          { name: "name4", checkType: "email", checkRule: "", errorMsg: "邮箱格式错误" },
          { name: "name5", checkType: "notsame", checkRule: "请选择性别", errorMsg: "请选择性别" },
          // 正则表达式验证演示 : 例如检查必须是有效数字
          { name: "name7", checkType: "reg", checkRule: "^(-?\\d+)$", errorMsg: "必须是有效数字" },
          { name: "name8", checkType: "idCard", checkRule: "", errorMsg: "身份证号码错误" }
        ];
        var formData = e.detail.value;
        formData.name5 = this.formData.name5;
        formatAppLog("log", "at pages/user/bind.vue:158", formData);
        var checkRes = graceChecker.check(formData, rule);
        if (checkRes) {
          if (formData.name6.length < 1) {
            uni.showToast({ title: "请选择爱好!", icon: "none" });
            return false;
          }
          uni.showToast({ title: "验证通过!", icon: "none" });
        } else {
          uni.showToast({ title: graceChecker.error, icon: "none" });
        }
      },
      pickerChange: function(e) {
        this.genderIndex = e.detail.value;
        this.formData.name5 = this.gender[this.genderIndex];
      }
    }
  };
  function _sfc_render$e(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { style: { "background": "#f3f3f3" } }, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", { class: "gui-bg-white detail-items" }, [
            vue.createElementVNode(
              "form",
              {
                onSubmit: _cache[2] || (_cache[2] = (...args) => $options.submit && $options.submit(...args)),
                class: "mt15"
              },
              [
                vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding-x" }, [
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "支付宝账号"),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "text",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => $data.formData.name1 = $event),
                          name: "name1",
                          placeholder: "手机 / 邮箱"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name1]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "微信账号"),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "password",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.formData.name3 = $event),
                          name: "name3",
                          placeholder: "手机"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name3]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "uni-list-bar" })
                ]),
                vue.createElementVNode("view", { class: "gui-margin-top gui-flex gui-row gui-nowrap gui-justify-content-end gui-padding-x" }, [
                  vue.createElementVNode("button", {
                    type: "default",
                    class: "gui-button gui-bg-primary gui-noborder",
                    style: { "width": "168rpx" },
                    formType: "submit"
                  }, [
                    vue.createElementVNode("text", { class: "gui-color-white gui-button-text" }, "提交")
                  ])
                ]),
                vue.createElementVNode("view", { style: { "height": "60rpx" } })
              ],
              32
              /* NEED_HYDRATION */
            )
          ]),
          vue.createElementVNode("view", { class: "gui-bg-white detail-items mt15" }, [
            vue.createElementVNode(
              "form",
              {
                onSubmit: _cache[7] || (_cache[7] = (...args) => $options.submit && $options.submit(...args)),
                class: "mt15"
              },
              [
                vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding-x" }, [
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "银行名称："),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "text",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[3] || (_cache[3] = ($event) => $data.formData.name1 = $event),
                          name: "name1",
                          placeholder: "手机 / 邮箱"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name1]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "开户行姓名："),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "text",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[4] || (_cache[4] = ($event) => $data.formData.name1 = $event),
                          name: "name1",
                          placeholder: "手机 / 邮箱"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name1]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "银行账号"),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "text",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[5] || (_cache[5] = ($event) => $data.formData.name1 = $event),
                          name: "name1",
                          placeholder: "手机 / 邮箱"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name1]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                    vue.createElementVNode("text", { class: "gui-form-label" }, "开户行地址"),
                    vue.createElementVNode("view", { class: "gui-form-body" }, [
                      vue.withDirectives(vue.createElementVNode(
                        "input",
                        {
                          type: "password",
                          class: "gui-form-input",
                          "onUpdate:modelValue": _cache[6] || (_cache[6] = ($event) => $data.formData.name3 = $event),
                          name: "name3",
                          placeholder: "手机"
                        },
                        null,
                        512
                        /* NEED_PATCH */
                      ), [
                        [vue.vModelText, $data.formData.name3]
                      ])
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "uni-list-bar" })
                ]),
                vue.createElementVNode("view", { class: "gui-margin-top gui-flex gui-row gui-nowrap gui-justify-content-end gui-padding-x" }, [
                  vue.createElementVNode("button", {
                    type: "default",
                    class: "gui-button gui-bg-primary gui-noborder",
                    style: { "width": "168rpx" },
                    formType: "submit"
                  }, [
                    vue.createElementVNode("text", { class: "gui-color-white gui-button-text" }, "提交")
                  ])
                ]),
                vue.createElementVNode("view", { style: { "height": "60rpx" } })
              ],
              32
              /* NEED_HYDRATION */
            )
          ]),
          vue.createElementVNode("view", { class: "gui-bg-white detail-items mt15" }, [
            vue.createElementVNode("view", { class: "fb fz16" }, "填写须知"),
            vue.createElementVNode("view", { class: "tipbox mt15" }, [
              vue.createElementVNode("view", { class: "t" }, "1: 支付宝/微信账户姓名必须与实名认证名保持一致，否则无法提现"),
              vue.createElementVNode("view", { class: "t" }, "2: 每日支付宝/微信个人账户转入上线是5万元，企业账户转入上线是10万元，如超出额度请联系客服。"),
              vue.createElementVNode("view", { class: "t" }, "3: 目前使用支付宝/微信账户进行提现，但若后期平台策略发生变更将更具后续进行调整和另行通知，谢谢。")
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesUserBind = /* @__PURE__ */ _export_sfc(_sfc_main$f, [["render", _sfc_render$e], ["__scopeId", "data-v-3aa84f84"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/bind.vue"]]);
  const _sfc_main$e = {
    data() {
      return {
        formData: {
          name1: "GraceUI",
          name2: "888888",
          name3: "888888",
          name4: "888888@qq.com",
          name5: "请选择性别",
          name7: "123456",
          name8: ""
        },
        gender: ["请选择性别", "男", "女"],
        genderIndex: 0
      };
    },
    methods: {
      submit: function(e) {
        var rule = [
          { name: "name1", checkType: "string", checkRule: "1,10", errorMsg: "**应为1-10个字符" },
          { name: "name2", checkType: "string", checkRule: "6,", errorMsg: "密码至少6个字符" },
          { name: "name3", checkType: "samewith", checkRule: "name2", errorMsg: "两次密码输入不一致" },
          { name: "name4", checkType: "email", checkRule: "", errorMsg: "邮箱格式错误" },
          { name: "name5", checkType: "notsame", checkRule: "请选择性别", errorMsg: "请选择性别" },
          // 正则表达式验证演示 : 例如检查必须是有效数字
          { name: "name7", checkType: "reg", checkRule: "^(-?\\d+)$", errorMsg: "必须是有效数字" },
          { name: "name8", checkType: "idCard", checkRule: "", errorMsg: "身份证号码错误" }
        ];
        var formData = e.detail.value;
        formData.name5 = this.formData.name5;
        formatAppLog("log", "at pages/user/settings.vue:184", formData);
        var checkRes = graceChecker.check(formData, rule);
        if (checkRes) {
          if (formData.name6.length < 1) {
            uni.showToast({ title: "请选择爱好!", icon: "none" });
            return false;
          }
          uni.showToast({ title: "验证通过!", icon: "none" });
        } else {
          uni.showToast({ title: graceChecker.error, icon: "none" });
        }
      },
      pickerChange: function(e) {
        this.genderIndex = e.detail.value;
        this.formData.name5 = this.gender[this.genderIndex];
      }
    }
  };
  function _sfc_render$d(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, null, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createElementVNode("text", { class: "gui-text gui-color-gray" }, "表单验证演示")
        ]),
        vue.createElementVNode("view", null, [
          vue.createElementVNode(
            "form",
            {
              onSubmit: _cache[7] || (_cache[7] = (...args) => $options.submit && $options.submit(...args))
            },
            [
              vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding-x" }, [
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "字符验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "text",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => $data.formData.name1 = $event),
                        name: "name1",
                        placeholder: "请输入内容"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name1]
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "密码验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "password",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.formData.name2 = $event),
                        name: "name2",
                        placeholder: "请输入内容"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name2]
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "密码验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "password",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[2] || (_cache[2] = ($event) => $data.formData.name3 = $event),
                        name: "name3",
                        placeholder: "请输入内容"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name3]
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "邮箱验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "text",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[3] || (_cache[3] = ($event) => $data.formData.name4 = $event),
                        name: "name4",
                        placeholder: "请输入内容"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name4]
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "选择验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.createElementVNode("picker", {
                      mode: "selector",
                      range: $data.gender,
                      onChange: _cache[4] || (_cache[4] = (...args) => $options.pickerChange && $options.pickerChange(...args))
                    }, [
                      vue.createElementVNode("view", { class: "gui-flex gui-rows gui-nowrap gui-space-between gui-align-items-center" }, [
                        vue.createElementVNode(
                          "text",
                          { class: "gui-text" },
                          vue.toDisplayString($data.gender[$data.genderIndex]),
                          1
                          /* TEXT */
                        ),
                        vue.createElementVNode("text", { class: "gui-form-icon gui-icons gui-text-center gui-color-gray" }, "")
                      ])
                    ], 40, ["range"])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "多选验证"),
                  vue.createElementVNode("view", {
                    class: "gui-form-body",
                    style: { "padding": "20rpx 0" }
                  }, [
                    vue.createElementVNode("checkbox-group", {
                      name: "name6",
                      class: "gui-flex gui-rows gui-wrap"
                    }, [
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "1" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "读书")
                      ]),
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "2" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "运动")
                      ]),
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "3" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "音乐")
                      ]),
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "4" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "书法")
                      ]),
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "5" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "旅行")
                      ]),
                      vue.createElementVNode("label", { class: "gui-check-item" }, [
                        vue.createElementVNode("checkbox", { value: "6" }),
                        vue.createElementVNode("text", { class: "gui-text gui-primary-color" }, "其他")
                      ])
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "正则验证"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "text",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[5] || (_cache[5] = ($event) => $data.formData.name7 = $event),
                        name: "name7",
                        placeholder: "请输入内容"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name7]
                    ])
                  ])
                ]),
                vue.createElementVNode("view", { class: "gui-form-item gui-border-b" }, [
                  vue.createElementVNode("text", { class: "gui-form-label" }, "身份证号"),
                  vue.createElementVNode("view", { class: "gui-form-body" }, [
                    vue.withDirectives(vue.createElementVNode(
                      "input",
                      {
                        type: "text",
                        class: "gui-form-input",
                        "onUpdate:modelValue": _cache[6] || (_cache[6] = ($event) => $data.formData.name8 = $event),
                        name: "name8",
                        placeholder: "请输入身份证号码"
                      },
                      null,
                      512
                      /* NEED_PATCH */
                    ), [
                      [vue.vModelText, $data.formData.name8]
                    ])
                  ])
                ])
              ]),
              vue.createElementVNode("view", { class: "gui-margin-top gui-flex gui-row gui-nowrap gui-justify-content-end gui-padding-x" }, [
                vue.createElementVNode("button", {
                  type: "default",
                  class: "gui-button gui-bg-primary gui-noborder",
                  style: { "width": "168rpx" },
                  formType: "submit"
                }, [
                  vue.createElementVNode("text", { class: "gui-color-white gui-button-text" }, "提交")
                ])
              ]),
              vue.createElementVNode("view", { style: { "height": "60rpx" } })
            ],
            32
            /* NEED_HYDRATION */
          )
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesUserSettings = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["render", _sfc_render$d], ["__file", "D:/App/分销系统/cc_fenxiao/pages/user/settings.vue"]]);
  const _sfc_main$d = {
    name: "gui-select-list",
    props: {
      items: { type: Array, default: function() {
        return [];
      } },
      type: { type: String, default: "radio" },
      checkedType: { type: String, default: "right" },
      isBorder: { type: Boolean, default: true },
      maxSize: { type: Number, default: 0 },
      checkedClass: { type: Array, default: function() {
        return ["gui-primary-color"];
      } }
    },
    data() {
      return {
        dataIn: []
      };
    },
    created: function() {
      this.dataIn = this.items;
    },
    watch: {
      items: function(val) {
        this.dataIn = val;
      }
    },
    methods: {
      // 获取选中数据的索引
      getSelectedIndex: function() {
        var tmpArr = [];
        this.dataIn.forEach((item, idx) => {
          if (item.checked) {
            tmpArr.push(idx);
          }
        });
        return tmpArr;
      },
      // 选择数据
      choose: function(e) {
        var index = e.currentTarget.dataset.index;
        if (this.type == "radio") {
          if (this.dataIn[index].checked) {
            this.dataIn[index].checked = false;
            this.$emit("change", -1);
          } else {
            for (let i = 0; i < this.dataIn.length; i++) {
              this.dataIn[i].checked = false;
            }
            this.dataIn[index].checked = true;
            this.$emit("change", index);
          }
        } else {
          if (this.dataIn[index].checked) {
            this.dataIn[index].checked = false;
          } else {
            if (this.maxSize > 0) {
              var size = 0;
              this.dataIn.forEach((item) => {
                if (item.checked) {
                  size++;
                }
              });
              size++;
              if (size > this.maxSize) {
                this.$emit("maxSed");
                return;
              }
            }
            this.dataIn[index].checked = true;
          }
          var sedArr = [];
          for (let i = 0; i < this.dataIn.length; i++) {
            if (this.dataIn[i].checked) {
              sedArr.push(i);
            }
          }
          this.$emit("change", sedArr);
        }
      }
    },
    emits: ["change", "maxSed"]
  };
  function _sfc_render$c(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "gui-select-list" }, [
      (vue.openBlock(true), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList($data.dataIn, (item, index) => {
          return vue.openBlock(), vue.createElementBlock("view", {
            key: index,
            class: "gui-select-list-item gui-flex gui-row gui-nowrap gui-align-items-center",
            "data-index": index,
            onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.choose && $options.choose(...args), ["stop"]))
          }, [
            $props.checkedType == "ring" && !item.checked ? (vue.openBlock(), vue.createElementBlock("text", {
              key: 0,
              class: "gui-select-list-icon gui-icons fb gui-block gui-select-list-ring gui-select-list-icon-l gui-color-gray"
            }, "")) : vue.createCommentVNode("v-if", true),
            $props.checkedType == "ring" && item.checked ? (vue.openBlock(), vue.createElementBlock(
              "text",
              {
                key: 1,
                class: vue.normalizeClass(["gui-select-list-icon gui-icons fb gui-block gui-select-list-ring gui-select-list-icon-l gui-fade-in", $props.checkedClass])
              },
              "",
              2
              /* CLASS */
            )) : vue.createCommentVNode("v-if", true),
            vue.createElementVNode(
              "view",
              {
                class: vue.normalizeClass(["gui-select-list-body gui-flex gui-row gui-nowrap gui-align-items-center", [$props.isBorder && index < $data.dataIn.length - 1 ? "gui-border-b" : ""]])
              },
              [
                item.img ? (vue.openBlock(), vue.createElementBlock("image", {
                  key: 0,
                  src: item.img,
                  class: "gui-select-list-img",
                  mode: "aspectFill"
                }, null, 8, ["src"])) : vue.createCommentVNode("v-if", true),
                vue.createElementVNode("view", { class: "gui-select-list-content" }, [
                  vue.createElementVNode(
                    "text",
                    { class: "gui-block gui-select-list-title gui-primary-text" },
                    vue.toDisplayString(item.title),
                    1
                    /* TEXT */
                  ),
                  item.desc ? (vue.openBlock(), vue.createElementBlock(
                    "text",
                    {
                      key: 0,
                      class: "gui-select-list-desc gui-block gui-third-text"
                    },
                    vue.toDisplayString(item.desc),
                    1
                    /* TEXT */
                  )) : vue.createCommentVNode("v-if", true)
                ]),
                item.checked && $props.checkedType == "right" ? (vue.openBlock(), vue.createElementBlock(
                  "text",
                  {
                    key: 1,
                    class: vue.normalizeClass(["gui-icons gui-block gui-select-list-icon fb", $props.checkedClass])
                  },
                  "",
                  2
                  /* CLASS */
                )) : vue.createCommentVNode("v-if", true)
              ],
              2
              /* CLASS */
            )
          ], 8, ["data-index"]);
        }),
        128
        /* KEYED_FRAGMENT */
      ))
    ]);
  }
  const __easycom_0$2 = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["render", _sfc_render$c], ["__scopeId", "data-v-1485e3e2"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-select-list.vue"]]);
  const _sfc_main$c = {
    name: "dialog",
    data() {
      return {
        open: false,
        nodes: [],
        xyhtml: `<div class="markdown-body">
				   <p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: center; color: rgb(0, 0, 0); white-space: normal; line-height: 21px;"><strong><span style="font-size: 16px; line-height: 24px; font-family: Arial;">&nbsp;</span></strong><strong><span style="font-size: 16px; line-height: 24px; font-family: 宋体;">学习512课堂用户服务协议</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px;"><span style="line-height: 21px; font-family: Arial;">&nbsp;</span></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">1. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">协议条款的确认和接纳</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">1.1 </span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">欢迎注册、登录学习512课堂平台（以下简称“本平台”或“学习512课堂”）、使用学习512课堂平台服务（以下简称“本服务”或“学习512课堂服务”），请您仔细阅读以下全部内容<strong><span style="text-decoration: underline;">（特别是粗体标注的内容）</span></strong>。本协议是您与学习512课堂之间关于学习512课堂服务的条款，内容包括本协议正文、</span><a href="http://reg.163.com/agreement_wap.shtml?v=20171127" target="_blank"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">《网易邮箱账号服务条款》、</span></a><a href="http://reg.163.com/agreement_mobile_wap.shtml" target="_blank"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">《网易手机账号服务条款》</span></a>&nbsp;<span style="font-size: 14px; line-height: 21px; font-family: 宋体;">、</span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">本协议明确援引的其他协议及</span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂已经发布的或将来可能发布的各类协议和规则。所有协议内容为本协议不可分割的组成部分，与本协议正文具有同等法律效力。除另行明确声明外，您使用学习512课堂服务的行为将受本协议约束。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">1.2 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">如您选择“点击同意”本协议或者以其他方式开始使用学习512课堂服务，即表示已经与学习512课堂达成协议，并自愿接受本协议的所有内容。您确认，您应当具备中华人民共和国法律规定的与您行为相适应的民事行为能力，确保有能力对您使用成诚科技公司提供服务的一切行为独立承担责任。若您不具备前述主体资格或您是未满十八周岁的未成年人，请在您的监护人的陪同下阅读本服务条款，并在取得他们对您使用服务的行为，以及对本服务条款的同意之后，使用本服务；成诚科技公司在依据法律规定或本协议约定要求您承担责任时，有权向您的监护人追偿。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">1.3 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂有权根据法律规范及运营的合理需要，不断修改和完善本协议，并在本平台（study.163.com）公告。如您继续使用学习512课堂服务，即意味着同意并自愿遵守修改后的协议条款，亦或您有权终止使用。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">定义</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.1 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">您</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">：指注册<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号并经学习512课堂审核同意后，在学习512课堂上观看、学习课程的用户。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.2 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂平台</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">（或学习512课堂不时修改的其他名称）：是成诚科技公司出品的，与优秀教育者与教育机构合作，为用户提供教学内容的生成、传播和消费服务，由成诚科技公司所有和经营的有关教育、学习等数字内容聚合、管理的线上教育产品，包括但不限于web端（网站网址：study.163.com及其不时更换的域名）及其各移动端（包括但不限于更新后的版本）。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.3 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">课程</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">：指由学习512课堂提供的，在学习512课堂平台上陈列、展示、供用户观看、学习的视频、语音、文字、图片、链接等课程作品/制品。本定义下的课程包括免费课程与收费课程。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.3.1 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">免费课程</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">：指学习512课堂无偿提供的，供用户免费观看、使用的课程。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.3.2 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">收费课程</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">：指学习512课堂有偿提供的，供用户付费观看、使用的课程。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">2.3.3 VIP</span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">会员权益：指会员在注册成功后，根据向学习512课堂支付对价，所获得的针对学习512课堂特定课程的优惠购买价格、课程免费观看资格、免费阅读电子书、优惠券礼包及其他会员特有优惠权益，具体内容以<a href="https://study.163.com/order/protocol.htm?id=10240" target="_blank">《学习512课堂VIP会员使用协议》</a></span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">、云课堂官网、相关页面上发布的公告、规则为准。</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">3. 账</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">号注册与使用</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">3.1 </span><span style="font-family: 宋体;">您可浏览学习512课堂上的课程信息，如您希望观看、学习该课程，您需先登录您的账号，或注册学习512课堂认可的账号并登录。如您选用其他第三方<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号登录学习512课堂的，您应保证第三方<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号的稳定性、真实性以及可用性，如因第三方<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号原因（如第三方<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号被封号）致使您无法登录学习512课堂的，您应与第三方<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号的所属公司联系。您注册登录的<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号是学习512课堂确认您身份的唯一依据。</span></p><p style="margin: 0px 0px 0px 27px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">您充分了解并同意，您必须为自己注册<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号下的一切行为负责，包括但不限于您所发表的任何内容以及由此产生的任何后果。您应对本服务中的内容自行加以判断，并承担因使用内容而引起的所有风险，包括因对内容的正确性、完整性或实用性的依赖而产生的风险。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">3.2 </span><span style="font-family: 宋体;">注册完成后，请您妥善保存有关<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号和密码，并对该<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号进行的所有活动和行为负责。如因您自身原因（包括但不限于转让<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号、与他人共用、自己泄露等）或您所用计算机或其他终端产品感染病毒或木马，而导致<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号密码泄露、遗失或其他损失后果将由您独自承担。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="line-height: 21px; font-family: 宋体;">3.3</span><span style="font-family: 宋体;">您在注册<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号时承诺遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线，不得在<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号注册资料中出现违法和不良信息。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">3.4</span><span style="font-family: 宋体;">学习512课堂有权对您提供的<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号注册资料进行审查，若发现或者收到举报确认注册资料存在不准确、不真实，或含有违法、不良信息，学习512课堂有权不予注册，并保留终止您使用学习512课堂平台的权利。若您以虚假信息骗取<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号注册或<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号注册资料存在侵权、违法和不良信息的，学习512课堂有权采取通知限期改正、暂停使用、注销登记等措施。对于冒用其他机构或他人名义注册<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号名称的，学习512课堂有权注销该<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号，并向政府主管部门进行报告。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">3.5</span><span style="font-family: 宋体;">根据相关法律、法规规定以及考虑到成诚科技产品服务的重要性，您同意：</span></p><p style="margin: 0px 0px 0px 27px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">（1）在注册<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号时提交有效准确的个人信息进行实名认证；</span></p><p style="margin: 0px 0px 0px 27px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">（2）提供及时、详尽及准确的<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>户注册资料；</span></p><p style="margin: 0px 0px 0px 27px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">（3）不得以营利、任何不正当手段或以违反诚实信用原则等为自己或他人开通、使用本服务；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">&nbsp;&nbsp;</span><span style="font-family: 宋体;">（4）对其自身<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号中的所有行为和事件负全责，不得售卖、转借<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号，不得私自进行<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号的交易、私自进行学习512课堂虚拟货币（包括但不限于成诚科技学习币等）或其他积分等交易活动。</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 21px; background: white;"><span style="font-family: 宋体;">如出现以上任何行为之一的，将按照本服务条款承担相应的违约责任，如因此导致您无法继续使用学习512课堂相关服务的，您应当自行承担后果。</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 21px; background: white;"><span style="line-height: 21px; font-family: 宋体;">3.6 </span><strong><span style="line-height: 21px; font-family: 宋体;">您若发现自身<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号或密码被他人非法使用或有登录、使用异常情况的，应及时通知学习512课堂，学习512课堂将按照本协议或法律规定进行处理。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="line-height: 21px; font-family: 宋体;">3.7</span><span style="line-height: 21px; font-family: 宋体;">您应当通过成诚科技官方提供的正当合法途径付费以获得全部服务，其他非官方途径付费充值的，均不享受成诚科技公司保护，如成诚科技公司发现用户未支付学习512课堂官网对价或未通过学习512课堂官网途径获取学习512课堂服务的，成诚科技公司有权限制、中止或终止用户<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号的登录和使用，停止向用户提供服务，成诚科技公司无需向用户退还已支付的任何费用。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="line-height: 21px; font-family: 宋体;">3.8</span></strong><strong><span style="line-height: 21px; font-family: 宋体;">您在购买学习512课堂课程后，应当在课程有效期限内完成学习。如课程已到期，用户要求继续学习的或要求退款的，成诚科技公司不予支持。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="line-height: 21px; font-family: 宋体;">3.9</span></strong><strong><span style="line-height: 21px; font-family: 宋体;">您了解并同意，学习512课堂可能会基于自身原因（包括但不限于：更新课程内容、改进课程安排）不定期的对付费服务内容或功能进行更新而无需经过您的事先同意。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="line-height: 21px; font-family: 宋体;">3.10</span></strong><strong><span style="line-height: 21px; font-family: 宋体;">您应使用学习512课堂正版课程资料，若用户出现包括但不限于违规倒卖课程、违规使用听课<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号、在课程服务使用中反复使用侮辱语言、严重扰乱课堂秩序、威胁辱骂老师、恶意批量差评 、恶意骚扰客服、煽动用户退款、冒充学习512课堂工作人员等行为，一经证实，成诚科技公司有权将此听课<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号做封号处理并保留追究法律责任的权利。</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px;">&nbsp;</p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">4. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">数据</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; text-indent: 28px; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">在法律法规允许的范围内，用户同意学习512课堂<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号所有权，以及与注册、使用学习512课堂<span style="color: rgb(0, 0, 0); font-family: 宋体; font-size: 14px; text-align: justify; text-wrap: wrap; background-color: rgb(255, 255, 255);">账</span>号相关的服务数据和记录，包括但不限于所有注册、登录、消费记录和相关的使用统计数据，<strong>归成诚科技公司所有。除非另有证明，学习512课堂储存在其服务器上的数据是您使用学习512课堂服务的唯一有效证据。</strong></span></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">5. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂使用规则</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1 </span><span style="font-family: 宋体;">您不得利用学习512课堂平台制作、复制、发布、传播如下法律、法规和政策禁止的内容：</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.1 </span><span style="font-family: 宋体;">反对宪法所确定的基本原则的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.2 </span><span style="font-family: 宋体;">危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.3 </span><span style="font-family: 宋体;">损害国家荣誉和利益的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.4 </span><span style="font-family: 宋体;">煽动民族仇恨、民族歧视，破坏民族团结的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.5 </span><span style="font-family: 宋体;">破坏国家宗教政策，宣扬邪教和封建迷信的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.6 </span><span style="font-family: 宋体;">散布谣言，扰乱社会秩序，破坏社会稳定的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.7 </span><span style="font-family: 宋体;">散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.8 </span><span style="font-family: 宋体;">侮辱或者诽谤他人，侵害他人合法权益的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.9 </span><span style="font-family: 宋体;">煽动非法集会、结社、游行、示威、聚众扰乱社会秩序；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.10 </span><span style="font-family: 宋体;">以非法民间组织名义活动的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.1.11 </span><span style="font-family: 宋体;">含有法律、法规和政策禁止的其他内容的信息。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2 </span><span style="font-family: 宋体;">您不得利用学习512课堂平台制作、复制、发布、传播包括但不限于如下干扰平台的正常运营，以及侵犯其他主体或第三方合法权益的内容：</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.1 </span><span style="font-family: 宋体;">广告、骚扰、垃圾信息的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.2 </span><span style="font-family: 宋体;">涉及他人隐私、个人信息或资料的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.3 </span><span style="font-family: 宋体;">侵害他人名誉权、肖像权、知识产权、商业秘密等合法权利的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.4 </span><span style="font-family: 宋体;">强制、诱导其他用户关注、点击链接页面或分享信息的；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.5 </span><span style="font-family: 宋体;">虚构事实、隐瞒真相以误导、欺骗他人的；</span></p><p style="margin: 0px 0px 0px 42px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.6 </span><span style="font-family: 宋体;">未经学习512课堂书面许可利用平台为第三方进行推广的（包括但不限于加入第三方链接、广告等行为）；</span></p><p style="margin: 0px 0px 0px 42px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.7 </span><span style="font-family: 宋体;">未经学习512课堂书面许可使用插件、外挂或其他第三方工具、服务接入学习512课堂平台和相关系统；</span></p><p style="margin: 0px 0px 0px 42px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.8 </span><span style="font-family: 宋体;">利用平台从事任何违法犯罪活动的；</span></p><p style="margin: 0px 0px 0px 42px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.9 </span><span style="font-family: 宋体;">制作、发布与以上行为相关的方法、工具，或对此类方法、工具进行运营或传播，无论这些行为是否为商业目的；</span></p><p style="margin: 0px 0px 0px 35px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.2.10 </span><span style="font-family: 宋体;">其他违反法律法规规定或干扰平台正常运营的行为。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.3 </span><strong><span style="font-family: 宋体;">您了解并同意，学习512课堂有权应有关部门的要求，向其提供您提交给学习512课堂或在使用学习512课堂服务中存储于学习512课堂服务器的必要信息。如您涉嫌侵犯他人合法权益，则学习512课堂有权在初步判断涉嫌侵权行为存在的情况下，向权利人提供关于您的前述必要信息。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">5.4</span></strong><strong><span style="font-family: 宋体;">您知悉并同意，您购买的付费服务仅限于您自行使用，您无权对购买的收费课程及相关增值服务进行出售、转让、许可、传播或以其他方式使除您自己以外的第三方（包括但不限于自然人、法人或其他组织）使用。</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 21px; background: white;"><span style="font-family: 宋体;">学习512课堂有权视您违反本协议行为的严重程度，对您采取以下单项或多项措施：</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">5.4.1</span></strong><strong><span style="font-family: 宋体;">取消您继续使用该付费服务的权利；</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">5.4.2</span></strong> <strong><span style="font-family: 宋体;">限制、暂停/终止提供全部或部分服务；</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.4.3 </span><span style="font-family: 宋体;">删除违规内容；</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">5.4.4</span></strong><strong><span style="font-family: 宋体;">要求您退还通过出售、转让、许可等其他方式取得的收益；</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.4.5 </span><span style="font-family: 宋体;">暂时/永久封禁账号；</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.4.6 </span><span style="font-family: 宋体;">其他学习512课堂采取的合理措施。</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">如您因违规被暂停部分或全部服务，终止部分服务、被限制/中止账号的登录和使用，对您已购买的课程费用，成诚科技公司将不予退回。如您被终止全部服务或终止账号登录的，学习512课堂无需向您退还已支付的任何课程费用。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.5 </span><strong><span style="font-family: 宋体;">如果您违反本协议的行为给学习512课堂或其他第三方造成损失的，您应当对此承担法律责任。您承担法律责任的形式包括但不限于：对受到侵害者进行赔偿；</span></strong><span style="font-family: 宋体;">以及在学习512课堂首先承担了因您的行为导致的行政处罚或侵权损害赔偿责任后，您应给予学习512课堂等额的赔偿。</span><strong><span style="font-family: 宋体;">若您账户内有剩余款项的，学习512课堂有权不予退回，若不足以赔偿学习512课堂损失的，学习512课堂仍有权要求您补足相应的损失。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.6 </span><span style="font-family: 宋体;">如果您行使本协议规定的权利而购买/接受学习512课堂以外的第三方提供的内容或服务，如因此发生纠纷的，您应向销售/提供该内容或服务的第三方主张权利，与学习512课堂无关。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">5.7 </span><span style="font-family: 宋体;">对于在本平台上的内容, 学习512课堂不对其适用性或满足您特定需求及目的进行任何明示或者默示的担保。除非成诚科技公司以书面形式明确约定，成诚科技公司对于用户以任何方式（包括但不限于包含、经由、连接或下载）从本网站所获得的任何内容信息，包括但不限于音频内容、广告内容、其他用户信息、商户信息、用户评价内容等，不保证其准确性、完整性、可靠性。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">&nbsp;</span></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">服务收费</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; text-indent: 28px; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂平台可能涉及付费项目，付费项目包括但不限于购买学习512课堂提供的付费课程以及其他增值服务。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.1</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">您理解并同意：学习512课堂提供付费服务实行先付款后使用的方式，您及时、足额、合法的支付所需的款项是您获得该等服务的前提。</span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">您有权选择使用学习512课堂认可的支付方式，您理解并确认支付服务由学习512课堂之外具备合法资质的第三方提供，该等支付服务的使用条件及规范由您与支付服务提供方确定，与学习512课堂无关。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.2</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">如您选择购买学习512课堂付费服务，请您务必仔细了解本协议及确认相关具体信息（包括但不限于课程的内容、开课的时间、开课的期限、课程价格等），并根据操作提示，确认自己的账号并选择相关操作选项。学习512课堂发布的付费服务可能对使用的时间、次数等做出一定限制；如超出限制范围，可能影响您继续使用该等服务，您需要再次付费购买。一经购买，<strong>即视为您认可该项服务标明之价格；您购买成功后，该项服务即时生效。成诚科技公司可能会根据服务的整体规划，对服务相关权益细则、收费标准、方式等进行修改和变更，前述修改、变更，成诚科技公司将在相应服务页面进行展示。您若需要获取、使用学习512课堂付费会员服务，请先提前了解清楚当时关于学习512课堂付费会员服务的收费标准、方式等信息。</strong></span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.3</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">具体会员服务种类、价格、内容以相关服务页面公布、实际提供的内容为准。您可以自行根据需要选择相应服务。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.4</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">您可以在不同设备（“设备”指包括但不限于计算机及移动电话、平板电脑等移动终端设备，下同）上使用学习512课堂的账号，您知晓并同意成诚科技有权设置用户的同一账号在不同设备终端使用的终端总数，并有权限制同一账户同一时间只能在一个终端上登录使用。超过上述范围使用的，成诚科技公司有权部分或全部中止或终止对您的服务。您应自行承担超范围使用而导致的任何损失。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.5</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">成诚科技公司不对所提供的付费项目的适用性或满足用户特定需求及目的进行任何明示或者默示的担保。您在购买前应确认自身的需求，同时仔细查阅相关介绍、说明。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.6</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂提供授课、授课时间可能会因教学安排、老师等原因进行调整，请以实际课程安排为准。如用户对调整有异议，可按照官网公示的售后政策申请退款或换课。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.7</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">当用户购买课程后，如授课老师身体出现疾病、怀孕等特殊情况或因国家法律法规或相关政策之规定不再适合担任授课老师时，学习512课堂经核实后，可以调整授课时间或更换老师；当老师因个人原因无法继续授课时，学习512课堂原则上要求老师依据工作要求完成未完成的课时，但如老师拒绝执行，学习512课堂可以在课前或课中更换老师。发生本条所述情况，学习512课堂均会提前通知用户，为用户替换合适的老师，做好原老师及接班老师的顺利衔接。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px;">&nbsp;</span></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">6.8</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">关于学习512课堂售后政策</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; text-indent: 28px; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">为了更好的帮助和提升用户使用学习512课堂服务，成诚科技公司将按照学习512课堂网站上公布的<a href="https://study.163.com/about/aboutus.htm#/about?aboutType=10" target="_blank"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">《学习512课堂售后政策》</span></strong></a>、</span></strong><a href="https://study.163.com/topics/kaoyan_sale_policy/" target="_blank"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">《成诚科技考研售后政策》</span></strong></a><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">等相关协议或规则为用户完成课程购买后的售后服务。请用户完整阅读上述售后政策，以帮助用户更好地体验和学习学习512课堂。</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; text-indent: 28px; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">7. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">隐私</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">政策</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 17.5px;"><strong><span style="line-height: 17.5px; font-family: 宋体; color: rgb(21, 21, 21);">尊重用户隐私是学习512课堂的一项基本政策。成诚科技公司将按照学习512课堂上公布的<a>《</a></span></strong><a href="https://study.163.com/topics/privacy_policy" target="_blank"><span style="color: blue;"><strong><span style="line-height: 17.5px; font-family: 宋体;">学习512课堂隐私政策</span></strong></span></a><span style="color: blue;"><strong><span style="line-height: 17.5px; font-family: 宋体;">》</span></strong></span>&nbsp;<strong><span style="line-height: 17.5px; font-family: 宋体; color: rgb(21, 21, 21);">收集、存储、使用、披露和保护您的个人信息，请您完整阅读上述隐私权政策，以帮助您更好地保护您的个人信息。</span></strong></p><p style="margin: 0px; font-size: 14px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-indent: 28px; line-height: 17.5px;"><strong><span style="line-height: 17.5px; font-family: 宋体; color: rgb(21, 21, 21);">如您是未满</span></strong><strong><span style="line-height: 17.5px; font-family: Helvetica; color: rgb(21, 21, 21);">18</span></strong><strong><span style="line-height: 17.5px; font-family: 宋体; color: rgb(21, 21, 21);">周岁的未成年人，请您通知您的监护人共同阅读并确认</span></strong><a href="https://study.163.com/topics/privacy_policy_children" target="_blank"><strong><span style="line-height: 17.5px; font-family: 宋体; color: purple;">《学习512课堂未成年人个人信息保护规则及监护人须知》</span></strong></a>&nbsp;<strong><span style="line-height: 17.5px; font-family: 宋体; color: rgb(21, 21, 21);">，并在您使用服务、提交个人信息之前，务必寻求他们的同意和指导。</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;</span></strong></p><p style="margin-right: 0px; margin-left: 0px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); text-indent: 0px; white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8. </span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">其他约定</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">8.1 </span></strong><strong><span style="font-family: 宋体;">学习512课堂对不可抗力导致的损失不承担责任。本协议所指不可抗力包括但不限于：天灾、法律法规或政府指令的变更，因网络服务特性而特有的原因，例如境内外基础电信运营商的故障、计算机或互联网相关技术缺陷、互联网覆盖范围限制、计算机病毒、黑客攻击等因素，及其他合法范围内的不能预见、不能避免并不能克服的客观情况。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">8.2 </span></strong><strong><span style="font-family: 宋体;">学习512课堂可能会提供第三方国际互联网网站或资源链接，除非另有声明外，学习512课堂无法对第三方网站服务进行控制，因此由于下载、传播、使用或依赖上述网站或资源所产生的损失或损害，成诚科技公司不承担任何责任。</span></strong></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><span style="font-family: 宋体;">8.3 </span><strong><span style="font-family: 宋体;">在适用法律允许的最大范围内，成诚科技公司不就因用户使用学习512课堂引起的，或在任何方面与成诚科技公司的产品和服务有关的任何意外的、非直接的、特殊的、或间接的损害或请求（包括但不限于因人身伤害、因隐私泄漏、因未能履行包括诚信或合理谨慎在内的任何责任、因过失和因任何其他金钱上的损失或其他损失而造成的损害赔偿）承担任何责任。 </span></strong><span style="font-family: 宋体;">在适用法律允许的最大范围内，成诚科技公司明确表示不提供任何其他类型的保证，不论是明示的或默示的，包括但不限于适销性、适用性、可靠性、准确性、完整性、无病毒以及无错误的任何默示保证和责任。</span><strong><span style="font-family: 宋体;"> 除法律法规另有明确规定外，成诚科技公司对用户承担的全部责任，无论因何原因或何种行为方式，始终不超过用户因使用学习512课堂而支付给成诚科技公司的费用（如有）。</span></strong><span style="font-family: 宋体;">在适用法律允许的最大范围内，成诚科技公司及其许可人不会向用户做出以下声明或担保：</span><span style="font-family: 宋体;"> (A) </span><span style="font-family: 宋体;">用户对服务的使用会满足用户的需求；</span><span style="font-family: 宋体;"> (B) </span><span style="font-family: 宋体;">用户对服务的使用会连续无中断、及时、安全或没有错误；（</span><span style="font-family: 宋体;">C</span><span style="font-family: 宋体;">）</span> <span style="font-family: 宋体;">用户使用相关服务而获得的任何信息一律准确可靠；</span> <span style="font-family: 宋体;">（</span><span style="font-family: 宋体;">D</span><span style="font-family: 宋体;">）作为相关服务的一部分向用户提供的任何软件所发生的操作或功能瑕疵将获得修正。</span></p><p style="margin: 0px 0px 0px 28px; font-size: 14px; font-family: Calibri; text-align: justify; color: rgb(0, 0, 0); white-space: normal; line-height: 21px; background: white;"><strong><span style="font-family: 宋体;">8.4</span></strong> <strong><span style="font-family: 宋体;">服务中止、中断及终止：</span></strong><strong><span style="font-family: 宋体;">网易</span></strong><strong><span style="font-family: 宋体;">云课堂</span></strong><strong><span style="font-family: 宋体;">出于自身商业决策、执行行政</span></strong><strong><span style="">/</span></strong><strong><span style="font-family: 宋体;">司法机关的命令、维护本服务的安全、维护其他用户的正常使用与数据安全、回应其他主体的侵权诉求等理由，或者</span></strong><strong><span style="font-family: 宋体;">网易</span></strong><strong><span style="font-family: 宋体;">云课堂</span></strong><strong><span style="font-family: 宋体;">根据自身的判断，认为您的行为涉嫌违反本协议内容或涉嫌违反法律法规的规定的，则</span></strong><strong><span style="font-family: 宋体;">网易</span></strong><strong><span style="font-family: 宋体;">云课堂</span></strong><strong><span style="font-family: 宋体;">有权</span></strong><strong><span style="font-family: 宋体;">变更、</span></strong><strong><span style="font-family: 宋体;">中止、中断或终止向您提供服务</span></strong><strong><span style="font-family: 宋体;">，且无须向您或任何第三方承担责任</span></strong><strong><span style="font-family: 宋体;">。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.5 </span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">所有发给您的通知都可通过用户短信、站内信、电话、电子邮件、常规的信件或在学习512课堂平台或网易网站内显著位置公告的方式进行传送。学习512课堂将通过上述方法之一将消息传递给您，告知您协议的修改、服务变更、或其它重要事情。同时，成诚科技公司保留在学习512课堂中投放商业性广告的权利以及利用用户登录的网易邮箱发布商业性广告的权利，包括但不限于在网站、APP登录页面及登录后任何页面内放置商业广告、向用户发送商业性广告短信、邮件在用户发出的电子邮件内附上商业性广告及／或广告链接、电话销售等形式，但用户有权选择拒绝接受此类广告等信息。成诚科技公司将按照法律规定履行广告及推广相关义务，用户应当自行判断该等信息的真实性和可靠性并为自己的判断行为负责。除法律法规明确规定外，用户因该等信息进行的购买或交易或因前述内容遭受的损害或损失，用户应自行承担，成诚科技公司不予承担责任。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.6</span></strong><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">所有权及知识产权：学习512课堂平台上所有内容，包括但不限于文字、软件、声音、图片、录像、图表、网站架构、网站画面的安排、网页设计、学习512课堂为您提供的课程及其他资料均由学习512课堂或其他权利人依法拥有其知识产权，包括但不限于著作权、商标权、专利权等。非经学习512课堂或其他权利人书面同意您不得擅自使用、修改、复制、录像、传播、改变、散布、发行或发表上述内容。如有违反，您同意承担由此给学习512课堂或其他权利人造成的一切损失。</span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.7</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">为保证产品、服务的安全性及产品功能的一致性，成诚科技公司可能会对学习512课堂软件进行更新。<strong>用户应将软件更新到最新版本，如用户未及时更新到最新版本，成诚科技公司不保证用户一定能正常使用学习512课堂服务。</strong></span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.8 </span><strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">本协议适用中华人民共和国的法律（不含冲突法）。</span></strong><span style="font-size: 14px; line-height: 21px; font-family: 宋体;"> 当本协议的任何内容与中华人民共和国法律相抵触时，应当以法律规定为准，同时相关内容将按法律规定进行修改或重新解释，而本协议其他部分的法律效力不变。&nbsp;</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.9</span><strong><span style="text-decoration: underline;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">凡因本协议引起的或与本协议有关的任何争议,双方应协商解决。协商不成时，任何一方均可向被告所在地有管辖权的人民法院提起诉讼。</span></span></strong></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.10 </span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">学习512课堂不行使、未能及时行使或者未充分行使本协议或者按照法律规定所享有的权利，不应被视为放弃该权利，也不影响学习512课堂在将来行使该权利。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; text-align: justify; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">8.11 </span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">如果您对本协议内容有任何疑问，请登录学习512课堂，联系在线客服向我们反馈，或可发送邮件至study@service.netease.com获取相关信息。</span></p><p style="margin-right: 0px; margin-left: 28px; font-size: 16px; font-family: Calibri; color: rgb(0, 0, 0); white-space: normal; line-height: 24px; background: white;"><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-size: 14px; line-height: 21px; font-family: 宋体;">成诚科技公司</span></p><p style="margin: 0px; font-size: 14px; font-family: Calibri;"><br></p><p><br></p>
				</div>`
      };
    },
    created() {
      this.nodes = parserHtml.parserHTML(this.xyhtml);
    },
    methods: {
      handleClose() {
        this.open = false;
      },
      handleOpen() {
        this.open = true;
      }
    }
  };
  function _sfc_render$b(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", null, [
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "ui-modal_box f-cb"
      }, [
        vue.createElementVNode("rich-text", { nodes: $data.nodes }, null, 8, ["nodes"]),
        vue.createElementVNode("view", {
          class: "btnbox",
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClose && $options.handleClose(...args))
        }, "知道了")
      ])) : vue.createCommentVNode("v-if", true),
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 1,
        class: "overlay",
        onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClose && $options.handleClose(...args))
      })) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_1$2 = /* @__PURE__ */ _export_sfc(_sfc_main$c, [["render", _sfc_render$b], ["__scopeId", "data-v-d564aa09"], ["__file", "D:/App/分销系统/cc_fenxiao/components/dialog/xieyi.vue"]]);
  const _sfc_main$b = {
    name: "gui-popup",
    props: {
      bgColor: { type: String, default: "rgba(0, 0, 0, 0.7)" },
      position: { type: String, default: "center" },
      width: { type: String, default: "580rpx" },
      canCloseByShade: { type: Boolean, default: true },
      zIndex: { type: Number, default: 999 },
      top: { type: Number, default: 0 },
      duration: { type: Number, default: 280 },
      isSwitchPage: { type: Boolean, default: false }
    },
    data() {
      return {
        show: false,
        out: false
      };
    },
    methods: {
      open: function() {
        this.out = false;
        this.show = true;
        this.$emit("open");
      },
      closebysd: function() {
        if (this.canCloseByShade) {
          this.close();
        }
      },
      close: function() {
        this.out = true;
        setTimeout(() => {
          this.show = false;
          this.$emit("close");
        }, 350);
      },
      stopfun: function(e) {
        e.stopPropagation();
        return null;
      }
    },
    emits: ["close", "open"]
  };
  function _sfc_render$a(_ctx, _cache, $props, $setup, $data, $options) {
    return $data.show ? (vue.openBlock(), vue.createElementBlock("view", { key: 0 }, [
      vue.createCommentVNode(" 居中 "),
      $props.position == "center" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 0,
          class: vue.normalizeClass(["gui-popup gui-flex gui-column gui-justify-content-center gui-align-items-center", [$data.out ? "gui-fade-out" : "gui-fade-in"]]),
          ref: "guipopup",
          onClick: _cache[1] || (_cache[1] = vue.withModifiers((...args) => $options.closebysd && $options.closebysd(...args), ["stop"])),
          onTouchmove: _cache[2] || (_cache[2] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"])),
          style: vue.normalizeStyle({
            backgroundColor: $props.bgColor,
            zIndex: $props.zIndex,
            top: $props.top + "px",
            animationDuration: $props.duration + "ms"
          })
        },
        [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["gui-popup-content gui-popup-center", [$data.out ? "gui-scale-out" : "gui-scale-in"]]),
              onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
              ref: "guiPopupCenter",
              style: vue.normalizeStyle({ width: $props.width, animationDuration: $props.duration + "ms" })
            },
            [
              vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, NEED_HYDRATION */
      )) : vue.createCommentVNode("v-if", true),
      vue.createCommentVNode(" 顶部 "),
      $props.position == "top" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 1,
          class: vue.normalizeClass(["gui-popup gui-flex gui-column", [$data.out ? "gui-fade-out" : "gui-fade-in"]]),
          style: vue.normalizeStyle({
            backgroundColor: $props.bgColor,
            zIndex: $props.zIndex,
            top: $props.top + "px",
            animationDuration: $props.duration + "ms"
          }),
          ref: "guipopup",
          onClick: _cache[4] || (_cache[4] = vue.withModifiers((...args) => $options.closebysd && $options.closebysd(...args), ["stop"])),
          onTouchmove: _cache[5] || (_cache[5] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"]))
        },
        [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["gui-popup-content gui-popup-top", [$data.out ? "gui-top-out" : "gui-top-in"]]),
              onClick: _cache[3] || (_cache[3] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
              ref: "guiPopupTop",
              style: vue.normalizeStyle({ animationDuration: $props.duration + "ms" })
            },
            [
              vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, NEED_HYDRATION */
      )) : vue.createCommentVNode("v-if", true),
      vue.createCommentVNode(" 底部 "),
      $props.position == "bottom" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 2,
          class: vue.normalizeClass(["gui-popup gui-flex gui-column gui-justify-content-end", [$data.out ? "gui-fade-out" : "gui-fade-in"]]),
          style: vue.normalizeStyle({
            backgroundColor: $props.bgColor,
            zIndex: $props.zIndex,
            top: $props.top + "px",
            animationDuration: $props.duration + "ms"
          }),
          ref: "guipopup",
          onClick: _cache[7] || (_cache[7] = vue.withModifiers((...args) => $options.closebysd && $options.closebysd(...args), ["stop"])),
          onTouchmove: _cache[8] || (_cache[8] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"]))
        },
        [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["gui-popup-content gui-popup-bottom", ["gui-dark-bg-level-3", $data.out ? "gui-bottom-out" : "gui-bottom-in"]]),
              onClick: _cache[6] || (_cache[6] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
              ref: "guiPopupBottom",
              style: vue.normalizeStyle({ animationDuration: $props.duration + "ms" })
            },
            [
              vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, NEED_HYDRATION */
      )) : vue.createCommentVNode("v-if", true),
      vue.createCommentVNode(" 左侧 "),
      $props.position == "left" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 3,
          class: vue.normalizeClass(["gui-popup gui-flex gui-column", [$data.out ? "gui-fade-out" : "gui-fade-in"]]),
          ref: "guipopup",
          onClick: _cache[10] || (_cache[10] = vue.withModifiers((...args) => $options.closebysd && $options.closebysd(...args), ["stop"])),
          onTouchmove: _cache[11] || (_cache[11] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"])),
          style: vue.normalizeStyle({
            backgroundColor: $props.bgColor,
            zIndex: $props.zIndex,
            top: $props.top + "px",
            animationDuration: $props.duration + "ms"
          })
        },
        [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["gui-popup-content gui-flex1 gui-flex gui-column gui-popup-left", [$data.out ? "gui-left-out" : "gui-left-in"]]),
              onClick: _cache[9] || (_cache[9] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
              ref: "guiPopupLeft",
              style: vue.normalizeStyle({ width: $props.width, animationDuration: $props.duration + "ms" })
            },
            [
              vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, NEED_HYDRATION */
      )) : vue.createCommentVNode("v-if", true),
      vue.createCommentVNode(" 右侧 "),
      $props.position == "right" ? (vue.openBlock(), vue.createElementBlock(
        "view",
        {
          key: 4,
          class: vue.normalizeClass(["gui-popup gui-flex gui-column gui-align-items-end", [$data.out ? "gui-fade-out" : "gui-fade-in"]]),
          ref: "guipopup",
          onClick: _cache[13] || (_cache[13] = vue.withModifiers((...args) => $options.closebysd && $options.closebysd(...args), ["stop"])),
          onTouchmove: _cache[14] || (_cache[14] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"])),
          style: vue.normalizeStyle({
            backgroundColor: $props.bgColor,
            zIndex: $props.zIndex,
            top: $props.top + "px",
            animationDuration: $props.duration + "ms"
          })
        },
        [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass(["gui-popup-content gui-flex1 gui-flex gui-column gui-popup-right", [$data.out ? "gui-right-out" : "gui-right-in"]]),
              onClick: _cache[12] || (_cache[12] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop"])),
              ref: "guiPopupRight",
              style: vue.normalizeStyle({ width: $props.width, animationDuration: $props.duration + "ms" })
            },
            [
              vue.renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, NEED_HYDRATION */
      )) : vue.createCommentVNode("v-if", true)
    ])) : vue.createCommentVNode("v-if", true);
  }
  const __easycom_0$1 = /* @__PURE__ */ _export_sfc(_sfc_main$b, [["render", _sfc_render$a], ["__scopeId", "data-v-c6cddaae"], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-popup.vue"]]);
  const _sfc_main$a = {
    name: "gui-modal",
    props: {
      width: { type: String, default: "580rpx" },
      isCloseBtn: { type: Boolean, default: true },
      closeBtnStyle: { type: String, default: "font-size:28rpx;" },
      title: { type: String, default: "" },
      titleStyle: { type: String, default: "line-height:100rpx; font-size:28rpx; font-weight:700;" },
      canCloseByShade: { type: Boolean, default: true },
      zIndex: { type: Number, default: 99 },
      customClass: { type: Array, default: function() {
        return ["gui-bg-white", "gui-dark-bg-level-3"];
      } }
    },
    methods: {
      open: function() {
        this.$refs.guipopupformodal.open();
        this.$emit("open");
      },
      close: function() {
        this.$refs.guipopupformodal.close();
        this.$emit("close");
      },
      stopfun: function(e) {
        e.stopPropagation();
        return null;
      },
      eClose: function() {
        this.$emit("close");
      }
    },
    emits: ["open", "close"]
  };
  function _sfc_render$9(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_popup = resolveEasycom(vue.resolveDynamicComponent("gui-popup"), __easycom_0$1);
    return vue.openBlock(), vue.createBlock(_component_gui_popup, {
      ref: "guipopupformodal",
      width: $props.width,
      canCloseByShade: $props.canCloseByShade,
      zIndex: $props.zIndex,
      onClose: $options.eClose
    }, {
      default: vue.withCtx(() => [
        vue.createElementVNode(
          "view",
          {
            class: vue.normalizeClass([$props.customClass, "gui-relative"]),
            onClick: _cache[1] || (_cache[1] = vue.withModifiers((...args) => $options.stopfun && $options.stopfun(...args), ["stop", "prevent"]))
          },
          [
            vue.createElementVNode("view", null, [
              $props.title != "" ? (vue.openBlock(), vue.createElementBlock(
                "text",
                {
                  key: 0,
                  class: "gui-block gui-text-center gui-primary-text",
                  style: vue.normalizeStyle($props.titleStyle)
                },
                vue.toDisplayString($props.title),
                5
                /* TEXT, STYLE */
              )) : vue.createCommentVNode("v-if", true)
            ]),
            vue.createElementVNode("view", null, [
              vue.renderSlot(_ctx.$slots, "content")
            ]),
            vue.createElementVNode("view", null, [
              vue.renderSlot(_ctx.$slots, "btns")
            ]),
            $props.isCloseBtn ? (vue.openBlock(), vue.createElementBlock(
              "text",
              {
                key: 0,
                class: "gui-popup-close gui-block gui-absolute-rt gui-icons gui-color-gray",
                style: vue.normalizeStyle("width:76rpx; height:76rpx; line-height:76rpx; text-align:center;" + $props.closeBtnStyle),
                onClick: _cache[0] || (_cache[0] = vue.withModifiers((...args) => $options.close && $options.close(...args), ["stop", "prevent"]))
              },
              "",
              4
              /* STYLE */
            )) : vue.createCommentVNode("v-if", true)
          ],
          2
          /* CLASS */
        )
      ]),
      _: 3
      /* FORWARDED */
    }, 8, ["width", "canCloseByShade", "zIndex", "onClose"]);
  }
  const __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["render", _sfc_render$9], ["__file", "D:/App/分销系统/cc_fenxiao/Grace6/components/gui-modal.vue"]]);
  var demoData = [
    { img: "/static/pay/alipay.png", title: "支付宝", checked: true },
    { img: "/static/pay/weixin.png", title: "微信", checked: false },
    { img: "/static/pay/huabei.png", title: "花呗分期", checked: false },
    { img: "/static/pay/code.png", title: "兑换码", checked: false }
  ];
  const _sfc_main$9 = {
    data() {
      return {
        lists: [],
        dialogXieYiRef: null,
        currentIndex: 0
      };
    },
    onLoad() {
      this.lists = demoData;
    },
    methods: {
      change1: function(e) {
        this.currentIndex = e;
        if (e == 3) {
          this.open1();
        }
      },
      handleBack() {
        uni.switchTab({ url: "/pages/tabbar/me" });
      },
      handlePay(e) {
        uni.navigateTo({
          url: "/pages/order/paysuccess"
        });
      },
      open1: function() {
        this.$refs.guimodal1.open();
      },
      close1: function() {
        this.$refs.guimodal1.close();
      },
      confirm1: function() {
        this.$refs.guimodal1.close();
      },
      openXieyi() {
        this.$refs.dialogXieYiRef.handleOpen();
      }
    }
  };
  function _sfc_render$8(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_select_list = resolveEasycom(vue.resolveDynamicComponent("gui-select-list"), __easycom_0$2);
    const _component_cc_dialog_xieyi = resolveEasycom(vue.resolveDynamicComponent("cc-dialog-xieyi"), __easycom_1$2);
    const _component_gui_modal = resolveEasycom(vue.resolveDynamicComponent("gui-modal"), __easycom_2);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, {
      customHeader: true,
      class: "ksd-orderpage gui-bg-gray"
    }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding" }, [
          vue.createElementVNode("image", {
            src: "/static/imgs/bg.jpg",
            style: { "width": "100%", "height": "200px" }
          }),
          vue.createElementVNode("view", { class: "mt5" }, [
            vue.createElementVNode("view", { class: "fb fz16 gui-color-black3" }, "来自福布斯精英的25节金融思维课"),
            vue.createElementVNode("view", { class: "gui-flex gui-space-between gui-align-items-center" }, [
              vue.createElementVNode("view", { class: "fz12 gui-color-gray mt5" }, [
                vue.createElementVNode("text", null, "永久可看"),
                vue.createElementVNode("text", { class: "ml10 mr10" }, "4100在学习")
              ]),
              vue.createElementVNode("view", { class: "gui-box-tags" }, [
                vue.createElementVNode("text", { class: "tag icon-tag-dj" }, "独家"),
                vue.createElementVNode("text", { class: "tag icon-tag-lz" }, "连载")
              ])
            ]),
            vue.createElementVNode("view", { class: "mt5 gui-color-red" }, "￥199")
          ])
        ]),
        vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding mt10" }, [
          vue.createElementVNode("view", { class: "fz16 fb mb10" }, "支付方式"),
          vue.createVNode(_component_gui_select_list, {
            onChange: $options.change1,
            items: $data.lists
          }, null, 8, ["onChange", "items"])
        ]),
        vue.createElementVNode("view", { class: "gui-bg-white gui-dark-bg-level-3 gui-padding mt10 fz12" }, [
          vue.createElementVNode("view", {
            class: "gui-color-gray",
            onClick: _cache[0] || (_cache[0] = (...args) => $options.openXieyi && $options.openXieyi(...args))
          }, [
            vue.createTextVNode(" 提交订单则表示您同意"),
            vue.createElementVNode("text", { class: "gui-color-black" }, "《学习512用户服务协议》"),
            vue.createTextVNode("和 "),
            vue.createElementVNode("text", { class: "gui-color-black" }, "《学习512售后政策》"),
            vue.createTextVNode("等 ")
          ])
        ]),
        vue.createVNode(
          _component_cc_dialog_xieyi,
          { ref: "dialogXieYiRef" },
          null,
          512
          /* NEED_PATCH */
        ),
        vue.createElementVNode("view", { class: "um-order-ux-order-v2-go-pay_wrap" }, [
          vue.createElementVNode("view", { class: "um-order-ux-order-v2-go-pay_content f-cb" }, [
            vue.createElementVNode("view", { class: "gui-flex gui-align-items-center" }, [
              vue.createElementVNode("view", { class: "v2-go-pay_content_title fz13" }, "实付金额："),
              vue.createElementVNode("view", { class: "v2-go-pay_content_price_account ml3 fb" }, [
                vue.createElementVNode("text", { class: "v2-go-pay_content_price_account_icon" }, "¥"),
                vue.createTextVNode("199.00 ")
              ])
            ]),
            vue.createElementVNode(
              "view",
              {
                class: "um-order-ux-order-v2-go-pay_content_btn f-fr",
                onClick: _cache[1] || (_cache[1] = (...args) => $options.handlePay && $options.handlePay(...args)),
                style: { "touch-action": "pan-y" }
              },
              vue.toDisplayString($data.currentIndex == 3 ? "立即兑换" : "立即支付"),
              1
              /* TEXT */
            )
          ])
        ]),
        vue.createVNode(
          _component_gui_modal,
          {
            customClass: ["gui-bg-white", "gui-dark-bg-level-3", "gui-border-radius"],
            ref: "guimodal1",
            title: "优惠码兑换"
          },
          {
            content: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "gui-padding gui-bg-gray gui-dark-bg-level-2" }, [
                vue.createElementVNode("input", {
                  "auto-focus": "",
                  placeholder: "请输入兑换码",
                  class: "gui-text-center"
                })
              ])
            ]),
            btns: vue.withCtx(() => [
              vue.createElementVNode("view", { class: "gui-flex gui-row gui-space-between" }, [
                vue.createElementVNode("view", {
                  "hover-class": "gui-tap",
                  class: "modal-btns gui-flex1",
                  style: { "margin-right": "80rpx" }
                }, [
                  vue.createElementVNode("text", {
                    class: "modal-btns gui-color-gray",
                    onClick: _cache[2] || (_cache[2] = (...args) => $options.close1 && $options.close1(...args))
                  }, "取消")
                ]),
                vue.createElementVNode("view", {
                  "hover-class": "gui-tap",
                  class: "modal-btns gui-flex1",
                  style: { "margin-left": "80rpx" }
                }, [
                  vue.createElementVNode("text", {
                    class: "modal-btns gui-primary-color",
                    onClick: _cache[3] || (_cache[3] = (...args) => $options.confirm1 && $options.confirm1(...args))
                  }, "兑换")
                ])
              ])
            ]),
            _: 1
            /* STABLE */
          },
          512
          /* NEED_PATCH */
        )
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesOrderConfirm = /* @__PURE__ */ _export_sfc(_sfc_main$9, [["render", _sfc_render$8], ["__scopeId", "data-v-324e7894"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/order/confirm.vue"]]);
  const _sfc_main$8 = {
    data() {
      return {
        scroll: false,
        items: [
          [80, "", "我的订单"],
          [100, "", "优惠券"],
          [50, "", "关注店铺"],
          [99, "", "收藏"]
        ]
      };
    },
    methods: {
      toDetail() {
        uni.navigateTo({
          url: "/pages/shop/detail"
        });
      },
      toOrder() {
        uni.navigateTo({
          url: "/pages/user/order"
        });
      }
    }
  };
  function _sfc_render$7(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { style: { "background": "#f3f3f3" } }, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding-m" }, [
          vue.createElementVNode("view", {
            class: "gui-bg-white detail-items gui-border-radius",
            style: { "padding": "60px 0" }
          }, [
            vue.createElementVNode("view", { class: "flex flex-c align-items" }, [
              vue.createElementVNode("view", null, [
                vue.createElementVNode("text", { class: "gui-icons gui-color-green fz32 fb" }, "")
              ]),
              vue.createElementVNode("view", { class: "mt20" }, [
                vue.createElementVNode("text", { class: "fz20 fb gui-color-green" }, "支付成功")
              ]),
              vue.createElementVNode("view", { class: "mt20" }, [
                vue.createElementVNode("button", {
                  type: "default",
                  onClick: _cache[0] || (_cache[0] = (...args) => $options.toOrder && $options.toOrder(...args)),
                  class: "gui-button-mini gui-gtbg-green",
                  style: { "width": "282rpx" }
                }, [
                  vue.createElementVNode("text", { class: "gui-color-white gui-button-text-mini gui-icons" }, " 点击查看订单")
                ])
              ])
            ])
          ]),
          vue.createElementVNode(
            "view",
            {
              class: "gui-bg-white detail-items mt10",
              ref: "like"
            },
            [
              vue.createElementVNode("view", { class: "fb fz18" }, "猜你喜欢"),
              vue.createElementVNode("view", { class: "cainibox mt20" }, [
                (vue.openBlock(), vue.createElementBlock(
                  vue.Fragment,
                  null,
                  vue.renderList(10, (num) => {
                    return vue.createElementVNode("view", {
                      class: "items flex mb15",
                      onClick: _cache[1] || (_cache[1] = (...args) => $options.toDetail && $options.toDetail(...args))
                    }, [
                      vue.createElementVNode("view", null, [
                        vue.createElementVNode("image", { src: "/static/logo.png" })
                      ]),
                      vue.createElementVNode("view", { class: "ml15" }, [
                        vue.createElementVNode("view", { class: "tit fz15 fb" }, "炙鹿寿司|【品质】寿司拼盘套餐(共20件)"),
                        vue.createElementVNode("view", { class: "flex mt5 align-items" }, [
                          vue.createElementVNode("text", { class: "red fz16 fb" }, "￥28"),
                          vue.createElementVNode("text", { class: "textdel fz11 gui-color-gray ml2" }, "￥71.40"),
                          vue.createElementVNode("text", { class: "red fz12 ml5" }, "2.9折"),
                          vue.createElementVNode("text", { class: "gui-color-gray fz12 ml10" }, "已销售100+")
                        ]),
                        vue.createElementVNode("view", {
                          class: "gui-bg-gray mt8 fz12 flex justify-content-between align-items",
                          style: { "padding": "5px 10px" }
                        }, [
                          vue.createElementVNode("view", null, [
                            vue.createElementVNode("text", { class: "gui-icons" }, ""),
                            vue.createElementVNode("text", { class: "ml5" }, "榴莲嘟嘟(番禺万民城店)")
                          ]),
                          vue.createElementVNode("view", { class: "gui-color-gray" }, "396m")
                        ])
                      ])
                    ]);
                  }),
                  64
                  /* STABLE_FRAGMENT */
                ))
              ])
            ],
            512
            /* NEED_PATCH */
          )
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesOrderPaysuccess = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["render", _sfc_render$7], ["__scopeId", "data-v-ae70eb78"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/order/paysuccess.vue"]]);
  const _sfc_main$7 = {
    name: "dialog",
    data() {
      return {
        open: false,
        nodes: [],
        xyhtml: `<div class="markdown-body"><div class="privacy-dialog">
					  <div class="">
						<h3 style="text-align:center;">感谢您使用学习512！</h3>
						<p>为了更好地保障您的个人权益，请认真阅读<a target="_blank" href="//reg.163.com/agreement_wap.shtml?v=20171127">《使用协议》</a>、<a target="_blank" href="//study.163.com/topics/privacy_policy">《隐私政策》</a>和<a target="_blank" href="//study.163.com/topics/protocol_20005/">《服务条款》</a>的全部内容，同意并接受全部条款后开始使用我们的产品和服务。</p>
						<p>若不同意，将无法使用我们的产品和服务。</p>
					  </div>
					</div>
				</div>`
      };
    },
    created() {
      this.nodes = parserHtml.parserHTML(this.xyhtml);
    },
    methods: {
      handleClose() {
        this.open = false;
      },
      handleOpen() {
        this.open = true;
      }
    }
  };
  function _sfc_render$6(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "" }, [
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "ui-modal_box f-cb"
      }, [
        vue.createElementVNode("rich-text", { nodes: $data.nodes }, null, 8, ["nodes"]),
        vue.createElementVNode("view", {
          class: "btnbox",
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClose && $options.handleClose(...args))
        }, "知道了")
      ])) : vue.createCommentVNode("v-if", true),
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 1,
        class: "overlay",
        onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClose && $options.handleClose(...args))
      })) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["render", _sfc_render$6], ["__scopeId", "data-v-99aeb015"], ["__file", "D:/App/分销系统/cc_fenxiao/components/dialog/user.vue"]]);
  const _sfc_main$6 = {
    name: "dialog",
    data() {
      return {
        open: false,
        nodes: [],
        xyhtml: `<div class="gui-padding"><p style="margin-top: 21px;margin-bottom: 21px;text-align: center;line-height: 29px"><strong><span style="font-size:16px;font-family:宋体">学习512云课堂课程推广服务协议</span></strong></p><p style="margin-top: 10px;margin-bottom: 10px;text-align: left;text-indent: 24px;line-height: 29px"><span style="font-size:12px;font-family:宋体">欢迎注册并参与学习512云课堂课程推广，本协议是您与学习512云课堂之间就学习512云课堂课程推广服务等相关事宜所订立的协议。<strong>在参与学习512云课堂课程推广前，您应</strong></span><strong><span style="font-size:12px;font-family: 宋体;color:black">审慎阅读、充分理解本协议的全部内容</span></strong><strong><span style="font-size:12px;font-family: 宋体">，</span></strong><strong><span style="font-size: 12px;font-family:宋体;background:white">特别是加粗部分以及免除或者限制责任的条款</span></strong><span style="font-size: 12px;font-family:宋体;color:black">。如果</span><span style="font-size:12px;font-family:宋体">您</span><span style="font-size:12px;font-family:宋体;color:black">对本协议的条款存有疑问，可向</span><span style="font-size:12px;font-family:宋体">学习512云课堂</span><span style="font-size:12px;font-family:宋体;color:black">指定的对接人员进行询问，对接人员将向</span><span style="font-size:12px;font-family:宋体">您</span><span style="font-size:12px;font-family:宋体;color:black">解释条款内容。如果</span><span style="font-size:12px;font-family:宋体">您</span><span style="font-size:12px;font-family:宋体;color:black">不同意本协议的任一内容，或者无法准确理解条款的解释，或者就</span><span style="font-size:12px;font-family:宋体">学习512云课堂</span><span style="font-size:12px;font-family:宋体;color:black">指定人员对条款进行的解释存在异议，请不要同意本协议或进行本协议项下任何操作。一<strong>旦</strong></span><strong><span style="font-size:12px;font-family:宋体">您</span></strong><strong><span style="font-size:12px;font-family: 宋体;color:black">点击“我已阅读并同意”本协议，即表示您已理解并接受本协议缔约的所有条件及全部内容，同意受本协议约束，</span></strong><span style="font-size:12px;font-family:宋体">学习512云课堂</span><span style="font-size:12px;font-family:宋体;color:black">有权制定与本协议相关的推广规则，并依据该等规则及本协议对于您进行相应的审查、管理，或在您出现违反推广规则或违反本协议约定时进行相应的处理。届时您无权以未阅读本协议的内容或者任何其他理由，主张本协议无效，或要求撤销本协议。</span></p><p style="margin-top: 10px;margin-bottom: 10px;text-indent: 24px;line-height: 29px"><span style="font-size:12px">本协议内容包括协议正文、《学习512邮箱账号服务条款》（</span><a href="http://reg.163.com/agreement.shtml"><span style="font-size: 12px;color:purple">http://reg.163.com/agreement.shtml</span></a><span style="font-size:12px">）、本协议下述协议明确援引的其他协议、学习512云课堂已经发布的或将来可能发布的各类规则。所有规则为本协议不可分割的组成部分，与协议正文具有同等法律效力。<strong>学习512云课堂</strong></span><strong><span style="font-size:12px">有权基于业务发展需要适时更改本协议条款，</span></strong><strong><span style="font-size:12px">一旦相关内容发生变动，学习512云课堂将会通过电子邮件或网站公告等方式提示您。</span></strong><strong><span style="font-size:12px">若您不同意对相关内容所做的修改，则应以书面形式通知学习512云课堂终止本协议并立即停止推广行为，且无权向学习512云课堂主张因您未立即停止而产生的服务费；若您继续参与学习512云课堂课程推广的，则视为您同意对本协议内容所做的修改。</span></strong></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">一、定义</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px;background: white"><span style="font-size:12px;font-family:宋体;color:black">除非另有约定，下列术语在本协议中具有以下含义：</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">1.</span></strong><strong><span style="font-size:12px;font-family:宋体;color:black">学习512云课堂：</span></strong><span style="font-size:12px;font-family:宋体;color:black">指</span><span style="font-size:12px;font-family:宋体">学习512公司所有和经营的数字内容聚合、管理和分发的平台产品，旨在为用户提供教学内容的生成、传播和消费服务。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">2.</span></strong><strong><span style="font-size:12px;font-family:宋体;color:black">推广规则：</span></strong><span style="font-size:12px;font-family:宋体;color:black">指在学习512云课堂或其他相关平台上已经发布或将来可能发布的各种规范性文件，包括但不限于您</span><span style="font-size:12px;font-family:宋体">参与学习512云课堂课程推广</span><span style="font-size:12px;font-family:宋体;color:black">所应遵守的相应规则及其他细则、规范、政策、通知、公告等规范性文件。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">3.</span></strong><strong><span style="font-size:12px;font-family:宋体;color:black">推广课程：</span></strong><span style="font-size:12px;font-family:宋体">指经学习512云课堂选择的且供您在本协议项下推广渠道进行推广的课程。具体可推广的课程范围以云课堂指定页面实时显示为准。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体">4.</span></strong><strong><span style="font-size:12px;font-family:宋体;color:black">推广渠道：</span></strong><span style="font-size:12px;font-family:宋体;color:black">指您参与</span><span style="font-size: 12px;font-family:宋体">学习512云课堂课程推广</span><span style="font-size:12px;font-family:宋体;color:black">的，拥有和/或具有合法运营权、代理权的信息网络发布空间，包括但不限于符合国家规定的网页、论坛、SNS和/或即时通讯软件等。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">5.</span></strong><strong><span style="font-size:12px;font-family: 宋体;color:black">推广链接：</span></strong><span style="font-size:12px;font-family:宋体;color:black">指您在学习512云课堂指定页面自主获取的，用于分享在本协议项下推广渠道上供用户点击访问推广课程页面的代码链接。该链接将使用特定跟踪链接格式（带有您的专属参数），并指向特定的推广课程购买页面。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体">6.</span></strong><strong><span style="font-size:12px;font-family:宋体">用户：</span></strong><span style="font-size:12px;font-family:宋体">指通过您分享的推广链接访问学习512云课堂，并在对应页面购买特定推广课程的自然人、法人、其他组织。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体">7.</span></strong><strong><span style="font-size:12px;font-family:宋体">有效订单：</span></strong><span style="font-size:12px;font-family:宋体">指订单状态为交易成功并且售后状态为未发起售后，或者发起部分售后并且售后完成、售后关闭或者售后撤销的</span><span style="font-size:12px;font-family:宋体">订单。双方确认，以下订单均不列入有效订单范围：</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px">1</span><span style="font-size:12px;font-family:宋体">）</span><span style="font-size:12px;font-family:宋体">尚在交易中的订单、因各类原因导致交易失败的订单以及发生退货退款的订单；</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px">2</span><span style="font-size:12px;font-family:宋体">）用户未通过您分享的专属推广链接进入学习512云课堂购买推广课程而产生的订单，例如用户通过经输入一个普通的互联网搜索查询词或关键词后从搜索结果（即是自然的、免费的或未付费的搜索结果）所生成或显示的链接；</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px">3</span><span style="font-size:12px;font-family:宋体">）由于用户自身原因导致您的参数丢失后直接登陆学习512云课堂产生的购买行为，例如，用户通过您分享的推广链接访问学习512云课堂对应页面后，进入其他页面（即非该链接指向的特定页面）完成相同或其他课程购买而产生的订单。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">二、推广模式</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1.</span><span style="font-size:12px">您</span><span style="font-size:12px;letter-spacing: -0">为学习512云课堂上课程销售业务提供推广服务，即您</span><span style="font-size:12px;color:black">在拥有和/或具有合法经营权、代理权的推广渠道上分享本协议项下推广链接，以方便用户对学习512云课堂的访问以及在指定页面上购买推广</span><span style="font-size:12px">。学习512云课堂按照服务结算结算标准向您结算服务费用。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px;font-family:宋体">2.</span><span style="font-size:12px;font-family:宋体">您需先注册或登录学习512云课堂认可的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号后进行登录，并根据页面提示提交相关的信息资料。在没有明确相反证据情况下，您登录的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号和密码是学习512云课堂确认您的身份的唯一依据。您应保管好自己的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号和密码，如您未保管好自己的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号和密码而对自己、学习512云课堂或第三方造成损害的，您将负全部责任。另外，您应对您<span style="text-indent: 24px; text-wrap: wrap;">账</span>号中的所有活动和事件负全责。您同意若发现有非法使用您的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号或出现安全漏洞的情况，立即通告学习512云课堂。学习512云课堂有权根据运营需求或者其他原因对账号注册和登录方式进行变更，关于您使用账号的具体规则，请同时遵循学习512云课堂或其他第三方公司的相关账号使用协议。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px;font-family:宋体">3.</span><span style="font-size:12px;font-family:宋体">学习512云课堂有权对您提交的信息资料进行审核并决定是否通过。审核通过后，您即取得推广资格并取得相关使用权限，可登录学习512云课堂自主获取并在推广渠道上分享推广链接。</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">4.</span><span style="font-size: 12px;font-family:宋体;color:black">您了解并同意，学习512云课堂有权对您参与学习512云课堂课程推广的权限进行管理、调整，包括但不限于关闭已开通权限、开通其他功能的使用权限等。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">三、费用及结算</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1.</span><span style="font-size:12px;font-family:宋体">通过推广渠道分享课程专属推广链接且用户完成购买后您可获得相应的服务费，具体根据用户在学习512云课堂上通过您分享的推广链接在对应页面上购买指定推广课程所产生的有效订单净销售额（即扣除优惠券、红包等成本后的课程销售净额，下同），即按照</span><span style="font-size:12px">CPS</span><span style="font-size:12px;font-family:宋体">（</span><span style="font-size:12px">COST PER SALES</span><span style="font-size:12px;font-family:宋体">，按实际销售额提成服务费模式）方式进行结算，结算数据以学习512云课堂系统记录为准。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2.</span><span style="font-size:12px;font-family:宋体">服务费结算标准（服务费率）以学习512云课堂指定页面实时显示为准。学习512云课堂有权根据行业及自身业务发展情况单方面调整服务费结算标准并在网站上予以公布，自公布之日起适用新的服务费结算标准，若您不同意对相关标准所做的调整，则应以书面形式通知学习512云课堂终止本协议并立即停止课程推广行为。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family: 宋体">服务费核算：服务费</span></strong><strong><span style="font-size:12px">=</span></strong><strong><span style="font-size:12px;font-family: 宋体">有效订单净销售额</span></strong><strong><span style="font-size:12px">*</span></strong><strong><span style="font-size:12px;font-family: 宋体">服务费率</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">3.</span><span style="font-size:12px;font-family:宋体">每通过带有您专属参数的推广链接产生一笔交易成功的订单即可结算服务费，您可登陆学习512云课堂</span><span style="font-size:12px">—“</span><span style="font-size:12px;font-family:宋体">我的</span><span style="font-size:12px">”</span><span style="font-size:12px;font-family:宋体">个人中心中查询可结算服务费数据明细。就可结算服务费数据所示金额您可申请提现，在提现前，您需要提供学习512云课堂认可的经实名登记的收款账户。<strong>您确认并同意，您申请提现时显示的可结算服务费金额未排除</strong></span><strong><span style="font-size:12px;font-family: 宋体">因各类原因导致发生退货退款的订单以及其他非有效订单，</span></strong><strong><span style="font-size:12px;font-family: 宋体">任一交易成功的订单如发生退货退款或其他无效情况的，学习512云课堂将在实际结算费用中扣除相应订单对应的服务费款项。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">4.</span></strong><strong><span style="font-size:12px;font-family: 宋体">学习512云课堂将于每月</span></strong><strong><span style="font-size:12px">10</span></strong><strong><span style="font-size:12px;font-family: 宋体">日</span></strong><strong><span style="font-size:12px">-20</span></strong><strong><span style="font-size:12px;font-family: 宋体">日（遇节假日顺延）之间一次性支付您在上一月最后一日（含当日）前申请提现的有效订单的实际结算服务费金额至您提供的收款账户中，例如：</span></strong><strong><span style="font-size:12px">12</span></strong><strong><span style="font-size:12px;font-family: 宋体">月</span></strong><strong><span style="font-size:12px">10</span></strong><strong><span style="font-size:12px;font-family: 宋体">日</span></strong><strong><span style="font-size:12px">-20</span></strong><strong><span style="font-size:12px;font-family: 宋体">日将结算</span></strong><strong><span style="font-size:12px">11</span></strong><strong><span style="font-size:12px;font-family: 宋体">月</span></strong><strong><span style="font-size:12px">30</span></strong><strong><span style="font-size:12px;font-family: 宋体">日前申请提现的全部有效订单的实际结算服务费，而您在</span></strong><strong><span style="font-size:12px">11</span></strong><strong><span style="font-size:12px;font-family: 宋体">月</span></strong><strong><span style="font-size:12px">30</span></strong><strong><span style="font-size:12px;font-family: 宋体">日后申请提现的将在次年</span></strong><strong><span style="font-size:12px">1</span></strong><strong><span style="font-size:12px;font-family: 宋体">月</span></strong><strong><span style="font-size:12px">10</span></strong><strong><span style="font-size:12px;font-family: 宋体">日</span></strong><strong><span style="font-size:12px">-20</span></strong><strong><span style="font-size:12px;font-family: 宋体">日进行结算。</span></strong><span style="font-size:12px;font-family:宋体">您确认并同意，可结算服务费用为未扣除税费和转账手续费等金额，在此过程中产生的您个人所得税及手续费等法律法规或支付机构规定的税费和转账手续费等由学习512云课堂代扣代缴，如若涉及其他相关税费（包括但不限于增值税、附加税等）则由您自行完成申报。学习512云课堂服务费结算系统可能会因故导致无法按时结算，此种情况下，不视为学习512云课堂违约，但学习512云课堂应当及时排查并修复系统故障，以争取尽快向您支付相关服务费。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">5.</span><span style="font-size:12px;font-family:宋体">您如对学习512云课堂支付的实际结算费用数据明细有异议须及时向学习512云课堂指定人员提出，双方人工核对结算费用，经核算确有差异的，学习512云课堂通过您提供的收款账户向您支付差额；若经核算仍不能确认清楚的，结算费用以学习512云课堂核算的为准。对实际结算服务费数据明细产生超过</span><span style="font-size:12px">30</span><span style="font-size:12px;font-family:宋体">日而您未提出异议的视为无异议。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">6.</span><span style="font-size:12px;font-family:宋体">您知悉并同意，任一有效订单在学习512云课堂据此向您支付相应服务费后发生退货退款的，学习512云课堂将在下一次的结算费用中扣除相应款项，下一次结算费用不足以抵扣的，从之后的结算费用中继续扣除，直至全部扣回为止。如在本协议终止后发生上述情形的，您应向学习512云课堂退还对应部分的服务费。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">7.</span><span style="font-size:12px;font-family:宋体">您认可，如出现下述任一情形，订单则视为无效，学习512云课堂将不予支付您服务费：</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1</span><span style="font-size:12px;font-family:宋体">）由于不可抗力（包括但不限于服务器意外关机、网络意外中断、学习512云课堂受到恶意攻击及磁盘意外损坏等，定义见本协议第</span><span style="font-size:12px">6</span><span style="font-size:12px;font-family:宋体">条）而导致销售数据丢失；</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2</span><span style="font-size:12px;font-family:宋体">）您采取任何措施故意或客观上会使得用户对学习512云课堂产生任何误解或迷惑的（例如，您试图影响学习512云课堂或相关网站任何功能或交易过程的定购、浏览等）；</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">3</span><span style="font-size:12px;font-family:宋体">）用户订单为用于再度销售或任何商业用途的非自用性质；</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">4</span><span style="font-size:12px;font-family:宋体">）您提供服务过程中弄虚作假，如虚构交易订单等；</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">5</span><span style="font-size:12px;font-family:宋体">）您对运营商</span><span style="font-size:12px">dns</span><span style="font-size:12px;font-family: 宋体">域名进行劫持，包括但不限于表现为：</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">a</span><span style="font-size:12px;font-family:宋体">）用户自行访问学习512云课堂，但您强制用户跳转到带有跟踪代码的学习512云课堂网站页面，或在学习512云课堂网站或推广链接后增加跟踪代码；</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">b</span><span style="font-size:12px;font-family:宋体">）将学习512云课堂网站网页地址或推广链接中学习512云课堂或任何他方的跟踪代码修改成您自己的跟踪代码。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">四、您的权利和义务</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1.</span><span style="font-size:12px;font-family:宋体">您承诺并保证您具备履行本协议所需的能力和资质。为了服务的顺利、平稳进行，您应尽最大努力保证服务方式的灵活性。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2.</span><span style="font-size:12px;font-family:宋体">您确认您通过学习512云课堂所绑定或者以其他学习512云课堂认可的方式提供的收款账号为本协议项下的指定收款账号，您保证该账号为您自身所持有并经实名登记，否则因此产生的一切责任均由您自行承担。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">3.</span></strong><strong><span style="font-size:12px;font-family: 宋体">您承诺您在履行本协议时填写、登录、使用及发布的<span style="text-indent: 24px; text-wrap: wrap;">账</span>号名称、头像、简介、评论等信息资料应遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线，不得在相关信息资料中出现违法和不良信息，且不得有以下情形：</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">1</span></strong><strong><span style="font-size:12px;font-family: 宋体">）违反宪法或法律法规规定的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">2</span></strong><strong><span style="font-size:12px;font-family: 宋体">）危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">3</span></strong><strong><span style="font-size:12px;font-family: 宋体">）损害国家荣誉和利益的，损害公共利益的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">4</span></strong><strong><span style="font-size:12px;font-family: 宋体">）煽动民族仇恨、民族歧视，破坏民族团结的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">5</span></strong><strong><span style="font-size:12px;font-family: 宋体">）破坏国家宗教政策，宣扬邪教和封建迷信的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">6</span></strong><strong><span style="font-size:12px;font-family: 宋体">）散布谣言，扰乱社会秩序，破坏社会稳定的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">7</span></strong><strong><span style="font-size:12px;font-family: 宋体">）散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">8</span></strong><strong><span style="font-size:12px;font-family: 宋体">）侮辱或者诽谤他人，侵害他人合法权益的；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">9</span></strong><strong><span style="font-size:12px;font-family: 宋体">）含有法律、行政法规禁止的其他内容的。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family: 宋体">否则学习512云课堂有权立即暂停或终止您的账号、不予支付服务费并终止与您的合作，并有权追回已支付的全部服务费。此外，学习512云课堂有权要求您赔偿学习512云课堂及其关联公司因此造成的损失。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">4.</span></strong><strong><span style="font-size:12px;font-family: 宋体">您保证</span></strong><strong><span style="font-size:12px;font-family:宋体">您</span></strong><strong><span style="font-size:12px;font-family: 宋体;color:black">参与</span></strong><strong><span style="font-size:12px;font-family:宋体">学习512云课堂课程推广的</span></strong><strong><span style="font-size:12px;font-family: 宋体">渠道，</span></strong><strong><span style="font-size:12px;font-family:宋体">为推广、宣传云课堂课程而发布的推广链接、信息内容，以及履行本协议的所有其他行为均符合有关法律法规的规定，并不侵犯学习512云课堂及</span></strong><strong><span style="font-size:12px">/</span></strong><strong><span style="font-size:12px;font-family: 宋体">或任何第三方的合法权益。如因上述内容或行为违反相关法律法规规定，及</span></strong><strong><span style="font-size:12px">/</span></strong><strong><span style="font-size:12px;font-family: 宋体">或侵犯学习512云课堂及</span></strong><strong><span style="font-size:12px">/</span></strong><strong><span style="font-size:12px;font-family: 宋体">或任何第三方的合法权益而产生的任何争议、纠纷、诉讼、</span></strong><strong> </strong><strong><span style="font-size:12px;font-family: 宋体">仲裁、处罚、赔偿等，均由您自行负责处理并承担所有相关法律责任，与学习512云课堂无涉。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">5.</span><span style="font-size: 12px;font-family:宋体;color:black">您应仅在经学习512云课堂确认的推广渠道进行分享，不得擅自制作任何性质的链接放于其他渠道进行推广，尤其不得采取包括但不限于制作、安装木马程序等作弊手段进行推广。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">6.</span><span style="font-size:12px;font-family:宋体;color:black">您应遵守相关推广渠道的使用条件及规范，依法挑选和审查拟将推广的信息及（或）推广形式，并核查您推广后信息展示的情况，及时终止不法或不良信息的推广。您应为您的推广行为自行承担责任和风险，与学习512云课堂无关。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">7.</span><span style="font-size:12px;font-family:宋体;color:black">您有权在学习512云课堂上自主选择符合本协议约定的推广课程进行推广并在推广渠道中分享推广链接。您应按照本协议及相关规则要求并以学习512云课堂认可的方式在推广渠道上分享推广链接，该链接应置放于经双方确认的推广渠道的显著位置并须随时更新。您应保证上述链接的格式、落地页等符合学习512云课堂要求，未经学习512云课堂许可，不得随意修改、篡改、复制、许可第三方使用等。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">8.</span></strong><strong><span style="font-size:12px;font-family:宋体;color:black">学习512云课堂允许您在本协议有效期内仅为履行本协议之目的合理使用从学习512云课堂上获取的图形和/或文本。您确认上述图形或文本仅限于推广链接的分享，您不得修改、扭曲、损害相关内容的原有设计，不得以任何可能对学习512云课堂及其关联公司的商誉造成负面影响的方式或任何对您造成误导的方式使用该等图形或文本。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">9.</span></strong><strong><span style="font-size:12px;font-family: 宋体;color:black">未经学习512云课堂书面许可，您不得使用学习512云课堂或其关联公司的名称、商标或logo等企业标识，不得在推广渠道中使用学习512云课堂和学习512云课堂相近的域名或在搜索引擎上购买相近的关键词（包括但不限于近似、变异、拼错等），不得从事任何代表学习512云课堂或其关联公司的任何推广、营销或其他广告活动（包括但不限于线上、线下方式的推广、营销或其它广告活动，宣传材料的印刷），也不得采取任何作为或不作为措施，使得用户对学习512云课堂与您之间的关系产生误解或混淆。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">10.</span><span style="font-size:12px;font-family:宋体">您承诺，在履行本协议过程中，以扩大宣传、推广学习512云课堂及其形象、课程和服务为本协议的根本目的，并保证在履行本协议项下约定时遵循诚实信用原则，不损害学习512云课堂及其关联公司的任何利益。</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><strong><span style="font-size:12px;font-family:宋体;color:black">11.</span></strong><strong><span style="font-size:12px;font-family: 宋体;color:black">您应确保将通过推广渠道分享推广链接所提供的推广课程与任何第三方所提供的商品相同或实质上相似的任何商品和/或服务明确区分，不致造成混淆并误导用户，且不得以任何方式影响、干涉学习512云课堂的正常经营行为。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">12.</span><span style="font-size: 12px;font-family:宋体;color:black">您应当及时更新推广链接。当推广链接内容中宣传的推广课程下架后或者不再适用于本协议后，您应当立即删除并停止分享该推广链接。您违反此约定的，就该推广链接将无法获得任何服务费，同时，您赔偿由此给学习512云课堂造成的全部损失。</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">13.</span><span style="font-size: 12px;font-family:宋体;color:black">未经学习512云课堂事先书面许可，您在合作期间不得以学习512云课堂或学习512公司名义向任何第三方以收取费用或发放一定的现金、代金券、积分等方式开展合作，不得在推广渠道上发布或发放学习512云课堂任何形式的优惠券信息。一经发现，学习512云课堂有权立即单方解除本协议并取消您的推广资格，并有权拒绝支付全部或部分未发放的服务费作为您违约金。</span></p><p style="margin-top: 7px;margin-bottom: 7px;text-align: left;line-height: 29px"><span style="font-size:12px;font-family:宋体;color:black">14.</span><span style="font-size: 12px;font-family:宋体;color:black">您明确理解并同意，如因违反有关法律法规或者本协议之规定，使学习512云课堂及其关联公司等遭受任何损失包括但不限于收到任何第三方的索赔、任何行政管理部门的处罚和可能遭受的其他任何损失（包括合理的律师费用），您应进行全额赔偿，您在此不可撤销地同意并授权学习512云课堂径行扣除您账户内已结算未提现的部分或全部收入。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">五、知识产权及保密</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1.</span><span style="font-size:12px;font-family:宋体">学习512云课堂或相关网站上的所有内容，以及您基于本协议项下服务而产生的全部成果，包括但不限于文字、软件、声音、图片、录像、图表、网站架构、网站画面的安排、网页设计、在广告中的全部内容、课程、链接以及其它信息均由学习512云课堂或其关联公司依法拥有其知识产权或其他权益，</span> <span style="font-size:12px;font-family:宋体">包括但不限于商标权、专利权、著作权、商业秘密等。非经学习512云课堂书面同意，您不得擅自使用、修改、全部或部分复制、公开传播、改变、散布、发行或公开发表、转载、引用、链接、抓取或以其他方式使用学习512云课堂程序或上述内容。若您违反本条约定给学习512云课堂或其关联公司造成损失的，您应当予以赔偿。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2.</span><span style="font-size:12px;font-family:宋体">您在本协议的履行过程中获知的学习512云课堂或其关联公司的保密信息，包括但不限于销售数据、您信息、经营计划、经营方式、客户清单、市场机会、会计或财务记录等与学习512云课堂或其关联公司业务有关的任何信息，在未经学习512云课堂事先书面同意的情况下，不得向任何第三方披露，并不得为履行本协议义务以外的其他任何目的使用保密信息，否则应承担学习512云课堂及其关联公司因此而受到的全部损失。此保密义务在本协议终止后继续有效。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">六、不可抗力及其他免责事由</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">1.</span></strong><strong><span style="font-size:12px;font-family: 宋体">您理解并同意，在履行本协议过程中，可能会遇到不可抗力等风险因素，使本协议项下合作发生中断。不可抗力是指不能预见、不能克服并不能避免且对一方或双方造成重大影响的客观事件，包括但不限于自然灾害如洪水、地震、瘟疫流行和风暴等以及社会事件如战争、动乱、政府行为等。出现上述情况时，学习512云课堂将努力在第一时间与相关单位配合，及时进行修复，但是由此给您造成的损失学习512云课堂在法律允许的范围内免责。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">2.</span></strong><strong><span style="font-size:12px;font-family: 宋体">学习512云课堂不保证：学习512云课堂及本协议项下合作无错误及不会中断、所有缺陷已被更正、或不会受到病毒或任何其它因素的损害。除非有法律明确规定，学习512云课堂在此明确声明不承担任何明示或默示的担保责任，包括但不限于对推广链接及其所示推广课程、推广素材的性能、适用性或不侵权的担保。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">3.</span></strong><strong><span style="font-size:12px;font-family: 宋体">您理解并同意，在履行本协议的过程中，可能会遇到网络信息或他人行为带来的风险，学习512云课堂不对任何信息的真实性、适用性、合法性承担责任，也不对因侵权行为给您造成的损害负责。这些风险包括但不限于：</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">1</span></strong><strong><span style="font-size:12px;font-family: 宋体">）来自他人匿名或冒名的含有威胁、诽谤、令人反感或非法内容的信息；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">2</span></strong><strong><span style="font-size:12px;font-family: 宋体">）因履行本协议，遭受他人误导、欺骗或其他导致或可能导致的任何心理、生理上的伤害以及经济上的损失；</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">3</span></strong><strong><span style="font-size:12px;font-family: 宋体">）其他因网络信息或他人行为引起的风险。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">4.</span></strong><strong><span style="font-size:12px;font-family: 宋体">学习512云课堂依据本协议约定获得处理违法违规内容的权利，该权利不构成学习512云课堂的义务或承诺，学习512云课堂不能保证及时发现违法行为或进行相应处理。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">5.</span></strong><strong><span style="font-size:12px;font-family: 宋体">学习512云课堂有权以向您发送通知方式（包括电子邮件或网站公告等方式）独立决定终止本协议，本协议终止后，双方应尽快对本协议项下应付未付的服务费进行清算。若因您的行为违反法律法规的规定或本协议的规定，学习512云课堂可随时终止本协议且不需要向您支付任何费用，并有权要求您承担相应的责任。</span></strong></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">七、违约责任</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">1.</span></strong><strong><span style="font-size:12px;font-family: 宋体">如果非因不可抗力因素，您违反本协议规定的，在接到学习512云课堂通知后您应于</span></strong><strong><span style="font-size:12px">1</span></strong><strong><span style="font-size:12px;font-family: 宋体">个工作日内纠正，如仍未达到学习512云课堂要求，学习512云课堂有权视违约情节轻重，单方提出解除协议，终止您账号，并有权向您追回已支付的全部服务费，且您必须赔偿由此给学习512云课堂及其关联公司造成的全部损失。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2.</span><span style="font-size:12px;font-family:宋体">双方均有过错的，应根据各方实际过错程度，分别承担各自的违约责任。但是无论如何，学习512云课堂在本协议项下的累计总赔偿责任不应超过学习512云课堂应付和已付您的服务费总额。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">3.</span><span style="font-size:12px;font-family:宋体">除非本协议另有明确约定，如您违反本协议载明的陈述及保证，则学习512云课堂有权要求您按照违约期间所获得的全部收益的两倍向学习512云课堂支付违约金。</span></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">八、适用法律与争议解决</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">1.</span></strong><strong><span style="font-size:12px;font-family: 宋体">本协议适用中华人民共和国大陆地区施行的法律。当本协议的任何内容与中华人民共和国法律相抵触时，应当以法律规定为准，同时相关协议将按法律规定进行修改或重新解释，而本协议其他部分的法律效力不变。</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><strong><span style="font-size:12px">2.</span></strong><strong><span style="font-size:12px;font-family: 宋体">凡因本协议引起的或与本协议有关的任何争议，均应提交中国国际经济贸易仲裁委员会，按照申请仲裁时该会现行有效的仲裁规则进行仲裁。仲裁裁决是终局的，对双方均有约束力。</span></strong></p><p style="margin: 21px 0 10px;line-height: 29px;background: white"><strong><span style=";font-family:宋体;color:black">九、其他</span></strong></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">1.</span><span style="font-size:12px;font-family:宋体">双方在此声明：双方不会因根据本协议产生的合作，构成任何形式的劳动或雇佣关系。学习512云课堂除向您支付本协议约定的费用外，不承担您的任何其他费用，包括但不限于因推广活动所产生的成本、社会保险、福利和医疗保险费用等。</span></p><p style="margin-top: 7px;margin-bottom: 7px;line-height: 29px"><span style="font-size:12px">2.</span><span style="font-size:12px;font-family:宋体">本协议不包含任何可能理解为双方之间设立一种代理或合伙关系的内容。您无权代表学习512云课堂对外缔结协议。您不得以学习512云课堂的名义开展任何与约定的推广无关的活动或者从事违法犯罪活动，否则一切后果由您自行承担，同时学习512云课堂保留追究其法律责任的权利。</span></p><p><br></p></div>`
      };
    },
    created() {
      this.nodes = parserHtml.parserHTML(this.xyhtml);
    },
    methods: {
      handleClose() {
        this.open = false;
      },
      handleOpen() {
        this.open = true;
      }
    }
  };
  function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", null, [
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "ui-modal_box f-cb"
      }, [
        vue.createElementVNode("rich-text", { nodes: $data.nodes }, null, 8, ["nodes"]),
        vue.createElementVNode("view", {
          class: "btnbox",
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClose && $options.handleClose(...args))
        }, "知道了")
      ])) : vue.createCommentVNode("v-if", true),
      $data.open ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 1,
        class: "overlay",
        onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClose && $options.handleClose(...args))
      })) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const __easycom_1$1 = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["render", _sfc_render$5], ["__scopeId", "data-v-0531cd51"], ["__file", "D:/App/分销系统/cc_fenxiao/components/dialog/tuiguang.vue"]]);
  const _imports_0 = "/static/logo.png";
  const _imports_1 = "/static/imgs/bg.jpg";
  const _sfc_main$5 = {
    data() {
      return {
        dialogtgref: null,
        dialogShareref: null,
        currentIndex: 0
      };
    },
    methods: {
      handleIncome() {
        uni.switchTab({
          url: "/pages/tabbar/income"
        });
      },
      handleOpen() {
        this.$refs.dialogtgref.handleOpen();
      },
      handleChangeImg(index) {
        this.currentIndex = index;
      },
      handleShare() {
        this.$refs.dialogShareref.handleOpen();
      }
    }
  };
  function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_cc_dialog_user = resolveEasycom(vue.resolveDynamicComponent("cc-dialog-user"), __easycom_0);
    const _component_cc_dialog_tuiguang = resolveEasycom(vue.resolveDynamicComponent("cc-dialog-tuiguang"), __easycom_1$1);
    return vue.openBlock(), vue.createElementBlock("view", { class: "ui-cps-wrap" }, [
      vue.createElementVNode("view", { class: "ui-cps-top" }, [
        vue.createElementVNode("view", {
          class: "sharebtn",
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleShare && $options.handleShare(...args))
        }, [
          vue.createElementVNode("text", { class: "gui-icons gui-block gui-text" }, "")
        ]),
        vue.createVNode(
          _component_cc_dialog_user,
          { ref: "dialogShareref" },
          null,
          512
          /* NEED_PATCH */
        ),
        vue.createElementVNode("view", { class: "ui-cps-img" }, [
          vue.createElementVNode("image", {
            class: "um-cps-ux-promoter-info-composite_img",
            src: "/static/imgs/bg.jpg"
          }),
          vue.createElementVNode("view", { class: "ui-cps-char gui-color-white mt10 fz13 gui-flex gui-align-items-center" }, [
            vue.createElementVNode("text", {
              class: "fz28 mr5",
              style: { "margin-top": "-4px" }
            }, "☛"),
            vue.createTextVNode("长按上方图片保存并发送朋友圈 ")
          ])
        ]),
        vue.createElementVNode("view", { class: "gui-flex gui-footer-img gui-padding" }, [
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass([[$data.currentIndex == 0 ? "active" : ""], "img-item"]),
              onClick: _cache[1] || (_cache[1] = ($event) => $options.handleChangeImg(0))
            },
            [
              vue.createElementVNode("image", { src: _imports_0 })
            ],
            2
            /* CLASS */
          ),
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass([[$data.currentIndex == 1 ? "active" : ""], "img-item"]),
              onClick: _cache[2] || (_cache[2] = ($event) => $options.handleChangeImg(1))
            },
            [
              vue.createElementVNode("image", { src: _imports_1 })
            ],
            2
            /* CLASS */
          ),
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass([[$data.currentIndex == 2 ? "active" : ""], "img-item"]),
              onClick: _cache[3] || (_cache[3] = ($event) => $options.handleChangeImg(2))
            },
            [
              vue.createElementVNode("image", { src: _imports_0 })
            ],
            2
            /* CLASS */
          ),
          vue.createElementVNode(
            "view",
            {
              class: vue.normalizeClass([[$data.currentIndex == 3 ? "active" : ""], "img-item"]),
              onClick: _cache[4] || (_cache[4] = ($event) => $options.handleChangeImg(3))
            },
            [
              vue.createElementVNode("image", { src: _imports_0 })
            ],
            2
            /* CLASS */
          )
        ])
      ]),
      vue.createElementVNode("view", { class: "mt20" }, [
        vue.createElementVNode("view", { class: "gui-flex gui-justify-content-center" }, [
          vue.createElementVNode("view", {
            onClick: _cache[5] || (_cache[5] = (...args) => $options.handleIncome && $options.handleIncome(...args)),
            class: "gui-button gui-color-white gui-border gui-bg-red",
            style: { "width": "200rpx", "padding": "0 10px!important" }
          }, [
            vue.createElementVNode("text", { class: "button" }, "查看我的收益")
          ])
        ]),
        vue.createElementVNode("view", { class: "gui-padding fz13 gui-color-gray" }, [
          vue.createTextVNode("关注“学习512精品课推广”微信服务号，及时获取推广佣金到账信息。分享表示同意"),
          vue.createElementVNode("text", {
            class: "gui-color-green",
            onClick: _cache[6] || (_cache[6] = (...args) => $options.handleOpen && $options.handleOpen(...args))
          }, "《学习512课程推广服务协议》")
        ]),
        vue.createVNode(
          _component_cc_dialog_tuiguang,
          { ref: "dialogtgref" },
          null,
          512
          /* NEED_PATCH */
        )
      ])
    ]);
  }
  const PagesShareShare = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$4], ["__scopeId", "data-v-ceb22cc9"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/share/share.vue"]]);
  const _sfc_main$4 = {
    name: "income",
    data() {
      return {};
    }
  };
  function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "cc-box-container" }, [
      (vue.openBlock(), vue.createElementBlock(
        vue.Fragment,
        null,
        vue.renderList(20, (num) => {
          return vue.createElementVNode("view", { class: "cc-boxc" }, [
            vue.createElementVNode("view", { class: "tagb" }, "推广统计"),
            vue.createElementVNode("view", { class: "gui-flex gui-column gui-column-items" }, [
              vue.createElementVNode("view", { class: "tit" }, "Redis与SpringBoot互联网实战"),
              vue.createElementVNode("view", { class: "dt" }, "2024-02-04 12:05:41"),
              vue.createElementVNode("view", { class: "gui-flex gui-align-items-center" }, [
                vue.createElementVNode("view", { class: "desc fb fz11" }, "统计数：4112"),
                vue.createElementVNode("view", {
                  class: "desc fb fz11",
                  style: { "margin-left": "20px" }
                }, "成交数：4112")
              ])
            ])
          ]);
        }),
        64
        /* STABLE_FRAGMENT */
      ))
    ]);
  }
  const __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$3], ["__scopeId", "data-v-c5fac738"], ["__file", "D:/App/分销系统/cc_fenxiao/components/income/state.vue"]]);
  const _sfc_main$3 = {
    data() {
      return {
        currentIndex: 0,
        isLoad: false,
        navItems: [{
          id: 1,
          name: "推广数据统计"
        }, {
          id: 2,
          name: "佣金余额明细"
        }, {
          id: 3,
          name: "提现记录"
        }]
      };
    },
    onLoad() {
      setTimeout(() => {
        this.isLoad = true;
      }, 1e3);
    },
    methods: {
      handleBack() {
        uni.switchTab({
          url: "/pages/tabbar/me"
        });
      },
      navchange(e) {
        this.currentIndex = e;
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_logo = resolveEasycom(vue.resolveDynamicComponent("logo"), __easycom_0$c);
    const _component_cc_income_state = resolveEasycom(vue.resolveDynamicComponent("cc-income-state"), __easycom_1);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", { class: "gui-padding" }, [
          vue.createVNode(_component_logo, {
            title: "推广统计分析",
            showback: "",
            issearch: false,
            onBack: $options.handleBack,
            btntext: ""
          }, null, 8, ["onBack"])
        ]),
        vue.createElementVNode("view", { class: "tipbox" }, "推广订单过了冻结周期后，对应的收益转为可提现"),
        vue.createElementVNode("view", { style: { "padding-top": "0" } }, [
          !$data.isLoad ? (vue.openBlock(), vue.createElementBlock("view", {
            key: 0,
            class: "demo-bg demo-title gui-border-radius"
          })) : vue.createCommentVNode("v-if", true),
          vue.createVNode(_component_cc_income_state)
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesStateState = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$2], ["__scopeId", "data-v-5b779960"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/state/state.vue"]]);
  const _sfc_main$2 = {
    data() {
      return {
        scroll: false,
        items: [
          [80, "", "我的订单"],
          [100, "", "优惠券"],
          [50, "", "关注店铺"],
          [99, "", "收藏"]
        ]
      };
    },
    methods: {
      toDetail() {
        uni.navigateTo({
          url: "/pages/shop/detail"
        });
      },
      toOrder() {
        uni.navigateTo({
          url: "/pages/user/order"
        });
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { style: { "background": "#f3f3f3" } }, {
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "flex justify-content-center align-items",
          style: { "height": "100vh" }
        }, [
          vue.createElementVNode("view", { class: "detail-items gui-border-radius" }, [
            vue.createElementVNode("view", { class: "flex flex-c align-items" }, [
              vue.createElementVNode("view", null, [
                vue.createElementVNode("text", { class: "gui-icons gui-color-red fz32 fb" }, "")
              ]),
              vue.createElementVNode("view", { class: "mt20" }, [
                vue.createElementVNode("text", { class: "fz20 fb gui-color-red" }, "你账号已被冻结！")
              ]),
              vue.createElementVNode("view", { class: "mt20" }, [
                vue.createElementVNode("button", {
                  type: "default",
                  onClick: _cache[0] || (_cache[0] = (...args) => $options.toOrder && $options.toOrder(...args)),
                  class: "gui-button-mini gui-gtbg-red",
                  style: { "width": "282rpx" }
                }, [
                  vue.createElementVNode("text", { class: "gui-color-white gui-button-text-mini gui-icons" }, " 联系客服")
                ])
              ])
            ])
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesErrorPermission = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$1], ["__scopeId", "data-v-b90db7b2"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/error/permission.vue"]]);
  const _sfc_main$1 = {
    data() {
      return {
        // 切换导航
        navItems: [{
          id: 1,
          name: "可使用"
        }, {
          id: 2,
          name: "已使用"
        }, {
          id: 3,
          name: "已过期"
        }]
      };
    },
    created() {
    },
    methods: {}
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_gui_switch_navigation = resolveEasycom(vue.resolveDynamicComponent("gui-switch-navigation"), __easycom_0$6);
    const _component_gui_page = resolveEasycom(vue.resolveDynamicComponent("gui-page"), __easycom_1$8);
    return vue.openBlock(), vue.createBlock(_component_gui_page, { customHeader: true }, {
      gHeader: vue.withCtx(() => []),
      gBody: vue.withCtx(() => [
        vue.createElementVNode("view", {
          class: "ps gui-bg-white",
          style: { "z-index": "100" }
        }, [
          vue.createCommentVNode(" 切换导航 "),
          vue.createElementVNode("view", { class: "gui-border-b gui-padding-small" }, [
            vue.createVNode(_component_gui_switch_navigation, {
              items: $data.navItems,
              isCenter: true,
              size: 180,
              lineHeight: "60rpx",
              width: "750",
              textAlign: "center",
              activeLineWidth: "180rpx",
              activeLineHeight: "6rpx",
              activeLineClass: ["gui-gtbg-red"],
              margin: 10,
              onChange: _ctx.navChange
            }, null, 8, ["items", "onChange"])
          ]),
          vue.createElementVNode("view", { class: "gui-padding-m" }, [
            (vue.openBlock(), vue.createElementBlock(
              vue.Fragment,
              null,
              vue.renderList(20, (num) => {
                return vue.createElementVNode("view", { class: "list-coupon-item gui-padding" }, [
                  vue.createElementVNode("view", { class: "flex align-items" }, [
                    vue.createElementVNode("view", { class: "price red fb ml10 mt10" }, [
                      vue.createElementVNode("text", { class: "fz20" }, "￥"),
                      vue.createElementVNode("text", { class: "fz48" }, "50")
                    ]),
                    vue.createElementVNode("view", { class: "ml20" }, [
                      vue.createElementVNode("view", { class: "tit fz14" }, "无金额门槛"),
                      vue.createElementVNode("view", { class: "tit fz14 mt5" }, "2019.02.21 13:56－2019.02.22 13:56")
                    ])
                  ]),
                  vue.createElementVNode("view", { class: "gui-padding fz14" }, [
                    vue.createElementVNode("view", null, "优惠提供：java架构漫谈"),
                    vue.createElementVNode("view", { class: "mt4" }, "适用范围：深入剖析dubbo源码的设计原理")
                  ])
                ]);
              }),
              64
              /* STABLE_FRAGMENT */
            ))
          ])
        ])
      ]),
      _: 1
      /* STABLE */
    });
  }
  const PagesCouponCoupon = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render], ["__scopeId", "data-v-96ba783d"], ["__file", "D:/App/分销系统/cc_fenxiao/pages/coupon/coupon.vue"]]);
  __definePage("pages/tabbar/index", PagesTabbarIndex);
  __definePage("pages/tabbar/me", PagesTabbarMe);
  __definePage("pages/tabbar/income", PagesTabbarIncome);
  __definePage("pages/login/login", PagesLoginLogin);
  __definePage("pages/login/pwd", PagesLoginPwd);
  __definePage("pages/category/course", PagesCategoryCourse);
  __definePage("pages/category/downloads", PagesCategoryDownloads);
  __definePage("pages/category/note", PagesCategoryNote);
  __definePage("pages/category/rule", PagesCategoryRule);
  __definePage("pages/course/detail", PagesCourseDetail);
  __definePage("pages/course/category", PagesCourseCategory);
  __definePage("pages/course/special", PagesCourseSpecial);
  __definePage("pages/note/detail", PagesNoteDetail);
  __definePage("pages/downloads/detail", PagesDownloadsDetail);
  __definePage("pages/user/message", PagesUserMessage);
  __definePage("pages/user/xieyi", PagesUserXieyi);
  __definePage("pages/shopcart/shopcart", PagesShopcartShopcart);
  __definePage("pages/search/search", PagesSearchSearch);
  __definePage("pages/search/detail", PagesSearchDetail);
  __definePage("pages/user/bind", PagesUserBind);
  __definePage("pages/user/settings", PagesUserSettings);
  __definePage("pages/order/confirm", PagesOrderConfirm);
  __definePage("pages/order/paysuccess", PagesOrderPaysuccess);
  __definePage("pages/share/share", PagesShareShare);
  __definePage("pages/state/state", PagesStateState);
  __definePage("pages/error/permission", PagesErrorPermission);
  __definePage("pages/coupon/coupon", PagesCouponCoupon);
  const _sfc_main = {
    onLaunch: function() {
      const domModule = requireNativePlugin("dom");
      domModule.addRule("fontFace", {
        "fontFamily": "graceuiiconfont",
        "src": "url('https://at.alicdn.com/t/c/font_823462_whtuj4ktcl.ttf?t=1703075468532')"
      });
    },
    onShow: function() {
    },
    onHide: function() {
    }
  };
  const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["__file", "D:/App/分销系统/cc_fenxiao/App.vue"]]);
  const GraceRequestConfig = {
    // api 基础 url
    apiBaseUrl: "http://192.168.0.185/API/",
    // 调试模式 [ false 关闭调试输出，项目发包时请设置此项为 false ]
    debug: true,
    // 本地 Token 数据键名称
    localTokenKeyName: "reqToken",
    // 提交时基础数据令牌 header 数据名称
    requestHeaderTokenName: "RTK",
    // 用户登录 Token 数据键名称
    userTokenKeyName: "uToken",
    // 提交时用户数据令牌 header 数据名称
    requestUserTokenName: "UTK",
    // token 有效期, 单位 秒 ，要与后端保持一致
    expiredTime: 3600,
    // post 方式 header[content-type] 默认值
    postHeaderDefault: "application/x-www-form-urlencoded",
    // 接口请求秘钥 与后端开发人员协商格式及获取
    apiKey: "******",
    // 基础 token 获取 api 接口地址
    baseTokenUrl: "http://192.168.0.185/API/InitBaseToken",
    // 从服务器获取 token 函数
    // 因后端语言不同、思路不同，请跟据自己的 api 情况完成 token 获取函数
    // 一个示例，实际开发请跟据自己情况复写此函数	
    getToken: function() {
      var apiKey = this.apiKey;
      var baseTokenUrl = this.baseTokenUrl;
      return new Promise((resolve, reject) => {
        uni.request({
          // token 授权 api 服务器地址
          url: baseTokenUrl,
          // 请求秘钥
          data: { "appKey": apiKey },
          // 请求方式 POST
          method: "GET",
          // 返回值类型
          dataType: "json"
        }).then((res) => {
          var data = res.data;
          formatAppLog("log", "at custom/graceRequestConfig.js:50", data);
          if (data.errcode == 0) {
            uni.setStorageSync(this.localTokenKeyName, data.data);
            var dateObj = /* @__PURE__ */ new Date();
            var cTime = dateObj.getTime();
            cTime += "";
            uni.setStorageSync("GraceRequestTokenTime", cTime);
            resolve(data.data);
          } else {
            reject("token error 01");
          }
        }).catch((e) => {
          reject("token error 01");
        });
      });
    },
    // 记录 用户 token 到本地方法
    // 格式 loginToken-uid
    // 您可以根据项目需要继续改进
    writeLoginToken: function(token, uid) {
      var loginToken = token + "-" + uid;
      uni.setStorageSync(this.userTokenKeyName, loginToken);
      return;
    },
    // 获取基础 token 失败提示函数
    tokenErrorMessage: function() {
      uni.showToast({
        title: "请求失败, 请重试",
        icon: "none"
      });
    }
  };
  const GraceRequest = {
    // token 数据记录
    token: "",
    // 
    getTokenFromApi: function(count) {
      return new Promise((resolve, reject) => {
        this.debug("第 " + count + " 次 token 请求");
        let p = GraceRequestConfig.getToken();
        p.then((res) => {
          this.token = res;
          resolve(res);
        }).catch((err) => {
          reject("token error");
        });
      });
    },
    // 获取 token
    getToken: function(count) {
      return new Promise((resolve, reject) => {
        var token = uni.getStorageSync(GraceRequestConfig.localTokenKeyName);
        if (!token || token == "") {
          let p = this.getTokenFromApi(1);
          p.then((res) => {
            this.token = res;
            resolve(res);
          }).catch((err) => {
            let p2 = this.getTokenFromApi(2);
            p2.then((res) => {
              this.token = res;
              resolve(res);
            }).catch((err2) => {
              reject(err2);
            });
          });
        } else {
          var expiredTime = GraceRequestConfig.expiredTime;
          expiredTime *= 1e3;
          var tokenTime = Number(uni.getStorageSync("GraceRequestTokenTime"));
          tokenTime += expiredTime;
          var dateObj = /* @__PURE__ */ new Date();
          var cTime = dateObj.getTime();
          if (tokenTime > cTime) {
            this.token = token;
            resolve(token);
          } else {
            let p = this.getTokenFromApi(1);
            p.then((res) => {
              this.token = res;
              resolve(res);
            }).catch((err) => {
              let p2 = this.getTokenFromApi(2);
              p2.then((res) => {
                this.token = res;
                resolve(res);
              }).catch((err2) => {
                reject(err2);
              });
            });
          }
        }
      });
    },
    // 设置默认值补齐
    requestInit: function(sets, withLoginToken, url) {
      if (!sets.data) {
        sets.data = {};
      }
      if (!sets.header) {
        sets.header = {};
      }
      if (!sets.timeout) {
        sets.timeout = GraceRequestConfig.expiredTime;
      }
      if (!sets.dataType) {
        sets.dataType = "json";
      }
      var requestTokenName = "";
      {
        requestTokenName = GraceRequestConfig.requestHeaderTokenName;
      }
      sets.header[requestTokenName] = this.token;
      if (withLoginToken) {
        var userTokenName = "";
        {
          userTokenName = GraceRequestConfig.requestUserTokenName;
        }
        var loginToken = this.checkLogin();
        if (loginToken) {
          sets.header[userTokenName] = loginToken;
        }
      }
      {
        sets.requestUrl = GraceRequestConfig.apiBaseUrl + url;
      }
      return sets;
    },
    // 服务端 token 错误处理
    tokenErrorHandle: function(res) {
      if (res.data && res.data == "token error") {
        uni.removeStorageSync(GraceRequestConfig.localTokenKeyName);
        return true;
      }
      return false;
    },
    // 请求基础函数
    base: function(url, sets, withLoginToken, type, isSign) {
      return new Promise(async (resolve, reject) => {
        let p = this.getToken();
        p.then((token) => {
          if (this.befor != null) {
            this.befor();
            this.befor = null;
          }
          sets = this.requestInit(sets, withLoginToken, url);
          if (type == "POST") {
            if (!sets.header["content-type"]) {
              sets.header["content-type"] = GraceRequestConfig.postHeaderDefault;
            }
          }
          if (!isSign) {
            isSign = false;
          }
          if (isSign) {
            sets.data = this.sign(sets.data);
          }
          uni.request({
            url: sets.requestUrl,
            data: sets.data,
            timeout: sets.timeout,
            dataType: sets.dataType,
            header: sets.header,
            method: type
          }).then((data) => {
            if (this.after != null) {
              this.after();
              this.after = null;
            }
            resolve(data.data);
          }).catch((err) => {
            if (this.after != null) {
              this.after();
              this.after = null;
            }
            reject(err);
          });
        }).catch((err) => {
          if (this.after != null) {
            this.after();
            this.after = null;
          }
          reject(err);
        });
      });
    },
    // GET 请求
    get: function(url, sets, withLoginToken) {
      return new Promise((resolve, reject) => {
        if (!sets) {
          sets = {};
        }
        if (!withLoginToken) {
          withLoginToken = false;
        }
        let p = this.base(url, sets, withLoginToken, "GET");
        p.then((res) => {
          if (this.tokenErrorHandle(res)) {
            let p2 = this.base(url, sets, withLoginToken, "GET");
            p2.then((res2) => {
              resolve(res2);
            }).catch((err) => {
              reject(err);
            });
          } else {
            resolve(res);
          }
        }).catch((err) => {
          reject(err);
        });
      });
    },
    // POST 请求
    post: function(url, sets, isSign, withLoginToken) {
      return new Promise((resolve, reject) => {
        if (!sets) {
          sets = {};
        }
        if (!isSign) {
          isSign = false;
        }
        if (!withLoginToken) {
          withLoginToken = false;
        }
        let p = this.base(url, sets, withLoginToken, "POST", isSign);
        p.then((res) => {
          if (this.tokenErrorHandle(res)) {
            let p2 = this.base(url, sets, withLoginToken, "POST", isSign);
            p2.then((res2) => {
              resolve(res2);
            }).catch((err) => {
              reject(err);
            });
          } else {
            resolve(res);
          }
        }).catch((err) => {
          reject(err);
        });
      });
    },
    // upload
    upload: function(url, filePath, fileType, sets, withLoginToken) {
      return new Promise(async (resolve, reject) => {
        let p = this.getToken();
        p.then((token) => {
          if (this.befor != null) {
            this.befor();
            this.befor = null;
          }
          sets = this.requestInit(sets, withLoginToken, url);
          if (!sets.name) {
            sets.name = "file";
          }
          uni.uploadFile({
            url: sets.requestUrl,
            filePath,
            name: sets.name,
            formData: sets.data,
            header: sets.header
          }).then((data) => {
            if (this.after != null) {
              this.after();
              this.after = null;
            }
            resolve(data.data);
          }).catch((err) => {
            if (this.after != null) {
              this.after();
              this.after = null;
            }
            reject(err);
          });
        }).catch((err) => {
          if (this.after != null) {
            this.after();
            this.after = null;
          }
          reject(err);
        });
      });
    },
    // debug 函数
    debug: function(content) {
      formatAppLog("log", "at Grace6/js/request.js:260", content);
    },
    // 签名算法
    sign: function(data) {
      if (data.gracesign) {
        delete data.gracesign;
      }
      var vals = [];
      Object.keys(data).sort().map((key) => {
        vals.push(data[key]);
      });
      vals.push(this.token);
      var sign = md5.md5(vals.join("-"));
      data.gracesign = sign;
      return data;
    },
    // 登录检查
    // 登录后在本地保存一个 token
    checkLogin: function(notLoginDo) {
      var loginToken = uni.getStorageSync(GraceRequestConfig.userTokenKeyName);
      if (!loginToken || loginToken == "") {
        loginToken = "";
        if (notLoginDo) {
          uni.showToast({ title: "请登录", icon: "none", mask: true });
          setTimeout(() => {
            notLoginDo();
          }, 1500);
        }
        return false;
      }
      return loginToken;
    },
    // 跳转到登录页面
    gotoLogin: function(path, opentype) {
      if (!path) {
        path = "../login/login";
      }
      if (!opentype) {
        opentype = "redirect";
      }
      switch (opentype) {
        case "redirect":
          uni.redirectTo({ url: path });
          break;
        case "navigate":
          uni.navigateTo({ url: path });
          break;
        case "switchTab":
          uni.switchTab({ url: path });
          break;
      }
    }
  };
  uni.gRequest = GraceRequest;
  function createApp() {
    const app = vue.createVueApp(App);
    return {
      app
    };
  }
  const { app: __app__, Vuex: __Vuex__, Pinia: __Pinia__ } = createApp();
  uni.Vuex = __Vuex__;
  uni.Pinia = __Pinia__;
  __app__.provide("__globalStyles", __uniConfig.styles);
  __app__._component.mpType = "app";
  __app__._component.render = () => {
  };
  __app__.mount("#app");
})(Vue);
